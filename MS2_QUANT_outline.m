function varargout = MS2_QUANT_outline(varargin)
% MS2_QUANT_OUTLINE M-file for MS2_QUANT_outline.fig

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MS2_QUANT_outline_OpeningFcn, ...
                   'gui_OutputFcn',  @MS2_QUANT_outline_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MS2_QUANT_outline is made visible.
function MS2_QUANT_outline_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

%- Set font-size to 10
%  For whatever reason are all the fonts on windows are set back to 8 when the .fig is openend
h_font_8 = findobj(handles.h_ms2quant_outline,'FontSize',8);
set(h_font_8,'FontSize',10)

%- Get installation directory of FISH-QUANT and initiate 
p = mfilename('fullpath');        
handles.MQ_path = fileparts(p); 
handles         = MQ_start_up_v1(handles);

%- Change name of GUI
set(handles.h_ms2quant_outline,'Name', ['MS2-QUANT ', handles.version, ': outline designer']);

%- Export figure handle to workspace - will be used in Close All button of main Interface
assignin('base','h_MQ_outline',handles.h_ms2quant_outline)

%=== Options for TxSite detection
handles.parameters_quant = MQ_TS_settings_init_v1;

%= Some parameters
handles.image_struct = [];

%= Default parameter for loading of GUI - might be overwritten by later functions
handles.child = 0;    
handles.reg_counter  = 1;  % Avoid having cells with same name after deleting one. 
handles.axis_fig = [];

handles.flag_OPB = 0;   % Indicate if region to quantify OPB is defined
handles.flag_BGD = 0;   % Indicate if background region is defined, if yes assign corresponding number

handles.file_name_settings = [];
handles.par_microscope.pixel_size.xy = 160;
handles.par_microscope.pixel_size.z  = 300;   
handles.par_microscope.RI            = 1.458;   
handles.par_microscope.NA            = 1.25;
handles.par_microscope.Em            = 568;   
handles.par_microscope.Ex            = 568;
handles.par_microscope.type          = 'widefield';  

%- Other parameters
handles.status_image  = 1; 
handles.status_draw   = 0;        % Used to avoid multiple calls of draw functions
handles.v_axis        = [];

%- Other parameters
handles.status_zoom = 0;
handles.h_zoom = rand(1);
handles.status_pan = 0;
handles.h_pan = rand(1);
handles.status_plot_first = 1;  % Indicate if plot command was never used


%- Slider values for contrast: RAW
handles.slider_contr_min_img  = 0;
handles.slider_contr_max_img  = 1;  

%- Slider values for contrast: FILTERED
handles.slider_contr_min_filt  = 0;
handles.slider_contr_max_filt  = 1;


%- Update status of various controls
set(handles.button_finished,'Enable', 'off');    
set(handles.h_ms2quant_outline,'WindowStyle','normal')

%set(handles.button_open_image,'Enable', 'on');   
%set(handles.pop_up_parameters,'Enable', 'on');    
%set(handles.button_parameters,'Enable', 'on'); 

%= Load data if called from other GUI
if not(isempty(varargin))

    if strcmp( varargin{1},'HandlesMainGui')
        
        handles.child = 1;        
        
        handles_MAIN = varargin{2};

        handles.reg_prop       = handles_MAIN.reg_prop;
        handles.par_microscope = handles_MAIN.par_microscope;

        handles.file_name_image    = handles_MAIN.file_name_image;
        handles.path_name_image    = handles_MAIN.path_name_image;
        handles.file_name_settings = handles_MAIN.file_name_settings;
        handles.file_name_image_filtered = handles_MAIN.file_name_image_filtered;
        handles.reg_counter       = size(handles.reg_prop,2) +1;     % Avoid having cells with same name after deleting one. 

        
        %- For saving of regions
        handles.path_sub_results        = handles_MAIN.path_sub_results;
        handles.path_sub_results_status = handles_MAIN.path_sub_results_status;

        handles.path_sub_regions        = handles_MAIN.path_sub_regions;
        handles.path_sub_regions_status = handles_MAIN.path_sub_regions_status;
        
        %- Change name of GUI
        set(handles.h_ms2quant_outline,'Name', ['MS2-QUANT ', handles.version, ': outline designer - ', handles.file_name_image ]);
       
        %- BGD of image
        bgd_img = handles_MAIN.BGD_img;
        if isempty(bgd_img)
            set(handles.txt_img_bgd,'String','');
        else
            set(handles.txt_img_bgd,'String',num2str(bgd_img));
        end
        
        %- Analyze image
        if not(isempty(handles.file_name_image))
        	
            %- Get first image in stack
            global MQ_img_stack MQ_img_MIP
            
            img_stack_first     = MQ_img_stack(1).data;  % Necessary to define binary mask for OPB measurement 
            [dim.Y dim.X dim.Z] = size(img_stack_first);
            
            handles.dim = dim;
            
            %- Raw image
            handles.img_PLOT      = handles_MAIN.img_PLOT; 
            handles.img_MIP       = MQ_img_MIP.raw; 
            handles.img_min       = min(handles.img_PLOT(:)); 
            handles.img_max       = round(1.5*max(handles.img_PLOT(:))); 
            handles.img_diff      = handles.img_max-handles.img_min; 
            
            %- Filtered image
            handles.img_filt_PLOT  = handles_MAIN.img_filt_PLOT; 
            handles.img_filt_MIP   = MQ_img_MIP.filt;             
            handles.img_filt_min   = min(handles.img_filt_PLOT(:)); 
            handles.img_filt_max   = round(1.5*max(handles.img_filt_PLOT(:))); 
            handles.img_filt_diff  = handles.img_filt_max-handles.img_filt_min;            
            
            %- Other properties of the image
            handles.status_image  = 1;  
            [img_dim.Y img_dim.X] = size(handles.img_PLOT);
            handles.img_dim       = img_dim;  
            handles.N_slice      = handles_MAIN.N_slice;
              
            %- Analyze regions
            handles = analyze_outline(hObject, eventdata, handles);           
            
        end
      
        %- Update status of various controls
        set(handles.button_finished,'Enable', 'on');
        set(handles.h_ms2quant_outline,'WindowStyle','normal')
        %set(handles.pop_up_parameters,'Enable', 'off');    
        %set(handles.button_parameters,'Enable', 'off');         
        
    elseif strcmp( varargin{1},'par_microscope')         
        handles.par_microscope = varargin{2};        
    end 
end

%- Check which elements should be enabled
GUI_enable(handles)

%- Plot image
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);

%- UIWAIT makes MS2_QUANT_outline wait for user response (see UIRESUME)
if not(isempty(varargin))
    if strcmp( varargin{1},'HandlesMainGui')
        uiwait(handles.h_ms2quant_outline);
    end
end


%== Check with controls should be enabled
function GUI_enable(handles)

if handles.status_image 
     set(handles.button_region_new,'Enable', 'on');    
     set(handles.button_region_modify,'Enable', 'on');        
     set(handles.button_region_delete,'Enable', 'on');    
     set(handles.listbox_region,'Enable', 'on');  
else
     set(handles.button_region_new,'Enable', 'off');    
     set(handles.button_region_modify,'Enable', 'off');        
     set(handles.button_region_delete,'Enable', 'off');    
     set(handles.listbox_region,'Enable', 'off');  
end

%- Delete/modify only possible if listbox populated
if isempty(get(handles.listbox_region,'String'))    
     set(handles.button_region_modify,'Enable', 'off');
     set(handles.button_region_delete,'Enable', 'off'); 
     set(handles.checkbox_OPB,'Enable', 'off');   
     set(handles.checkbox_BGD,'Enable', 'off'); 
     set(handles.button_change_name,'Enable', 'off');  
  
     
else
     set(handles.button_region_modify,'Enable', 'on');
     set(handles.button_region_delete,'Enable', 'on');
     set(handles.checkbox_OPB,'Enable', 'on'); 
     set(handles.checkbox_BGD,'Enable', 'on'); 
     set(handles.button_change_name,'Enable', 'on');  

end


%== Parameter that are returned
function varargout = MS2_QUANT_outline_OutputFcn(hObject, eventdata, handles) 

%- Only if called from another GUI
if handles.child
    varargout{1} = handles.reg_prop;
    varargout{2} = handles.flag_OPB;
    varargout{3} = handles.flag_BGD;
    
    bgd_img = str2double(get(handles.txt_img_bgd,'String'));
    
    if isnan(bgd_img);
        bgd_img = [];
    end
    varargout{4} = bgd_img;
    
    delete(handles.h_ms2quant_outline);
end


%== Resume GUI when called from other GUI
function button_finished_Callback(hObject, eventdata, handles)
uiresume(handles.h_ms2quant_outline)


% --- Executes when user attempts to close h_ms2quant_outline.
function h_ms2quant_outline_CloseRequestFcn(hObject, eventdata, handles)
if handles.child 
    uiresume(handles.h_ms2quant_outline)
else
    delete(handles.h_ms2quant_outline)
    if isfield(handles,'axes_sep')
        try
            delete(handles.axes_sep)
        catch; end
    end
end


% =========================================================================
% Load and save
% =========================================================================

%== Save outline
function menu_save_outline_Callback(hObject, eventdata, handles)

%- Assign background
bgd_img = str2double(get(handles.txt_img_bgd,'String'));

if isnan(bgd_img);
    bgd_img = [];
end

handles.BGD_img = bgd_img;
handles.file_name_regions = MQ_save_region_v3(handles,[]);


%== Load outline
function menu_load_outline_Callback(hObject, eventdata, handles)
warndlg('Not yet implemented')



%== Function to analyze loaded outline 
function handles = analyze_outline(hObject, eventdata, handles)


%- Populate list with names of cells
reg_prop = handles.reg_prop;

if not(isempty(reg_prop))

    N_reg = size(reg_prop,2);
    for i_reg = 1:N_reg
        
        %- Add fields needed for modification
        reg_prop(i_reg).reg_type = 'Polygon';
        
        reg_pos = [];
        reg_pos(:,1)  = reg_prop(i_reg).x;
        reg_pos(:,2)  = reg_prop(i_reg).y;
        
        reg_prop(i_reg).reg_pos = reg_pos;
        
        %- Get string with name of reg
        str_list_reg{i_reg} =  reg_prop(i_reg).label;
        
        %- Check if one region is defined as background
        flag_OPB = reg_prop(i_reg).flag_OPB;
        if flag_OPB
            handles.flag_OPB = i_reg;
        end
        
        %- Check if one region is defined as background
        flag_BGD = reg_prop(i_reg).flag_BGD;
        if flag_BGD
            handles.flag_BGD = i_reg;
        end
        
    end

    set(handles.listbox_region,'String', str_list_reg);
end

%- Save parameters
handles.reg_prop    = reg_prop;
handles.reg_counter = size(reg_prop,2)+1;
guidata(hObject, handles);

%- Show plot
set(handles.listbox_region,'Value',1);


% =========================================================================
% Functions to manipulate cells
% =========================================================================

%== New region
function button_region_new_Callback(hObject, eventdata, handles)

if not(handles.status_draw)
    
    %- Set status that one object is currently constructed
    handles.status_draw = 1;
    guidata(hObject, handles);

    %- Determine if plot should be done in separate figure
    fig_sep = 0; %get(handles.checkbox_sep_window,'Value');
    handles = plot_decide_window(hObject, eventdata, handles);

    %- Get current list
    str_list = get(handles.listbox_region,'String');
    N_reg   = size(str_list,1);
    ind_reg = N_reg+1;

    %- Draw region
    str_reg = get(handles.pop_up_region, 'String');
    val_reg = get(handles.pop_up_region,'Value');
    param.reg_type = str_reg{val_reg}; 
    
    param.h_axes   = gca;
    param.pos      = [];

    reg_result = FQ_draw_region_v1(param);

    if ~isempty(reg_result)
        position = reg_result.position;
        handles.axis_fig  = axis;

        %- Check that corrdinates are not outside of image
        img_dim = handles.img_dim;
        x_pos = round(position(:,1)); 
        y_pos = round(position(:,2)); 

        x_pos(x_pos < 1)         = 1;
        x_pos(x_pos > img_dim.X) = img_dim.X;

        y_pos(y_pos < 1)         = 1;
        y_pos(y_pos > img_dim.Y) = img_dim.Y;

        %- Save position
        handles.reg_prop(ind_reg).reg_type = reg_result.reg_type;
        handles.reg_prop(ind_reg).reg_pos  = reg_result.reg_pos;

        handles.reg_prop(ind_reg).x      = [];
        handles.reg_prop(ind_reg).x(1,:) = x_pos;  % v3: Has to be a row vector to agree with read-in from files 
        handles.reg_prop(ind_reg).y      = [];
        handles.reg_prop(ind_reg).y(1,:) = y_pos;  % v3: Has to be a row vector to agree with read-in from files

        %- Add entry at the end and update list
        str_reg = ['Region_', num2str(handles.reg_counter)];
        str_list{ind_reg} = str_reg;

        handles.reg_prop(ind_reg).label    = str_reg;
        handles.reg_prop(ind_reg).flag_OPB = 0;
        handles.reg_prop(ind_reg).flag_BGD = 0;

        set(handles.listbox_region,'String',str_list)
        set(handles.listbox_region,'Value',ind_reg)

        handles.reg_counter = handles.reg_counter+1;

        if fig_sep
            handles.v_axis = axis(handles.axes_sep);
        end

        %- Save and show results
        handles.status_draw = 0;
        handles = plot_image(handles,handles.axes_image);

        guidata(hObject, handles);
    end
    
    %- UIWAIT makes MS2_QUANT_outline wait for user response (see UIRESUME)
    %- New call is necessary since impoly breaks first call
    if handles.child;
        uiwait(handles.h_ms2quant_outline);
    end
end


%== Modify region
function button_region_modify_Callback(hObject, eventdata, handles)

if not(handles.status_draw)
    
    %- Set status that one object is currently constructed
    handles.status_draw = 1;
    guidata(hObject, handles);

    %- Determine if plot should be done in separate figure
    fig_sep = 0; %get(handles.checkbox_sep_window,'Value');
    handles = plot_decide_window(hObject, eventdata, handles);

    %- Extract index and properties of highlighted cell
    ind_sel  = get(handles.listbox_region,'Value');

    reg_prop = handles.reg_prop(ind_sel);
    
    %- Check if reg-type is defined
    is_reg_type = isfield(reg_prop,'reg_type');
       
    if is_reg_type    
        
        reg_type  = reg_prop.reg_type;
        reg_pos   = reg_prop.reg_pos;

        %- Modify region region
        param.reg_type = reg_type;
        param.h_axes   = gca;
        param.pos      = reg_pos;

        reg_result = FQ_draw_region_v1(param);
       
        reg_prop.reg_type  = reg_result.reg_type;
        reg_prop.reg_pos   = reg_result.reg_pos;
     
        %- Check that corrdinates are not outside of image
        position = reg_result.position;
        img_dim = handles.img_dim;
        x_pos = round(position(:,1)); 
        y_pos = round(position(:,2)); 

        x_pos(x_pos < 1)         = 1;
        x_pos(x_pos > img_dim.X) = img_dim.X;

        y_pos(y_pos < 1)         = 1;
        y_pos(y_pos > img_dim.Y) = img_dim.Y;

        %- Save position
        reg_prop.x      = [];
        reg_prop.x(1,:) = x_pos;% v3: Has to be a row vector to agree with read-in from files 
        reg_prop.y      = [];
        reg_prop.y(1,:) = y_pos;  % v3: Has to be a row vector to agree with read-in from files

        handles.reg_prop(ind_sel) = reg_prop;
        handles.axis_fig     = axis;
        
        if fig_sep
            handles.v_axis = axis(handles.axes_sep);
        end
        
        %- Save results
        handles.status_draw = 0;
        handles = plot_image(handles,handles.axes_image);
        guidata(hObject, handles);

        %- UIWAIT makes MS2_QUANT_outline wait for user response (see UIRESUME)
        %- New call is necessary since impoly breaks first call
        if handles.child;
            uiwait(handles.h_ms2quant_outline);    
        end
    else
        msgbox('Geometry cant be modified - only deleted','Outline definition','warn'); 
    end
end


%== Delete region
function button_region_delete_Callback(hObject, eventdata, handles)

%- Show plot
plot_image(handles,handles.axes_image);

%- Ask user to confirm choice
choice = questdlg('Do you really want to delete this region?', 'MS2-QUANT', 'Yes','No','No');

if strcmp(choice,'Yes')
    
    %- Extract index of highlighted cell
    str_list = get(handles.listbox_region,'String');
    ind_sel  = get(handles.listbox_region,'Value');
    
    %- Allow definition of region for OPB
    if handles.reg_prop(ind_sel).flag_OPB;
        handles.flag_OPB = 0;
    end
    
    %- Allow definition of background region
    if handles.reg_prop(ind_sel).flag_BGD;
        handles.flag_BGD = 0;
    end
    
    %- Delete highlighted cell
    str_list(ind_sel) = [];
    handles.reg_prop(ind_sel) = [];   
    set(handles.listbox_region,'String',str_list)
    set(handles.listbox_region,'Value',1)           
    
    %- Save results
    handles = plot_image(handles,handles.axes_image);
    guidata(hObject, handles);
    
    %- Show plot
    set(handles.listbox_region,'Value',1);
    listbox_region_Callback(hObject, eventdata, handles)   
    
end


%== Delete ALL regions
function choice = button_region_delete_ALL_Callback(hObject, eventdata, handles)

%- Ask user to confirm choice
choice = questdlg('Do you really want to delete ALL regions?', 'MS2-QUANT', 'Yes','No','No');

if strcmp(choice,'Yes')
    
    handles.reg_prop = struct('label', {}, 'x', {}, 'y', {}, 'pos', {}, ...
                                   'flag_BGD',[],'flag_OPB',[],'fit_limits',{},...
                                   'quant_fit_summary_all', [], 'quant_fit_summary_all_first',[]);set(handles.listbox_region,'String','')
    set(handles.listbox_region,'Value',1)  
    handles.reg_counter = 1;
    
    handles = plot_image(handles,handles.axes_image);
    guidata(hObject, handles);
    
    %- Show plot
    set(handles.listbox_region,'Value',1);
    listbox_region_Callback(hObject, eventdata, handles)   
    
end


%== Listbox region
function listbox_region_Callback(hObject, eventdata, handles)
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);


%== Region used to correct for observational photobleaching
function checkbox_OPB_Callback(hObject, eventdata, handles)

reg_prop = handles.reg_prop;    
ind_reg  = get(handles.listbox_region,'Value');
flag_OPB = reg_prop(ind_reg).flag_OPB;

if flag_OPB
  handles.reg_prop(ind_reg).flag_OPB = 0;
  handles.flag_OPB = 0;
else   
    if handles.flag_OPB == 0 
        
        handles.reg_prop(ind_reg).flag_OPB = 1;
        handles.flag_OPB                   = ind_reg;
        
%         %- Get binary mask
%         x = handles.reg_prop(ind_reg).x;
%         y = handles.reg_prop(ind_reg).y;
%         
%         m = handles.dim.Y; 
%         n = handles.dim.X;         
%         
%         BW_2D = poly2mask(x, y, m, n);
%         BW_3D = repmat(BW_2D,[1,1,handles.dim.Z]);
%         
%         %- Assing parameters
%         handles.reg_prop(ind_reg).BW_2D = BW_2D;
%         handles.reg_prop(ind_reg).BW_3D = BW_3D;
        
    else
        text_warning = ['OPB region already defined: # ' ,num2str(handles.flag_OPB)];
        warndlg(text_warning,'Background region');
    end
end

%- Save results
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);


%== Region used for background subtraction
function checkbox_BGD_Callback(hObject, eventdata, handles)

reg_prop = handles.reg_prop;    
ind_reg = get(handles.listbox_region,'Value');
flag_BGD = reg_prop(ind_reg).flag_BGD;

if flag_BGD
  handles.reg_prop(ind_reg).flag_BGD = 0;
  handles.flag_BGD = 0;
else   
    if handles.flag_BGD == 0 
        handles.reg_prop(ind_reg).flag_BGD = 1;
        handles.flag_BGD = ind_reg;
        
%         %- Get binary mask
%         x = handles.reg_prop(ind_reg).x;
%         y = handles.reg_prop(ind_reg).y;
%         
%         m = handles.dim.Y; 
%         n = handles.dim.X; 
%         
%         BW_2D = poly2mask(x, y, m, n);
%         BW_3D = repmat(BW_2D,[1,1,handles.dim.Z]);
%         
%         %- Assing parameters
%         handles.reg_prop(ind_reg).BW_2D = BW_2D;
%         handles.reg_prop(ind_reg).BW_3D = BW_3D;
        
    else
        text_warning = ['Background region already defined: # ' ,num2str(handles.flag_BGD)];
        warndlg(text_warning,'Background region');
    end
end

%- Save results
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);


%== Change name
function button_change_name_Callback(hObject, eventdata, handles)
ind_reg = get(handles.listbox_region,'Value');

dlgTitle = 'Change name of region';
prompt(1) = {'Name'};
defaultValue{1} = handles.reg_prop(ind_reg).label;

userValue = inputdlg(prompt,dlgTitle,1,defaultValue);

if( ~ isempty(userValue))
   handles.reg_prop(ind_reg).label = userValue{1};
   handles = analyze_outline(hObject, eventdata, handles);
   handles = plot_image(handles,handles.axes_image);
   guidata(hObject, handles);
end




% =========================================================================
% Autodetect
% =========================================================================

%== Autodetect TxSites
function button_auto_detect_Callback(hObject, eventdata, handles)
choice = button_region_delete_ALL_Callback(hObject, eventdata, handles);

if strcmp(choice,'Yes')
    disp('Auto-detection of transcription sites ... please wait .... ')

    %- Delete region properties
    handles.reg_prop = struct('label', {}, 'x', {}, 'y', {}, 'pos', {}, ...
                                   'flag_BGD',[],'flag_OPB',[],'fit_limits',{},...
                                   'quant_fit_summary_all', [], 'quant_fit_summary_all_first',[]);set(handles.listbox_region,'String','')
  
    
    %- Parameters
    parameters.int_th       = str2double(get(handles.text_th_auto_detect,'String'));
    parameters.conn         = handles.parameters_quant.conn;
    parameters.min_dist     = handles.parameters_quant.min_dist;
    parameters.crop_image   = handles.parameters_quant.crop_image;
    parameters.flags.output = 0;

    parameters.pixel_size   = handles.par_microscope.pixel_size;
    parameters.reg_prop     = handles.reg_prop;

    %- Detect and analyse
  
    handles.reg_prop        = MQ_TxSite_autodetect_v1(handles.img_disp,parameters);
    handles                 = analyze_outline(hObject, eventdata, handles);
    handles                 = plot_image(handles,handles.axes_image);

    %- Save results
    handles.parameters_auto_detect.int_th = parameters.int_th  ;
    handles.parameters_auto_detect.conn   = parameters.conn  ;

    %- Show plot
    plot_image(handles,handles.axes_image);

    guidata(hObject, handles); 
    disp('Auto-detection of transcription sites ... FINISHED .... ')
end

%== Autodetect TxSites: options
function menu_options_Callback(hObject, eventdata, handles)
handles.parameters_quant = MQ_TS_settings_detect_modify_v1(handles.parameters_quant);
guidata(hObject, handles);



% =========================================================================
% Plot
% =========================================================================

%== Decide which image to show
function popupmenu_img_select_Callback(hObject, eventdata, handles)

%== Select which image
str_img = get(handles.popupmenu_img_select,'String');
val_img = get(handles.popupmenu_img_select,'Value');

switch str_img{val_img}
    
    case 'Raw image'
        set(handles.slider_contrast_min,'Value',handles.slider_contr_min_img);
        set(handles.slider_contrast_max,'Value',handles.slider_contr_max_img);
        
    case 'Filtered image'
        set(handles.slider_contrast_min,'Value',handles.slider_contr_min_filt);
        set(handles.slider_contrast_max,'Value',handles.slider_contr_max_filt);
end

%- Plot
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);


%== Decide which plot window to use
function handles = plot_decide_window(hObject, eventdata, handles)

%- Determine if plot should be done in separate figure
fig_sep = 0; %get(handles.checkbox_sep_window,'Value');

if fig_sep
    
    %- Has there already been a figure handle?
    if isfield(handles,'axes_sep')
        
        %- Is this figure handle still present?
        
        %  Handles is deletec
        if not(ishandle(handles.axes_sep))
            figure;
            handles.axes_sep = gca;
            handles.axis_fig = [];
            guidata(hObject, handles);  
            
        %  Handles is still there
        else
            axes(handles.axes_sep);            
        end
    
    % New figure handles    
    else
         figure;
         handles.axes_sep = gca;
         handles.axis_fig = [];
         guidata(hObject, handles);
         
    end
    
    plot_image(handles,handles.axes_sep);
    if not(isempty(handles.v_axis))
        axis(handles.v_axis);
    end
end


%== Plot function
function handles = plot_image(handles,axes_select)

%== Select output axis
if isempty(axes_select)
    figure
else
    axes(axes_select)
end
v = axis;

%== Determine which view: MIP or 3D stack
str = get(handles.pop_up_view, 'String');
val = get(handles.pop_up_view,'Value');

% Set experimental settings based on selection
switch str{val};    
    case 'Maximum projection' 
        flag_MIP = 1;  
    
    case 'Z-stack'        
        flag_MIP = 0;
        ind_slice = str2double(get(handles.text_z_slice,'String'));
end

%== Select which image
str_img = get(handles.popupmenu_img_select,'String');
val_img = get(handles.popupmenu_img_select,'Value');


if strcmpi(str_img{val_img},'Filtered image')
   if not(isfield( handles,'img_filt_min'))
       str_img{val_img} = 'Raw image';
   end
    
end
    

switch str_img{val_img}
    
    case 'Raw image'
        img_min  = handles.img_min;
        img_diff = handles.img_diff;
        if flag_MIP == 1
            img_plot = handles.img_PLOT;
        else
            img_plot = handles.img_MIP(:,:,ind_slice);
        end
        
    case 'Filtered image'
        img_min  = handles.img_filt_min;
        img_diff = handles.img_filt_diff;  
        
        if flag_MIP == 1
            img_plot = handles.img_filt_PLOT;
        else
            img_plot = handles.img_filt_MIP(:,:,ind_slice);
        end
end
handles.img_disp = img_plot;

%== Determine the contrast of the image

%- Minimum
slider_min = get(handles.slider_contrast_min,'Value');
contr_min  = slider_min*img_diff+img_min;
set(handles.text_contr_min,'String',num2str(round(contr_min)));

%- Maximum
slider_max = get(handles.slider_contrast_max,'Value');
contr_max = slider_max*img_diff+img_min;
if contr_max < contr_min
    contr_max = contr_min+1;
end
set(handles.text_contr_max,'String',num2str(round(contr_max)));

%== Save slider values
switch str_img{val_img}
     case 'Raw image'         
            handles.slider_contr_min_img  = slider_min;
            handles.slider_contr_max_img  = slider_max;  
     case 'Filtered image'      
            handles.slider_contr_min_filt  = slider_min;
            handles.slider_contr_max_filt  = slider_max;
end

%== Plot image
imshow(img_plot,[contr_min contr_max])
colormap(hot)

%=== Region
hold on
if isfield(handles,'reg_prop')    
    reg_prop = handles.reg_prop;    
    if not(isempty(reg_prop))  
        
        %- Plot outlines of all defined regions
        for i = 1:size(reg_prop,2)
            x = reg_prop(i).x;
            y = reg_prop(i).y;
            plot([x,x(1)],[y,y(1)],'b','Linewidth', 2)     
            
            label = reg_prop(i).label;            
            label_new = strrep(label, '_', ' ');
            
            text(min(x),min(y)-15,label_new,'Color','b');
            
        end
        
        %- Plot selected cell in different color
        ind_reg = get(handles.listbox_region,'Value');
        x = reg_prop(ind_reg).x;
        y = reg_prop(ind_reg).y;
        plot([x,x(1)],[y,y(1)],'g','Linewidth', 2)     
        
        %- Check if selected region is OPB region
        flag_OPB = reg_prop(ind_reg).flag_OPB;
        
        if flag_OPB
           set(handles.checkbox_OPB,'Value',1); 
        else
           set(handles.checkbox_OPB,'Value',0); 
        end
        
        %- Check if selected region is background region
        flag_BGD = reg_prop(ind_reg).flag_BGD;
        
        if flag_BGD
           set(handles.checkbox_BGD,'Value',1); 
        else
           set(handles.checkbox_BGD,'Value',0); 
        end
        
      
    end        
end
    
hold off

%- Same zoom as before
if not(handles.status_plot_first)
    axis(v);
end

%- Save everything
handles.status_plot_first = 0;


%= Check which elements should be enabled
GUI_enable(handles)


%== Slider minimum contrast
function slider_contrast_min_Callback(hObject, eventdata, handles)
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);


%== Slider maximum contrast
function slider_contrast_max_Callback(hObject, eventdata, handles)
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);


%== Slider for slice
function slider_slice_Callback(hObject, eventdata, handles)
N_slice      = handles.N_slice;
slider_value = get(handles.slider_slice,'Value');
 
ind_slice = round(slider_value*(N_slice-1)+1);
set(handles.text_z_slice,'String',num2str(ind_slice)); 
handles = plot_image(handles,handles.axes_image); 
guidata(hObject, handles); 


%== Up one slice
function button_slice_incr_Callback(hObject, eventdata, handles)
N_slice = handles.N_slice;

%- Get next value for slice
ind_slice = str2double(get(handles.text_z_slice,'String'))+1;
if ind_slice > N_slice;ind_slice = N_slice;end
set(handles.text_z_slice,'String',ind_slice);

%-Update slider
slider_value = (ind_slice-1)/(N_slice-1);
set(handles.slider_slice,'Value',slider_value);

%- Save and plot image
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);


%== Down one slice
function button_slice_decr_Callback(hObject, eventdata, handles)
N_slice = handles.N_slice;

%- Get next value for slice
ind_slice = str2double(get(handles.text_z_slice,'String'))-1;
if ind_slice <1;ind_slice = 1;end
set(handles.text_z_slice,'String',ind_slice);

%-Update slider
slider_value = (ind_slice-1)/(N_slice-1);
set(handles.slider_slice,'Value',slider_value);

%- Save and plot image
handles = plot_image(handles,handles.axes_image);
guidata(hObject, handles);


%== Selection which image
function pop_up_view_Callback(hObject, eventdata, handles)

str = get(handles.pop_up_view, 'String');
val = get(handles.pop_up_view,'Value');

% Set experimental settings based on selection
switch str{val};
    
    case 'Maximum projection' 
        set(handles.text_z_slice,'String',NaN);
        set(handles.slider_slice,'Value',0);
        
        set(handles.button_slice_decr,'Enable','off');
        set(handles.button_slice_incr,'Enable','off');        
        set(handles.slider_slice,'Enable','off'); 
    
    case 'Z-stack'
        set(handles.text_z_slice,'String',1);
        set(handles.slider_slice,'Value',0);
        
        set(handles.button_slice_decr,'Enable','on');
        set(handles.button_slice_incr,'Enable','on');        
        set(handles.slider_slice,'Enable','on'); 
end


plot_image(handles,handles.axes_image);


%== Selection which image
function checkbox_sep_window_Callback(hObject, eventdata, handles)

status_sep = get(handles.checkbox_sep_window,'Value');
if status_sep == 0
    handles.v_axis = [];
end


%== Zoom
function button_zoom_in_Callback(hObject, eventdata, handles)
if handles.status_zoom == 0
    h_zoom = zoom;
    set(h_zoom,'Enable','on');
    handles.status_zoom = 1;
    handles.status_pan  = 0;
    handles.h_zoom      = h_zoom;
else
    set(handles.h_zoom,'Enable','off');    
    handles.status_zoom = 0;
end
guidata(hObject, handles);


%== Pan
function button_pan_Callback(hObject, eventdata, handles)
if handles.status_pan == 0
    h_pan = pan;
    set(h_pan,'Enable','on');
    handles.status_pan  = 1;
    handles.status_zoom = 0;
    handles.h_pan      = h_pan;    
else
    set(handles.h_pan,'Enable','off');    
    handles.status_pan = 0;
end
guidata(hObject, handles);


%== Cursor
function button_cursor_Callback(hObject, eventdata, handles)

%- Deactivate zoom
if ishandle(handles.h_zoom)
    set(handles.h_zoom,'Enable','off');  
end

%- Deactivate pan
if ishandle(handles.h_pan)
    set(handles.h_pan,'Enable','off');  
end

%-Datacursormode
dcm_obj = datacursormode;

set(dcm_obj,'SnapToDataVertex','off');
set(dcm_obj,'UpdateFcn',@(x,y)myupdatefcn(x,y,handles))


%=== Function for Data cursor
function txt = myupdatefcn(empt,event_obj,handles)

pos    = get(event_obj,'Position');

%- Update cursor accordingly
img_disp = handles.img_disp;
x_pos = round(pos(1));
y_pos = round(pos(2));

txt = {['X: ',num2str(x_pos)],...
       ['Y: ',num2str(y_pos)],...
       ['Int: ',num2str(round(img_disp(y_pos,x_pos)))]};

   
% =========================================================================
% Experimental parameters
% =========================================================================

% %== Modify parameters
% function button_parameters_Callback(hObject, eventdata, handles)
% 
% par_microscope = handles.par_microscope;
% 
% dlgTitle = 'Experimental parameters';
% 
% prompt(1) = {'Pixel-size xy [nm]'};
% prompt(2) = {'Pixel-size z [nm]'};
% prompt(3) = {'Refractive index'};
% prompt(4) = {'Numeric aperture NA'};
% prompt(5) = {'Emission wavelength'};
% prompt(6) = {'Excitation wavelength'};
% prompt(7) = {'Microscope'};
% 
% defaultValue{1} = num2str(par_microscope.pixel_size.xy);
% defaultValue{2} = num2str(par_microscope.pixel_size.z);
% defaultValue{3} = num2str(par_microscope.RI);
% defaultValue{4} = num2str(par_microscope.NA);
% defaultValue{5} = num2str(par_microscope.Em);
% defaultValue{6} = num2str(par_microscope.Ex);
% defaultValue{7} = num2str(par_microscope.type);
% 
% userValue = inputdlg(prompt,dlgTitle,1,defaultValue);
% 
% if( ~ isempty(userValue))
%     par_microscope.pixel_size.xy = str2double(userValue{1});
%     par_microscope.pixel_size.z  = str2double(userValue{2});   
%     par_microscope.RI            = str2double(userValue{3});   
%     par_microscope.NA            = str2double(userValue{4});
%     par_microscope.Em            = str2double(userValue{5});   
%     par_microscope.Ex            = str2double(userValue{6});
%     par_microscope.type    = userValue{7};   
% end
% 
% handles.par_microscope = par_microscope;
% set(handles.pop_up_parameters,'Value',3);
% guidata(hObject, handles);
% pop_up_parameters_Callback(hObject, eventdata, handles) 
% 
% 
% %== Popup control
% function pop_up_parameters_Callback(hObject, eventdata, handles)
% par_microscope = handles.par_microscope;
% 
% % Determine the selected data set.
% str = get(handles.pop_up_parameters, 'String');
% val = get(handles.pop_up_parameters,'Value');
% 
% % Set experimental settings based on selection
% switch str{val};
%     
%     case 'Betrand lab' 
%     par_microscope.pixel_size.xy = 160;
%     par_microscope.pixel_size.z  = 300;   
%     par_microscope.RI            = 1.458;   
%     par_microscope.NA            = 1.25;
%     par_microscope.Em            = 568;   
%     par_microscope.Ex            = 568;
%     par_microscope.type          = 'widefield';    
% 
%     
%     case 'Darzacq lab' 
%     par_microscope.pixel_size.xy = 64;
%     par_microscope.pixel_size.z  = 200;   
%     par_microscope.RI            = 1.458;   
%     par_microscope.NA            = 1.4;
%     par_microscope.Em            = 568;   
%     par_microscope.Ex            = 568;
%     par_microscope.type          = 'widefield';   
%     
%     case 'User settings' 
%  
% end
% 
% %- Update handles structure
% handles.par_microscope = par_microscope;
% guidata(hObject, handles);
% 


% =========================================================================
% NOT USED
% =========================================================================


%- Text to define background if image
function txt_img_bgd_Callback(hObject, eventdata, handles)

function listbox_TS_CreateFcn(hObject, eventdata, handles)

function listbox_region_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pop_up_parameters_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function slider_contrast_min_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function slider_contrast_max_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function slider_slice_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function pop_up_view_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pop_up_region_Callback(hObject, eventdata, handles)

function pop_up_region_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_th_auto_detect_Callback(hObject, eventdata, handles)

function text_th_auto_detect_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Untitled_1_Callback(hObject, eventdata, handles)

function menu_tools_Callback(hObject, eventdata, handles)

function h_ms2quant_outline_DeleteFcn(hObject, eventdata, handles)


function menu_TS_Callback(hObject, eventdata, handles)

function popupmenu_img_select_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function txt_img_bgd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function menu_main_Callback(hObject, eventdata, handles)
