function varargout = MS2_QUANT_detect(varargin)
% MS2_QUANT_DETECT M-file for MS2_QUANT_detect.fig

% Edit the above text to modify the response to help MS2_QUANT_detect

% Last Modified by GUIDE v2.5 17-Dec-2015 23:05:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MS2_QUANT_detect_OpeningFcn, ...
                   'gui_OutputFcn',  @MS2_QUANT_detect_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MS2_QUANT_detect is made visible.
function MS2_QUANT_detect_OpeningFcn(hObject, eventdata, handles, varargin)

%== Set font-size to 10 - In WIN all the fonts set back to 8
h_font_8 = findobj(handles.h_MQ_detect,'FontSize',8);
set(h_font_8,'FontSize',10)

%== Get installation directory of FISH-QUANT and initiate 
p               = mfilename('fullpath');        
handles.MQ_path = fileparts(p); 
handles         = MQ_start_up_v1(handles);


%= Change name of GUI
set(handles.h_MQ_detect,'Name', ['MS2-QUANT ', handles.version, ': detection']);

%= Export figure handle to workspace 
assignin('base','h_MQ_detect',handles.h_MQ_detect)

%== Initialize Bioformats
[status, ver_bf] = bfCheckJavaPath(1);
disp(['Bio-Formats ',ver_bf,' will be used.'])
 

% === Initialize GUI
%  (a) settings up the parameters
%  (b) preparing GUI with these parameters

handles = MQ_init_v5(handles);
handles = MQ_init_fit_limits_v1(handles);
handles = MQ_populate_v2(handles);

%- Load default settings
file_name_full = fullfile(handles.MQ_path,'MS2-QUANT_default_par.txt');
settings_load  = MQ_settings_load_v2(file_name_full,[]);
if isfield(settings_load,  'par_microscope');
    handles.par_microscope = settings_load.par_microscope;
end

%- Global restriction or not
handles.status_restr_global = 0;
handles.BGD_img             = 0; % Stays the same even if new images are loaded
handles.OPB.curves          = [];

%=== Options for TxSite quantification
handles.options_quant = MQ_settings_quant_init_v2;

handles.status_plot_first = 1;  % Indicate if plot command was never used
%pop_up_exp_default_Callback(hObject, eventdata, handles) 

%- Default file-names
handles.file_name_suffix_quant  = ['_QUANT_', datestr(date,'yymmdd'), '.txt'];

% Update handles structure
handles.output = hObject;
guidata(hObject, handles);
MQ_detect_enable_controls_v4(handles)


% --- Outputs from this function are returned to the command line.
function varargout = MS2_QUANT_detect_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;


%== Close request function
function h_MQ_detect_CloseRequestFcn(hObject, eventdata, handles)
button = questdlg('Are you sure that you want to close the GUI?','RESET GUI','Yes','No','No');

if strcmp(button,'Yes')    
   delete(hObject);   
   clear global MQ_img_stack MQ_img_MIP MQ_img_stack_3D
end


% =========================================================================
% LOAD .mat file from batch processing
% =========================================================================


%== Load results from batch processing (.mat file)
function menu_load_batch_proc_Callback(hObject, eventdata, handles)


cla(handles.axes_image)
handles = MQ_batch_load_handles_v5(handles);
fprintf('\n>>> Loading pre-processed file')

%- Only if handles are not empty
if not(isempty(handles))
    
    %- Reset fit limits
    handles = MQ_init_fit_limits_v1(handles);
    
    %== Analyze image
    
    %- Slider values for contrast: RAW
    handles.slider_contr_min_img  = 0;
    handles.slider_contr_max_img  = 1;  

    %- Slider values for contrast: FILTERED
    handles.slider_contr_min_img_filt  = 0;
    handles.slider_contr_max_img_filt  = 1;
    
    %- Slider values for contrast: BGD subtracted image
    handles.slider_contr_min_img_M_bgd  = 0;
    handles.slider_contr_max_img_M_bgd  = 1;
    
    %- Slider values for contrast: BGD
    handles.slider_contr_min_bgd  = 0;
    handles.slider_contr_max_bgd  = 1;
    
    handles = analyze_image(hObject, eventdata, handles);
    handles = analyze_reg_prop(hObject, eventdata, handles);

    %- Folders for results
    handles.path_sub_results        = handles.folder_sub_results;
    handles.path_sub_regions        = handles.folder_sub_results;
    handles.path_sub_results_status = 1;
    handles.path_sub_regions_status = 1;
    
    %- No settings file is saved
    handles.file_name_settings = [];
    
    %== Other parameters
        
    %- Filtering
    if isfield(handles.filter, 'factor_bgd') 
        set(handles.text_kernel_factor_bgd_xy,'String',num2str(handles.filter.factor_bgd ));
        set(handles.text_kernel_factor_bgd_z,'String',num2str(handles.filter.factor_bgd ));
        
        set(handles.text_kernel_factor_filter_xy,'String',num2str(handles.filter.factor_psf));
        set(handles.text_kernel_factor_filter_z,'String',num2str(handles.filter.factor_psf));
    else
        set(handles.text_kernel_factor_bgd_xy,'String',num2str(handles.filter.factor_bgd_xy));
        set(handles.text_kernel_factor_bgd_z,'String',num2str(handles.filter.factor_bgd_z));
        
        set(handles.text_kernel_factor_filter_xy,'String',num2str(handles.filter.factor_psf_xy));
        set(handles.text_kernel_factor_filter_z,'String',num2str(handles.filter.factor_psf_z));  
        
    end
    
    %- Various flags
    handles.status_image       = 1;
    handles.status_filtered    = 1;    % Image filtered
    handles.status_outline     = 1;    % Outlines defined
    handles.status_plot_first  = 1;    % Indicate if plot command was never used
    handles.status_time_series = 1;
    handles.status_OPB_quant   = 1;
    handles.status_quant       = 0;    % Pre-detection
    
    %- Check if detection was already performed
    handles.status_detect      = 0;    % Pre-detection
    if isfield(handles.reg_prop,'pos')
        if ~isempty(handles.reg_prop(1).pos)
            handles.status_detect      = 1;
        end
    end

    %- Set enable button to use OPB correction
    set(handles.checkbox_opb_apply,'Value',handles.flag_opb_apply ); 
   
    %- Set pre-detection to filtered image if they are different
    global MQ_img_MIP
    
    if isequal(MQ_img_MIP.raw,MQ_img_MIP.filt)
       set(handles.pop_up_img_sel,'Value',1);      % Save selection to raw image
       set(handles.popup_detect_image,'Value',1);  % Save selection to raw image
    else
       set(handles.pop_up_img_sel,'Value',2);      % Save selection to filtered image
       set(handles.popup_detect_image,'Value',2);  % Save selection to filtered image
    end
   
    %- Sliders for selection of plane
    set(handles.text_z_slice,'String','1');
    set(handles.slider_slice,'Value',0);
    
    %- Enable
    MQ_detect_enable_controls_v4(handles);
    handles = plot_image(hObject, eventdata, handles);
    guidata(hObject, handles);
  
end
fprintf('\n   File loaded!\n   Continue with detection.\n')

%==========================================================================
%==== Folders
%==========================================================================

%== Subfolder for results: define
function menu_folder_results_define_Callback(hObject, eventdata, handles)

%- User-dialog
dlgTitle = 'Sub-folder for results';
prompt_avg(1) = {'Name [relative to folder with images'};
defaultValue_avg{1} = num2str(handles.path_sub_results);

options.Resize='on';
userValue = inputdlg(prompt_avg,dlgTitle,1,defaultValue_avg,options);

%- Return results if specified
if( ~ isempty(userValue))
    handles.path_sub_results     = userValue{1};
    guidata(hObject, handles); 
end


%== Subfolder for results: enable/disable
function menu_folder_results_enable_Callback(hObject, eventdata, handles)

status_results = handles.path_sub_results_status;

if status_results
    handles.path_sub_results_status = 0;
else
    handles.path_sub_results_status = 1;
end

guidata(hObject, handles);
MQ_detect_enable_controls_v4(handles)


%== Subfolder for regions: define
function menu_folder_region_define_Callback(hObject, eventdata, handles)
%- User-dialog
dlgTitle = 'Sub-folder for regions';
prompt_avg(1) = {'Name [relative to folder with images'};
defaultValue_avg{1} = num2str(handles.path_sub_regions);

options.Resize='on';
userValue = inputdlg(prompt_avg,dlgTitle,1,defaultValue_avg,options);

%- Return results if specified
if( ~ isempty(userValue))
    handles.path_sub_regions     = userValue{1};
    guidata(hObject, handles); 
end


%== Subfolder for results: enable/define
function menu_folder_region_enable_Callback(hObject, eventdata, handles)
status_region = handles.path_sub_regions_status;

if status_region
    handles.path_sub_regions_status = 0;
else
    handles.path_sub_regions_status = 1;
end

guidata(hObject, handles);
MQ_detect_enable_controls_v4(handles)



%==========================================================================
%==== Experimental settings
%==========================================================================

%== Modify the experimental settings
function button_define_exp_Callback(hObject, eventdata, handles)
par_microscope = handles.par_microscope;

dlgTitle = 'Experimental parameters';

prompt(1) = {'Pixel-size xy [nm]'};
prompt(2) = {'Pixel-size z [nm]'};
prompt(3) = {'Refractive index'};
prompt(4) = {'Numeric aperture NA'};
prompt(5) = {'Emission wavelength'};
prompt(6) = {'Excitation wavelength'};
prompt(7) = {'Microscope'};
prompt(8) = {'Time between frames [s]'};

defaultValue{1} = num2str(par_microscope.pixel_size.xy);
defaultValue{2} = num2str(par_microscope.pixel_size.z);
defaultValue{3} = num2str(par_microscope.RI);
defaultValue{4} = num2str(par_microscope.NA);
defaultValue{5} = num2str(par_microscope.Em);
defaultValue{6} = num2str(par_microscope.Ex);
defaultValue{7} = num2str(par_microscope.type);
defaultValue{8} = num2str(par_microscope.dT);

userValue = inputdlg(prompt,dlgTitle,1,defaultValue);

if( ~ isempty(userValue))
    par_microscope.pixel_size.xy = str2double(userValue{1});
    par_microscope.pixel_size.z  = str2double(userValue{2});   
    par_microscope.RI            = str2double(userValue{3});   
    par_microscope.NA            = str2double(userValue{4});
    par_microscope.Em            = str2double(userValue{5});   
    par_microscope.Ex            = str2double(userValue{6});
    par_microscope.type          = userValue{7}; 
    par_microscope.dT            = str2double(userValue{8});  
end

handles.par_microscope = par_microscope;

%- Update theoretical PSF
handles = update_PSF_theo(hObject, eventdata, handles);

guidata(hObject, handles);


%==== Function to update theoretical PSF
function handles = update_PSF_theo(hObject, eventdata, handles)

%- Calculate theoretical PSF and show it 
[PSF_theo.xy_nm, PSF_theo.z_nm] = sigma_PSF_BoZhang_v1(handles.par_microscope);
PSF_theo.xy_pix = PSF_theo.xy_nm / handles.par_microscope.pixel_size.xy ;
PSF_theo.z_pix  = PSF_theo.z_nm  / handles.par_microscope.pixel_size.z ;

set(handles.text_psf_theo_xy,'String',num2str(round(PSF_theo.xy_nm)));
set(handles.text_psf_theo_z, 'String',num2str(round(PSF_theo.z_nm)));

%- Update handles structure
handles.PSF_theo       = PSF_theo;
guidata(hObject, handles);


% =========================================================================
%   Image: load and analyse
% =========================================================================


% === Load new image
function button_load_image_Callback(hObject, eventdata, handles)

global MQ_img_stack MQ_img_MIP

status_update(hObject, eventdata, handles,{'Loading stack. Please wait .... '});

button = questdlg('Loading new image will delete results of previous analysis. Continue?','Load new image','Yes','No','No');

set(handles.h_MQ_detect,'Pointer','watch');

if strcmp(button,'Yes')   
    
    %- Slider values for contrast: RAW
    handles.slider_contr_min_img  = 0;
    handles.slider_contr_max_img  = 1;  

    %- Slider values for contrast: FILTERED
    handles.slider_contr_min_img_filt  = 0;
    handles.slider_contr_max_img_filt  = 1;
    
    %- Slider values for contrast: raw bgd subtracted
    handles.slider_contr_min_img_raw_bgd  = 0;
    handles.slider_contr_max_img_raw_bgd  = 1;
    
    %- Slider values for contrast: OPB corrected - bgd subtracted
    handles.slider_contr_min_img_opd_bgd  = 0;
    handles.slider_contr_max_img_opd_bgd  = 1;
    
    %- What kind of data will be read in?
    sel_val = get(handles.popupmenu2,'Value');
    sel_str = get(handles.popupmenu2,'String');    
    img_type = sel_str{sel_val};
    
    %- Load image
    image_specs.type = img_type;
    image_specs.N_Z  = [];
    image_specs.name = [];
    image_specs.path = [];

    
    handles.file_name_settings = [];
    
    [handles, status_image, image_struct, img_MIP_xy] = MQ_load_images_v4(handles,image_specs);
      
    
    %- If image is loaded
    if status_image
    
        %- Assing image stack
        MQ_img_stack  = image_struct;

        %- Assign various MIP's
        MQ_img_MIP.raw          = img_MIP_xy; 
        MQ_img_MIP.filt         = img_MIP_xy; 
        MQ_img_MIP.raw_bgd_sub  = img_MIP_xy; 
        MQ_img_MIP.opb_bgd_sub  = img_MIP_xy; 
   
        %- Analyze image            
        handles = analyze_image(hObject, eventdata, handles);

        %- Reset parameters
        handles.reg_prop          = struct('label', {}, 'x', {}, 'y', {}, 'pos', {}, ...
                           'flag_BGD',[],'flag_OPB',[],'fit_limits', {},...
                           'quant_fit_summary_all', [], 'quant_fit_summary_all_first',[]);  handles.status_image       = 1;

        handles.status_filtered    = 0;    % Image filtered
        handles.status_outline     = 0;    % Outlines defined
        handles.status_detect      = 0;    % Pre-detection
        handles.status_quant       = 0;
        handles.status_plot_first  = 1;    % Indicate if plot command was never used
        handles.status_time_series = 1;
        handles.status_OPB_quant   = 0;

        handles.flag_BGD        = 0;    % BGD region for correction of OPB defined
        handles.flag_OPB        = 0;    % BGD region for correction of OPB defined

        %- Reset fit limits
        handles = MQ_init_fit_limits_v1(handles);

        %- Set some controls back to default
        set(handles.checkbox_opb_apply,'Value',0);
        set(handles.checkbox_quant_fit_data,'Value',1);
        
        %- Remove field with OPB correction
        if isfield(handles, 'OPB')
            handles = rmfield(handles, 'OPB');
        end
        
        %- Plot results and enable controls
        set(handles.pop_up_img_sel,'Value',1);     % Save selection to raw image
        set(handles.popup_detect_image,'Value',1);  % Save selection to raw image

        %- Sliders for selection of plane
        set(handles.text_z_slice,'String','1');
        set(handles.slider_slice,'Value',0);

        %- Housekeeping
        MQ_detect_enable_controls_v4(handles);
        handles = plot_image(hObject, eventdata, handles);
        guidata(hObject, handles);                  
    end
end          

status_update(hObject, eventdata, handles,{'Loading stack. Finished .... '});
set(handles.h_MQ_detect,'Pointer','arrow');           

                
% === Function to analyze image
function handles = analyze_image(hObject, eventdata, handles)

%- Call external function
handles = MQ_GUI_analyze_image(handles);

%- Generate names to save data
[dum, name_base] = fileparts(handles.file_name_image);
folder_save = 'MQ_results';
handles.path_sub_regions = folder_save;
handles.path_sub_results = folder_save;

%- Save everything
guidata(hObject, handles); 


% === Perform analysis on MIP in XY (only for 3D images)
function checkbox_data_analyze_2D_Callback(hObject, eventdata, handles)

global MQ_img_stack MQ_img_MIP MQ_img_stack_3D

status_analysis_MIP = get(handles.checkbox_data_analyze_2D,'Value');

if status_analysis_MIP
    button = questdlg('Entire analysis (including filtering) will be performed on MIP in XY. Continue?','Load new image','Yes','No','No');

    if strcmp(button,'Yes')   
        
        %- Assign dimensionality of image
        handles.img_type = '2D';

        %- Save old data
        MQ_img_stack_3D    = MQ_img_stack;
        MQ_img_stack       = {};
        MQ_img_stack.data  = MQ_img_MIP.raw;
        handles.status_filtered = 0;
        
        %- Analyze image            
        handles = analyze_image(hObject, eventdata, handles);
   
    else
        set(handles.checkbox_data_analyze_2D,'Value',0); 
    end
    
else
    
     button = questdlg('Entire analysis (including filtering) will be performed again in 3D. Continue?','Load new image','Yes','No','No');

    if strcmp(button,'Yes')   
        
        %- Assign dimensionality of image
        handles.img_type = '3D';

        %- Save old data
        MQ_img_stack = MQ_img_stack_3D;
        handles.status_filtered = 0;
        
        %- Analyze image            
        handles = analyze_image(hObject, eventdata, handles);
   
    else
        set(handles.checkbox_data_analyze_2D,'Value',1); 
    end  
end

%- Save everything
guidata(hObject, handles);


% =========================================================================
%   Save results
% =========================================================================

% === Save region & results
function menu_save_region_results_Callback(hObject, eventdata, handles)
menu_save_region_Callback(hObject, eventdata, handles);
menu_save_quant_Callback(hObject, eventdata, handles);

% === Save regions
function menu_save_region_Callback(hObject, eventdata, handles)

%- Save settings
if isempty(handles.file_name_settings)
    handles = menu_save_settings_Callback(hObject, eventdata, handles);
end

%- Save regions
handles.file_name_regions = MQ_save_region_v3(handles,[]);
guidata(hObject, handles); 


% === Save results of quantification
function menu_save_quant_Callback(hObject, eventdata, handles)

%- Save settings
if isempty(handles.file_name_settings)
    handles = menu_save_settings_Callback(hObject, eventdata, handles);
end

%== Save time-series - each TxSite separately
if handles.status_time_series == 1

    %- Default names
    suffix_results = handles.file_name_suffix_quant; 

    %- User-dialog
    dlgTitle        = 'Names for result files';
    prompt(1)       = {'Suffix for result files'};
    defaultValue{1} = suffix_results;

    options.Resize='on';
    %options.WindowStyle='normal';

    userValue = inputdlg(prompt,dlgTitle,1,defaultValue,options);

    %- Save results of individual regions
    if( ~ isempty(userValue))

        suffix_results = userValue{1};
        reg_prop = handles.reg_prop;   

        for i_reg = 1:length(reg_prop)

            reg_prop_loop = reg_prop(i_reg);

            if reg_prop_loop.flag_OPB == 0 && reg_prop_loop.flag_BGD == 0 

                
                %- Get file name
                name_file = reg_prop_loop.label;

                file_name_save   = [name_file,suffix_results];
               
                %- Get path name
                if handles.path_sub_results_status == 1;
                   path_save = fullfile(handles.path_name_image,handles.path_sub_results);

                   is_dir = exist(path_save,'dir'); 

                   if is_dir == 0
                       mkdir(path_save)
                   end

                else
                    path_save = handles.path_name_image;
                end

                %- Full name to save file
                file_name_full   = fullfile(path_save,file_name_save); 
  
                parameters.reg_prop           = reg_prop_loop;
                parameters.par_microscope     = handles.par_microscope;
                parameters.path_name_image    = handles.path_name_image;
                parameters.file_name_image    = handles.file_name_image;
                parameters.file_name_settings = handles.file_name_settings;
                parameters.version            = handles.version;
                parameters.BGD_img            = handles.BGD_img;
                parameters.path_save          = path_save;
                parameters.label              = reg_prop_loop.label;
                parameters.detect_int         = handles.par_detect.int_min;
                           
                
                %- Photobleaching correction
                if isfield(handles,'OPB')
                    parameters.OPB_curves = handles.OPB.curves;
                else
                    parameters.OPB_curves = [];
                end
                
                
                %- Limits of fit
                if isempty(reg_prop_loop.fit_limits)
                    parameters.limits = handles.fit_limits;
                else
                    parameters.limits = reg_prop_loop.fit_limits;
                end   

                MQ_results_save_v6(file_name_full,parameters);
            end

        end
    end
    
%== Individual images - save all TS together    
elseif handles.status_time_series == 0
    
    %- User-dialog for file-name
    [dum name] = fileparts(handles.file_name_image);
    
    dlgTitle        = 'Names for result files';
    prompt(1)       = {'Name'};
    defaultValue{1} = [name,'_TxSite_quant_',datestr(date,'yymmdd'),'.txt'];

    options.Resize = 'on';
    userValue = inputdlg(prompt,dlgTitle,1,defaultValue,options);
    
    %- Save results of individual regions
    if( ~ isempty(userValue))

        name_results = userValue{1};

        %- Get path name
        if handles.path_sub_results_status == 1;
           path_save = fullfile(handles.path_name_image,handles.path_sub_results);

           is_dir = exist(path_save,'dir'); 

           if is_dir == 0
               mkdir(path_save)
           end

        else
            path_save = handles.path_name_image;
        end

        %- Full name to save file
        file_name_full   = fullfile(path_save,name_results); 
        
        %- Get region properties
        reg_prop = handles.reg_prop;   
        i_site = 1;
        
        quant_summ = [];
        
        for i_reg = 1:length(reg_prop)

            reg_prop_loop = reg_prop(i_reg);

            if reg_prop_loop.flag_OPB == 0 && reg_prop_loop.flag_BGD == 0 
                
                quant_summ(i_site,:) = [reg_prop_loop.quant.integrate_int, ...
                                        reg_prop_loop.fit_amp, ...
                                        reg_prop_loop.quant.sum,...
                                        reg_prop_loop.quant.pix,...
                                        reg_prop_loop.quant_fit_summary_all, ...
                                        reg_prop_loop.quant.bgd_sum,...
                                        reg_prop_loop.quant.bgd_mean];
                                    
                site_label{i_site,:} = reg_prop_loop.label;
                i_site = i_site + 1;                    
            end  
        end        
        
        if not(isempty(quant_summ))
        
            par_save.quant_summ         = quant_summ;
            par_save.site_label         = site_label;
            par_save.par_microscope     = handles.par_microscope;
            par_save.path_name_image    = handles.path_name_image;
            par_save.file_name_image    = handles.file_name_image;
            par_save.file_name_settings = handles.file_name_settings;
            par_save.version            = handles.version;
            par_save.BGD_img            = handles.BGD_img;
            par_save.limits             = handles.fit_limits;
            par_save.path_sub_results_status = handles.path_sub_results_status;
            par_save.path_sub_results        = handles.path_sub_results;
                
            MQ_results_save_indiv_img_v4(file_name_full,par_save);
        end
            

    end
    
end


% === Save settings
function handles = menu_save_settings_Callback(hObject, eventdata, handles)

%- Go to results path
current_dir = pwd;
if handles.path_sub_results_status == 1;
   path_save = fullfile(handles.path_name_image,handles.path_sub_results);

   is_dir = exist(path_save,'dir'); 

   if is_dir == 0
       mkdir(path_save)
   end

else
    path_save = handles.path_name_image;
end
cd(path_save)

%- Save results
handles.file_name_settings = MQ_settings_save_v3([],handles);
guidata(hObject, handles); 

%- Go back to original folder
cd(current_dir)


% ==== Save currently displayed image: slice
function menu_save_image_slice_Callback(hObject, eventdata, handles)
global MQ_img_stack

if not(isempty(handles.img_disp ))

    %- Which slices
    ind_plot = str2double(get(handles.text_z_slice,'String'));
    
    %-- Default name: which image
    str_img = get(handles.pop_up_img_sel,'String');
    val_img = get(handles.pop_up_img_sel,'Value');

    switch str_img{val_img}

        case 'Raw image'
            img_type  = 'RAW';
            img_save  = MQ_img_stack(ind_plot).data_opb;
            
        case 'Filtered image'  
            img_type  = 'FILT';
            img_save  = MQ_img_stack(ind_plot).data_filt;
            
        case 'Background subtracted'  
            img_type  = 'BGD_subtract';
            img_save  = MQ_img_stack(ind_plot).data_opb - MQ_img_stack(ind_plot).bgd;
            
        case 'Background image'
            img_type  = 'BGD';
            img_save  = MQ_img_stack(ind_plot).bgd;
    end

    
    %- Name of base
    [dum name_base] = fileparts(handles.file_name_image); 
    
    
    %- Go to results path
    if handles.path_sub_results_status == 1;
       path_save = fullfile(handles.path_name_image,handles.path_sub_results);

       is_dir = exist(path_save,'dir'); 

       if is_dir == 0
           mkdir(path_save)
       end

    else
        path_save = handles.path_name_image;
    end

    
    %- Default name to save
    file_name_save   = [name_base,'_',num2str(ind_plot),'_',img_type,'.tif'];
    file_name_save_full = fullfile(path_save ,file_name_save);
    
    [file_name,path_name] = uiputfile(file_name_save_full,'Specify file name to save filtered image'); 
    

    if file_name ~= 0
        name_save = fullfile(path_name,file_name);
        %img_save  = handles.img_disp;
        image_save_v2(img_save,name_save);       
    end
end


% ==== Save currently displayed image: entire stack
function menu_save_image_stack_Callback(hObject, eventdata, handles)

global MQ_img_stack

if not(isempty(handles.img_disp ))

    
    %-- Default name: which image
    str_img = get(handles.pop_up_img_sel,'String');
    val_img = get(handles.pop_up_img_sel,'Value');

    switch str_img{val_img}

        case 'Raw image'
            img_type  = 'RAW';
            
        case 'Filtered image'  
            img_type  = 'FILT';
             
        case 'Background subtracted'  
            img_type  = 'BGD_subtract';
            
        case 'Background image'
            img_type  = 'BGD';
            
    end
     
    
    %- Specify output format
    choice_format = questdlg('Save time-series as one stacked TIF or individual stacks?', ...
	'Specify output format','One stack', 'Individual','One stack');
   
    if ~isempty(choice_format)

        %- Default name to save
        [dum name_base] = fileparts(handles.file_name_image); 
        file_name_save  = [name_base,'_',img_type];


        %- Generate folder to save individual z-stacks
        switch choice_format        

            case 'One stack'
                file_name_save_full   = fullfile(handles.path_name_image ,[file_name_save,'.tif']);
                [file_name,path_name] = uiputfile(file_name_save_full,'Specify file name to save filtered image'); 
                if file_name ~= 0 
                    name_save = fullfile(path_name,file_name);
                    flag_continue = 1;
                else
                    flag_continue = 0;
                end

            case 'Individual'  
                prompt = {'Folder name:'};
                dlg_title = 'Specify number of folder that should be used to save images';
                num_lines = 1;
                def = {file_name_save};
                answer = inputdlg(prompt,dlg_title,num_lines,def,'on'); 
                if ~isempty(answer)
                    name_save = fullfile(handles.path_name_image,answer{1});               
                    flag_continue = 1;
                else
                    flag_continue = 0;
                end
        end


        if flag_continue == 1

            MQ_image_save_v1(name_save,img_type,choice_format,16);

        end
    end
end




% =========================================================================
%   Load results
% =========================================================================

% === Load regions
function menu_load_region_Callback(hObject, eventdata, handles)


%- Go to results path
current_dir = pwd;
if not(isempty(handles.path_name_image))
    
    if handles.path_sub_regions_status == 1;
        folder_name_test = fullfile(handles.path_name_image,handles.path_sub_regions);
    
        is_dir = exist(folder_name_test,'dir'); 
    end

   if is_dir == 0
       path_load = handles.path_name_image;
   else
       path_load = folder_name_test;
   end
      
else
    path_load = pwd;
end
cd(path_load)

%- Get file name
[file_name_results,path_name_results] = uigetfile({'*.txt'},'Select file');

if file_name_results ~= 0
    
    
    %- Load region
    [reg_prop, file_names, par_microscope, BGD_img] = MQ_load_region_v3(fullfile(path_name_results,file_name_results));   
    
    %- Assign results
    handles.reg_prop           = reg_prop;
    handles.par_microscope     = par_microscope;
    handles.file_name_settings = file_names.settings;
    handles.BGD_img            = BGD_img;
    handles                    = analyze_reg_prop(hObject, eventdata, handles); 
    
    %- Load settings if defined
    if not(isempty(handles.file_name_settings))
       name_settings_full = fullfile(path_name_results,handles.file_name_settings);
       
        if exist(name_settings_full)
            handles = load_settings(name_settings_full, handles);
        end
    end
    
    %- Update theoretical PSF
    handles = update_PSF_theo(hObject, eventdata, handles);
    
    %- Check if positions are defined
    handles.status_detect = not(isempty([handles.reg_prop.pos]));
    
    %- Save and update
    handles.status_outline = 1;
    MQ_detect_enable_controls_v4(handles);
    guidata(hObject, handles);
    
end

%- Go back to original folder
cd(current_dir)


% === Load settings
function menu_load_settings_Callback(hObject, eventdata, handles)

%- Go to results path
current_dir = pwd;
if not(isempty(handles.path_name_image))

    if handles.path_sub_results_status == 1;
        folder_name_test = fullfile(handles.path_name_image,handles.path_sub_results);
    
        is_dir = exist(folder_name_test,'dir'); 
    end

   if is_dir == 0
       path_load = handles.path_name_image;
   else
       path_load = folder_name_test;
   end
      
else
    path_load = pwd;
end
cd(path_load)

%- Get settings
[file_name_settings,path_name_settings] = uigetfile({'*.txt'},'Select file with settings');

if file_name_settings ~= 0
    name_settings = fullfile(path_name_settings,file_name_settings);
    handles = load_settings(name_settings, handles);
        
    %- Update theoretical PSF
    handles = update_PSF_theo(hObject, eventdata, handles);
    
    %- Save and enable controls
    guidata(hObject, handles);
    MQ_detect_enable_controls_v4(handles);
end

%- Go back to original folder
cd(current_dir)


% == Function to load the settings
function handles = load_settings(name_settings, handles)
handles = MQ_settings_load_v2(name_settings,handles);

if not(isfield(handles.options_quant,'N_pix_sum'))
    handles.options_quant.N_pix_sum.xy = 1;
    handles.options_quant.N_pix_sum.z = 1;
end

%=== Set pre-detection settings

%- Detection on which image
type_image = handles.par_detect.flags.image;

switch type_image
    case 'raw'
        set(handles.popup_detect_image,'Value',1);   
    case 'filt'
         set(handles.popup_detect_image,'Value',2);    
end

%- Filtering settings
set(handles.text_kernel_factor_bgd_xy,'String',num2str(handles.filter.factor_bgd_xy));
set(handles.text_kernel_factor_bgd_z,'String',num2str(handles.filter.factor_bgd_xy));

set(handles.text_kernel_factor_filter_xy,'String',num2str(handles.filter.factor_psf_xy));
set(handles.text_kernel_factor_filter_z,'String',num2str(handles.filter.factor_psf_z));

%- Which mode of detection
mode_detect = handles.par_detect.flags.detect_mode;

switch mode_detect
    case 'brightest'
        set(handles.popup_predetect,'Value',1);   
    case 'conncomp'
         set(handles.popup_predetect,'Value',2);   
    case 'nonlocmax'
         set(handles.popup_predetect,'Value',2);
end

%- Minimum intensity
set(handles.text_detect_min_int,'String',num2str(handles.par_detect.int_min)); 




% =========================================================================
%   Region definition
% =========================================================================

%== Define outlines
function button_outline_define_Callback(hObject, eventdata, handles)
[handles.reg_prop  handles.flag_OPB handles.flag_BGD handles.BGD_img] = MS2_QUANT_outline('HandlesMainGui',handles);
handles.status_outline = 1;
handles = analyze_reg_prop(hObject, eventdata, handles); 
guidata(hObject, handles);


%== Function to analyze detected regions
function handles = analyze_reg_prop(hObject, eventdata, handles)

%- Analyze with external function
handles = MQ_GUI_analyze_reg_prop(handles);

%- Save and analyze results
set(handles.pop_up_outline_sel_reg,'String',handles.str_menu);
set(handles.pop_up_outline_sel_reg,'Value',1);
handles = pop_up_outline_sel_reg_Callback(hObject, eventdata, handles);        

%- Enable outline selection
MQ_detect_enable_controls_v4(handles);
handles = plot_image(hObject, eventdata, handles);

%- Save everything
guidata(hObject, handles); 
status_update(hObject, eventdata, handles,{'Regions for analysis defined.'})        


%== Function to analyze detected regions
function handles = pop_up_outline_sel_reg_Callback(hObject, eventdata, handles)
plot_image(hObject, eventdata, handles);


% =========================================================================
%   Observational photobleaching
% =========================================================================


% === Measurement of observational photobleaching
function button_OPB_quantify_Callback(hObject, eventdata, handles)

%- Call external function to quantify OPB
handles = MQ_GUI_OPB_quantify(handles);
handles.status_OPB_quant = 1;

%- Save results
set(handles.checkbox_opb_apply,'Value',1);
MQ_detect_enable_controls_v4(handles);  
guidata(hObject, handles);
set(handles.h_MQ_detect,'Pointer','arrow');

guidata(hObject, handles);


% === Enable OPB bleaching correction
function checkbox_opb_apply_Callback(hObject, eventdata, handles)
helpdlg('When filtered images are used in the analysis, filtering step has to be repeated after changing this option.','ENABLE/DISABLE OPB correction')


% =========================================================================
%   FILTERING
% =========================================================================

%== Filter images
function button_filter_Callback(hObject, eventdata, handles)

%- Call external function
set(handles.h_MQ_detect,'Pointer','watch');
status_update(hObject, eventdata, handles,{'Filtering started.'});    
handles =  MQ_GUI_filter(handles);

%== Analyze image
handles = analyze_image(hObject, eventdata, handles);

%== Plot filtered image
set(handles.popup_detect_image,'Value',2)
set(handles.pop_up_img_sel,'Value',2)
handles = plot_image(hObject, eventdata, handles);
guidata(hObject, handles);

%== Enable controls
handles.status_filtered = 1;
MQ_detect_enable_controls_v4(handles); 

status_update(hObject, eventdata, handles,{'Filtering finished.'});    
set(handles.h_MQ_detect,'Pointer','arrow');


% =========================================================================
%   Detection
% =========================================================================

% === Pre-detection
function button_predetect_Callback(hObject, eventdata, handles)

%== Detection on which image
type_select = get(handles.popup_detect_image,'Value');

if type_select == 1 
    flags.image = 'raw';        
elseif type_select == 2
    flags.image = 'filt';
    
    if not(handles.status_filtered )
        flags.image = 'raw';
        status_update(hObject, eventdata, handles,{'NOT FILTERED IMAGE DEFINED. USE RAW IMAGE INSTEAD!'});
        warndlg('NOT FILTERED IMAGE DEFINED. USE RAW IMAGE INSTEAD!','MS2-QUANT: detection');
    end
end 

%== Which detection mode
type_detect = get(handles.popup_predetect,'Value');

%- Brightest pixel
if type_detect == 1
    flags.detect_mode = 'brightest';
    
%- Connected components
elseif type_detect == 2
    flags.detect_mode = 'conncomp';

%- Non local maximum 
elseif type_detect == 3
    flags.detect_mode = 'nonlocmax'; 
end 

%- Update GUI and enable controls
status_update(hObject, eventdata, handles,{'Pre-detection .... in progress ..... '})

%- Perform pre-detection
if handles.status_time_series == 1
    flags.output  = 1; 
else
    flags.output  = 0;
end

par_detect.par_microscope = handles.par_microscope;
par_detect.flags          = flags;
par_detect.int_min        = str2double(get(handles.text_detect_min_int,'String')); %- Minimum intensity for detection
par_detect.img_type       = handles.img_type;

%- Pre-detection
handles.reg_prop = MQ_detect_v11(handles.reg_prop,par_detect);
    
%- Update GUI and enable control
handles.status_detect = 1;    % Pre-detections
handles.par_detect    = par_detect;

guidata(hObject, handles);  
handles = plot_image(hObject, eventdata, handles);
MQ_detect_enable_controls_v4(handles);
status_update(hObject, eventdata, handles,{'Pre-detection: FINISHED'})
    

%=== Correct detected position
function button_correct_detection_Callback(hObject, eventdata, handles)

%- Delete region inspector if present
if isfield(handles,'h_impixregion')   
    if ishandle(handles.h_impixregion)
        delete(handles.h_impixregion)
    end
end

%- Deactivate zoom
if ishandle(handles.h_zoom)
    set(handles.h_zoom,'Enable','off');  
end

%- Deactivate pan
if ishandle(handles.h_pan)
    set(handles.h_pan,'Enable','off');  
end

%- Plot location of spot if present
ind_plot = str2double(get(handles.text_z_slice,'String'));
ind_reg  = get(handles.pop_up_outline_sel_reg,'Value');

if isfield(handles,'reg_prop')    
    reg_prop = handles.reg_prop;    
    if not(isempty(reg_prop))  
        pos_TS = reg_prop(ind_reg).pos;        
        if not(isempty(pos_TS))
            pos_TS_frame = pos_TS(ind_plot);
        
            p = impoint(handles.axes_image, pos_TS_frame.x,pos_TS_frame.y);
            p = wait(p);
            
            handles.reg_prop(ind_reg).pos(ind_plot).x = round(p(1));
            handles.reg_prop(ind_reg).pos(ind_plot).y = round(p(2));
            
            
            %- Save new positions
            set(handles.button_corr_reuse,'enable','on')
            set(handles.txt_pos_reuse_start,'enable','on')
            set(handles.txt_pos_reuse_end,'enable','on')
            handles.x_new = round(p(1));
            handles.y_new = round(p(2));  
            
            %- Set fields to resuse position
            set(handles.txt_pos_reuse_start,'String',num2str(ind_plot))
            set(handles.txt_pos_reuse_end,'String',num2str(ind_plot+1))

            %- Save data
            guidata(hObject, handles);
            plot_image(hObject, eventdata, handles);
        end        
    end        
end


%=== Reuse detected position
function button_corr_reuse_Callback(hObject, eventdata, handles)

choice = questdlg('Are you sure that you want to correct multiple positions?', ...
	mfilename, 'Yes','No','Yes');

if strcmp(choice,'Yes')

    ind_start = str2double(get(handles.txt_pos_reuse_start,'String'));
    ind_end = str2double(get(handles.txt_pos_reuse_end,'String'));
    
    %- Plot location of spot if present
    ind_reg  = get(handles.pop_up_outline_sel_reg,'Value');

    if isfield(handles,'reg_prop')    
        reg_prop = handles.reg_prop;    
        if not(isempty(reg_prop))  
            pos_TS = reg_prop(ind_reg).pos;        
            if not(isempty(pos_TS))
                for i_change = ind_start:ind_end
                              
                    handles.reg_prop(ind_reg).pos(i_change).x = handles.x_new;
                    handles.reg_prop(ind_reg).pos(i_change).y = handles.y_new;
                end
                
                %- Save data
                set(handles.text_z_slice,'String',num2str(ind_end))
                guidata(hObject, handles);
                plot_image(hObject, eventdata, handles);
                
            end        
        end        
    end

end



% =========================================================================
%   Quantification
% =========================================================================

% === Quantify transcription sites
function button_TS_quant_Callback(hObject, eventdata, handles)

set(handles.h_MQ_detect,'Pointer','watch');
status_update(hObject, eventdata, handles,{'Quantification: STARTED ... '})

%- Which image should be analyzed
dum_str  = get(handles.popup_select_fit,'String');
dum_val  = get(handles.popup_select_fit,'Value');
par_fit.flag_struct.fit_data  = dum_str{dum_val};

%- For fitting    
options_fit.pixel_size =  handles.par_microscope.pixel_size;
options_fit.par_start  =  [];
options_fit.PSF_theo   = handles.PSF_theo;
options_fit.fit_limits = handles.fit_limits;

%- Other parameters
par_fit.options_fit    = options_fit;
par_fit.par_microscope = handles.par_microscope;
par_fit.options_quant  = handles.options_quant;
par_fit.flag_struct.output = 0;
par_fit.flag_struct.fit_TS = 1;
par_fit.flag_struct.fit_TS_Gauss = get(handles.checkbox_quant_fit_data,'Value');

%- FIRST CHECK IMAGE TYPE
switch handles.img_type 
    
    %- 3D IMAGES
    case {'3D','3d'}

        %- THEN CHECK TYPE OF ANALYSIS
        switch par_fit.options_quant.flag_quant_dim

            case {'3D','3d'}
                par_fit.options_fit.fit_mode   =  'sigma_free_xz'; 
                handles.reg_prop = MQ_quant_3D_v11(handles.reg_prop,par_fit);
                handles.status_quant = 1;
                status_update(hObject, eventdata, handles,{'Quantification: ... FINISHED! '})

            otherwise
                handles.status_quant = 0;
                status_update(hObject, eventdata, handles,{'Invalide selection for quantification: ... FINISHED! '})
        end
        
    %- 2D IMAGES
    case {'2D','2d'}
            par_fit.options_fit.fit_mode   =  'sigma_free_xy';
            handles.reg_prop = MQ_quant_2D_v8(handles.reg_prop,par_fit);
            handles.status_quant = 1;
            status_update(hObject, eventdata, handles,{'Quantification: ... FINISHED! '})        
end

%- Save results
MQ_detect_enable_controls_v4(handles);  
guidata(hObject, handles);
set(handles.h_MQ_detect,'Pointer','arrow');


% === Restrict fitting parameters
function button_quant_restrict_Callback(hObject, eventdata, handles)

parameters.reg_prop   = handles.reg_prop;
parameters.fit_limits = handles.fit_limits; 

parameters.reg_list   = get(handles.pop_up_outline_sel_reg,'String');
parameters.reg_sel    = get(handles.pop_up_outline_sel_reg,'Value');
parameters.status_restr_global = handles.status_restr_global;
parameters.int_min    = str2double(get(handles.text_detect_min_int,'String')); %- Minimum intensity for detection

[handles.reg_prop handles.status_restr_global handles.fit_limits] = MS2_QUANT_restrict_par(parameters);
guidata(hObject, handles);


% === Show results of quantification
function button_show_results_Callback(hObject, eventdata, handles)
ind_reg = get(handles.pop_up_outline_sel_reg,'Value');
      
if handles.reg_prop(ind_reg).flag_OPB == 0 && handles.reg_prop(ind_reg).flag_BGD == 0 

    %- Get results of quantification
    par_plot.summary_fit = handles.reg_prop(ind_reg).quant_fit_summary_all;
    par_plot.fit_amp     = handles.reg_prop(ind_reg).fit_amp;
    par_plot.quant       = handles.reg_prop(ind_reg).quant;
    par_plot.reg.x       = handles.reg_prop(ind_reg).x * handles.par_microscope.pixel_size.xy; 
    par_plot.reg.y       = handles.reg_prop(ind_reg).y * handles.par_microscope.pixel_size.xy; 
      
    %- Pre-detected positions 
    x_nm = ( [handles.reg_prop(ind_reg).pos.x]  ) * handles.par_microscope.pixel_size.xy;
    y_nm = ( [handles.reg_prop(ind_reg).pos.y]  ) * handles.par_microscope.pixel_size.xy;
    
    par_plot.pos_TS(:,1) = x_nm;
    par_plot.pos_TS(:,2) = y_nm;
   
    %- Get information about region
    par_plot.dim         = handles.reg_prop(ind_reg).dim;

    %- Plot results 
    MQ_show_results_v4(par_plot)
    
elseif handles.reg_prop(ind_reg).flag_OPB == 1    
    
    intensity = handles.reg_prop(ind_reg).int_avg_OPB; 
    figure
    plot(intensity)
    xlabel('Frame number')
    ylabel('Intensity')
    title('Measurement for bleaching correction')
    
    
elseif handles.reg_prop(ind_reg).flag_BGD == 1     
    
    intensity = handles.reg_prop(ind_reg).int_avg_OPB; 
    figure
    plot(intensity)
    xlabel('Frame number')
    ylabel('Intensity')
    title('Background outside of the cell')
end


% =========================================================================
%   PLOT FUNCTIONS
% =========================================================================

% === Open image in cellc
function button_open_cellc_Callback(hObject, eventdata, handles)

%- Assign parameters
parameters.FileName = handles.file_name_image;
parameters.PathName = handles.path_name_image;
parameters.img      = handles.img_disp;
 
cellc(parameters)


% ==== Function to plot the image
function handles = plot_image(hObject, eventdata, handles)

global MQ_img_MIP

datacursormode off
N_slice = handles.N_slice; 

%- Select output axis
axes(handles.axes_image)
cla;
v = axis;

%- Plot image   
ind_plot = str2double(get(handles.text_z_slice,'String'));
       
if isnan(ind_plot)
    ind_plot = 1;
end

%== Select which image
str_img = get(handles.pop_up_img_sel,'String');
val_img = get(handles.pop_up_img_sel,'Value');

switch str_img{val_img}
    
    case 'Raw image'
        img_min  = handles.img_min;
        img_diff = handles.img_diff;
        img_plot = MQ_img_MIP.raw(:,:,ind_plot);
        
    case 'Raw - bgd subtracted'    
        img_min  = handles.img_raw_bgd_min;
        img_diff = handles.img_raw_bgd_diff;
        img_plot = MQ_img_MIP.raw_bgd_sub(:,:,ind_plot);
      
    case 'Filtered image'  
        img_min  = handles.img_filt_min;
        img_diff = handles.img_filt_diff;
        img_plot = MQ_img_MIP.filt(:,:,ind_plot);
        
    case 'PB corrected - bgd sub'  
        img_min  = handles.img_opd_bgd_min;
        img_diff = handles.img_opd_bgd_diff;
        img_plot = MQ_img_MIP.opb_bgd_sub(:,:,ind_plot);
        
%     case 'Background image'
%         img_min  = handles.bgd_min;
%         img_diff = handles.bgd_diff;
%         img_plot = MQ_img_MIP.bgd_MIP(:,:,ind_plot);  
end
handles.img_disp = img_plot;

%== Determine the contrast of the image

%- Minimum
slider_min = get(handles.slider_contrast_min,'Value');
contr_min  = slider_min*img_diff+img_min;
set(handles.text_contr_min,'String',num2str(round(contr_min)));

%- Maximum
slider_max = get(handles.slider_contrast_max,'Value');
contr_max = slider_max*img_diff+img_min;
if contr_max < contr_min
    contr_max = contr_min+1;
end
set(handles.text_contr_max,'String',num2str(round(contr_max)));


%== Save slider values
switch str_img{val_img}
     case 'Raw image'         
            handles.slider_contr_min_img  = slider_min;
            handles.slider_contr_max_img  = slider_max;  
     
    case 'Filtered image'      
            handles.slider_contr_min_img_filt  = slider_min;
            handles.slider_contr_max_img_filt  = slider_max;
            
     case 'Raw - bgd subtracted'      
            handles.slider_contr_min_img_raw_bgd  = slider_min;
            handles.slider_contr_max_img_raw_bgd  = slider_max;   
            
     case 'PB corrected - bgd sub'      
            handles.slider_contr_min_img_opd_bgd  = slider_min;
            handles.slider_contr_max_img_opd_bgd  = slider_max;
end

%== Plot image       
handles.h_img = imshow(img_plot,[contr_min contr_max]); 
title(['slice #' , num2str(ind_plot) , ' of ', num2str(N_slice)],'FontSize',9);
colormap(hot)
handles.img_plot_GUI = img_plot;


%== Plot outline of regions

%- Check if regions should be plotted or not
flag_plot_reg = get(handles.checkbox_show_regions,'Value');

if flag_plot_reg
    hold on
    if isfield(handles,'reg_prop')    
        reg_prop = handles.reg_prop;    
        if not(isempty(reg_prop))  

            for i = 1:size(reg_prop,2)
                x = reg_prop(i).x;
                y = reg_prop(i).y;
                plot([x,x(1)],[y,y(1)],'b','Linewidth', 2)     
            end

            %- Plot selected region in different color
            ind_reg = get(handles.pop_up_outline_sel_reg,'Value');
            x = reg_prop(ind_reg).x;
            y = reg_prop(ind_reg).y;
            plot([x,x(1)],[y,y(1)],'g','Linewidth', 2)

            %- Plot position of detected spot
            pos_TS = reg_prop(ind_reg).pos;

            if not(isempty(pos_TS))
                plot(pos_TS(ind_plot).x,pos_TS(ind_plot).y,'+g','Linewidth', 2,'MarkerSize',15)            
            end
        end        
    end
    hold off
end


%- Same zoom as before
if not(handles.status_plot_first)
    axis(v);
end

%- Save everything
handles.status_plot_first = 0;
guidata(hObject, handles); 


%== Plot regions or not
function checkbox_show_regions_Callback(hObject, eventdata, handles)
handles = plot_image(hObject, eventdata, handles);
guidata(hObject, handles);


%== Slider: contrast minimum
function slider_contrast_min_Callback(hObject, eventdata, handles)
handles = plot_image(hObject, eventdata, handles);
guidata(hObject, handles);


%== Slider: contrast maximum
function slider_contrast_max_Callback(hObject, eventdata, handles)
handles = plot_image(hObject, eventdata, handles);
guidata(hObject, handles); 


%== Slider to select z-plane
function slider_slice_Callback(hObject, eventdata, handles)
N_slice      = handles.N_slice;
slider_value = get(handles.slider_slice,'Value');

ind_slice = round(slider_value*(N_slice-1)+1);
set(handles.text_z_slice,'String',num2str(ind_slice));

handles = plot_image(hObject, eventdata, handles);
guidata(hObject, handles); 


%== Up one slice
function button_slice_incr_Callback(hObject, eventdata, handles)
N_slice     = handles.N_slice;
slider_incr = str2double(get(handles.edit_step_size,'String'));


%- Get next value for slice
ind_slice = str2double(get(handles.text_z_slice,'String'))+slider_incr;
if ind_slice > N_slice;ind_slice = N_slice;end
set(handles.text_z_slice,'String',ind_slice);

%-Update slider
slider_value = (ind_slice-1)/(N_slice-1);
set(handles.slider_slice,'Value',slider_value);

%- Save and plot image
handles = plot_image(hObject, eventdata, handles);
guidata(hObject, handles);


%== Down one slice
function button_slice_decr_Callback(hObject, eventdata, handles)
N_slice      = handles.N_slice;
slider_incr = str2double(get(handles.edit_step_size,'String'));

%- Get next value for slice
ind_slice = str2double(get(handles.text_z_slice,'String'))-slider_incr;
if ind_slice <1;ind_slice = 1;end
set(handles.text_z_slice,'String',ind_slice);

%-Update slider
slider_value = (ind_slice-1)/(N_slice-1);
set(handles.slider_slice,'Value',slider_value);

%- Save and plot image
handles = plot_image(hObject, eventdata, handles);
guidata(hObject, handles);


%== Select raw vs. filtered image
function handles = pop_up_img_sel_Callback(hObject, eventdata, handles)

%== Select which window
str_img = get(handles.pop_up_img_sel,'String');
val_img = get(handles.pop_up_img_sel,'Value');

switch str_img{val_img}
    
    case 'Raw image'
        set(handles.slider_contrast_min,'Value',handles.slider_contr_min_img);
        set(handles.slider_contrast_max,'Value',handles.slider_contr_max_img);
        
    case 'Filtered image'
        set(handles.slider_contrast_min,'Value',handles.slider_contr_min_img_filt);
        set(handles.slider_contrast_max,'Value',handles.slider_contr_max_img_filt);
        
   case 'Raw - bgd subtracted'  
        set(handles.slider_contrast_min,'Value',handles.slider_contr_min_img_raw_bgd);
        set(handles.slider_contrast_max,'Value',handles.slider_contr_max_img_raw_bgd);   
        
   case 'PB corrected - bgd sub' 
        set(handles.slider_contrast_min,'Value',handles.slider_contr_min_img_opd_bgd);
        set(handles.slider_contrast_max,'Value',handles.slider_contr_max_img_opd_bgd);  
        
end

%- Plot
handles = plot_image(hObject, eventdata, handles);
guidata(hObject, handles); 


%=== Image region
function button_image_region_Callback(hObject, eventdata, handles)
if not(isfield(handles,'h_impixregion'))
    handles.h_impixregion = impixelregion(handles.axes_image);
else 
    if not(ishandle(handles.h_impixregion))
        handles.h_impixregion = impixelregion(handles.axes_image);
    end
end
guidata(hObject, handles);


%== ZOOM button
function button_zoom_in_Callback(hObject, eventdata, handles)

if handles.status_zoom == 0
    zoom on
    handles.status_zoom = 1;
    handles.status_pan  = 0;
else
    zoom off
    handles.status_zoom = 0;
end
guidata(hObject, handles);


%== PAN button
function menu_pan_Callback(hObject, eventdata, handles)

if handles.status_pan == 0
    pan on
    handles.status_pan  = 1;
    handles.status_zoom = 0;
        
else
    pan off     
    handles.status_pan = 0;
end
guidata(hObject, handles);


%== Cursor
function button_image_cursor_Callback(hObject, eventdata, handles)

%- Deactivate zoom
if ishandle(handles.h_zoom)
    set(handles.h_zoom,'Enable','off');  
end

%- Deactivate pan
if ishandle(handles.h_pan)
    set(handles.h_pan,'Enable','off');  
end

%-Datacursormode
dcm_obj = datacursormode;

set(dcm_obj,'SnapToDataVertex','off');
set(dcm_obj,'UpdateFcn',@(x,y)myupdatefcn(x,y,handles))


%=== Function for Data cursor
function txt = myupdatefcn(empt,event_obj,handles)

pos    = get(event_obj,'Position');
x_pos = round(pos(1));
y_pos = round(pos(2));
img_plot = handles.img_disp;
txt = {['X: ',num2str(x_pos)],...
       ['Y: ',num2str(y_pos)],...
       ['Int: ',num2str(round(img_plot(y_pos,x_pos)))]};

%=== Key's do change plane 
function h_MQ_detect_KeyPressFcn(hObject, eventdata, handles)

if handles.status_image 
    switch eventdata.Key     
        case 'rightarrow'
            button_slice_incr_Callback(hObject, eventdata, handles)
            
        case 'leftarrow'
            button_slice_decr_Callback(hObject, eventdata, handles)
    end   
end


% =========================================================================
%   Options for various functions
% =========================================================================

%== Photobleaching correction
function menu_options_OPB_Callback(hObject, eventdata, handles)

%- User-dialog
dlgTitle = 'Options for OPB correction quantification';
prompt_avg(1) = {'Fit mode: lin (linear), exp (3 exponentials)'};
defaultValue_avg{1} = num2str(handles.opb_fit_mode);

options.Resize='on';
userValue = inputdlg(prompt_avg,dlgTitle,1,defaultValue_avg,options);

%- Return results if specified
if( ~ isempty(userValue))
    handles.opb_fit_mode     = userValue{1};
    guidata(hObject, handles); 
end

%== Detection
function menu_options_detection_Callback(hObject, eventdata, handles)
helpdlg('No options can be modified yet.',mfilename)

%== Quantification
function menu_options_quant_Callback(hObject, eventdata, handles)
handles.options_quant = MQ_settings_quant_mod_v4(handles.options_quant);
status_update(hObject, eventdata, handles,{'  ';'## Options modified'});         
guidata(hObject, handles);
handles.flag_fit_pos_restrict = 1;    % Restrict position of center of PSF


% =========================================================================
%   Smaller misc. functions
% =========================================================================

%== Update status
function status_update(hObject, eventdata, handles,status_text)
status_old = get(handles.list_box_status,'String');
status_new = [status_old;status_text];
set(handles.list_box_status,'String',status_new);
set(handles.list_box_status,'ListboxTop',round(size(status_new,1)));
drawnow
guidata(hObject, handles); 


% =========================================================================
%   NOT USED
% =========================================================================


function checkbox_fit_fixed_width_Callback(hObject, eventdata, handles)

function checkbox_parallel_computing_Callback(hObject, eventdata, handles)

function text_kernel_factor_bgd_xy_Callback(hObject, eventdata, handles)

function text_kernel_factor_bgd_xy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_kernel_factor_filter_xy_Callback(hObject, eventdata, handles)

function text_kernel_factor_filter_xy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pop_up_exp_default_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function slider_slice_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function slider_contrast_max_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function slider_contrast_min_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function popupmenu2_Callback(hObject, eventdata, handles)

function popupmenu2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function list_box_status_Callback(hObject, eventdata, handles)

function list_box_status_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pop_up_img_sel_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popup_predetect_Callback(hObject, eventdata, handles)

function popup_predetect_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pop_up_outline_sel_reg_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popup_detect_image_Callback(hObject, eventdata, handles)

function popup_detect_image_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function checkbox_subtract_bgd_Callback(hObject, eventdata, handles)

function Untitled_1_Callback(hObject, eventdata, handles)

function Untitled_2_Callback(hObject, eventdata, handles)

function menu_save_Callback(hObject, eventdata, handles)

function Untitled_3_Callback(hObject, eventdata, handles)

function text_detect_min_int_Callback(hObject, eventdata, handles)

function text_detect_min_int_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function menu_folders_Callback(hObject, eventdata, handles)

function menu_folder_region_Callback(hObject, eventdata, handles)

function menu_folder_results_Callback(hObject, eventdata, handles)

function checkbox_quant_fit_data_Callback(hObject, eventdata, handles)

function edit_step_size_Callback(hObject, eventdata, handles)

function edit_step_size_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_kernel_factor_bgd_z_Callback(hObject, eventdata, handles)

function text_kernel_factor_bgd_z_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_kernel_factor_filter_z_Callback(hObject, eventdata, handles)

function text_kernel_factor_filter_z_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function button_slice_incr_ButtonDownFcn(hObject, eventdata, handles)

function button_slice_incr_KeyPressFcn(hObject, eventdata, handles)

function h_MQ_detect_ButtonDownFcn(hObject, eventdata, handles)

function menu_save_image_Callback(hObject, eventdata, handles)

function txt_pos_reuse_start_Callback(hObject, eventdata, handles)

function txt_pos_reuse_start_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function txt_pos_reuse_end_Callback(hObject, eventdata, handles)

function txt_pos_reuse_end_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function checkbox_fit_raw_bgd_Callback(hObject, eventdata, handles)


% --- Executes on selection change in popup_select_fit.
function popup_select_fit_Callback(hObject, eventdata, handles)
% hObject    handle to popup_select_fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_select_fit contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_select_fit


% --- Executes during object creation, after setting all properties.
function popup_select_fit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_select_fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
