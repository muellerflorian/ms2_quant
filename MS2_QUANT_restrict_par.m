function varargout = MS2_QUANT_restrict_par(varargin)
% MS2_QUANT_RESTRICT_PAR MATLAB code for MS2_QUANT_restrict_par.fig
% Last Modified by GUIDE v2.5 19-Jan-2012 17:30:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MS2_QUANT_restrict_par_OpeningFcn, ...
                   'gui_OutputFcn',  @MS2_QUANT_restrict_par_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MS2_QUANT_restrict_par is made visible.
function MS2_QUANT_restrict_par_OpeningFcn(hObject, eventdata, handles, varargin)


%= Export figure handle to workspace 
assignin('base','h_MQ_restrict',handles.h_MQ_restrict)

%== Set font-size to 10 - In WIN all the fonts set back to 8
h_font_8 = findobj(handles.h_MQ_restrict,'FontSize',8);
set(h_font_8,'FontSize',10)

if not(isempty(varargin))
    
    handles.child = 1; 
    parameters = varargin{1};
    
    handles.fit_limits  = parameters.fit_limits;
    handles.reg_prop    = parameters.reg_prop;   
    
    handles.int_min     = parameters.int_min;
    
    set(handles.pop_up_outline_sel_reg,'String',parameters.reg_list);
    set(handles.pop_up_outline_sel_reg,'Value',parameters.reg_sel);
    
    [handles.reg_stats handles.reg_prop status_reg_analyze] = analyze_regions(handles.reg_prop,handles.fit_limits,handles.int_min);
    
    %- Global or individual restriction
    handles.status_restr_global = parameters.status_restr_global;
    
     if handles.status_restr_global
         set(handles.checkbox_restrict_global,'Value',1);
         set(handles.checkbox_restrict_separate,'Value',0);
         handles = checkbox_restrict_global_Callback(hObject, eventdata, handles);
    else
         set(handles.checkbox_restrict_global,'Value',0);
         set(handles.checkbox_restrict_separate,'Value',1);
         checkbox_restrict_separate_Callback(hObject, eventdata, handles)
     end   
end     


%- Save results of previous analysis
handles.fit_limits.sigma_xy_min_prev = handles.fit_limits.sigma_xy_min;
handles.fit_limits.sigma_xy_max_prev = handles.fit_limits.sigma_xy_max;

handles.fit_limits.sigma_z_min_prev  = handles.fit_limits.sigma_z_min;
handles.fit_limits.sigma_z_max_prev  = handles.fit_limits.sigma_z_max;

handles.fit_limits.bgd_min_prev      = handles.fit_limits.bgd_min;
handles.fit_limits.bgd_max_prev      = handles.fit_limits.bgd_max;


% Choose default command line output for MS2_QUANT_restrict_par
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
plot_all(hObject, eventdata, handles)

% UIWAIT makes MS2_QUANT_restrict_par wait for user response (see UIRESUME)
uiwait(handles.h_MQ_restrict);


% --- Outputs from this function are returned to the command line.
function varargout = MS2_QUANT_restrict_par_OutputFcn(hObject, eventdata, handles) 

%- Only if called from another GUI
if handles.child
    varargout{1} = handles.reg_prop;
    varargout{2} = handles.status_restr_global;
    varargout{3} = handles.fit_limits;
    delete(handles.h_MQ_restrict);
end


%== Executes when user attempts to close h_MQ_restrict.
function h_MQ_restrict_CloseRequestFcn(hObject, eventdata, handles)
if handles.child 
    uiresume(handles.h_MQ_restrict)
else
    delete(handles.h_MQ_restrict) 
end


%== Finished with assignment
function button_done_Callback(hObject, eventdata, handles)
uiresume(handles.h_MQ_restrict)



%==========================================================================
%  General housekeeping
%==========================================================================


%== Analyze regions
function [reg_stats reg_prop status_reg_analyze] = analyze_regions(reg_prop,fit_limits,int_min)

N_reg = length(reg_prop);
status_reg_analyze = 0;

for i_reg = 1:N_reg
    
        if reg_prop(i_reg).flag_BGD == 0 && reg_prop(i_reg).flag_OPB == 0
            
            reg_stats(i_reg).flag_TS = 1;
            summary_fit       = reg_prop(i_reg).quant_fit_summary_all;
            summary_fit_first = reg_prop(i_reg).quant_fit_summary_all_first;   
            
            
            if not(isempty(summary_fit))

                %- Consider only frames with the filtered intensity above the threshold
                if isfield(reg_prop(i_reg),'detect_int')
                    detect_int = reg_prop(i_reg).detect_int;
                else
                    detect_int = inf*ones(size(reg_prop(i_reg).time));
                end    
                ind_avg    = (detect_int>int_min);

                %- Calculate mean, median, stdev
                status_reg_analyze = 1;
                reg_stats(i_reg).status_analyze = 1;
                reg_stats(i_reg).ind_avg = ind_avg;
                
                reg_stats(i_reg).sigmaxy_mean =   round(mean(summary_fit(ind_avg,3)));
                reg_stats(i_reg).sigmaz_mean  =   round(mean(summary_fit(ind_avg,4)));
                reg_stats(i_reg).bgd_mean     =   round(mean(summary_fit(ind_avg,2)));

                reg_stats(i_reg).sigmaxy_median = round(median(summary_fit(ind_avg,3)));
                reg_stats(i_reg).sigmaz_median  = round(median(summary_fit(ind_avg,4)));
                reg_stats(i_reg).bgd_median     = round(median(summary_fit(ind_avg,2)));

                reg_stats(i_reg).sigmaxy_stdev =  round(std(summary_fit(ind_avg,3)));
                reg_stats(i_reg).sigmaz_stdev  =  round(std(summary_fit(ind_avg,4)));
                reg_stats(i_reg).bgd_stdev     =  round(std(summary_fit(ind_avg,2)));


                %- PREVIOUS: Calculate mean, median, stdev
                reg_stats(i_reg).sigmaxy_first_mean =  round(mean(summary_fit_first(ind_avg,3)));
                reg_stats(i_reg).sigmaz_first_mean  =  round(mean(summary_fit_first(ind_avg,4)));
                reg_stats(i_reg).bgd_first_mean     =  round(mean(summary_fit_first(ind_avg,2)));

                reg_stats(i_reg).sigmaxy_first_median =  round(median(summary_fit_first(ind_avg,3)));
                reg_stats(i_reg).sigmaz_first_median  =  round(median(summary_fit_first(ind_avg,4)));
                reg_stats(i_reg).bgd_first_median     =  round(median(summary_fit_first(ind_avg,2)));

                reg_stats(i_reg).sigmaxy_first_stdev =  round(std(summary_fit_first(ind_avg,3)));
                reg_stats(i_reg).sigmaz_first_stdev  =  round(std(summary_fit_first(ind_avg,4)));
                reg_stats(i_reg).bgd_first_stdev     =  round(std(summary_fit_first(ind_avg,2)));            

                %- Set default range 
                if isempty(reg_prop(i_reg).fit_limits)
                   reg_prop(i_reg).fit_limits.sigma_xy_min = fit_limits.sigma_xy_min_def;
                   reg_prop(i_reg).fit_limits.sigma_xy_max = fit_limits.sigma_xy_max_def;
                   reg_prop(i_reg).fit_limits.sigma_z_min  = fit_limits.sigma_z_min_def;
                   reg_prop(i_reg).fit_limits.sigma_z_max  = fit_limits.sigma_z_max_def;
                   reg_prop(i_reg).fit_limits.bgd_min      = fit_limits.bgd_min_def;
                   reg_prop(i_reg).fit_limits.bgd_max      = fit_limits.bgd_max_def;      
                end
            else
                reg_stats(i_reg).status_analyze = 0;
            end
  
        else          
           reg_stats(i_reg).flag_TS = 0;
        end
end



%==========================================================================
%  Assign global range
%==========================================================================

%== Global range for all fitting
function handles = checkbox_restrict_global_Callback(hObject, eventdata, handles)

status_restr_global = get(handles.checkbox_restrict_global,'Value');

if status_restr_global
    
    set(handles.checkbox_restrict_separate,'Value',0);
    
    set(handles.button_range_no,'Enable','off');
    set(handles.button_range_default,'Enable','off');
    set(handles.button_range_stdev,'Enable','off');
    set(handles.button_range_mean,'Enable','off'); 
    
    set(handles.checkbox_limits_previous,'Enable','on');
    set(handles.checkbox_limits_default,'Enable','on'); 
    
    handles = range_global_assign(hObject, eventdata, handles);    
else
    
    set(handles.checkbox_restrict_separate,'Value',1);
    
    set(handles.button_range_no,'Enable','on');
    set(handles.button_range_default,'Enable','on');
    set(handles.button_range_stdev,'Enable','on');
    set(handles.button_range_mean,'Enable','on');   
    
    set(handles.checkbox_limits_previous,'Enable','off');
    set(handles.checkbox_limits_default,'Enable','off');     
end

%- Save results
handles.status_restr_global = status_restr_global;
guidata(hObject, handles);


%== Separate range for all fitting
function checkbox_restrict_separate_Callback(hObject, eventdata, handles)

status_restr_sep = get(handles.checkbox_restrict_separate,'Value');

if status_restr_sep
    
    set(handles.checkbox_restrict_global,'Value',0);
    handles.status_restr_global = 0;
    
    set(handles.button_range_no,'Enable','on');
    set(handles.button_range_default,'Enable','on');
    set(handles.button_range_stdev,'Enable','on');
    set(handles.button_range_mean,'Enable','on'); 
    
    set(handles.checkbox_limits_previous,'Enable','off');
    set(handles.checkbox_limits_default,'Enable','off');     
else
    set(handles.checkbox_restrict_global,'Value',1);
    handles.status_restr_global = 1;
    
    set(handles.button_range_no,'Enable','off');
    set(handles.button_range_default,'Enable','off');
    set(handles.button_range_stdev,'Enable','off');
    set(handles.button_range_mean,'Enable','off');
    
    set(handles.checkbox_limits_previous,'Enable','on');
    set(handles.checkbox_limits_default,'Enable','on'); 
end

%- Save results
guidata(hObject, handles);


%== Assign global range for fitting
function handles = range_global_assign(hObject, eventdata, handles)

N_reg = length(handles.reg_prop);

for i_reg = 1:N_reg
    
    if handles.reg_prop(i_reg).flag_BGD == 0 && handles.reg_prop(i_reg).flag_OPB == 0
             
           handles.reg_prop(i_reg).fit_limits.sigma_xy_min = handles.fit_limits.sigma_xy_min;
           handles.reg_prop(i_reg).fit_limits.sigma_xy_max = handles.fit_limits.sigma_xy_max;
           handles.reg_prop(i_reg).fit_limits.sigma_z_min  = handles.fit_limits.sigma_z_min;
           handles.reg_prop(i_reg).fit_limits.sigma_z_max  = handles.fit_limits.sigma_z_max;
           handles.reg_prop(i_reg).fit_limits.bgd_min      = handles.fit_limits.bgd_min;
           handles.reg_prop(i_reg).fit_limits.bgd_max      = handles.fit_limits.bgd_max;      
    end 
end

%- Save results
guidata(hObject, handles);
plot_all(hObject, eventdata, handles)


%== Assign limits from previous analysis 
function checkbox_limits_previous_Callback(hObject, eventdata, handles)
checkbox_previous = get(handles.checkbox_limits_previous,'Value');

if checkbox_previous
    set(handles.checkbox_limits_default,'Value',0);
    
    handles.fit_limits.sigma_xy_min = handles.fit_limits.sigma_xy_min_prev;
    handles.fit_limits.sigma_xy_max = handles.fit_limits.sigma_xy_max_prev ;
    handles.fit_limits.sigma_z_min  = handles.fit_limits.sigma_z_min_prev;
    handles.fit_limits.sigma_z_max  = handles.fit_limits.sigma_z_max_prev;
    handles.fit_limits.bgd_min      = handles.fit_limits.bgd_min_prev;
    handles.fit_limits.bgd_max      = handles.fit_limits.bgd_max_prev; 
    
    handles = range_global_assign(hObject, eventdata, handles);
    guidata(hObject, handles);
    plot_all(hObject, eventdata, handles)
end


%== Assign default limits
function checkbox_limits_default_Callback(hObject, eventdata, handles)
checkbox_default = get(handles.checkbox_limits_default,'Value');

if checkbox_default
    set(handles.checkbox_limits_previous,'Value',0);
    
    handles.fit_limits.sigma_xy_min = handles.fit_limits.sigma_xy_min_def;
    handles.fit_limits.sigma_xy_max = handles.fit_limits.sigma_xy_max_def ;
    handles.fit_limits.sigma_z_min  = handles.fit_limits.sigma_z_min_def;
    handles.fit_limits.sigma_z_max  = handles.fit_limits.sigma_z_max_def;
    handles.fit_limits.bgd_min      = handles.fit_limits.bgd_min_def;
    handles.fit_limits.bgd_max      = handles.fit_limits.bgd_max_def; 
    
    handles = range_global_assign(hObject, eventdata, handles);
    guidata(hObject, handles);
    plot_all(hObject, eventdata, handles)
end


%==========================================================================
%  Assign range for individual regions
%==========================================================================

%== Range back to default
function button_range_no_Callback(hObject, eventdata, handles)

N_reg = length(handles.reg_prop);

for i_reg = 1:N_reg
    
    if handles.reg_prop(i_reg).flag_BGD == 0 && handles.reg_prop(i_reg).flag_OPB == 0
             
           handles.reg_prop(i_reg).fit_limits.sigma_xy_min = handles.fit_limits.sigma_xy_min_def;
           handles.reg_prop(i_reg).fit_limits.sigma_xy_max = handles.fit_limits.sigma_xy_max_def;
           handles.reg_prop(i_reg).fit_limits.sigma_z_min  = handles.fit_limits.sigma_z_min_def;
           handles.reg_prop(i_reg).fit_limits.sigma_z_max  = handles.fit_limits.sigma_z_max_def;
           handles.reg_prop(i_reg).fit_limits.bgd_min      = handles.fit_limits.bgd_min_def;
           handles.reg_prop(i_reg).fit_limits.bgd_max      = handles.fit_limits.bgd_max_def;   
       
    end
    
end

guidata(hObject, handles);
plot_all(hObject, eventdata, handles)


%== Assign default range: mean +/- stdev for sigma, mean for amp
function button_range_default_Callback(hObject, eventdata, handles)

status_first = get(handles.checkbox_first,'Value');
reg_stats = handles.reg_stats;

N_reg = length(handles.reg_prop);

for i_reg = 1:N_reg
    
    if handles.reg_prop(i_reg).flag_BGD == 0 && handles.reg_prop(i_reg).flag_OPB == 0
        if status_first      
           handles.reg_prop(i_reg).fit_limits.sigma_xy_min = reg_stats(i_reg).sigmaxy_first_median - reg_stats(i_reg).sigmaxy_first_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_xy_max = reg_stats(i_reg).sigmaxy_first_median + reg_stats(i_reg).sigmaxy_first_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_z_min  = reg_stats(i_reg).sigmaz_first_median  - reg_stats(i_reg).sigmaz_first_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_z_max  = reg_stats(i_reg).sigmaz_first_median  + reg_stats(i_reg).sigmaz_first_stdev;
           handles.reg_prop(i_reg).fit_limits.bgd_min      = reg_stats(i_reg).bgd_first_median;
           handles.reg_prop(i_reg).fit_limits.bgd_max      = reg_stats(i_reg).bgd_first_median     + 1;       
        else
           handles.reg_prop(i_reg).fit_limits.sigma_xy_min = reg_stats(i_reg).sigmaxy_median - reg_stats(i_reg).sigmaxy_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_xy_max = reg_stats(i_reg).sigmaxy_median + reg_stats(i_reg).sigmaxy_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_z_min  = reg_stats(i_reg).sigmaz_median  - reg_stats(i_reg).sigmaz_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_z_max  = reg_stats(i_reg).sigmaz_median  + reg_stats(i_reg).sigmaz_stdev;
           handles.reg_prop(i_reg).fit_limits.bgd_min      = reg_stats(i_reg).bgd_median;
           handles.reg_prop(i_reg).fit_limits.bgd_max      = reg_stats(i_reg).bgd_median     + 1;                   
        end
    end
    
end

guidata(hObject, handles);
plot_all(hObject, eventdata, handles)


%== Assign range: mean +/- stdev
function button_range_stdev_Callback(hObject, eventdata, handles)

status_first = get(handles.checkbox_first,'Value');
reg_stats = handles.reg_stats;

N_reg = length(handles.reg_prop);

for i_reg = 1:N_reg
    
    if handles.reg_prop(i_reg).flag_BGD == 0 && handles.reg_prop(i_reg).flag_OPB == 0
       if status_first           
           handles.reg_prop(i_reg).fit_limits.sigma_xy_min = reg_stats(i_reg).sigmaxy_first_median - reg_stats(i_reg).sigmaxy_first_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_xy_max = reg_stats(i_reg).sigmaxy_first_median + reg_stats(i_reg).sigmaxy_first_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_z_min  = reg_stats(i_reg).sigmaz_first_median  - reg_stats(i_reg).sigmaz_first_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_z_max  = reg_stats(i_reg).sigmaz_first_median  + reg_stats(i_reg).sigmaz_first_stdev;
           handles.reg_prop(i_reg).fit_limits.bgd_min      = reg_stats(i_reg).bgd_first_median     - reg_stats(i_reg).sigmaz_first_stdev;
           handles.reg_prop(i_reg).fit_limits.bgd_max      = reg_stats(i_reg).bgd_first_median     + reg_stats(i_reg).bgd_first_stdev;      
       else
           handles.reg_prop(i_reg).fit_limits.sigma_xy_min = reg_stats(i_reg).sigmaxy_median - reg_stats(i_reg).sigmaxy_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_xy_max = reg_stats(i_reg).sigmaxy_median + reg_stats(i_reg).sigmaxy_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_z_min  = reg_stats(i_reg).sigmaz_median  - reg_stats(i_reg).sigmaz_stdev;
           handles.reg_prop(i_reg).fit_limits.sigma_z_max  = reg_stats(i_reg).sigmaz_median  + reg_stats(i_reg).sigmaz_stdev;
           handles.reg_prop(i_reg).fit_limits.bgd_min      = reg_stats(i_reg).bgd_median     - reg_stats(i_reg).sigmaz_stdev;
           handles.reg_prop(i_reg).fit_limits.bgd_max      = reg_stats(i_reg).bgd_median     + reg_stats(i_reg).bgd_stdev;      
       end
    end
end

guidata(hObject, handles);
plot_all(hObject, eventdata, handles)


%== Assign range: mean 
function button_range_mean_Callback(hObject, eventdata, handles)
status_first = get(handles.checkbox_first,'Value');
reg_stats = handles.reg_stats;

N_reg = length(handles.reg_prop);

for i_reg = 1:N_reg
    
    if handles.reg_prop(i_reg).flag_BGD == 0 && handles.reg_prop(i_reg).flag_OPB == 0
       if status_first              
           handles.reg_prop(i_reg).fit_limits.sigma_xy_min = reg_stats(i_reg).sigmaxy_first_median ;
           handles.reg_prop(i_reg).fit_limits.sigma_xy_max = reg_stats(i_reg).sigmaxy_first_median +1;
           handles.reg_prop(i_reg).fit_limits.sigma_z_min  = reg_stats(i_reg).sigmaz_first_median  ;
           handles.reg_prop(i_reg).fit_limits.sigma_z_max  = reg_stats(i_reg).sigmaz_first_median  +1;
           handles.reg_prop(i_reg).fit_limits.bgd_min      = reg_stats(i_reg).bgd_first_median     ;
           handles.reg_prop(i_reg).fit_limits.bgd_max      = reg_stats(i_reg).bgd_first_median     +1;      
       else
           handles.reg_prop(i_reg).fit_limits.sigma_xy_min = reg_stats(i_reg).sigmaxy_median ;
           handles.reg_prop(i_reg).fit_limits.sigma_xy_max = reg_stats(i_reg).sigmaxy_median +1;
           handles.reg_prop(i_reg).fit_limits.sigma_z_min  = reg_stats(i_reg).sigmaz_median  ;
           handles.reg_prop(i_reg).fit_limits.sigma_z_max  = reg_stats(i_reg).sigmaz_median  +1;
           handles.reg_prop(i_reg).fit_limits.bgd_min      = reg_stats(i_reg).bgd_median     ;
           handles.reg_prop(i_reg).fit_limits.bgd_max      = reg_stats(i_reg).bgd_median     +1;               
       end
    end
end

guidata(hObject, handles);
plot_all(hObject, eventdata, handles)


%==========================================================================
%  Buttons to select minimum
%==========================================================================

%== Sigma-XY: min
function button_sigmax_min_Callback(hObject, eventdata, handles)
axes(handles.axes_sigmaxy)
[x,y] = ginput(1);

set(handles.text_sigmaxy_min,'String',num2str(round(y)));
handles = text_sigmaxy_min_Callback(hObject, eventdata, handles);
guidata(hObject, handles);


%== Sigma-XY: max
function button_sigmax_max_Callback(hObject, eventdata, handles)
axes(handles.axes_sigmaxy)
[x,y] = ginput(1);

set(handles.text_sigmaxy_max,'String',num2str(round(y)));
handles = text_sigmaxy_max_Callback(hObject, eventdata, handles);
guidata(hObject, handles);


%== Sigma-Z: min
function button_sigmaz_min_Callback(hObject, eventdata, handles)
axes(handles.axes_sigmaz)
[x,y] = ginput(1);

set(handles.text_sigmaz_min,'String',num2str(round(y)));
handles = text_sigmaz_min_Callback(hObject, eventdata, handles);
guidata(hObject, handles);


%== Sigma-Z: max
function button_sigmaz_max_Callback(hObject, eventdata, handles)
axes(handles.axes_sigmaz)
[x,y] = ginput(1);

set(handles.text_sigmaz_max,'String',num2str(round(y)));
handles = text_sigmaz_max_Callback(hObject, eventdata, handles);
guidata(hObject, handles);


%== BGD: min
function button_bgd_min_Callback(hObject, eventdata, handles)
axes(handles.axes_bgd)
[x,y] = ginput(1);

set(handles.text_bgd_min,'String',num2str(round(y)));
handles = text_bgd_min_Callback(hObject, eventdata, handles);
guidata(hObject, handles);


%== BGD: max
function button_bgd_max_Callback(hObject, eventdata, handles)
axes(handles.axes_bgd)
[x,y] = ginput(1);

set(handles.text_bgd_max,'String',num2str(round(y)));
handles = text_bgd_max_Callback(hObject, eventdata, handles);
guidata(hObject, handles);



%==========================================================================
%  TEXT BOXES
%==========================================================================

%== Sigma-XY: min
function handles = text_sigmaxy_min_Callback(hObject, eventdata, handles)

%- Assing new values for fit
if handles.status_restr_global
    handles.fit_limits.sigma_xy_min = str2double(get(handles.text_sigmaxy_min,'String'));
    handles                         = range_global_assign(hObject, eventdata, handles);
    
    %- Save and plot
    guidata(hObject, handles);
end
plot_all(hObject, eventdata, handles)


%== Sigma-XY: max
function handles = text_sigmaxy_max_Callback(hObject, eventdata, handles)


%- Assing new values for fit
if handles.status_restr_global
    handles.fit_limits.sigma_xy_max = str2double(get(handles.text_sigmaxy_max,'String'));
    handles                         = range_global_assign(hObject, eventdata, handles);
    
    %- Save and plot
    guidata(hObject, handles);
end
plot_all(hObject, eventdata, handles)


%== Sigma-Z: min
function handles = text_sigmaz_min_Callback(hObject, eventdata, handles)

%- Assing new values for fit
if handles.status_restr_global
    handles.fit_limits.sigma_z_min = str2double(get(handles.text_sigmaz_min,'String'));
    handles = range_global_assign(hObject, eventdata, handles);
    
    %- Save and plot
    guidata(hObject, handles);
end
plot_all(hObject, eventdata, handles)


%== Sigma-Z: max
function handles = text_sigmaz_max_Callback(hObject, eventdata, handles)

%- Assing new values for fit
if handles.status_restr_global
    
    handles.fit_limits.sigma_z_max = str2double(get(handles.text_sigmaz_max,'String'));
    handles = range_global_assign(hObject, eventdata, handles);
    
    %- Save and plot
    guidata(hObject, handles);
end
plot_all(hObject, eventdata, handles)


%== BGD: min
function handles = text_bgd_min_Callback(hObject, eventdata, handles)


%- Assing new values for fit
if handles.status_restr_global
    handles.fit_limits.bgd_min = str2double(get(handles.text_bgd_min,'String'));
    handles = range_global_assign(hObject, eventdata, handles);
    
    %- Save and plot
    guidata(hObject, handles);
end
plot_all(hObject, eventdata, handles)


%== BGD: max
function handles = text_bgd_max_Callback(hObject, eventdata, handles)

%- Assing new values for fit
if handles.status_restr_global
    handles.fit_limits.bgd_max = str2double(get(handles.text_bgd_max,'String'));
    handles = range_global_assign(hObject, eventdata, handles);
   
    %- Save and plot
    guidata(hObject, handles);
end
plot_all(hObject, eventdata, handles)


%== Select region
function pop_up_outline_sel_reg_Callback(hObject, eventdata, handles)
plot_all(hObject, eventdata, handles)


%==========================================================================
%  MISC commands
%==========================================================================


%== Previous analysis
function checkbox_first_Callback(hObject, eventdata, handles)
plot_all(hObject, eventdata, handles)


%== Sigma-XY: max
function plot_all(hObject, eventdata, handles)

i_reg        = get(handles.pop_up_outline_sel_reg,'Value');
reg_stats    = handles.reg_stats(i_reg);
reg_prop     = handles.reg_prop(i_reg);
status_first = get(handles.checkbox_first,'Value');

if reg_stats.flag_TS
    
    %- Get data
    if status_first
        summary_fit = reg_prop.quant_fit_summary_all_first;
    else
        summary_fit = reg_prop.quant_fit_summary_all;
    end
    
    %- Set levels   
    if ~isempty(reg_prop.fit_limits)
        set(handles.text_sigmaxy_min,'String',num2str(reg_prop.fit_limits.sigma_xy_min));
        set(handles.text_sigmaxy_max,'String',num2str(reg_prop.fit_limits.sigma_xy_max));
 
        set(handles.text_sigmaz_min,'String',num2str(reg_prop.fit_limits.sigma_z_min));
        set(handles.text_sigmaz_max,'String',num2str(reg_prop.fit_limits.sigma_z_max));
  
        set(handles.text_bgd_min,'String',num2str(reg_prop.fit_limits.bgd_min));
        set(handles.text_bgd_max,'String',num2str(reg_prop.fit_limits.bgd_max));
    else
        set(handles.text_sigmaxy_min,'String',num2str(handles.fit_limits.sigma_xy_min));
        set(handles.text_sigmaxy_max,'String',num2str(handles.fit_limits.sigma_xy_max));
 
        set(handles.text_sigmaz_min,'String',num2str(handles.fit_limits.sigma_z_min));
        set(handles.text_sigmaz_max,'String',num2str(handles.fit_limits.sigma_z_max));
  
        set(handles.text_bgd_min,'String',num2str(handles.fit_limits.bgd_min));
        set(handles.text_bgd_max,'String',num2str(handles.fit_limits.bgd_max)); 
    end
        
        
    if reg_stats.status_analyze
    %- Set median and mean values
    
        set(handles.text_sigmaxy_median,'String',num2str(reg_stats.sigmaxy_median));
        set(handles.text_sigmaxy_mean,'String',num2str(reg_stats.sigmaxy_mean));

        set(handles.text_sigmaz_median,'String',num2str(reg_stats.sigmaz_median));
        set(handles.text_sigmaz_mean,'String',num2str(reg_stats.sigmaz_mean));

        set(handles.text_bgd_median,'String',num2str(reg_stats.bgd_median));
        set(handles.text_bgd_mean,'String',num2str(reg_stats.bgd_mean));
    
    
       %-- Sigma_xy
       if size(summary_fit,1)>1
           x = (1:length(reg_stats.ind_avg));
           
           axes(handles.axes_sigmaxy)
           plot(summary_fit(:,3),'k','Color',[0.5 0.5 0.5])
           hold on
               plot(x(reg_stats.ind_avg),summary_fit(reg_stats.ind_avg,3),'k','Color','k','LineWidth',1.5)

               plot([0 length(summary_fit(:,3))],[reg_prop.fit_limits.sigma_xy_min reg_prop.fit_limits.sigma_xy_min],'g');
               plot([0 length(summary_fit(:,3))],[reg_prop.fit_limits.sigma_xy_max reg_prop.fit_limits.sigma_xy_max],'r');
               if not(isempty(reg_stats.sigmaxy_median))
                   plot([0 length(summary_fit(:,3))],[reg_stats.sigmaxy_median reg_stats.sigmaxy_median],'b');
               end 
           hold off
           v2 = axis;
           axis([0 length(summary_fit(:,3)) v2(3) v2(4)])
           box on

           %-- Sigma_z
           axes(handles.axes_sigmaz)
           plot(summary_fit(:,4),'k','Color',[0.5 0.5 0.5])
           hold on
               plot(x(reg_stats.ind_avg),summary_fit(reg_stats.ind_avg,4),'k','Color','k','LineWidth',1.5)
           
               plot([0 length(summary_fit(:,4))],[reg_prop.fit_limits.sigma_z_min reg_prop.fit_limits.sigma_z_min],'g');
               plot([0 length(summary_fit(:,4))],[reg_prop.fit_limits.sigma_z_max reg_prop.fit_limits.sigma_z_max],'r');
               if not(isempty(reg_stats.sigmaz_median))
                   plot([0 length(summary_fit(:,3))],[reg_stats.sigmaz_median reg_stats.sigmaz_median],'b');
               end
               hold off
           v2 = axis;
           axis([0 length(summary_fit(:,4)) v2(3) v2(4)])
           box on


           %-- BGD
           axes(handles.axes_bgd)
           plot(summary_fit(:,2),'k','Color',[0.5 0.5 0.5])
           hold on
               plot(x(reg_stats.ind_avg),summary_fit(reg_stats.ind_avg,2),'k','Color','k','LineWidth',1.5)
               
               plot([0 length(summary_fit(:,2))],[reg_prop.fit_limits.bgd_min reg_prop.fit_limits.bgd_min],'g');
               plot([0 length(summary_fit(:,2))],[reg_prop.fit_limits.bgd_max reg_prop.fit_limits.bgd_max],'r');
               if not(isempty(reg_stats.bgd_median))
                   plot([0 length(summary_fit(:,2))],[reg_stats.bgd_median reg_stats.bgd_median],'b');
               end
               hold off
           v2 = axis;
           axis([0 length(summary_fit(:,2)) v2(3) v2(4)])
           box on
       end
    end
   
else
   cla(handles.axes_sigmaxy)
   cla(handles.axes_sigmaz)
   cla(handles.axes_bgd)
   
   
    %- Set levels   
    set(handles.text_sigmaxy_min,'String','');
    set(handles.text_sigmaxy_max,'String','');
 
    set(handles.text_sigmaz_min,'String','');
    set(handles.text_sigmaz_max,'String','');
  
    set(handles.text_bgd_min,'String','');
    set(handles.text_bgd_max,'String','');
        
    %- Set median and mean values
    set(handles.text_sigmaxy_median,'String','NO TS or fit');
    set(handles.text_sigmaxy_mean,'String','');
    
    set(handles.text_sigmaz_median,'String','NO TS or fit');
    set(handles.text_sigmaz_mean,'String','');
    
    set(handles.text_bgd_median,'String','NO TS or fit');
    set(handles.text_bgd_mean,'String','');
   
end


% =========================================================================
% Not used
% =========================================================================

function text_sigmaxy_min_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_sigmaxy_max_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_sigmaz_min_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_sigmaz_max_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_bgd_min_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_bgd_max_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pop_up_outline_sel_reg_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
