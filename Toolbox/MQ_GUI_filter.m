function handles =  MQ_GUI_filter(handles)

global MQ_img_stack MQ_img_MIP

%- Correction of OPB
handles.flag_opb_apply = get(handles.checkbox_opb_apply,'Value'); 

if handles.flag_opb_apply && isfield(handles,'OPB')

    OPP_curves  = handles.OPB.curves;

    %- Check if photobleaching correction curves are available
    if not(isempty(OPP_curves))
        opb_norm    = OPP_curves(:,4);
        bgd_imaging = handles.bgd_imaging;

    else
        warndlg('OPB: quantification has to be performed first!',mfilename);
        return
        
    end
else
    opb_norm = [];
    status_update(hObject, eventdata, handles,{'Filtering: OPB will not be performed.'}) 
end

%- Parameters for filtering       
kernel_bgd_xy = str2double(get(handles.text_kernel_factor_bgd_xy,'String'));
kernel_bgd_z  = str2double(get(handles.text_kernel_factor_bgd_z,'String'));

kernel_SNR_xy = str2double(get(handles.text_kernel_factor_filter_xy,'String'));
kernel_SNR_z  = str2double(get(handles.text_kernel_factor_filter_z,'String'));

filter.pad    = 3*kernel_bgd_xy;

%- What kind of data will be read in?   
switch handles.img_type

    %- 3D Time-stack data
    case {'3D','3d'}  
    
        nFrames = length(MQ_img_stack);
        
        %- Allocate space
        MQ_img_stack(nFrames).filt = MQ_img_stack(1).data;
        MQ_img_stack(nFrames).raw_bgd_sub       = MQ_img_stack(1).data;
        MQ_img_stack(nFrames).opb_bgd_sub  = MQ_img_stack(1).data;
       
        %== Loop over time-points
        fprintf('\n= Filtering %d time-points (one . per frame) \n',nFrames)
        MQ_img_stack_local = MQ_img_stack;
        
        parfor ind = 1:nFrames

            %- Plot points as indicator where we are 
            %if mod(ind, 100) == 1; fprintf('\n%5.0f ',ind); end; fprintf('.')
            if mod(ind, 100) == 1; fprintf('\n '); end; fprintf('.')
            
            img = MQ_img_stack_local(ind).data;

            %=== Background subtraction for raw image
            img_pad = double(padarray(img,[filter.pad filter.pad filter.pad],'symmetric','both'));

            %- Apply Gaussian smoothing to image: background image
            if kernel_bgd_xy ~=0 || kernel_bgd_z ~=0
                bgd_filt = gaussSmooth(img_pad,  [kernel_bgd_xy kernel_bgd_xy kernel_bgd_z], 'same');    
                img_diff = img_pad-bgd_filt;    
                img_diff = img_diff.*(img_diff>0);      % Set negative values to zero

            else
                bgd_filt = zeros(size(img_pad));
                img_diff = img_pad;
            end
            
            img_raw_bgd_sub = img_diff(filter.pad+1:end-filter.pad,filter.pad+1:end-filter.pad,filter.pad+1:end-filter.pad);

            
            %=== Correct for photobleaching - if defined
            if handles.flag_opb_apply
               img = img - bgd_imaging(ind);
               img = img ./ opb_norm(ind);
           
                %== FILTERING of 3d data

                %- [1] Padding of image
                img_pad = double(padarray(img,[filter.pad filter.pad filter.pad],'symmetric','both'));

                %- [2] Apply Gaussian smoothing to image: background image
                if kernel_bgd_xy ~=0 || kernel_bgd_z ~=0
                    bgd_filt = gaussSmooth(img_pad,  [kernel_bgd_xy kernel_bgd_xy kernel_bgd_z], 'same');    
                    img_diff = img_pad-bgd_filt;    
                    img_diff = img_diff.*(img_diff>0);      % Set negative values to zero

                else
                     img_diff = img_pad;
                end
                
                 img_opb_bgd_sub = img_diff(filter.pad+1:end-filter.pad,filter.pad+1:end-filter.pad,filter.pad+1:end-filter.pad);
            else
                img_opb_bgd_sub = img_raw_bgd_sub;
            end
            
            
            %- [3] Filter for spot enhancement
            if kernel_SNR_xy ~=0 || kernel_SNR_z ~=0
            
                img_smooth = gaussSmooth( img_diff, [kernel_SNR_xy kernel_SNR_xy kernel_SNR_z] , 'same');    
                img_smooth = img_smooth.*(img_smooth>0);
            else
                img_smooth = img_diff;
            end
            
            img_smooth = img_smooth(filter.pad+1:end-filter.pad,filter.pad+1:end-filter.pad,filter.pad+1:end-filter.pad);

            %- [4] Save images
            MQ_img_stack_local(ind).filt         = uint16(img_smooth);
            MQ_img_stack_local(ind).opb_bgd_sub  = uint16(img_opb_bgd_sub);
            MQ_img_stack_local(ind).raw_bgd_sub  = uint16(img_raw_bgd_sub);

            MQ_img_MIP_local_filt(:,:,ind)          = uint16(max(img_smooth,[],3));
            MQ_img_MIP_local_opb_bgd_sub(:,:,ind)   = uint16(max(img_opb_bgd_sub,[],3));
            MQ_img_MIP_local_raw_bgd_sub(:,:,ind)   = uint16(max(img_raw_bgd_sub,[],3));
        end
        fprintf('\nFiltering finished!\n')
end

%- Reassign to global
MQ_img_stack = MQ_img_stack_local;

MQ_img_MIP.filt = MQ_img_MIP_local_filt;
MQ_img_MIP.local_opb_bgd_sub = MQ_img_MIP_local_opb_bgd_sub;
MQ_img_MIP.local_raw_bgd_sub = MQ_img_MIP_local_raw_bgd_sub;