function MQ_detect_enable_controls_v4(handles)

%- Change name of GUI
if not(isempty(handles.file_name_image))
    set(handles.h_MQ_detect,'Name', ['MS2-QUANT ', handles.version, ': main interface - ', handles.file_name_image ]);
else
    set(handles.h_MQ_detect,'Name', ['MS2-QUANT ', handles.version, ': main interface']);
end


%- Status of sub-folder for results
status_results = handles.path_sub_results_status;

if status_results
    set(handles.menu_folder_results_enable,'Checked','on');
else
    set(handles.menu_folder_results_enable,'Checked','off');
end

%- Status of sub-folder for regions
status_region = handles.path_sub_regions_status;

if status_region
    set(handles.menu_folder_region_enable,'Checked','on');
else
    set(handles.menu_folder_region_enable,'Checked','off');
end

% ==== Image loaded
if handles.status_image    

    %- Enable various controls
    set(handles.button_outline_define,'Enable','on')
    set(handles.button_filter,'Enable','on')
    
    set(handles.text_kernel_factor_bgd_xy,'Enable','on')
    set(handles.text_kernel_factor_filter_xy,'Enable','on')
    set(handles.text_kernel_factor_bgd_z,'Enable','on')
    set(handles.text_kernel_factor_filter_z,'Enable','on')


else
    
    %- Disable various controls
    set(handles.button_outline_define,'Enable','off')
    set(handles.button_filter,'Enable','off')
    
    set(handles.text_kernel_factor_bgd_xy,'Enable','off')
    set(handles.text_kernel_factor_filter_xy,'Enable','off')
    set(handles.text_kernel_factor_bgd_z,'Enable','off')
    set(handles.text_kernel_factor_filter_z,'Enable','off')
    

end 


%==== Outline definition
if handles.status_image && strcmp(handles.img_type_orig,'3D')   
    set(handles.checkbox_data_analyze_2D,'Enable','on')
else
    set(handles.checkbox_data_analyze_2D,'Enable','off')
end
    

%==== Outline definition
if handles.status_outline
     set(handles.pop_up_outline_sel_reg,'Enable','on')
     set(handles.menu_save_region,'Enable','on')     
else
     set(handles.pop_up_outline_sel_reg,'Enable','off')
     set(handles.menu_save_region,'Enable','off')
end


%=== Pre-detection only with loaded image and defined outline
if handles.status_image && handles.status_outline 
    set(handles.button_predetect,'Enable','on')
else
    set(handles.button_predetect,'Enable','off')
end
    
%=== Quantification only with loaded image and defined outline
if handles.status_detect %%&& handles.status_filtered 
    set(handles.button_TS_quant,'Enable','on')
    set(handles.button_correct_detection,'Enable','on')
    set(handles.button_quant_restrict,'Enable','on')  
else
    set(handles.button_TS_quant,'Enable','off')
    set(handles.button_correct_detection,'Enable','off')
    set(handles.button_quant_restrict,'Enable','off')  
end


%== When quantification is finished
if handles.status_quant
    set(handles.button_show_results,'Enable','on')
    set(handles.menu_save_quant,'Enable','on')  
    set(handles.menu_save_region_results,'Enable','on')  
    
    
else
    set(handles.button_show_results,'Enable','off')
    set(handles.menu_save_quant,'Enable','off') 
    set(handles.menu_save_region_results,'Enable','off') 
end


%== Correction for observational photobleaching possible
if handles.flag_OPB
    set(handles.button_OPB_quantify,'Enable','on') 
else
    set(handles.button_OPB_quantify,'Enable','off')   
end

%== Correction for observational photobleaching finished
if handles.status_OPB_quant
    set(handles.checkbox_opb_apply,'Enable','on') 
else
    set(handles.checkbox_opb_apply,'Enable','off')   
end
