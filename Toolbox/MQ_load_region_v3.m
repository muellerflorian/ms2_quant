function [reg_prop, file_names, par_microscope, BGD_img, flag_file] = MQ_load_region_v3(file_name_load)
% Function to read in outline definition for cells

%- Prepare structure
reg_prop          = struct('label', {}, 'x', {}, 'y', {}, 'pos', {}, ...
                                   'flag_BGD',[],'flag_OPB',[],'fit_limits', {},...
                                   'quant_fit_summary_all', [], 'quant_fit_summary_all_first',[]);
                
par_microscope   = [];
file_names       = {};
BGD_img          = [];


%- Open file
fid  =  fopen(file_name_load,'r');

try 
    %=== Read in header of file

    %-  Line 1-2: Header row
    C = textscan(fid,'%s',1,'delimiter','\n');
    C = textscan(fid,'%s',1,'delimiter','\n');

    %-  Line 3: Comment
    C = textscan(fid,'%s',1,'delimiter','\n');

    %-  Line 4: Key word FILE
    C = textscan(fid,'%s',1,'delimiter','\t'); 

    %-  Line 4: File name
    C = textscan(fid,'%s',1,'delimiter','\n');   
    file_names.image = char(C{1});


    %-  Line 5: Key word FILTERED
    C = textscan(fid,'%s',1,'delimiter','\t'); 

    %-  Line 5: File name of filtered image
    C = textscan(fid,'%s',1,'delimiter','\n');   
    file_names.image_filtered = char(C{1});

    %-  Line 6: Key-word PARAMETERS
    C = textscan(fid,'%s',1,'delimiter','\n');

    %-  Line 7: Description of parameters of microscope
    C = textscan(fid,'%s',1,'delimiter','\n');

    %-  Line 8: Parameters of microscope
    C = textscan(fid,'%f32',7,'delimiter','\t');
    par_microscope.pixel_size.xy  = double(C{1}(1));
    par_microscope.pixel_size.z   = double(C{1}(2));
    par_microscope.RI  = double(C{1}(3));
    par_microscope.Ex  = double(C{1}(4));
    par_microscope.Em  = double(C{1}(5));
    par_microscope.NA  = double(C{1}(6));
    par_microscope.dT  = double(C{1}(7));
    
    C = textscan(fid,'%s',1,'delimiter','\n');
    par_microscope.type = char(C{1});

    %-  Line 9: Analysis settings
    C   = textscan(fid,'%s',1,'delimiter','\t');     % Key word: ANALYSIS-SETTINGS
    C   = textscan(fid,'%s',1,'delimiter','\n');     % File for detection settings
    file_names.settings = char(C{1});
    
    %-  Line 10: Imaging background
    C   = textscan(fid,'%s',1,'delimiter','\t');     % Key word: IMAGING-BACKGROUND
    C   = textscan(fid,'%f32',1,'delimiter','\n');     % Background
    BGD_img = double(C{1});
    

    %=== Running index for each cell per image
    ind_reg = 1;   % Initialize block index

    %- Line 10: Key-word Region
    C   = textscan(fid,'%s',1,'delimiter','\t');     % KEY WORD REGION

    while (~feof(fid)) 

        %- Label of region
        C = textscan(fid,'%s',1,'delimiter','\n');
        reg_prop(ind_reg).label = char(C{1});  
        
        %=== Type of region
        C = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'TYPE';
        C = textscan(fid,'%s',1,'delimiter','\n'); % Type of region
        
        %- OPB region
        if strcmp(C{1},'OPB')
            reg_prop(ind_reg).flag_OPB = 1;
        else
            reg_prop(ind_reg).flag_OPB = 0;
        end
        
        %- BGD region
        if strcmp(C{1},'BGD')
            reg_prop(ind_reg).flag_BGD = 1;
        else
            reg_prop(ind_reg).flag_BGD = 0;            
        end     

        
        %=== Polygon of region
        C = textscan(fid,'%s',1,'delimiter','\n'); % kKey word 'OUTLINE'
        
        %- Line 11: x-coordinates - stop when string 'END' is found
        C   = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'X_POS';

        C   = textscan(fid,'%s',1,'delimiter','\t');
        str = C{1};    
        i=1;    
        while not(strcmp(str,'END'))
            reg_prop(ind_reg).x(i) = str2double(str);

            %- Read in next one
            i   = i+1;
            C   = textscan(fid,'%s',1,'delimiter','\t');
            str = C{1};
        end

        C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change

        %- Line 12: y-coordinates - stop when string 'END' is found
        C   = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'Y_POS';

        C = textscan(fid,'%s',1,'delimiter','\t');
        str = C{1};
        i=1;

        while not(strcmp(str,'END'))
            reg_prop(ind_reg).y(i) = str2double(str);

            %- Read in next one
            i   = i+1;
            C   = textscan(fid,'%s',1,'delimiter','\t');
            str = C{1};
        end

        C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change  
        C = textscan(fid,'%s',1,'delimiter','\t');   % Read in identifier

       
        %- Test if positions are defined  
        status_read_next_line = 0;   %- Read last line in case positions are defined
      
        %== POSITIONS
        while strcmp(C{1},'POSITION')
 
 
             C = textscan(fid,'%s',1,'delimiter','\n'); % Line-break 
 
             %- X-coordinates
             C   = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'X_POS';
             C   = textscan(fid,'%s',1,'delimiter','\t');
             str = C{1};    
             i=1;    
             while not(strcmp(str,'END'))
                 reg_prop(ind_reg).pos(i).x = str2double(str);

                 %- Read in next one
                 i   = i+1;
                 C   = textscan(fid,'%s',1,'delimiter','\t');
                 str = C{1};
             end
 
             C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change
 
             %- Y-coordinates
             C = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'Y_POS';
             C = textscan(fid,'%s',1,'delimiter','\t');
             str = C{1};
             i=1;
 
             while not(strcmp(str,'END'))
                 reg_prop(ind_reg).pos(i).y = str2double(str);
 
                 %- Read in next one
                 i   = i+1;
                 C   = textscan(fid,'%s',1,'delimiter','\t');
                 str = C{1};
             end
             
             C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change
             
             %- Z-coordinates
             C = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'Z_POS';
             C = textscan(fid,'%s',1,'delimiter','\t');
             str = C{1};
             i=1;
 
             while not(strcmp(str,'END'))
                 reg_prop(ind_reg).pos(i).z = str2double(str);
 
                 %- Read in next one
                 i   = i+1;
                 C   = textscan(fid,'%s',1,'delimiter','\t');
                 str = C{1};
             end
             
             C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change
             
             %- X-sub-coordinates
             C   = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'X_POS_SUB';
             C   = textscan(fid,'%s',1,'delimiter','\t');
             str = C{1};    
             i=1;    
             while not(strcmp(str,'END'))
                 reg_prop(ind_reg).pos(i).x_sub = str2double(str);

                 %- Read in next one
                 i   = i+1;
                 C   = textscan(fid,'%s',1,'delimiter','\t');
                 str = C{1};
             end
 
             C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change
 
             %- Y-sub-coordinates
             C = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'Y_POS_SUB';
             C = textscan(fid,'%s',1,'delimiter','\t');
             str = C{1};
             i=1;
 
             while not(strcmp(str,'END'))
                 reg_prop(ind_reg).pos(i).y_sub = str2double(str);
 
                 %- Read in next one
                 i   = i+1;
                 C   = textscan(fid,'%s',1,'delimiter','\t');
                 str = C{1};
             end
             
             C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change
             
             %- Z-sub-coordinates
             C = textscan(fid,'%s',1,'delimiter','\t'); % Key word 'Z_POS_SUB';
             C = textscan(fid,'%s',1,'delimiter','\t');
             str = C{1};
             i=1;
 
             while not(strcmp(str,'END'))
                 reg_prop(ind_reg).pos(i).z_sub = str2double(str);
 
                 %- Read in next one
                 i   = i+1;
                 C   = textscan(fid,'%s',1,'delimiter','\t');
                 str = C{1};
             end
             
             C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change
             
             %==== NEW VERSION WITH DETECTION INTENSITY
             status_read_next_line = 1;
             C   = textscan(fid,'%s',1,'delimiter','\t');     % Potential KEY workd detect_int
             
             % Check if next keyword is detect_int
             if strcmp(C{1},'detect_int')
                 C = textscan(fid,'%s',1,'delimiter','\t');
                 str = C{1};
                 i=1;

                 while not(strcmp(str,'END'))
                     reg_prop(ind_reg).pos(i).detect_int = str2double(str);

                     %- Read in next one
                     i   = i+1;
                     C   = textscan(fid,'%s',1,'delimiter','\t');
                     str = C{1};
                 end

                 C = textscan(fid,'%s',1,'delimiter','\n');   % Read-in line-change 
                 C = textscan(fid,'%s',1,'delimiter','\t');   % Additional read necessary to match the one from above in case new file was read
             end
         end
%          
%          %- Read-in key word
%          if ~status_read_next_line
%             C = textscan(fid,'%s',1,'delimiter','\t');     % KEY WORD REGION    
%          end

        ind_reg = ind_reg+1;
    end  
    flag_file = 1;
    
catch    
    flag_file = 0;      
    
end
fclose ('all');

