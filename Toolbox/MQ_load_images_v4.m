function [handles, status_image, image_struct, img_MIP_xy] = MQ_load_images_v4(handles,image_specs)

   
%- Output
status_image = 0;
image_struct = [];
img_MIP_xy = [];

%- Get parameters
img_type        = image_specs.type;
N_Z             = image_specs.N_Z;
image_name.file = image_specs.name;
image_name.path = image_specs.path;

    
%- Check if file was empty
if image_name.file == 0
    return
end


%- Check if file-name is already specified
if isempty(image_name.file)
    status_file_name = 0;    
else
    status_file_name = 1;
    file_name_image = image_name.file;
    path_name_image = image_name.path;
end


%- Load file depending on type
switch img_type

    
      case '3D: one stack (xyzt)'      
       
       [image_struct, img_MIP_xy, path_name, file_name] = img_read_4D_stack_v3(image_name); 

        if not(isempty(image_struct))

            %-Assign dimensionality of image
            handles.img_type = '3D';
            handles.img_type_orig = '3D';

            %- Save data
            handles.path_name_image = path_name;
            handles.file_name_image = file_name;            

            status_image = 1;
        else
            status_image = 0;
        end  
    
    
    
    %- 2D MIP
    case '2D: stack with time-series of XY-MIP'    
        
        
        %- Ask for file-name
        if not(status_file_name)
            [file_name_image,path_name_image] = uigetfile({'*.tif';'*.stk';'*.dv';'*.TIF'},'Select file');
        end
               
        if file_name_image ~= 0

            %- Load image and plot
            par_load.flag.bfopen = 1;
            par_load.flag.output = 0;
            image_struct    = load_stack_data_v7(fullfile(path_name_image,file_name_image),par_load);
            img_MIP_xy      = image_struct.data;

            %- Assign dimensionality of image
            handles.img_type      = '2D';
            handles.img_type_orig = '2D';
            handles.file_name_image = file_name_image;
            handles.path_name_image = path_name_image;

            status_image = 1;
        else
            status_image = 0;
        end 

    case '3D: time-series of z-stacks'

        [image_struct img_MIP_xy path_name name_base] = read_stack_time_z_separate_v2(); 

        if not(isempty(image_struct))

            %-Assign dimensionality of image
            handles.img_type = '3D';
            handles.img_type_orig = '3D';

            handles.path_name_image = path_name;
            handles.file_name_image = name_base;

            status_image = 1;
        else
            status_image = 0;
        end 

    case '3D: time-series of individual images'

           [image_struct img_MIP_xy path_name name_base] = img_read_4D_individual_v1(); 

            if not(isempty(image_struct))

                %-Assign dimensionality of image
                handles.img_type = '3D';
                handles.img_type_orig = '3D';

                %- Save data
                handles.path_name_image = path_name;
                handles.file_name_image = name_base;  
                status_image = 1;
            else
                status_image = 0;
            end

    case '3D: one z-stack'
        
        if not(status_file_name)
            [file_name_image,path_name_image] = uigetfile({'*.tif';'*.stk';'*.dv';'*.TIF'},'Select file');
        end
        
        if file_name_image ~= 0

            %- Load image and plot
            par_load.flag.bfopen = 1;
            image_struct    = load_stack_data_v7(fullfile(path_name_image,file_name_image),par_load);
            img_MIP_xy      = max(image_struct.data,[],3);

            %-Assign dimensionality of image
            handles.img_type      = '3D';
            handles.img_type_orig = '3D';

            %- Save data
            handles.path_name_image = path_name_image;
            handles.file_name_image = file_name_image;

            status_image = 1;
        else
            status_image = 0;
        end  
end