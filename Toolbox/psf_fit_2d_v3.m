function [result, xdata] = psf_fit_2d_v3(img,xdata,options_fit,flag_struct)
%

% par_start is a structure containing starting guesses for the fit. If a
% field is specified this value will be used. If the fit_mode is set in
% such a way that this value is not a free fitting parameter it will be
% used instead as an input parameter for the function call. Allowed fields
% are:
%   sigmax 
%   sigmay
%   bgd
%   amp
%   centerx
%   centery

pixel_size  = options_fit.pixel_size;
PSF_theo    = options_fit.PSF_theo;
par_start   = options_fit.par_start;
fit_mode    = options_fit.fit_mode;
bound       = options_fit.bound;

    
%% Dimension of image
[dim.Y dim.X] = size(img);
N_pix         = dim.X*dim.Y;

%- Generate vectors describing the image    
if isempty(xdata)
    [Xs,Ys] = meshgrid(0:dim.X-1,0:dim.Y-1);  % 19/7/2012 order reversed!!!! Was wrong for unequal images
    X1      = reshape(Xs,1,N_pix);
    Y1      = reshape(Ys,1,N_pix);

    xdata(1,:) = double(X1)*pixel_size.xy;
    xdata(2,:) = double(Y1)*pixel_size.xy;
end

% Reformating image to fit data-format of axis vectors
ydata          = double(reshape(img,1,N_pix));


%% Analyze image to get initial conditions

%=== Determine center of mass - starting point for center
center_mass  = ait_centroid3d_v3(img,xdata);


%- Avoid problems when entire image is 0 --> func returns NaN
if isnan(center_mass(1));  center_mass(1) = 0.5 * (max(xdata(1,:)) - min(xdata(1,:))); end
if isnan(center_mass(2));  center_mass(2) = 0.5 * (max(xdata(2,:)) - min(xdata(2,:))); end  
    
if not(isfield(par_start,'centerx'));  par_start.centerx = center_mass(1); end
if not(isfield(par_start,'centery'));  par_start.centery = center_mass(2); end


%=== Min and Max of the image: quality check 
%  Starting point for amplitude and background
img_max   = max(img(:));
img_min   = (min(img(:)))*(min(img(:))>0) + 1* (min(img(:))<=0);

if not(isfield(par_start,'amp'));  par_start.amp = img_max-img_min; end
if not(isfield(par_start,'bgd'));  par_start.bgd = img_min;         end

%- Starting points for sigma
if isfield(par_start,'sigmax')  
    if isempty(par_start.sigmax)
        par_start.sigmax = PSF_theo.xy_nm; 
    end
else
    par_start.sigmax = PSF_theo.xy_nm; 
end

if isfield(par_start,'sigmay')  
    if isempty(par_start.sigmay)
        par_start.sigmay = PSF_theo.xy_nm; 
    end
else
    par_start.sigmay = PSF_theo.xy_nm; 
end


%== Options for fitting routine
options = optimset('Jacobian','off','Display','off','MaxIter',10000);

%% Boundary conditions
if isempty(bound)
    lb = [];
    ub = [];
else
    lb = bound.lb;
    ub = bound.ub;
end


%% Sigma as a fixed fitting parameter (to a user specified value)
if strcmp(fit_mode,'sigma_fixed')
   
    %- Initial conditions
    x_init = [  par_start.centerx, par_start.centery, ...
                par_start.amp,     par_start.bgd];

    %- Model parameters        
    par_mod{1} = 3;     % Flag to indicate sigma's are fixed
    par_mod{2} = xdata;         
    par_mod{3} = pixel_size;
    par_mod{4} = par_start.sigmax;
    par_mod{5} = par_start.sigmay;
         
    %-  Least Squares Curve Fitting
    [par_fit,resnorm,residual,exitflag,output] = lsqcurvefit(@fun_Gaussian_2D_v2,x_init, ...
        par_mod,ydata,lb,ub,options);

    %- Calculate best fit
    img_fit_lin = fun_Gaussian_2D_v2(par_fit,par_mod);
    img_fit     = reshape(img_fit_lin, size(img));
    
    %- Resize residuals
    if( numel(img) == numel(residual))
        im_residual = reshape(residual, size(img));
    else
        im_residual = ones(size(img));
    end

    %- Save results  
    result.sigmaX     = par_start.sigmax;          % Sigma X
    result.sigmaY     = par_start.sigmay;          % Sigma Y
    result.muX        = par_fit(1);                % Center X
    result.muY        = par_fit(2);                % Center Y
    result.amp        = par_fit(3);                % Amplitude
    result.bgd        = par_fit(4);                % Background  

    
%= Sigma as a free fitting paramter in xy      
elseif strcmp(fit_mode,'sigma_free_xy')

    %- Initial conditions
     x_init = [ par_start.sigmax,  ...
                par_start.centerx, par_start.centery, ...
                par_start.amp,     par_start.bgd];

    %- Model parameters
    par_mod{1} = 2;     % Flag to indicate that sigma_x = sigma_y
    par_mod{2} = xdata;         
    par_mod{3} = pixel_size;
   
    %-  Least Squares Curve Fitting
    try
        [par_fit,resnorm,residual,exitflag,output] = lsqcurvefit(@fun_Gaussian_2D_v2,x_init, ...
            par_mod,ydata,lb,ub,options);
    
    catch exitflag
        disp('Problems with fitting')
        disp(output)
        
    end

    %- Calculate best fit
    img_fit_lin = fun_Gaussian_2D_v2(par_fit,par_mod);
    img_fit     = reshape(img_fit_lin, size(img));
    
    %- Resize residuals
    if( numel(img) == numel(residual))
        im_residual = reshape(residual, size(img));
    else
        im_residual = ones(size(img));
    end

    %- Save results  
    result.sigmaX      = par_fit(1);           % Sigma X
    result.sigmaY      = par_fit(1);           % Sigma Y
    result.muX         = par_fit(2);           % Center X
    result.muY         = par_fit(3);           % Center Y
    result.amp         = par_fit(4);           % Amplitude
    result.bgd         = par_fit(5);           % Background
     
end


%= Save results
result.resnorm     = resnorm;
result.exitflag    = exitflag;
result.centroidX   = center_mass(1); % Center of mass X: starting point for fit of center
result.centroidY   = center_mass(2); % Center of mass Y: starting point for fit of center
result.output      = output;
result.maxI        = img_max;
result.im_residual = im_residual;
result.img_fit     = img_fit;


if flag_struct.output
    
    [dim_sub.Y dim_sub.X] = size(img);
    
     %- Min and max of image
    img_min = min(img(:));
    img_max = max(img(:));
    
    res_min = min(im_residual(:));
    res_max = max(im_residual(:));

    figure        
    %- Image
    subplot(1,3,1)
    imshow(img,[img_min img_max]) %,'XData',[0 (dim_sub.X-1)]*pixel_size.xy,'YData',[0 (dim_sub.Y-1)]*pixel_size.xy)
    title('Image - XY')    
    colorbar
    hold on
    %plot(result.muX, result.muY,'og')
    plot(result.muX/pixel_size.xy, result.muY/pixel_size.xy,'og')
    hold off

    %- Fit
    subplot(1,3,2)
    imshow(img_fit,[img_min img_max]) %,'XData',[0 (dim_sub.X-1)]*pixel_size.xy,'YData',[0 (dim_sub.Y-1)]*pixel_size.xy)
    title('FIT - XY')
    hold on
    %plot(result.muX, result.muY,'og')
    plot(result.muX/pixel_size.xy, result.muY/pixel_size.xy,'og')
    hold off
         
    %- Residuals
    subplot(1,3,3)
    imshow(im_residual,[res_min res_max],'XData',[0 (dim_sub.X-1)]*pixel_size.xy,'YData',[0 (dim_sub.Y-1)]*pixel_size.xy)
    title('RESID - XY')
    colorbar

end
