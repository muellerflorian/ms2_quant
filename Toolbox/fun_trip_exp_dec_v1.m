function [I,  data_all] = fun_trip_exp_dec_v1(parFit,parMod)
% Single exponential decrease I=A+B*exp(+t/C)+D*exp(+t/E)+F*exp(+t/G);
%
% functionFlag = 1 ... parFit = [B,C,D,E]

%% Assign parameters
functionFlag = parMod{1};
t            = parMod{2};

if functionFlag ==1
    A  = parFit(1);
    B  = parFit(2);
    C  = parFit(3);
    D  = parFit(4);
    E  = parFit(5);
    F  = parFit(6);
    G  = parFit(7);
    
elseif functionFlag ==2
    A  = parMod{3};
    B  = parFit(1);
    C  = parFit(2);
    D  = parFit(3);
    E  = parFit(4);
    F  = parFit(5);
    G  = parFit(6); 
    
elseif functionFlag == 3
    A  = parMod{3};
    
    B  = parFit(1);
    C  = parFit(2);
    D  = parFit(3);
    E  = parFit(4);
    
    F  = parMod{4};
    G  = parMod{5}; 
    
elseif functionFlag == 4
    A  = parMod{3};
    
    B  = parFit(1);
    C  = parFit(2);
    
    D  = parMod{6};
    E  = parMod{7};    
    F  = parMod{4};
    G  = parMod{5};    
    
end

%disp([num2str(B),' ', num2str(D),' ',num2str(C),' ', num2str(E),' '])


%% calculate curve 

if B == 0
    T1 = 0;
else
    T1 = B .* exp( -t./C);
end

if D == 0
    T2 = 0;
else
    T2 = D .* exp( -t./E);
end

if F == 0
    T3 = 0;
else
    T3 = F .* exp( -t./G);
end

I = A + T1 + T2 + T3;

ind_nan = find(isnan(I));
I(ind_nan) = 0;

data_all(:,1) = T1;
data_all(:,2) = T2;
data_all(:,3) = T3;



