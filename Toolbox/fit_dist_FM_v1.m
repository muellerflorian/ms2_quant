function [par_fit,data_fit,err,cts_data,bin_data,fit_all] = fit_dist_FM_v1(data,n_bins,exp_dist,norm)

%% Fit data to distributions

% ==== INPUT PARAMETERS
% n_bins  .... Number of bins in the histogram
% exp_num ...  Number of exponetials (currently exp1 or exp2 is supported)
% norm    ...  Which norm should be used to judge the quality
%                chi2 .... chi-squared norm

%- Calculate histogram
switch norm
    case 'L2'
        [cts_data, bin_data] = hist(data,n_bins);
        cts_max              = max(cts_data);
        bins_middle          = (bin_data(end) - bin_data(1)) / 2;

    case 'chi2'
        [h,p,stats] = chi2gof(data,'Nbins',n_bins);
        cts_data       = histc(data,stats.edges);
        bin_data       = stats.edges;
        
        cts_max              = max(cts_data);
        bins_middle          = (bin_data(end) - bin_data(1)) / 2;
        
end


%- Fit data with different norm

options = optimset('Display','final','MaxFunEvals', 400, 'MaxIter', 200,'FunValCheck', 'on','UseParallel','always');
switch norm
    
    
    %============ Chi-squared
    case 'chi2'
        

        switch exp_dist
            
            case 'exp1'
                
                par_mod    = {4,bin_data,0,0,0,0,0};
                par_start = [cts_max bins_middle];
                
                fitnessfcn = @(parFit)calc_chi2(parFit,cts_data,bin_data,exp_dist,par_mod); 
                par_fit     = fminsearch(fitnessfcn,par_start,options);                  
                [data_fit, fit_all]   = fun_trip_exp_dec_v1(par_fit,par_mod);
                err        = fitnessfcn(par_fit);
      
                
          case 'exp2'
              
                par_mod    = {3,bin_data,0,0,0};
                par_start = [cts_max/2 bins_middle/10 cts_max/2 bins_middle/2];
                
                fitnessfcn = @(parFit)calc_chi2(parFit,cts_data,bin_data,exp_dist,par_mod); 
                par_fit     = fminsearch(fitnessfcn,par_start,options); 
                
                
                [data_fit, fit_all]   =   fun_trip_exp_dec_v1(par_fit,par_mod);
                err        = fitnessfcn(par_fit);          
     
        end
        
        
    %============ L2-norm    
    case 'L2'
        

        switch exp_dist
            
            case 'exp1'
                par_mod    = {4,bin_data,0,0,0,0,0};
                par_start = [cts_max bins_middle];
                
                lb        = [0       0     ];
                ub        = [3*cts_max 100000 ];
                
                par_fit  = lsqcurvefit(@fun_trip_exp_dec_v1, par_start, par_mod, cts_data ,lb,ub);
                [data_fit, fit_all]   =  fun_trip_exp_dec_v1(par_fit,par_mod);
                err      = sum( (data_fit-cts_data) .^2);
                
                
            case 'exp2'
                par_mod    = {3,bin_data,0,0,0};
                par_start = [cts_max/2 bins_middle/10 cts_max/2 bins_middle/2];
                
                lb        = [0       0     0       0      ];
                ub        = [3*cts_max 100000 3*cts_max 100000];
                
                par_fit = lsqcurvefit(@fun_trip_exp_dec_v1, par_start, par_mod, cts_data ,lb,ub);
                [data_fit, fit_all]   =  fun_trip_exp_dec_v1(par_fit,par_mod);
                err      = sum( (data_fit-cts_data) .^2);
                
        end
end
        

end


function chi2 = calc_chi2(parFit,cts_data,bin,dist,par_mod)

%- Calculate specified distribution
switch dist
    
    case 'exp1'
        cts_fit    = round(fun_trip_exp_dec_v1(parFit,par_mod));
        
    case 'exp2'
        cts_fit    = round(fun_trip_exp_dec_v1(parFit,par_mod));

end

%- Consider only bins wiht at least a count of 3
ind_sum = cts_data > 0 & cts_fit > 0;

%- Calc chi2
chi2_all = ( (cts_data - cts_fit).^2 ./ cts_fit);
chi2      =  sum(chi2_all(ind_sum));


end




% 
% 
% %% Fit data to distributions
% 
% % ==== INPUT PARAMETERS
% % n_bins  .... Number of bins in the histogram
% % exp_num ...  Number of exponetials (currently exp1 or exp2 is supported)
% % norm    ...  Which norm should be used to judge the quality
% %                chi2 .... chi-squared norm
% 
% 
% 
% 
% %- Calculate histogram
% [cts_data, bin_data] = histc(data,n_bins);
% cts_max     = max(cts_data);
% bins_middle = (bin_data(end) - bin_data(1)) / 2;
% 
% %- Fit data with different norm
% 
% options = optimset('Display','final','MaxFunEvals', 400, 'MaxIter', 200,'FunValCheck', 'on','UseParallel','always');
% switch norm
%     
%     case 'chi2'
%         
% 
%         switch exp_dist
%             
%             case 'exp1'
%                 
%                 par_start = [cts_max bins_middle];
%                 
%                 fitnessfcn = @(parFit)calc_chi2_v1(parFit,cts_data,bin_data,exp_dist); % Define Location of file to compute obective function.
%                 par_fit     = fminsearch(fitnessfcn,par_start,options);  % call code to fit parameters to match data.
%                 
%                 par_mod    = {4,bin_data,0,0,0,0,0};
%                 data_fit   = fun_trip_exp_dec_v1(par_fit,par_mod);
%                 err        = fitnessfcn(par_fit);      
%                 
%           case 'exp2'
%                 
%                 par_start = [cts_max/2 bins_middle/10 cts_max/2 bins_middle/2];
%                 
%                 fitnessfcn = @(parFit)calc_chi2_v1(parFit,cts_data,bin_data,exp_dist); % Define Location of file to compute obective function.
%                 par_fit     = fminsearch(fitnessfcn,par_start,options);  % call code to fit parameters to match data.
%                 
%                 par_mod    = {3,bin_data,0,0,0};
%                 data_fit   = fun_trip_exp_dec_v1(par_fit,par_mod);
%                 err        = fitnessfcn(par_fit);          
%      
%         end
% end
% 
