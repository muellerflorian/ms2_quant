function [data_MS2, status_file, header, settings] = MQ_results_load_v1(file_name,parameters)
% Function to read in outline definition for cells

 
status_file = 1;

if nargin < 2
    flags.data_type = 'time_course';
else
    flags = parameters.flags;
end


if nargin < 1 
        [file_name_load,path_name_load] = uigetfile({'*.txt'},'Select MS2 quantification file(s).','MultiSelect', 'on');

    if file_name_load ~= 0
        file_name = fullfile(path_name_load,file_name_load);
    else
        status_file = 0;
    end
end 
    

%- Prepare structure
data_MS2 = [];
header    = {};


%- Open file

if status_file

    fid  =  fopen(file_name,'r');

    try 
        %=== Read in header of file

        %- Read-in each line until header 
        tline = fgetl(fid);

        %- Read until header with Key word Q_Integrate is encountered
        while (isempty(strfind(tline,'Q_Integrate')))
            tline = fgetl(fid);
            
            
            %- Is there and equal sign? Extract strings before and after
            k = strfind(tline, sprintf('\t'));    
            str_tag = tline(1:k-1);
            str_val = tline(k+1:end);


            %- Compare identifier before the equal sign to known identifier
            switch str_tag

                %- Paramters

                case 'IMAGING-BACKGROUND'
                    settings.BGD_img = str2double(str_val);
                    
                    
                %-Limits for fit: assigned
                case 'LIMIT-FIT-SIGMA-XY-MIN'            
                    settings.fit_limits.sigma_xy_min= str2double(str_val);

                case 'LIMIT-FIT-SIGMA-XY-MAX'
                    settings.fit_limits.sigma_xy_max = str2double(str_val);

                case 'LIMIT-FIT-SIGMA-Z-MIN'            
                    settings.fit_limits.sigma_z_min= str2double(str_val);

                case 'LIMIT-FIT-SIGMA-Z-MAX'
                    settings.fit_limits.sigma_z_max = str2double(str_val);           

                case 'LIMIT-FIT-BGD-MIN'            
                    settings.fit_limits.bgd_min= str2double(str_val);

                case 'LIMIT-FIT-BGD-MAX'
                    settings.fit_limits.bgd_max = str2double(str_val);      
 
            end            
        end

        %- Analyze header
        header_line    = tline;
        ind_log_tab    = (header_line == sprintf('\t'));
        num_cols    = 1 + sum(ind_log_tab);

        %- Get all headers
        ind_tab_header = find(ind_log_tab);
        ind_tab        = [0,ind_tab_header,length(header_line)+1];

        for i_tab =1:length(ind_tab)-1
            ind_start = ind_tab(i_tab)+1;
            ind_end   = ind_tab(i_tab+1)-1;

            header{i_tab} = header_line(ind_start:ind_end); 
        end


        switch flags.data_type

            case 'calibration'

                %- Read in data
                str_read_in = ['%s%f',repmat('%f', 1, num_cols-2)];

                %- Read in data
                C = textscan(fid, str_read_in,'HeaderLines',0,'delimiter','\t','CollectOutput',1);

                %- Get MS2-data
                data_MS2 = C{2};
                status_file = 1;

            case 'time_course'

                %- Read in data
                str_read_in = ['%f',repmat('%f', 1, num_cols-1)];

                %- Read in data
                C = textscan(fid, str_read_in,'HeaderLines',0,'delimiter','\t','CollectOutput',1);

                %- Get MS2-data
                data_MS2 = C{1};    
        end


    catch

        status_file = 0;      

    end
    fclose ('all');
end
