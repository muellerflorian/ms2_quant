function [img_stack, img_MIP_xy, path_name, file_name] = img_read_4D_stack_v3(image_name)

% Function to read time-series of z-stacks. Files have to be save in the
% same folder and differ in number which is at the end of the file-name. 

img_stack  = [];
img_MIP_xy = [];
path_name  = [];
file_name  = [];


%% Check if file-name is already specified
if isempty(image_name.file)
    [file_name,path_name] = uigetfile({'*.tif';'*.TIF';'*.STK';'*.stk';'*.dv';'*.DV'},'Select first file of stack','MultiSelect','off');
else
    file_name = image_name.file;
    path_name = image_name.path;
end


%% [1.a] Specify file names
if file_name ~= 0

        r = bfGetReader(fullfile(path_name,file_name));

        %=== Get the total number of images;
        N_img = r.getImageCount();
        fprintf('Total number of image: %g\n',N_img)

        %== Get size in Y and X
        N_Y = r.getSizeY;
        N_X = r.getSizeX;

        %=== Get the number of Z slices;
        N_Z = r.getSizeZ();
        fprintf('Number of z-slices: %g\n',N_Z)

        %- Check with user if N_Z = 1
        if N_Z == 1
            prompt = {'# z-slices:'};
            dlg_title = 'Specify number of z-slices';
            num_lines = 1; def = {'6'};
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            N_Z = str2double(answer{1});
        end

        N_T  = floor(N_img/N_Z);     
            
        
        %== Loop over time-points
        fprintf('\n= Processing: %d time-points (100 time-points per line) \n',N_T)
        
        for iT = 1: N_T
    
            %- Plot points as indicator where we are 
            if mod(iT, 100) == 1; fprintf('\n%5.0f ',iT); end; fprintf('.')
    
            %- Get start and end index of z-slice
            ind_start = (iT-1)*N_Z+1;
            ind_end   = iT*N_Z;
    
            %- Open image: OPB
            img_loop = zeros(N_Y,N_X,N_Z);
            ind_loop = 1;
            for ind_load = ind_start:ind_end
                img_loop(:,:,ind_loop) = bfGetPlane(r, ind_load);
                ind_loop = ind_loop +1;
            end
        
            img_loop = uint16(img_loop);


           %- Analyze image
           max_proj_xy = max(img_loop,[],3);

           %- Save results
           img_stack(iT).ind    = iT;
           img_stack(iT).data   = img_loop;  
           img_stack(iT).MIP_xy = max_proj_xy;
           img_MIP_xy(:,:,iT)   = max_proj_xy;
        end
        fprintf('\n')
            
    end
end

