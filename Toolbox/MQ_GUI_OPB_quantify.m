function handles = MQ_GUI_OPB_quantify(handles)


%== [1] Extract photobleaching parameters

%- For fitting    
options_fit.pixel_size =  handles.par_microscope.pixel_size;
options_fit.par_start  =  [];

options_fit.fit_limits = handles.fit_limits;
options_fit.PSF_theo   = handles.PSF_theo;

%- Other parameters
par_fit.options_fit        = options_fit;
par_fit.par_microscope     = handles.par_microscope;
par_fit.options_quant      = handles.options_quant;
par_fit.flag_struct.output = 0;
par_fit.flag_struct.fit_TS = 0;

switch handles.img_type 
    
    case {'3D','3d'}

        switch par_fit.options_quant.flag_quant_dim

            case {'3D','3d'}
                par_fit.options_fit.fit_mode   =  'sigma_free_xz'; 
                handles.reg_prop = MQ_quant_3D_v11(handles.reg_prop,par_fit);
         end
        
     otherwise
        warndlg('Invalide selection for quantification: ... will exit! ',mfilename)
end

%== [2] Get intensity value of OPB and BGD region
reg_prop = handles.reg_prop;
int      = reg_prop(handles.flag_OPB).int_avg; 


%- Comparison of measured intensity vs measured stdev
if 0
    int_std  = reg_prop(handles.flag_OPB).int_std; 
    figure, hold on, plot(int_std/max(int_std),'b'), plot(int/max(int),'r')
end

%- BGD value
if handles.flag_BGD
	bgd_imaging = reg_prop(handles.flag_BGD).int_avg;
    
elseif not(isempty(handles.BGD_img))
    bgd = handles.BGD_img;
    bgd_imaging = bgd*ones(size(int));    

else
    bgd_imaging = zeros(size(int));
end

handles.bgd_imaging = bgd_imaging;

%== [3] Quantify OPB
par_quant.dT             = handles.par_microscope.dT;
par_quant.flags.output   = 1;
par_quant.flags.save   = 0;
par_quant.flags.fit_mode = handles.opb_fit_mode;

[handles.OPB.par_fit, handles.OPB.time, handles.OPB.curves] = MQ_OPB_quant_v3(int,bgd_imaging,par_quant);