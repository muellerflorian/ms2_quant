function [reg_prop,h_fig] = MQ_detect_v10(reg_prop,parameters)


global MQ_img_stack 

h_fig = [];

%% Parameters
flags          = parameters.flags;
int_min        = parameters.int_min;
img_type       = parameters.img_type; 


%- Option to turn off visability of plot - helps with error messages when
%  processing many movies in a batch
if isfield(flags,'flag_save')
    flag_save = flags.flag_save;
else
    flag_save = 0;
end

% - Get parameters to save
if flag_save
    name_load        = parameters.name_load;
    name_suffix      = parameters.name_suffix;
    path_save_detect = parameters.path_save_detect;    
end


%% Analyze MQ_img_stack and regions
N_reg   = length(reg_prop);

switch img_type

    %- 3D
    case {'3D','3d'} 
        N_slice             = length(MQ_img_stack);
        [dim.Y dim.X dim.Z] = size(MQ_img_stack(1).data);
    
    %- 2D
    case {'2D','2d'} 
        [dim.Y dim.X N_slice] = size(MQ_img_stack.data);
        dim.Z   = 1;
end


%= Set image outside of nucleus to zero
[X_grid,Y_grid] = meshgrid(1:dim.X,1:dim.Y);


%% Loop over all images  

%- Loop over all regions
for i_reg = 1:N_reg

    if not(reg_prop(i_reg).flag_BGD) && not(reg_prop(i_reg).flag_OPB)
    
        %- Extract borders of region
        x_loop = reg_prop(i_reg).x;
        y_loop = reg_prop(i_reg).y;

        x_min = min(x_loop);
        x_max = max(x_loop);

        y_min = min(y_loop);
        y_max = max(y_loop);

        if x_min < 1;     x_min = 1;     end
        if x_max > dim.X; x_max = dim.X; end

        if y_min < 1;     y_min = 1;     end
        if y_max > dim.Y; y_max = dim.Y; end

        %- Mask for image
        mask_reg_2D = inpolygon(X_grid,Y_grid,x_loop,y_loop);
        mask_reg_3D = repmat(mask_reg_2D,[1,1,dim.Z]); 

        %- Set positions to zero
        pos_first.x = [];
        pos_first.y = [];
        pos_first.z = [];

        pos_assign.x = [];
        pos_assign.y = [];
        pos_assign.z = [];

        ind_no_pos  = [];

        %== Loop over slices
        for i_slice = 1:N_slice	

            %- Raw or filtered image
            switch flags.image

                %- Raw image
                case 'raw'

                    %- 2D or 3D image                
                    switch img_type
                        case {'3D','3d'} 
                            img_loop = MQ_img_stack(i_slice).data;
                        case {'2D','2d'} 
                            img_loop = MQ_img_stack.data(:,:,i_slice);
                    end

                %- Filtered image
                case 'filt'

                    %- 2D or 3D image                
                    switch img_type
                        case {'3D','3d'} 
                            img_loop = MQ_img_stack(i_slice).filt;
                        case {'2D','2d'} 
                            img_loop = MQ_img_stack.filt(:,:,i_slice);
                    end
            end

            %- Set pixels outside of region to zero
            img_loop(not(mask_reg_3D)) = 0;


            %- Different detection modes
            switch flags.detect_mode

                case 'brightest'                 
                    img_loop_reg                     = img_loop(y_min:y_max,x_min:x_max,:);
                    [reg_max.value  reg_max.ind_lin] = max(img_loop_reg(:));                
                    detect_int(i_slice)              = reg_max.value;

                    %- Value is larger than detection limit
                    if double(reg_max.value) > double(int_min)

                        [pos(i_slice).y_sub,pos(i_slice).x_sub,pos(i_slice).z_sub] = ind2sub(size(img_loop_reg),reg_max.ind_lin(1));

                        pos(i_slice).y          = pos(i_slice).y_sub + y_min-1;
                        pos(i_slice).x          = pos(i_slice).x_sub + x_min-1;
                        pos(i_slice).z          = pos(i_slice).z_sub;


                        pos_assign.x = pos(i_slice).x;
                        pos_assign.y = pos(i_slice).y;
                        pos_assign.z = pos(i_slice).z;

                        pos_assign.x_sub = pos(i_slice).x_sub;
                        pos_assign.y_sub = pos(i_slice).y_sub;
                        pos_assign.z_sub = pos(i_slice).z_sub;


                        if isempty(pos_first.x)
                            pos_first.x = pos(i_slice).x;
                            pos_first.y = pos(i_slice).y;
                            pos_first.z = pos(i_slice).z;

                            pos_first.x_sub = pos(i_slice).x_sub;
                            pos_first.y_sub = pos(i_slice).y_sub;
                            pos_first.z_sub = pos(i_slice).z_sub;                     

                        end

                    %- Value is smaller than detection limit
                    else   
                        if not(isempty(pos_assign.x))
                            pos(i_slice).x = pos_assign.x;
                            pos(i_slice).y = pos_assign.y;
                            pos(i_slice).z = pos_assign.z;

                            pos(i_slice).x_sub = pos_assign.x_sub;
                            pos(i_slice).y_sub = pos_assign.y_sub;
                            pos(i_slice).z_sub = pos_assign.z_sub;

                        else
                            ind_no_pos = [ind_no_pos;i_slice];                        
                        end
                    end
            end   

        end 

        %- Check if there any slices with no position
        N_no_pos = length(ind_no_pos);

        if N_no_pos

           if isempty(pos_first.x)

               pos_first.x = ceil((x_max + x_min) / 2);
               pos_first.y = ceil((y_max + y_min) / 2);
               pos_first.z = ceil(dim.Z/2);

               pos_first.x_sub = pos_first.x - x_min;
               pos_first.y_sub = pos_first.y - y_min;
               pos_first.z_sub = ceil(dim.Z/2);
           end       

           for i = 1: N_no_pos;
                i_slice =  ind_no_pos(i);
                pos(i_slice).x      = pos_first.x;
                pos(i_slice).y      = pos_first.y;
                pos(i_slice).z      = pos_first.z;

                pos(i_slice).x_sub  = pos_first.x_sub;
                pos(i_slice).y_sub  = pos_first.y_sub;
                pos(i_slice).z_sub  = pos_first.z_sub;

           end            
        end

        %- Assign values
        reg_prop(i_reg).pos        = pos; 
        reg_prop(i_reg).detect_int = detect_int;  

        %-- PLOT
        if flags.output

            if not(reg_prop(i_reg).flag_BGD) && not(reg_prop(i_reg).flag_OPB)

                %== Parameters to plot
                x_pix = ( [reg_prop(i_reg).pos.x]  ) ;
                y_pix = ( [reg_prop(i_reg).pos.y]  ) ;

                %== Figure: color coded XY position
                h_fig = figure; set(h_fig, 'Color', 'w');

                if flag_save
                    set(h_fig, 'visible', 'off')
                end
                
                ax1 = subplot(1,2,1);
                plotclr_v1(x_pix,y_pix,(1:N_slice),'o');
                title([reg_prop(i_reg).label, ': XY STARTING positions'])
                xlabel('Y')
                ylabel('X')
                zlabel('Time')
                hold on
                plot([reg_prop(i_reg).x reg_prop(i_reg).x(1)],[ reg_prop(i_reg).y, reg_prop(i_reg).y(1)],'b','Linewidth', 2)
                hold off
                set(h_fig, 'Color', 'w')
                axis square
                
                ax2 = subplot(1,2,2);
                plotclr_v1(x_pix,y_pix,(1:N_slice),'o');
                title([reg_prop(i_reg).label, ': XZ STARTING positions'])
                xlabel('Y')
                ylabel('X')
                zlabel('Time')
                hold on
                plot([reg_prop(i_reg).x reg_prop(i_reg).x(1)],[ reg_prop(i_reg).y, reg_prop(i_reg).y(1)],'b','Linewidth', 2)
                hold off
                view(ax2,90,0)
                
                
                if flag_save	
                    name_save_png = fullfile(path_save_detect,[name_load,'_',reg_prop(i_reg).label,'_',name_suffix]);    
                    saveas(h_fig,name_save_png,'png');
                    close(h_fig)    
                end            
                
            end
        end

    end
end

