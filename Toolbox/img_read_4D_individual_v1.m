function [img_stack img_MIP_xy path_name name_base] = img_read_4D_individual_v1()

% Function to read time-series of z-stacks. Files have to be save in the
% same folder and differ in number which is at the end of the file-name. 

img_stack  = [];
img_MIP_xy = [];
path_name  = [];
name_base  = [];

par_load.flag.bfopen = 1;

%% [1.a] Specify file names
[file_name_1st,path_name] = uigetfile({'*.tif';'*.TIF';'*.STK';'*.stk'},'Select first file of stack','MultiSelect','off');


if file_name_1st ~= 0

    [pathstr, name, ext_img]  = fileparts(fullfile(path_name,file_name_1st));

    name_base = name;
    %== [1.b] Define image sequence

    %- Get number of files in folder
    file_list       = dir(path_name);
    n_Files_default = 0;
    for i=1:size(file_list,1)

        [dum, dum, ext_loop]  = fileparts(fullfile(path_name, file_list(i).name));   

        %- Consider only files withe the same ending 
        if strcmp(ext_img,ext_loop)
            n_Files_default = n_Files_default +1;
            
            file_names_img{n_Files_default,1} = file_list(i).name;
        end
    end
    
    %- Separator between time-points and z-slice
    sep_t = '_';
    sep_z = '__';
    
    ind_start_t = 0;
    ind_start_z = 0;
    
    %- Find number of leading zeros and base-name  
    ind_underscore_single = strfind(name,sep_t);
    ind_underscore_double = strfind(name,sep_z);
    
    N_lead_zero_t = ind_underscore_single(2) - ind_underscore_single(1)-1;
    N_lead_zero_z = length(name) - ind_underscore_double(1)-1;
    
    name_base_def = name(1:ind_underscore_single(1)-1);

    %- Find first index of time and z-slice
    index_first_t    = sprintf('%0*d',N_lead_zero_t,ind_start_t);
    index_first_z    = sprintf('%0*d',N_lead_zero_z,ind_start_z);
    
    %- Find number of z-slices
    name_first_z_stack = [name_base_def,sep_t,index_first_t,sep_z];
    ind_first_z_stack = cell2mat(strfind(file_names_img,name_first_z_stack));
    N_Z = sum(ind_first_z_stack);
    
    %- Find number of time-points
    N_T = floor(n_Files_default / N_Z);
    
    
    %- ASK USER TO CONFIRMFind base of name
    dlg_title  = ['Define structure of file-name: ' file_name_1st];
    num_lines  = 1;
    prompt     = { 'Base name of image: ', ...
                   'Separator between base-name and time-point: ', ...
                   'Separator between time-point and z-stack: ', ...
                   'Number of time-points:',...
                   'Number of z-stacks:', ...
                   'Padding zeros for time-points (indicate total length of number)', ...
                   'Padding zeros for z-stacks (indicate total length of number)', ...
                   'Start index (time): ', ...
                   'Start index (Z): '};
               
               
    def_values  = {name_base_def, ...
                   sep_t, sep_z, ...
                   num2str(N_T), num2str(N_Z), ...
                   num2str(N_lead_zero_t), num2str(N_lead_zero_z), ...
                   num2str(ind_start_t), num2str(ind_start_z)};
               
    user_values = inputdlg(prompt,dlg_title,num_lines,def_values);

    
  
    if not(isempty(user_values))
        

        name_base_def   = user_values{1};
        sep_t           = user_values{2}; 
        sep_z           = user_values{3};
        N_T             = round(str2double(user_values{4})); 
        N_Z             = round(str2double(user_values{5})); 
        N_lead_zero_t   = round(str2double(user_values{6}));  
        N_lead_zero_z   = round(str2double(user_values{7})); 
        ind_start_t     = round(str2double(user_values{8}));  
        ind_start_z     = round(str2double(user_values{9})); 

        %=== [1.c] Load entire stack and calculate maximum projection
        counter   = 1;

        disp('Reading files. Please wait ....')
        
        %- Loop over files
        N_load   = N_T;
        
        %- Preallocate structure
        img_stack = struct('ind', [], 'name', [], 'data', [],'MIP_xy',[]);
        img_stack(N_load).ind = N_T;
        
        
        %- Keep track of what you read in
        fprintf('Loading stack: (of %d):     1',N_load);
        
        % Loop over time-points
        for ind_T = 1:N_T
            ind_T_loop = ind_T - (1-ind_start_t);
            
           % Show were you are
           fprintf('\b\b\b\b%4i',ind_T);

           %- Loop over z-stack
           img_loop = [];
           for ind_Z = 1:N_Z
               
                ind_Z_loop = ind_Z - (1-ind_start_z);
               
                %- Find first index of time and z-slice
                index_t    = sprintf('%0*d',N_lead_zero_t,ind_T_loop);
                index_z    = sprintf('%0*d',N_lead_zero_z,ind_Z_loop);
    
                %- Find number of z-slices
                name_loop           = [name_base_def,sep_t,index_t,sep_z,index_z,ext_img];
                img_dum = tiffread29(fullfile(path_name,name_loop));
                img_loop(:,:,ind_Z) = img_dum.data;

           end
           
           %- Analyze image
           max_proj_xy = max(img_loop,[],3);

           %- Save results
           img_stack(ind_T).ind    = ind_T;
           img_stack(ind_T).name   = name_loop;
           img_stack(ind_T).data   = img_loop;  
           img_stack(ind_T).MIP_xy = max_proj_xy;

           img_MIP_xy(:,:,ind_T)   = max_proj_xy;

        end
        fprintf('\n');
        
        disp('Reading files. FINSIHED.') 
                
    end
end    

