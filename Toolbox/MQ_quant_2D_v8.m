function [reg_prop,ind_TS] = MQ_quant_2D_v8(reg_prop,parameters)



global MQ_img_stack


ind_TS = [];  % Indicates which regions are TS

%% Parameters
par_microscope = parameters.par_microscope;
flag_struct    = parameters.flag_struct;
options_quant  = parameters.options_quant;
options_fit    = parameters.options_fit;
pixel_size     = par_microscope.pixel_size;

fit_limits     = parameters.options_fit.fit_limits;

fit_pos_restrict = options_quant.flag_fit_pos_restrict;
N_pix_sum        = options_quant.N_pix_sum;


%- Correction for observational photobleaching
if isfield(parameters,'opb_norm')
    opb_norm        = parameters.opb_norm;
    bgd_imaging     = parameters.bgd_imaging;
    flag_opb_apply  = parameters.flag_opb_apply;   
    status_filtered = parameters.status_filtered;
end


%% Extract parameters for quantification
reg_detect       = options_quant.reg_detect;
reg_detect.xy_nm = reg_detect.xy * par_microscope.pixel_size.xy;     

%% Regions for numerical integration
x_int.min = 0 - reg_detect.xy_nm;
x_int.max = 0 + reg_detect.xy_nm;

y_int.min = 0 - reg_detect.xy_nm;
y_int.max = 0 + reg_detect.xy_nm;

%% Analyze MQ_img_stack and regions
[dim.Y dim.X N_slice] = size(MQ_img_stack.data);

N_reg   = length(reg_prop);
N_10perc = floor(N_slice/10);


%% Get vector with time-points
dT    = par_microscope.dT;

tList = (0:N_slice-1)*dT;
tList = tList';


%% Loop over all images  
fprintf('Quantifying frames: (of %d):     1',N_slice);

for i_slice = 1:N_slice	
    
%     if rem(i_slice,N_10perc) == 0
%         disp(['Quantifying frames: ' num2str(round(100*(i_slice / N_slice))),'%'])
%     end
    
    fprintf('\b\b\b\b%4i',i_slice);
  
    %=== Image data for TS quant: quantifications for OPB done on raw img.
    if flag_struct.fit_TS
        
                
        %=== Image: use photobleaching correction
        if flag_opb_apply
            
            if status_filtered   % When image was filtered the OPB correction was already done
            	img_raw = MQ_img_stack.data_opb(:,:,i_slice);
            else
               img     = MQ_img_stack.data(:,:,i_slice) - bgd_imaging(i_slice);
               img_raw = img ./ opb_norm(i_slice);

            end
        else
            img_raw = MQ_img_stack.data(:,:,i_slice);
        end

        
        %=== Image: subtract background if specified    

        if options_quant.flag_subtract_bgd && isfield(MQ_img_stack,'bgd')
            bgd      = MQ_img_stack.bgd(:,:,i_slice);
            dum      = img_raw - bgd;
            img_loop = dum;
        else
            img_loop = img_raw;
            bgd      = zeros(size(img_raw));
            
        end
    end
          
    %=== Loop over all regions
    for i_reg = 1:N_reg        
        
        %== Analyze TxSite and region for background intensity differently
        
        %-- Region is OPB or BGD region - will be measured on raw image
        if reg_prop(i_reg).flag_OPB == 1 || reg_prop(i_reg).flag_BGD == 1
             
            BW_2D = reg_prop(i_reg).BW_2D;
            img_dum    = double(MQ_img_stack.data(:,:,i_slice));
            int_region = img_dum(logical(BW_2D)); 
            reg_prop(i_reg).int_avg(i_slice,1) = mean(int_region(:));    
            
            
            %- Quantify intensity in image aber PB correction
            if flag_struct.fit_TS
                int_region = double(img_raw(logical(BW_2D)));
                reg_prop(i_reg).int_avg_OPB(i_slice,1) = mean(int_region(:));  
            end
                
            
        %-- Region is a TxSite
        else
            ind_TS = [ind_TS;i_reg];
            if flag_struct.fit_TS
                
                %==== Determine sub-region
                pos   = reg_prop(i_reg).pos;

                pos_Y = pos(i_slice).y;
                pos_X = pos(i_slice).x;

                %- Limits of subregion
                x_min = pos_X-reg_detect.xy; if x_min < 1;      x_min = 1;     end
                x_max = pos_X+reg_detect.xy; if x_max > dim.X ; x_max = dim.X; end 

                y_min = pos_Y-reg_detect.xy; if y_min < 1;      y_min = 1;     end
                y_max = pos_Y+reg_detect.xy; if y_max > dim.Y ; y_max = dim.Y; end

                %- Use either raw image of after background subtraction
                img_crop = double(img_loop(y_min:y_max,x_min:x_max)); 

                %==== Fitting with 2D Gaussian
                if flag_struct.fit_TS_Gauss               

                    %- Prepare vectors describing grid
                    [dim_crop.Y dim_crop.X] = size(img_crop);
                    N_pix = dim_crop.X*dim_crop.Y;

                    axis_par.X_pix = (1:dim_crop.X);
                    axis_par.Y_pix = (1:dim_crop.Y);

                    axis_par.X_nm  = axis_par.X_pix*pixel_size.xy;
                    axis_par.Y_nm  = axis_par.Y_pix*pixel_size.xy;

                    [Xs,Ys] = meshgrid(axis_par.Y_pix,axis_par.X_pix);
                    X1         = reshape(Xs,1,N_pix);
                    Y1         = reshape(Ys,1,N_pix);

                    xdata = [];
                    xdata(1,:) = double(X1.*pixel_size.xy);
                    xdata(2,:) = double(Y1.*pixel_size.xy);      

                    %=== Fit with 2D Gaussian      

                    %- Boundaries    
                    switch options_fit.fit_mode
                        case 'sigma_free_xy'
                            
                            options_fit.par_start = [];

                            img_max = max(img_crop(:));
                            
                            %- Avoid problems when entire image is 0
                            if img_max == 0
                                img_max = 1e4;
                            end

                            if isempty(reg_prop(i_reg).fit_limits)
                                limits = fit_limits;
                            else
                                limits = reg_prop(i_reg).fit_limits;
                            end 


                            %- Restrict (or not) position of center
                            if fit_pos_restrict

                                fact_rest = fit_pos_restrict;

                                x_min_fit = mean(axis_par.X_nm) - fact_rest*pixel_size.xy;
                                x_max_fit = mean(axis_par.X_nm) + fact_rest*pixel_size.xy;

                                y_min_fit = mean(axis_par.Y_nm) - fact_rest*pixel_size.xy;
                                y_max_fit = mean(axis_par.Y_nm) + fact_rest*pixel_size.xy;

                            else

                                x_min_fit = 0;
                                x_max_fit = max(axis_par.X_nm);

                                y_min_fit = 0;
                                y_max_fit = max(axis_par.Y_nm);

                            end

                           %- Avoid identical limits by increasing the upper one by a little
                           if limits.sigma_xy_max == limits.sigma_xy_min
                               limits.sigma_xy_max = limits.sigma_xy_max + 0.1;
                           end


                           %- Avoid identical limits by increasing the upper one by a little
                           if limits.bgd_max == limits.bgd_min
                               limits.bgd_max = limits.bgd_max + 0.1;
                           end  

                            options_fit.bound.lb = [limits.sigma_xy_min y_min_fit x_min_fit  0          limits.bgd_min];  
                            options_fit.bound.ub = [limits.sigma_xy_max y_max_fit x_max_fit  2*img_max  limits.bgd_max];
                    end


                    %- Index of thresholded spots
                    FIT_Result           = psf_fit_2d_v3(img_crop,xdata,options_fit,flag_struct);
                    FIT_Result.axis_par  = axis_par;        
                    FIT_Result.dim_crop  = dim_crop;

                    %- Consider cropping in routine
                    spot_fit_result.mu_X_nm = FIT_Result.muX + x_min*pixel_size.xy;
                    spot_fit_result.mu_Y_nm = FIT_Result.muY + y_min*pixel_size.xy; 
                    spot_fit_result.mu_Z_nm = 0;

                    spot_fit_result.mu_X_px = round(spot_fit_result.mu_X_nm/pixel_size.xy);
                    spot_fit_result.mu_Y_px = round(spot_fit_result.mu_Y_nm/pixel_size.xy);
                    spot_fit_result.mu_Z_px = 0;        

                    %=== Integrated intensity of spot
                    par_mod_int(1)  = FIT_Result.sigmaX;
                    par_mod_int(2)  = FIT_Result.sigmaY;

                    par_mod_int(3)  = 0;
                    par_mod_int(4)  = 0;

                    par_mod_int(5)  = FIT_Result.amp ;
                    par_mod_int(6)  = 0 ;

                    int_spot.gauss_integrate = fun_Gaussian_2D_double_integral_v1(x_int,y_int,par_mod_int); 
                else
                     
                	FIT_Result.amp = 0;
                    FIT_Result.bgd = 0;
                    FIT_Result.sigmaX = 0;
                    spot_fit_result.mu_X_nm = 0;
                    spot_fit_result.mu_Y_nm = 0;
                    FIT_Result.resnorm = 0;
                    FIT_Result.centroidX = 0;
                    FIT_Result.centroidY = 0;
                    int_spot.gauss_integrate= 0;
                   
                end

                %=== Sum of intensity sourrounding brightest pixel in N-by-N box
                x_sub.min = pos_X - N_pix_sum.xy;
                x_sub.max = pos_X + N_pix_sum.xy;

                y_sub.min = pos_Y - N_pix_sum.xy;
                y_sub.max = pos_Y + N_pix_sum.xy;
                
                %- Catch positions at edge of image
                 if x_sub.min < 1 
                    x_sub.min = 1;
                    x_sub.max = x_sub.max + abs(x_sub.min) + 1;                
                end

                if  x_sub.max > dim.X    
                    x_sub.max = dim.X;
                    x_sub.min = x_sub.min - (dim.X-x_sub.max);  
                end
                
                if  y_sub.min < 1 
                    y_sub.min = 1;
                    y_sub.max = y_sub.max + abs(y_sub.min) + 1;                
                end

                if  y_sub.max > dim.Y    
                    y_sub.max = dim.Y;
                    y_sub.min = y_sub.min  - (dim.Y-y_sub.max) ;  
                end  
                
                %- Get intensity of actual image
                img_sub       = img_loop(y_sub.min:y_sub.max,x_sub.min:x_sub.max);
                int_spot.mean = mean(img_sub(:));    
                int_spot.sum  = sum(img_sub(:));   
                
                %- Get intensity of background
                img_bgd_sub       = bgd(y_sub.min:y_sub.max,x_sub.min:x_sub.max);
                int_spot.bgd_mean = mean(img_bgd_sub(:));    
                int_spot.bgd_sum  = sum(img_bgd_sub(:));                  
                

                %=== Save everything    
                reg_quant(i_reg).summary_fit(i_slice).val = [FIT_Result.amp FIT_Result.bgd FIT_Result.sigmaX 0 spot_fit_result.mu_X_nm spot_fit_result.mu_Y_nm 0 FIT_Result.resnorm FIT_Result.centroidX FIT_Result.centroidY 0];

                reg_quant(i_reg).summary_quant(i_slice).fit  = [FIT_Result.amp     int_spot.gauss_integrate];
                reg_quant(i_reg).summary_quant(i_slice).mean = [int_spot.mean];
                reg_quant(i_reg).summary_quant(i_slice).sum  = [int_spot.sum]; 
                reg_quant(i_reg).summary_quant(i_slice).pix  = [img_loop(pos_Y,pos_X) pos_Y pos_X  0 ];
                reg_quant(i_reg).summary_quant(i_slice).bgd_mean = int_spot.bgd_mean;
                reg_quant(i_reg).summary_quant(i_slice).bgd_sum  = int_spot.bgd_sum;  
                
            end
        end
    end
end

ind_TS = unique(ind_TS);

%=== Loop over all regions
if flag_struct.fit_TS
    for i_reg = 1:N_reg
        
        %- Assign time vector
        reg_prop(i_reg).time = tList;
        
        %- Not a OPB and a background region
        if reg_prop(i_reg).flag_OPB ~= 1 && reg_prop(i_reg).flag_BGD ~= 1

            %quant_summary_all = [];
            quant_fit_summary_all = [];
            fit_amp = [];

            quant = [];

            for i_slice = 1:N_slice	

                %- Get values for slide
                summary_fit   = reg_quant(i_reg).summary_fit(i_slice).val;
                summary_quant = reg_quant(i_reg).summary_quant(i_slice);

                %- Assign to output parameters
                quant_fit_summary_all(i_slice,:) = summary_fit;
                fit_amp(i_slice,:)               = summary_fit(1);       % Amplitude for fitting, first column before correction for photobleaching, second column after correction for photobleaching
                quant.integrate_int(i_slice,:)   = summary_quant.fit(2); % Integrated intensity before and after correction for observational photobleaching
                quant.pix(i_slice,:)             = summary_quant.pix;
                quant.sum(i_slice,:)             = summary_quant.sum;
                quant.mean(i_slice,:)            = summary_quant.mean;
                quant.bgd_sum(i_slice,:)         = summary_quant.bgd_sum;
                quant.bgd_mean(i_slice,:)        = summary_quant.bgd_mean; 
                
             end   
                
            %- Check if there are already fitting results
            if not(isfield(reg_prop(i_reg),'quant_fit_summary_all'))
                reg_prop(i_reg).quant_fit_summary_all_first = quant_fit_summary_all;
                reg_prop(i_reg).quant_fit_summary_all       = quant_fit_summary_all;
            else
                if not(isempty(reg_prop(i_reg).quant_fit_summary_all))
                    reg_prop(i_reg).quant_fit_summary_all       = quant_fit_summary_all;
                else
                    reg_prop(i_reg).quant_fit_summary_all_first = quant_fit_summary_all;
                    reg_prop(i_reg).quant_fit_summary_all       = quant_fit_summary_all;
                end
            end
           
            reg_prop(i_reg).quant_fit_summary_all = quant_fit_summary_all;
            reg_prop(i_reg).fit_amp               = fit_amp;  
            reg_prop(i_reg).quant                 = quant;
        end
    end
end
fprintf('\n');    