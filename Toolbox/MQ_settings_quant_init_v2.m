function options_quant = MQ_settings_quant_init_v2

options_quant.reg_detect.xy     = 3; % Size of region to average intensity [XY] 
options_quant.reg_detect.z      = 1; % Size of region to average intensity [Z]  
options_quant.flag_subtract_bgd = 1;  % Subtract background (filtered image)
options_quant.flag_quant_dim    = '3D';  % Quantification in 2D or in 3D
options_quant.flag_fit_pos_restrict = 1;
options_quant.N_pix_sum.xy = 1;  % Size of region to sum pixel intensity for quantification
options_quant.N_pix_sum.z = 1;  % Size of region to sum pixel intensity for quantification
