function [file_save,file_save_full] = MQ_save_region_v3(handles,file_save)

%- Change to correct folder
current_dir = pwd;

if handles.path_sub_regions_status == 1;
    folder_name = fullfile(handles.path_name_image,handles.path_sub_regions);
    
   is_dir = exist(folder_name,'dir'); 

   if is_dir == 0
       mkdir(folder_name)
   end
    
else
    folder_name = handles.path_name_image;
end

cd(folder_name)

%- If not file-name to save was provided
if isempty(file_save)    
    
    %- Ask user for file-name for spot results
    [dum, name_file] = fileparts(handles.file_name_image); 
    file_name_default_spot = [name_file,'__regions.txt'];
    [file_save,path_save] = uiputfile(file_name_default_spot,'Save outline of regions'); 
    file_save_full = fullfile(path_save,file_save);
    
else
    file_save_full = fullfile(folder_name,file_save);
    file_save = 1;    
end


% Only write if file_save specified
if file_save ~= 0
    
    par_microscope = handles.par_microscope;
    reg_prop       = handles.reg_prop;          
    
    fid = fopen(file_save_full,'w');
    
    %- Header 
    fprintf(fid,'MS2-QUANT\t%s\n', handles.version);
    fprintf(fid,'Region DEFINITION, %s\n', date);
    fprintf(fid,'%s\t%s\n','COMMENT','Outline definition performed in MS2-QUANT (Main program)');   
  
    %- File Name
    fprintf(fid,'%s\t%s\n','FILE',handles.file_name_image);
    fprintf(fid,'%s\t%s\n','PATH',handles.path_name_image);
    
    %- Experimental parameters
    fprintf(fid,'PARAMETERS\n');
    fprintf(fid,'Pix-XY\tPix-Z\tRI\tEx\tEm\tNA\tdT\tType\n');
    fprintf(fid,'%g\t%g\t%g\t%g\t%g\t%g\t%g\t%s\n', par_microscope.pixel_size.xy, par_microscope.pixel_size.z, par_microscope.RI, par_microscope.Ex, par_microscope.Em,par_microscope.NA,par_microscope.dT, par_microscope.type );
    
    %- Analysis settings
    fprintf(fid,'ANALYSIS-SETTINGS  \t%s\n', handles.file_name_settings);    
    fprintf(fid,'IMAGING-BACKGROUND \t%g\n', handles.BGD_img);
    
    %- Region list
    for i_reg = 1:size(reg_prop,2)
        
        %- TYPE of region
        fprintf(fid,'%s\t%s\n', 'REGION', reg_prop(i_reg).label);
       
        if     reg_prop(i_reg).flag_OPB == 1
            reg_type = 'OPB';
        elseif reg_prop(i_reg).flag_BGD == 1
            reg_type = 'BGD';
        else
            reg_type = 'TS';
        end
        
        
        fprintf(fid,'%s\t%s\n', 'TYPE', reg_type);
        
        %- Outline of region
        fprintf(fid,'%s\n', 'OUTLINE');
        fprintf(fid,'X_POS\t');
        fprintf(fid,'%g\t',reg_prop(i_reg).x);
        fprintf(fid,'END\n');
        fprintf(fid,'Y_POS\t');
        fprintf(fid,'%g\t',reg_prop(i_reg).y);
        fprintf(fid,'END\n');  
        
        %- If detected: detected positions
        if not(isempty(reg_prop(i_reg).pos))
            fprintf(fid,'%s\n', 'POSITION');  
            fprintf(fid,'X_POS\t');
            fprintf(fid,'%g\t',reg_prop(i_reg).pos.x);
            fprintf(fid,'END\n');
            fprintf(fid,'Y_POS\t');
            fprintf(fid,'%g\t',reg_prop(i_reg).pos.y);
            fprintf(fid,'END\n');
            fprintf(fid,'Z_POS\t');
            fprintf(fid,'%g\t',reg_prop(i_reg).pos.z);
            fprintf(fid,'END\n');
            fprintf(fid,'X_POS_SUB\t');
            fprintf(fid,'%g\t',reg_prop(i_reg).pos.x_sub);
            fprintf(fid,'END\n');
            fprintf(fid,'Y_POS_SUB\t');
            fprintf(fid,'%g\t',reg_prop(i_reg).pos.y_sub);
            fprintf(fid,'END\n');
            fprintf(fid,'Z_POS_SUB\t');
            fprintf(fid,'%g\t',reg_prop(i_reg).pos.z_sub);
            fprintf(fid,'END\n');
            
            if isfield(reg_prop(i_reg),'detect_int')
                fprintf(fid,'detect_int\t');
                fprintf(fid,'%g\t',reg_prop(i_reg).detect_int);
                fprintf(fid,'END\n');
            end
            
        end   
    end
    
    fclose(fid);
end

cd(current_dir)