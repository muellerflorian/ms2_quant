function [data_MS2, flag_file, header] = MQ_results_load_calibrated_v1(file_name)
% Function to read in outline definition for cells


%- Prepare structure
data_MS2 = [];
header    = {};
flag_file = 0;

%- Open file
fid  =  fopen(file_name,'r');

try 
    %=== Read in header of file

    %- Read-in each line until header 
    tline = fgetl(fid);
    
    %- Read until header with Key word N_mean_all is encountered
    while (isempty(strfind(tline,'Q_Integrate'))) &&  (isempty(strfind(tline,'Q_IntInt')))
        tline = fgetl(fid);
        
        if tline == -1
            disp(['File: ',file_name, ' could not be read.'])
            return
        end
    end
    
    %- Analyze header
    header_line    = tline;
    ind_log_tab    = (header_line == sprintf('\t'));
    num_cols       = 1 + sum(ind_log_tab);

    %- Get all headers
    ind_tab_header = find(ind_log_tab);
    ind_tab        = [0,ind_tab_header,length(header_line)+1];
    
    for i_tab =1:length(ind_tab)-1
        ind_start = ind_tab(i_tab)+1;
        ind_end   = ind_tab(i_tab+1)-1;
        
        header{i_tab} = header_line(ind_start:ind_end); 
    end
        
    %- Read in data
    str_read_in = ['%f',repmat('%f', 1, num_cols-1)];

    %- Read in data
    C = textscan(fid, str_read_in,'HeaderLines',0,'delimiter','\t','CollectOutput',1);

    %- Get MS2-data
    data_MS2 = C{1};    
    flag_file = 1;    
catch
    disp(file_name)
    flag_file = 0;      
    
end
fclose ('all');

