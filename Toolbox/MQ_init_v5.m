function handles = MQ_init_v5(handles,flag_2D)
%
% Initiate the GUI with all relevant parameter. Call populate function
% afterwards to use these values to prepare GUI. Split in two function
% occurs to allow loading of (partial settings).

if nargin <2
    flag_2D = 0;
end


%=== Status of current processing steps
handles.status_filtered  = 0;    % Image filterd
handles.status_image     = 0;    % Image loaded
handles.status_outline   = 0;    % Outlines defined
handles.status_detect    = 0;    % Pre-detection
handles.status_quant     = 0;
handles.status_OPB_quant = 0;

handles.flag_fit              = 0;    % Fit mode (0 for free parameters, 1 for fixed size parameters)
handles.flag_bgd              = 0;    % BGD region for correction of OPB defined
handles.flag_OPB              = 0;

% === Some parameters for plots
handles.image_struct       = [];
handles.status_data_cursor = 0;
handles.status_zoom        = 0;
handles.h_zoom             = rand(1);
handles.status_pan         = 0;
handles.h_pan              = rand(1);


%=== File-name and path-name
handles.img_disp = [];
handles.path_name_image          = [];
handles.file_name_image          = [];
handles.file_name_image_filtered = [];

handles.path_name_settings = [];
handles.file_name_settings = [];

handles.file_name_results  = [];
handles.file_name_regions  = [];


%=== File-name and path-name
handles.path_sub_results        = '';
handles.path_sub_results_status = 1;

handles.path_sub_regions        = '';
handles.path_sub_regions_status = 1;


%= Parameters describing the image and the regions
handles.reg_prop          = struct('label', {}, 'x', {}, 'y', {}, 'pos', {}, ...
                                   'flag_BGD',[],'flag_OPB',[],'fit_limits', {},...
                                   'quant_fit_summary_all', [], 'quant_fit_summary_all_first',[]);
handles.PSF_exp            = [];
           
handles.image_struct.data_filtered = [];
handles.image_struct.data = [];


%=== Default parameters for the experiment 
%  --> only if not already defined. Useful when processing multiple images 
%  and the same settings should be  used for all of them.
if not(isfield(handles,'par_microscope'))
    handles.par_microscope.pixel_size.xy = 160;
    handles.par_microscope.pixel_size.z  = 600;   
    handles.par_microscope.RI            = 1.458;   
    handles.par_microscope.NA            = 1.25;
    handles.par_microscope.Em            = 568;   
    handles.par_microscope.Ex            = 568;
    handles.par_microscope.type          = 'widefield';  
    handles.par_microscope.dT            = 3;
end

%- Filtering
if flag_2D
    handles.filter.factor_bgd_xy = 9; 
    handles.filter.factor_bgd_z  = 1;

    handles.filter.factor_psf_xy = 1;
    handles.filter.factor_psf_z  = 1;   
else
    handles.filter.factor_bgd_xy = 6; 
    handles.filter.factor_bgd_z  = 1;

    handles.filter.factor_psf_xy = 0.6;
    handles.filter.factor_psf_z  = 0.6;
end

%- Theoretical PSF - will be used to calculated the size of the detection region
[PSF_theo.xy_nm,PSF_theo.z_nm] = sigma_PSF_BoZhang_v1(handles.par_microscope);
PSF_theo.xy_pix                = PSF_theo.xy_nm / handles.par_microscope.pixel_size.xy ;
PSF_theo.z_pix                 = PSF_theo.z_nm  / handles.par_microscope.pixel_size.z ;
handles.PSF_theo               = PSF_theo;

%- Detection 
handles.par_detect.flags.image  = 'filt';
handles.par_detect.flags.detect_mode  = 'brightest';
handles.par_detect.int_min = 0; 

%- Correction for OPB
handles.opb_fit_mode = 'exp';

%- Fitting
handles.par_fit.sigma_XY_fixed = [];
handles.par_fit.sigma_Z_fixed  = [];




