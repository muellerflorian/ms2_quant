function settings = MQ_settings_load_v2(file_name,struct_store)
% Function to read in settings for analysis of FISH data
% Settings are stored in a simple format. Each property starts with the
% name followed by a '='and the actual value. There is NO space inbetween
% the equal sign and the identifier and the actual value!
%
% struct_store is the structure where the settings will be save. Can either
% be empty or a user defined structure. In FISH_QUANT the handles structure
% of the GUI will be the input. This way all the saved settings will be
% over-written while other will be untouched.


if isempty(struct_store)
    settings = {};
else
    settings = struct_store;
end


%- Open file
fid  =  fopen(file_name,'r');


% Read in each line and check if one of the known identifiers is present.
% If yes, assign the corresponding value

if fid == -1
    warndlg('Settings file cannot be opened','MQ_settings_load_v1'); 
else

    %- Loop through file until end of file
    while not(feof(fid))

        %- Extract string of entire line
        C   = textscan(fid,'%s',1,'delimiter','\n');
        str =  char(C{1});

        %- Is there and equal sign? Extract strings before and after
        k = strfind(str, '=');    
        str_tag = str(1:k-1);
        str_val = str(k+1:end);

        %- Compare identifier before the equal sign to known identifier
        switch str_tag

            
            
            %- Paramters
            
            case 'lambda_EM'
                settings.par_microscope.Em = str2double(str_val);

            case 'lambda_Ex'
                settings.par_microscope.Ex = str2double(str_val);             

            case 'NA'            
                settings.par_microscope.NA = str2double(str_val);            

            case 'RI'
                settings.par_microscope.RI = str2double(str_val);            

            case 'Microscope'
                settings.par_microscope.type = str_val;   

            case 'Pixel_XY'    
                settings.par_microscope.pixel_size.xy = str2double(str_val);                 

            case 'Pixel_Z' 
                settings.par_microscope.pixel_size.z = str2double(str_val); 

            case 'dT' 
                settings.par_microscope.dT = str2double(str_val);             
     
            %- Settings for subfolders
            case 'FOLDER_SUB_RESULTS' 
                settings.path_sub_results = (str_val); 

            case 'FOLDER_SUB_RESULTS_USE' 
                settings.path_sub_results_status = str2double(str_val); 
            
             case 'FOLDER_SUB_REGIONS' 
                settings.path_sub_regions = (str_val); 

            case 'FOLDER_SUB_REGIONS_USE' 
                settings.path_sub_regions_status = str2double(str_val);            

                
            %- Settings for filtering: old
            case 'Kernel_bgd'            
                settings.filter.factor_bgd_xy = str2double(str_val);
                settings.filter.factor_bgd_z  = str2double(str_val);

            case 'Kernel_psf'
                settings.filter.factor_psf_xy = str2double(str_val);
                settings.filter.factor_psf_z = str2double(str_val);
                
             %- Settings for filtering: new   
             case 'Kernel_bgd_xy'            
                settings.filter.factor_bgd_xy = str2double(str_val);

             case 'Kernel_bgd_z'            
                settings.filter.factor_bgd_z = str2double(str_val);               
                
             case 'Kernel_psf_xy'            
                settings.filter.factor_psf_xy = str2double(str_val);

             case 'Kernel_psf_z'            
                settings.filter.factor_psf_z = str2double(str_val);                 
                
                
            %- Settings for OPB correction
            case 'OPB_fit_mode'
                settings.opb_fit_mode      = str_val;
         
            %- Settings for Pre-detection
            case 'OPT_DETECT_IMG'            
                settings.par_detect.flags.image = (str_val);

            case 'OPT_DETECT_MODE'
                settings.par_detect.flags.detect_mode = (str_val);    
                
            case 'OPT_DETECT_MIN_INT'
                settings.par_detect.int_min = str2double(str_val);                  

                
            %- Settings for QUANTIFICATION
            
           case 'OPT_QUANT_REG_PIX_SUM_XY'            
                settings.options_quant.N_pix_sum.xy= str2double(str_val);

            case 'OPT_QUANT_REG_PIX_SUM_Z'
                settings.options_quant.N_pix_sum.z = str2double(str_val);
             
                    
            case 'OPT_QUANT_REG_XY'            
                settings.options_quant.reg_detect.xy= str2double(str_val);

            case 'OPT_QUANT_REG_Z'
                settings.options_quant.reg_detect.z = str2double(str_val);
            
            case 'OPT_QUANT_FLAG_BGD'            
                settings.options_quant.flag_subtract_bgd = str2double(str_val);

            case 'OPT_QUANT_FLAG_POS_REST'
                settings.options_quant.flag_fit_pos_restrict = str2double(str_val);
                
            case 'OPT_QUANT_FLAG_DIM'
                settings.options_quant.flag_quant_dim  = str_val;                
     
            %-Limits for fit: assigned
            case 'LIMIT_sigma_xy_min'            
                settings.fit_limits.sigma_xy_min= str2double(str_val);

            case 'LIMIT_sigma_xy_max'
                settings.fit_limits.sigma_xy_max = str2double(str_val);
   
            case 'LIMIT_sigma_z_min'            
                settings.fit_limits.sigma_z_min= str2double(str_val);

            case 'LIMIT_sigma_z_max'
                settings.fit_limits.sigma_z_max = str2double(str_val);           
            
            case 'LIMIT_bgd_min'            
                settings.fit_limits.bgd_min= str2double(str_val);

            case 'LIMIT_bgd_max'
                settings.fit_limits.bgd_max = str2double(str_val);           
            
                
             %-Limits for fit: default             
            case 'LIMIT_DEF_sigma_xy_min'            
                settings.fit_limits.sigma_xy_min_def= str2double(str_val);

            case 'LIMIT_DEF_sigma_xy_max'
                settings.fit_limits.sigma_xy_max_def = str2double(str_val);
   
            case 'LIMIT_DEF_sigma_z_min'            
                settings.fit_limits.sigma_z_min_def= str2double(str_val);

            case 'LIMIT_DEF_sigma_z_max'
                settings.fit_limits.sigma_z_max_def = str2double(str_val);           
            
            case 'LIMIT_DEF_bgd_min'            
                settings.fit_limits.bgd_min_def= str2double(str_val);

            case 'LIMIT_DEF_bgd_max'
                settings.fit_limits.bgd_max_def = str2double(str_val);      
        
                
            %-Limits for fit: previous             
            case 'LIMIT_PREV_sigma_xy_min'            
                settings.fit_limits.sigma_xy_min_prev= str2double(str_val);

            case 'LIMIT_PREV_sigma_xy_max'
                settings.fit_limits.sigma_xy_max_prev = str2double(str_val);
   
            case 'LIMIT_PREV_sigma_z_min'            
                settings.fit_limits.sigma_z_min_prev= str2double(str_val);

            case 'LIMIT_PREV_sigma_z_max'
                settings.fit_limits.sigma_z_max_prev = str2double(str_val);           
            
            case 'LIMIT_PREV_bgd_min'            
                settings.fit_limits.bgd_min_prev= str2double(str_val);

            case 'LIMIT_PREV_bgd_max'
                settings.fit_limits.bgd_max_prev = str2double(str_val);     
                
        end      

        
        %=== ADDED TO READ IN SETTINGS SAVED IN RESULTS FILE
        
        %- Is there an tab stop? Extract strings before and after
        k = strfind(str, sprintf('\t'));    
        str_tag = str(1:k-1);
        str_val = str(k+1:end);


        %- Compare identifier before the equal sign to known identifier
        switch str_tag

            %-Limits for fit: assigned
            case 'LIMIT-FIT-SIGMA-XY-MIN'            
                settings.fit_limits.sigma_xy_min= str2double(str_val);

            case 'LIMIT-FIT-SIGMA-XY-MAX'
                settings.fit_limits.sigma_xy_max = str2double(str_val);

            case 'LIMIT-FIT-SIGMA-Z-MIN'            
                settings.fit_limits.sigma_z_min= str2double(str_val);

            case 'LIMIT-FIT-SIGMA-Z-MAX'
                settings.fit_limits.sigma_z_max = str2double(str_val);           

            case 'LIMIT-FIT-BGD-MIN'            
                settings.fit_limits.bgd_min= str2double(str_val);

            case 'LIMIT-FIT-BGD-MAX'
                settings.fit_limits.bgd_max = str2double(str_val);      


        end
        
        
        
    end
    
    fclose(fid);
end


