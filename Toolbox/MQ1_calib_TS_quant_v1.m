function MQ1_calib_TS_quant_v1

%% SCRIPT to quantify transcription sites form MS2 measurements
%
%  Quantification is done with an average image of mRNA that will be
%  renormalized based on the estiamted values of each cell containing a
%  transcription site.
%
%  Best used in cell mode (see help file). Then you can process each block
%  (indicate with numbers by itself)


%% [1.a] Get FQ result files in which a TS should be quantified - multi-select is possible
current_path = pwd;
[file_name.results,folder.results] = uigetfile({'*.txt'},'Select file with results of spot detection where TS should be quantified.','MultiSelect', 'on');
cd(current_path);


%% [2] Define folder for images
folder.image = uigetdir(folder.results,'Define folder with images');


%% [3] Specify settings for TS quantification

%- Define default parameter for TS quantification
img =  FQ_img;

%- Ask user to change settings
choice = questdlg('Use default settings for TS quantification?','TxSite quantification', 'Yes','No','Yes');
if strcmp(choice,'No')
    img.modify_settings_TS(1);   
end


%% [4] Start TS quantification
parameters.flag_PSF_indiv = 1;
parameters.flag_quant_mRNA = 0;
parameters.flag_output = 0;
parameters.flag_save   = 1;
parameters.file_name = file_name;
parameters.folder    = folder;
parameters.img        = img;
 
% == Folder to save
folder_save = fullfile(parameters.folder.results,['TS_QUANT_' , datestr(now, 'yymmdd')]);  
if ~exist(folder_save,'dir')
   mkdir(folder_save)
   parameters.folder.save = folder_save;
else
   parameters.folder.save = folder_save; 
end

% === Quantify with individual PSF
Fun_MQ_calib_TS_quant_v8(parameters);
