function options_quant = MQ_settings_quant_mod_v4(options_quant)
% Function to modify the settings of TS quantification

%- User-dialog
dlgTitle = 'Options for MS2 quantification';
prompt_avg(1) = {'Size of detection region [XY]: center +/- pix'};
prompt_avg(2) = {'Size of detection region [Z]: center +/- pix'};
prompt_avg(3) = {'Size of region to sum intensity [XY]: center +/- pix'};
prompt_avg(4) = {'Size of region to sum intensity [Z]: center +/- pix'};
prompt_avg(5) = {'Subtract background (from filtering step): [1-YES]; [0-No]'};
prompt_avg(6) = {'Quantification in 2D or 3D:'};
prompt_avg(7) = {'Restrict location of center [0-no, value: +/- pixel of detected location]:'};

defaultValue_avg{1} = num2str(options_quant.reg_detect.xy);
defaultValue_avg{2} = num2str(options_quant.reg_detect.z);
defaultValue_avg{3} = num2str(options_quant.N_pix_sum.xy);
defaultValue_avg{4} = num2str(options_quant.N_pix_sum.z);
defaultValue_avg{5} = num2str(options_quant.flag_subtract_bgd);
defaultValue_avg{6} = options_quant.flag_quant_dim;
defaultValue_avg{7} = num2str(options_quant.flag_fit_pos_restrict);


options.Resize='on';
%options.WindowStyle='normal';
userValue = inputdlg(prompt_avg,dlgTitle,1,defaultValue_avg,options);

%- Return results if specified
if( ~ isempty(userValue))
    options_quant.reg_detect.xy         = str2double(userValue{1});
    options_quant.reg_detect.z          = str2double(userValue{2});
    options_quant.N_pix_sum.xy         = str2double(userValue{3});
    options_quant.N_pix_sum.z          = str2double(userValue{4});
    options_quant.flag_subtract_bgd     = str2double(userValue{5});
    options_quant.flag_quant_dim        = userValue{6};
    options_quant.flag_fit_pos_restrict = str2double(userValue{7});
end
