function MQ_show_results_v4(par_plot)

%== Get parameters
fit_amp     = par_plot.fit_amp;
quant       = par_plot.quant;
summary_fit = par_plot.summary_fit;
dim         = par_plot.dim;
pos_TS      = par_plot.pos_TS;
reg         = par_plot.reg;

N_T = length(summary_fit(:,6));

%=== Figure 1 - plot estimated paramters
h_fig1 = figure;
subplot(3,2,1)
hold on
plot(fit_amp(:,1),'b')
hold off
box on
title('Amplitude')
xlabel('# slice')
v= axis; axis([1 N_T v(3) v(4)]);

subplot(3,2,2)
plot(summary_fit(:,2))
title('BGD')
xlabel('# slice')
v= axis; axis([1 N_T v(3) v(4)]);

subplot(3,2,3)
plot(summary_fit(:,3))
title('Sigma XY')
xlabel('# slice')
v= axis; axis([1 N_T v(3) v(4)]);

subplot(3,2,4)
plot(summary_fit(:,4))
title('Sigma Z')
xlabel('# slice')
v= axis; axis([1 N_T v(3) v(4)]);

subplot(3,2,5)
plot(summary_fit(:,5),summary_fit(:,6),'+k')
title('Position XY')
axis image
xlabel('X')
ylabel('Y')

subplot(3,2,6)
plot(summary_fit(:,7))
title('Position Z')
xlabel('X')
ylabel('Y')
v= axis; axis([1 N_T v(3) v(4)]);

set(h_fig1,'Color','w')




%== Figure: color coded XY position
h_fig2 = figure;
h(1) = subplot(1,2,1);
plotclr_v1(pos_TS(:,1),pos_TS(:,2),(1:N_T),'o');
title('Color-coded XY STARTING positions')
xlabel('Y')
ylabel('X')
zlabel('Time')
axis([dim.x_min_nm dim.x_max_nm  dim.y_min_nm dim.y_max_nm 1 N_T])
hold on
plot([reg.x,reg.x(1)],[reg.y,reg.y(1)],'b','Linewidth', 2)
hold off

h(2) = subplot(1,2,2);
plotclr_v1(summary_fit(:,5),summary_fit(:,6),(1:N_T),'o');
title('Color-coded XY FITTED positions')
xlabel('X')
ylabel('Y')
zlabel('Time')
axis([dim.x_min_nm dim.x_max_nm  dim.y_min_nm dim.y_max_nm 1 N_T])
hold on
plot([reg.x,reg.x(1)],[reg.y,reg.y(1)],'b','Linewidth', 2)
hold off

set(h_fig2,'Color','w')


%- Link axes and assign range
global hlink
hlink = linkprop(h,{'CameraPosition','CameraUpVector'});


%===  Figure 3 - plot different ways to measure intensity of the transcription site
h_fig3 = figure;
subplot(2,2,1)
hold on
plot(fit_amp(:,1),'b')
hold off
box on
title('TxSite: amplitude')
xlabel('# slice')
v= axis; axis([1 N_T v(3) v(4)]);


subplot(2,2,2)
hold on
plot(quant.integrate_int(:,1),'b')
hold off
box on
title('TxSite: integrated Gaussian')
xlabel('# slice')
v= axis; axis([1 N_T v(3) v(4)]);

subplot(2,2,3)
hold on
plot(quant.pix(:,1),'b')
hold off
box on
title('TxSite: brightest pixel')
xlabel('# slice')
v= axis; axis([1 N_T v(3) v(4)]);

subplot(2,2,4)
hold on
plot(quant.sum(:,1),'b')
hold off
box on
title('TxSite: sum of all pixels')
v= axis; axis([1 N_T v(3) v(4)]);

xlabel('# slice')
set(h_fig3,'Color','w')

    