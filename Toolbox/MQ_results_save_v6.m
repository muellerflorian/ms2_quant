function [file_save, path_save] = MQ_results_save_v6(file_name_full,parameters)

reg_prop           = parameters.reg_prop;
par_microscope     = parameters.par_microscope;
path_name_image    = parameters.path_name_image;
file_name_image    = parameters.file_name_image;
file_name_settings = parameters.file_name_settings;
version            = parameters.version;
BGD_img            = parameters.BGD_img;
limits             = parameters.limits;
OPB_curves         = parameters.OPB_curves;
label              = parameters.label;

if isfield(parameters,'status_save_separate')   
    status_save_separate = parameters.status_save_separate; 
else
    status_save_separate = 1;
end


if isfield(parameters,'detect_int')
   detect_int = parameters.detect_int;
else
   detect_int = 0; 
   disp('MQ_results_save_v6: detection threshold is not defined')
end

%- Change to correct folder
%current_dir = pwd;
%cd(path_name_image)


%- Ask for file-name if it's not specified
if isempty(file_name_full)

    %- Ask user for file-name for spot results
    [dum, name_file] = fileparts(file_name_image); 
    file_name_default_spot = [name_file,'__QUANT.txt'];

    [file_save,path_save] = uiputfile(file_name_default_spot,'Save results of TXSite quantification');
    file_name_full        = fullfile(path_save,file_save);
    
    %- Ask user to specify comment
    prompt = {'Comment (cancel for no comment):'};
    dlg_title = 'User comment for file';
    num_lines = 1;
    def = {''};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
else   
    file_save = 1;
    path_save = fileparts(file_name_full); 
    answer = 'Batch detection';
end


% Only write if FileName specified
if file_save ~= 0
    
    fid = fopen(file_name_full,'w');
    
   %- Header 
    fprintf(fid,'MS2-QUANT\t%s\n', version);
    fprintf(fid,'Region DEFINITION, %s\n', date);
    fprintf(fid,'%s\t%s\n','COMMENT','Outline definition performed in MS2-QUANT (Main program)');   
  
    %- File Name
    fprintf(fid,'%s\t%s\n','FILE',file_name_image);
    fprintf(fid,'%s\t%s\n','PATH',path_name_image);
     
    %- Experimental parameters and analysis settings
    fprintf(fid,'PARAMETERS\n');
    fprintf(fid,'Pix-XY\tPix-Z\tRI\tEx\tEm\tNA\tdT\tType\n');
    fprintf(fid,'%g\t%g\t%g\t%g\t%g\t%g\t%g\t%s\n', par_microscope.pixel_size.xy, par_microscope.pixel_size.z, par_microscope.RI, par_microscope.Ex, par_microscope.Em,par_microscope.NA,par_microscope.dT, par_microscope.type );
    
    %- Analysis settings   
    fprintf(fid,'ANALYSIS-SETTINGS\t%s\n', file_name_settings);   
    fprintf(fid,'IMAGING-BACKGROUND\t%g\n', BGD_img);
    
    fprintf(fid,'LIMIT-FIT-SIGMA-XY-MIN\t%g\n', limits.sigma_xy_min);    
    fprintf(fid,'LIMIT-FIT-SIGMA-XY-MAX\t%g\n', limits.sigma_xy_max);
    fprintf(fid,'LIMIT-FIT-SIGMA-Z-MIN\t%g\n',  limits.sigma_z_min);    
    fprintf(fid,'LIMIT-FIT-SIGMA-Z-MAX\t%g\n',  limits.sigma_z_max);    
    fprintf(fid,'LIMIT-FIT-BGD-MIN\t%g\n',      limits.bgd_min);    
    fprintf(fid,'LIMIT-FIT-BGD-MAX\t%g\n',      limits.bgd_max);    
      
    fprintf(fid,'DETECTION-THRESHOLD\t%g\n',    detect_int);
    
    
    %- Outline of cell and detected spots
    for i_reg = 1:size(reg_prop,2)
                
        %- Outline of cell
        fprintf(fid,'%s\t%s\n', 'REGION', reg_prop(i_reg).label);
        fprintf(fid,'X_POS\t');
        fprintf(fid,'%g\t',reg_prop(i_reg).x);
        fprintf(fid,'END\n');
        fprintf(fid,'Y_POS\t');
        fprintf(fid,'%g\t',reg_prop(i_reg).y);
        fprintf(fid,'END\n');
    
        %- Time-course of transcriptions site
        % NOTE: fprintf works on columns - transformation is therefore needed
        if not(isempty(reg_prop(i_reg).quant.integrate_int));
           
           header_row =   'Time[s]\tQ_Integrate\tQ_AMP\tQ_Sum_pix\tQ_MIP_int\tPix_Y\tPix_X\tPix_Z\tAMP\tBGD\tSigmaXY\tSigmaZ\tMuY\tMuX\tMuZ\tRES\tCent_Y\tCent_X\tCent_Z\tOPB_time\tOPB_INT\tOPB_FIT\tOPB_FIT_norm\n';
            
           %- Assign zeros to OPB-curves if not defined
           if isempty(OPB_curves)
                OPB_curves = zeros(length(reg_prop(i_reg).time),4);
           end
            
           %- Output parameters
           quant_output = [double(reg_prop(i_reg).time) double(reg_prop(i_reg).quant.integrate_int) double(reg_prop(i_reg).fit_amp), ...
                               double(reg_prop(i_reg).quant.sum),double(reg_prop(i_reg).quant.pix), ...
                               double(reg_prop(i_reg).quant_fit_summary_all), double(OPB_curves)];
           
           %- String to define output    
           N_col = size(quant_output,2);
           string_print = [repmat('%g\t',1,N_col-1),'%g\n'];     
                
           %- Write output        
           fprintf(fid,'%s\n', 'QUANTIFICATION'); 
           fprintf(fid,header_row);
           fprintf(fid, string_print,quant_output');       
        end
    end
end
fclose(fid);
%cd(current_dir)


%% Make a copy of the file if the region name is TxSite
if strcmp(label,'TxSite')  && status_save_separate
    [ dum name_image] = fileparts(file_name_image);
    name_new = [name_image,'.txt'];
    name_new_full = fullfile(path_save,name_new);
    copyfile(file_name_full,name_new_full)
end