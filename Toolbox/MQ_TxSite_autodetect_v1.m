function reg_prop = MQ_TxSite_autodetect_v1(img_disp,parameters)

%= Extract parameters
int_th     = parameters.int_th;
conn       = parameters.conn;
min_dist   = parameters.min_dist;
crop_image = parameters.crop_image;
pixel_size = parameters.pixel_size;
reg_prop   = parameters.reg_prop;

crop_xy_pix = ceil(crop_image.xy_nm / pixel_size.xy);
crop_z_pix  = ceil(crop_image.z_nm / pixel_size.z);

N_reg         = length(reg_prop);
[dim_Y dim_X] = size(img_disp);


%= Connected components
img_bin = img_disp>int_th;
CC      = bwconncomp(img_bin,conn);
img_det = zeros(size(img_bin));


%= Position of sites
i_site_good = 1;
for i_site = 1: CC.NumObjects
         
    status_good = 1;
    
    ind_lin = CC.PixelIdxList{i_site};
    
    if length(ind_lin) > 1         
                
        img_det(ind_lin) = 1;
    
        [coord(i_site_good).Y coord(i_site_good).X coord(i_site_good).Z] = ind2sub(size(img_bin),ind_lin);
    
        coord(i_site_good).X_center = round(mean(coord(i_site_good).X));
        coord(i_site_good).Y_center = round(mean(coord(i_site_good).Y));
        coord(i_site_good).Z_center = round(mean(coord(i_site_good).Z));
    
        coord_center(i_site_good,:) = [coord(i_site_good).X_center coord(i_site_good).Y_center coord(i_site_good).Z_center];
        site_summary(i_site_good,:) = [i_site,img_disp(coord(i_site_good).Y_center, coord(i_site_good).X_center, coord(i_site_good).Z_center)];
        i_site_good = i_site_good +1;
    end
end

N_sites_good = i_site_good-1;


%== Avoid clusters by calculating pairwise distance

%- Pairwise distance
dist_center = pdist(coord_center,'euclidean');
dist_center = squareform(dist_center);

%- Distance > 0 (self-distance) and smaller than certain threshold
[ind_row,ind_col] = find(dist_center < min_dist & dist_center > 0 );
ind_pair = find(ind_col > ind_row); % Matrix is symmetric - take only one value 

ind_good = (1:N_sites_good)';
for i = 1:length(ind_pair)
    
   %- Get intensity of respective site
   ind_1st =  ind_row(ind_pair(i));
   ind_2nd =  ind_col(ind_pair(i));
   
   INT_1st = site_summary(ind_1st,2);
   INT_2nd = site_summary(ind_2nd,2);
  
   %- Delete site with smaller intensity from list 
   if INT_1st > INT_2nd       
       ind_delete = find(ind_good == ind_2nd);
   else
       ind_delete = find(ind_good == ind_1st);
   end
    
   if not(isempty(ind_delete))
       ind_good(ind_delete) = [];
   end
       
end

%== See to which cell site belongs
ind_new_site = N_reg+1;
for i = 1: length(ind_good)    
    
    i_site = ind_good(i);
    
    x_min = round(coord(i_site).X_center - crop_xy_pix);
    x_max = round(coord(i_site).X_center + crop_xy_pix);
    
    y_min = round(coord(i_site).Y_center - crop_xy_pix);
    y_max = round(coord(i_site).Y_center + crop_xy_pix);    

    z_min = round(coord(i_site).Z_center - crop_z_pix);
    z_max = round(coord(i_site).Z_center + crop_z_pix);        

    reg_prop(ind_new_site).label = ['TxSite_auto_',num2str(i)];
    reg_prop(ind_new_site).x = [x_min x_max x_max x_min];
    reg_prop(ind_new_site).y = [y_min y_min y_max y_max];
    reg_prop(ind_new_site).pos = [];
    reg_prop(ind_new_site).flag_BGD = 0;
    reg_prop(ind_new_site).flag_OPB = 0;
    reg_prop(ind_new_site).reg_type = 'Polygon';
    reg_prop(ind_new_site).reg_pos = [];
    reg_prop(ind_new_site).status_filtered = 0;
    reg_prop(ind_new_site).status_image = 1;
    reg_prop(ind_new_site).status_detect = 0;
    reg_prop(ind_new_site).status_fit = 0;
    
    ind_new_site = ind_new_site +1;
end