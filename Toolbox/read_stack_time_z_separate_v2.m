function [img_stack, img_MIP_xy, path_name, name_base] = read_stack_time_z_separate_v2()

% Function to read time-series of z-stacks. Files have to be save in the
% same folder and differ in number which is at the end of the file-name. 

img_stack  = [];
img_MIP_xy = [];
path_name  = [];
name_base  = [];

par_load.flag.bfopen = 1;

%% [1.a] Specify file names
[file_name_1st,path_name] = uigetfile({'*.tif';'*.TIF';'*.STK';'*.stk'},'Select first file of stack','MultiSelect','off');


if file_name_1st ~= 0

    [pathstr, name, ext_img]  = fileparts(fullfile(path_name,file_name_1st));


    %== [1.b] Define image sequence

    %- Get number of files in folder
    file_list       = dir(path_name);
    n_Files_default = 0;
    for i=1:size(file_list,1)

        [dum, dum, ext_loop]  = fileparts(fullfile(path_name, file_list(i).name));   

        %- Consider only files withe the same ending 
        if strcmp(ext_img,ext_loop)
            n_Files_default = n_Files_default +1;
        end
    end

    %- Find base of name
    index_first    = sprintf('%0*d',0,1);
    ind_first_name = strfind(name,num2str(index_first));
    ind_first_name = ind_first_name(end);
    name_base_def  = name(1:ind_first_name-1);

    %- Ask user to specify data
    dlg_title  = ['Define data - ' file_name_1st];
    num_lines  = 1;
    prompt     = { 'Number of images', ...
                   'Starting image',...
                   'Increment',...
                   'Padding zeros (0 for no otherwise indicated total length of number)', ...
                   'Base name of image (underscore might not show up): '};
    def_values  = {num2str(n_Files_default),'1','1','0',name_base_def};
    user_values = inputdlg(prompt,dlg_title,num_lines,def_values);

    if not(isempty(user_values))
        N_Files     = round(str2double(user_values{1}));
        ind_start   = round(str2double(user_values{2}));
        ind_incr    = round(str2double(user_values{3}));
        N_lead_zero = round(str2double(user_values{4}));
        name_base   = user_values{5};    

        %=== [1.c] Load entire stack and calculate maximum projection
        img_stack = {};
        counter   = 1;

        disp('Reading files. Please wait ....')
        
        %- Loop over files
        N_load   = length(ind_start:ind_incr:N_Files);
        
        %- Preallocate structure
        img_stack = struct('ind', [], 'name', [], 'data', [],'MIP_xy',[]);
        img_stack(N_load).ind = N_Files;
           
        img_first     = load_stack_data_v7(fullfile(path_name,file_name_1st),par_load);
        max_proj_xy   = max(img_first.data,[],3);
        [dim_Y dim_X] = size(max_proj_xy);
        img_MIP_xy    = zeros(dim_Y, dim_X,N_load);
        
        %- Keep track of what you read in
        fprintf('Loading image: frame (of %d):     1',N_load);
        ind_plot = 1;
        
        for ind = ind_start:ind_incr:N_Files
            
           % Show were you are
           fprintf('\b\b\b\b%4i',ind_plot);
           ind_plot = ind_plot+1;

           % Add padding zero
           ind_loop  = sprintf('%0*d',N_lead_zero,ind);
           name_loop = [name_base,num2str(ind_loop),ext_img]; 

           img_loop    = load_stack_data_v6(fullfile(path_name,name_loop),par_load);
           max_proj_xy = max(img_loop.data,[],3);

           %- Save results
           img_stack(counter).ind    = ind_loop;
           img_stack(counter).name   = name_loop;
           img_stack(counter).data   = img_loop.data;  
           img_stack(counter).MIP_xy = max_proj_xy;

           img_MIP_xy(:,:,counter)   = max_proj_xy;

           counter = counter+1;

        end
        fprintf('\n');
        
        disp('Reading files. FINSIHED.') 
                
    end
end    

