%== Function to analyze detected regions
function handles = MQ_GUI_analyze_reg_prop(handles)

reg_prop = handles.reg_prop;

%- Populate pop-up menu with labels of cells
N_reg = size(reg_prop,2);

if N_reg > 0

    %- Call pop-up function to show results and bring values into GUI
    for ind_reg = 1:N_reg
        
        %== Check if one region is defined as background
        flag_OPB = reg_prop(ind_reg).flag_OPB;
        if flag_OPB
            handles.flag_OPB = ind_reg;
        end
        
        %== Check if one region is defined as background
        flag_BGD = reg_prop(ind_reg).flag_BGD;
        if flag_BGD
               handles.flag_BGD = ind_reg;
        end
                        
        %=== Name of region
        handles.str_menu{ind_reg,1} = reg_prop(ind_reg).label;
        
        %== Get information about size of region
        dim.x_pix = max(reg_prop(ind_reg).x) - min(reg_prop(ind_reg).x);
        dim.y_pix = max(reg_prop(ind_reg).y) - min(reg_prop(ind_reg).y);
        
        dim.x_nm = dim.x_pix * handles.par_microscope.pixel_size.xy;
        dim.y_nm = dim.x_pix * handles.par_microscope.pixel_size.xy;
        
        dim.x_min = min(reg_prop(ind_reg).x);
        dim.x_max = max(reg_prop(ind_reg).x);
        
        dim.x_min_nm = dim.x_min * handles.par_microscope.pixel_size.xy;
        dim.x_max_nm = dim.x_max * handles.par_microscope.pixel_size.xy;
        
        dim.y_min = min(reg_prop(ind_reg).y);
        dim.y_max = max(reg_prop(ind_reg).y);
        
        dim.y_min_nm = dim.y_min * handles.par_microscope.pixel_size.xy;
        dim.y_max_nm = dim.y_max * handles.par_microscope.pixel_size.xy;
         
        %- Save information
        reg_prop(ind_reg).dim = dim;
        
        %== Get binary mask
  
        x = handles.reg_prop(ind_reg).x;
        y = handles.reg_prop(ind_reg).y;
        
        m = handles.dim.Y; 
        n = handles.dim.X;         
        
        BW_2D = poly2mask(x, y, m, n);
        BW_3D = repmat(BW_2D,[1,1,handles.dim.Z]);
        
        %- Assing parameters
        reg_prop(ind_reg).BW_2D = BW_2D;
        reg_prop(ind_reg).BW_3D = BW_3D;
        
    end  
else
    handles.str_menu = {' '};
end

handles.reg_prop = reg_prop;