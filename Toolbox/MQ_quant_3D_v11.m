function reg_prop = MQ_quant_3D_v11(reg_prop,parameters)

global MQ_img_stack


warning('off')

%% Parameters
par_microscope = parameters.par_microscope;
flag_struct    = parameters.flag_struct;
options_quant  = parameters.options_quant;
options_fit    = parameters.options_fit;             

pixel_size     = par_microscope.pixel_size;
fit_limits     = parameters.options_fit.fit_limits;
fit_pos_restrict = options_quant.flag_fit_pos_restrict;
N_pix_sum        = options_quant.N_pix_sum;


%- Which image to fit
if ~isfield(flag_struct,'fit_data')
    flag_fit_data = 'OPB corr - bgd sub';
else 
    flag_fit_data = flag_struct.fit_data;
end
%% Extract parameters for quantification
reg_detect       = options_quant.reg_detect;

reg_detect.xy_nm = reg_detect.xy * par_microscope.pixel_size.xy;     
reg_detect.z_nm  = reg_detect.z  * par_microscope.pixel_size.z;


%% Regions for numerical integration
x_int.min = 0 - reg_detect.xy_nm;
x_int.max = 0 + reg_detect.xy_nm;

y_int.min = 0 - reg_detect.xy_nm;
y_int.max = 0 + reg_detect.xy_nm;

z_int.min = 0 - reg_detect.z_nm;
z_int.max = 0 + reg_detect.z_nm;


%% Analyze MQ_img_stack and regions
N_slice = length(MQ_img_stack);
N_reg   = length(reg_prop);

[dim.Y, dim.X, dim.Z] = size(MQ_img_stack(1).data);


%% Get vector with time-points
dT    = par_microscope.dT;

tList = (0:N_slice-1)*dT;
tList = tList';

    
%% Loop over all images  
fprintf('Quantifying frames: (of %d):     1',N_slice);

%- Pre-allocate size of reg_quant
reg_quant(N_reg).summary_quant(N_slice).mean = [];

%- Loop
for i_slice = 1:N_slice	
    
    fprintf('\b\b\b\b%4i',i_slice);
  
    %=== Image data for TS quant: quantifications for OPB done on raw image.
    if flag_struct.fit_TS
        
        switch flag_fit_data
            
            case  'OPB corr - bgd sub'
                img_loop = double(MQ_img_stack(i_slice).opb_bgd_sub);  
                
            case 'Raw - bgd sub'
                img_loop = double(MQ_img_stack(i_slice).raw_bgd_sub); 
                
            case 'Raw'
                img_loop = double(MQ_img_stack(i_slice).data); 
        end
     
    end

    
    %=== Loop over all regions
    for i_reg = 1:N_reg   
        
        %== Analyze TxSite and region for background intensity differently
        
        %-- Region is OPB or BGD region - will be measured on raw image
        if reg_prop(i_reg).flag_OPB == 1 || reg_prop(i_reg).flag_BGD == 1
                        
            %- MIP in 2D
            MIP_xy = max(MQ_img_stack(i_slice).data,[],3);
            BW_2D  = reg_prop(i_reg).BW_2D;
            int_region = double(MIP_xy(logical(BW_2D)));
            
            %- Average pixel intensity 
            reg_prop(i_reg).int_avg(i_slice,1) = mean(int_region(:));   
            reg_prop(i_reg).int_std(i_slice,1) = std(int_region(:));  
            
            %- Quantify intensity in image after PB correction
            if flag_struct.fit_TS
                
                loop_MIP = max(MQ_img_stack(i_slice).opb_bgd_sub,[],3);              
                int_region = double(loop_MIP(logical(BW_2D)));
                
                reg_prop(i_reg).int_avg_OPB(i_slice,1) = mean(int_region(:));  
            end
            
        %-- Region is a TxSite
        else
            
            if flag_struct.fit_TS
                     
                %==== Determine sub-region
                pos   = reg_prop(i_reg).pos;

                pos_Y = pos(i_slice).y;
                pos_X = pos(i_slice).x;
                pos_Z = pos(i_slice).z;
 
                      
                %- Limits of subregion
                x_min = pos_X-reg_detect.xy; if x_min < 1;      x_min = 1;     end
                x_max = pos_X+reg_detect.xy; if x_max > dim.X ; x_max = dim.X; end 

                y_min = pos_Y-reg_detect.xy; if y_min < 1;      y_min = 1;     end
                y_max = pos_Y+reg_detect.xy; if y_max > dim.Y ; y_max = dim.Y; end

                z_min = pos_Z-reg_detect.z; if z_min < 1;      z_min = 1;     end
                z_max = pos_Z+reg_detect.z; if z_max > dim.Z ; z_max = dim.Z; end

                %- Use either raw image of after background subtraction
                img_crop     = double(img_loop(y_min:y_max,x_min:x_max,z_min:z_max));            

                
                %==== Fitting with 3D Gaussian
                if flag_struct.fit_TS_Gauss 
                    
                    %- Prepare vectors describing grid
                    [dim_crop.Y, dim_crop.X, dim_crop.Z] = size(img_crop);
                    N_pix = dim_crop.X*dim_crop.Y*dim_crop.Z;

                    axis_par.X_pix = (0:dim_crop.X-1);
                    axis_par.Y_pix = (0:dim_crop.Y-1);
                    axis_par.Z_pix = (0:dim_crop.Z-1);

                    axis_par.X_nm  = axis_par.X_pix*pixel_size.xy;
                    axis_par.Y_nm  = axis_par.Y_pix*pixel_size.xy;
                    axis_par.Z_nm  = axis_par.Z_pix*pixel_size.z;

                    [Xs,Ys,Zs] = meshgrid(axis_par.Y_pix,axis_par.X_pix,axis_par.Z_pix);
                    X1         = reshape(Xs,1,N_pix);
                    Y1         = reshape(Ys,1,N_pix);
                    Z1         = reshape(Zs,1,N_pix);

                    xdata = [];
                    xdata(1,:) = double(X1.*pixel_size.xy);
                    xdata(2,:) = double(Y1.*pixel_size.xy);
                    xdata(3,:) = double(Z1.*pixel_size.z);        


                    %=== Fit with 3D Gaussian      

                    %- Boundaries    
                    switch options_fit.fit_mode
                        case 'sigma_free_xz'
                            options_fit.par_start = [];

                            img_crop_max = max(img_crop(:));

                            if isempty(reg_prop(i_reg).fit_limits)
                                limits = fit_limits;
                            else
                                limits = reg_prop(i_reg).fit_limits;
                            end                    

                            %- Restrict (or not) position of center
                            if fit_pos_restrict

                                fact_rest = fit_pos_restrict;

                                x_min_fit = mean(axis_par.X_nm) - fact_rest*pixel_size.xy;
                                x_max_fit = mean(axis_par.X_nm) + fact_rest*pixel_size.xy;

                                y_min_fit = mean(axis_par.Y_nm) - fact_rest*pixel_size.xy;
                                y_max_fit = mean(axis_par.Y_nm) + fact_rest*pixel_size.xy;

                                z_min_fit = mean(axis_par.Z_nm) - fact_rest*pixel_size.z;
                                z_max_fit = mean(axis_par.Z_nm) + fact_rest*pixel_size.z;
                            else

                                x_min_fit = 0;
                                x_max_fit = max(axis_par.X_nm);

                                y_min_fit = 0;
                                y_max_fit = max(axis_par.Y_nm);

                                z_min_fit = 0;
                                z_max_fit = max(axis_par.Z_nm);
                            end

                           %- Avoid identical limits by increasing the upper one by a little
                           if limits.sigma_xy_max == limits.sigma_xy_min
                               limits.sigma_xy_max = limits.sigma_xy_max + 0.1;
                           end

                           %- Avoid identical limits by increasing the upper one by a little
                           if limits.sigma_z_max == limits.sigma_z_min
                               limits.sigma_z_max = limits.sigma_z_max + 0.1;
                           end                       

                           %- Avoid identical limits by increasing the upper one by a little
                           if limits.bgd_max == limits.bgd_min
                               limits.bgd_max = limits.bgd_max + 0.1;
                           end                         

                           %- Assign range
                           options_fit.bound.lb = [limits.sigma_xy_min  limits.sigma_z_min y_min_fit x_min_fit z_min_fit              0  limits.bgd_min];  
                           options_fit.bound.ub = [limits.sigma_xy_max  limits.sigma_z_max y_max_fit x_max_fit z_max_fit 2*img_crop_max  limits.bgd_max];

                    end

                    FIT_Result = psf_fit_3d_v7(img_crop,xdata,options_fit,flag_struct);
                    FIT_Result.axis_par  = axis_par;        
                    FIT_Result.dim_crop  = dim_crop;

                    %- Consider cropping in routine
                    spot_fit_result.mu_X_nm = FIT_Result.muX + x_min*pixel_size.xy;
                    spot_fit_result.mu_Y_nm = FIT_Result.muY + y_min*pixel_size.xy; 
                    spot_fit_result.mu_Z_nm = FIT_Result.muZ + z_min*pixel_size.z;

                    spot_fit_result.mu_X_px = round(spot_fit_result.mu_X_nm/pixel_size.xy);
                    spot_fit_result.mu_Y_px = round(spot_fit_result.mu_Y_nm/pixel_size.xy);
                    spot_fit_result.mu_Z_px = round(spot_fit_result.mu_Z_nm/pixel_size.z);        

                    %=== Integrated intensity of spot
                    par_mod_int(1)  = FIT_Result.sigmaX;
                    par_mod_int(2)  = FIT_Result.sigmaY;
                    par_mod_int(3)  = FIT_Result.sigmaZ;

                    par_mod_int(4)  = 0;
                    par_mod_int(5)  = 0;
                    par_mod_int(6)  = 0;

                    par_mod_int(7)  = FIT_Result.amp ;
                    par_mod_int(8)  = 0 ;

                    int_spot.gauss_integrate = fun_Gaussian_3D_triple_integral_v1(x_int,y_int,z_int,par_mod_int); 

                
                else
                    
                	FIT_Result.amp = 0;
                    FIT_Result.bgd = 0;
                    FIT_Result.sigmaX = 0;
                    FIT_Result.sigmaZ = 0;
                    spot_fit_result.mu_X_nm = 0;
                    spot_fit_result.mu_Y_nm = 0;
                    spot_fit_result.mu_Z_nm = 0;
                    FIT_Result.resnorm = 0;
                    FIT_Result.centroidX = 0;
                    FIT_Result.centroidY = 0;
                    FIT_Result.centroidZ = 0;

                    int_spot.gauss_integrate= 0;
       
                end
                
                
                %=== Sum of intensity sourrounding brightest pixel in NXY-by-NZ box

                %- Crop raw and filtered image in z
                img_loop_crop_XY = double(img_loop(y_min:y_max,x_min:x_max,:)); 

                %- Get maximum 
                [img_max.value, img_max.ind_lin]             = max(img_loop_crop_XY(:));
                [img_max.ind_y, img_max.ind_x, img_max.ind_z] = ind2sub(size(img_loop_crop_XY),img_max.ind_lin);
                
                x_sub.min = pos_X - N_pix_sum.xy;
                x_sub.max = pos_X + N_pix_sum.xy;

                y_sub.min = pos_Y - N_pix_sum.xy;
                y_sub.max = pos_Y + N_pix_sum.xy;

                z_sub.min = img_max.ind_z - N_pix_sum.z;
                z_sub.max = img_max.ind_z + N_pix_sum.z;

                
                %- Catch positions at edge of image
                 if x_sub.min < 1 
                    x_sub.min = 1;
                    x_sub.max = x_sub.max + abs(x_sub.min) + 1;                
                end

                if  x_sub.max > dim.X    
                    x_sub.max = dim.X;
                    x_sub.min = x_sub.min - (dim.X-x_sub.max);  
                end
                
                if  y_sub.min < 1 
                    y_sub.min = 1;
                    y_sub.max = y_sub.max + abs(y_sub.min) + 1;                
                end

                if  y_sub.max > dim.Y    
                    y_sub.max = dim.Y;
                    y_sub.min = y_sub.min  - (dim.Y-y_sub.max) ;  
                end
                
                if  z_sub.min < 1 
                    z_sub.min = 1;
                    z_sub.max = z_sub.max + abs(z_sub.min) + 1;                
                end

                if  z_sub.max > dim.Z    
                    z_sub.max = dim.Z;
                    z_sub.min = z_sub.min - (dim.Z-z_sub.max);  
                end

                %- Get intensity of actual image
                img_sub       = img_loop(y_sub.min:y_sub.max,x_sub.min:x_sub.max,z_sub.min:z_sub.max);
                int_spot.mean = mean(img_sub(:));    
                int_spot.sum  = sum(img_sub(:)); 
                
%                 %- Get intensity of background
%                 img_bgd_sub       = img.bgd(y_sub.min:y_sub.max,x_sub.min:x_sub.max,z_sub.min:z_sub.max);
%                 int_spot.bgd_mean = mean(img_bgd_sub(:));    
%                 int_spot.bgd_sum  = sum(img_bgd_sub(:));                

                %=== Save everything    
                reg_quant(i_reg).summary_fit(i_slice).val = [FIT_Result.amp FIT_Result.bgd FIT_Result.sigmaX FIT_Result.sigmaZ spot_fit_result.mu_X_nm spot_fit_result.mu_Y_nm spot_fit_result.mu_Z_nm FIT_Result.resnorm FIT_Result.centroidX FIT_Result.centroidY FIT_Result.centroidZ];

                reg_quant(i_reg).summary_quant(i_slice).fit      = [FIT_Result.amp    int_spot.gauss_integrate];
                reg_quant(i_reg).summary_quant(i_slice).mean     = [int_spot.mean];
                reg_quant(i_reg).summary_quant(i_slice).sum      = [int_spot.sum]; 
                reg_quant(i_reg).summary_quant(i_slice).pix      = [img_loop(pos_Y,pos_X,pos_Z) pos_Y pos_X  pos_Z ];  
                reg_quant(i_reg).summary_quant(i_slice).bgd_mean = 0;%int_spot.bgd_mean;
                reg_quant(i_reg).summary_quant(i_slice).bgd_sum  = 0;%int_spot.bgd_sum;            
            
            end
        end
    end
end

%=== Loop over all regions
if flag_struct.fit_TS
    for i_reg = 1:N_reg
        
        %- Assign time vector
        reg_prop(i_reg).time = tList;
        
        %- Not a OPB and a background region
        if reg_prop(i_reg).flag_OPB ~= 1 && reg_prop(i_reg).flag_BGD ~= 1


            quant_fit_summary_all = [];
            fit_amp = [];

            quant = [];

            for i_slice = 1:N_slice	

                %- Get values for slide
                summary_fit   = reg_quant(i_reg).summary_fit(i_slice).val;
                summary_quant = reg_quant(i_reg).summary_quant(i_slice);

                %- Assign to output parameters
                quant_fit_summary_all(i_slice,:) = summary_fit;
                
                fit_amp(i_slice,:)               = summary_fit(1);       % Amplitude for fitting, first column before correction for photobleaching, second column after correction for photobleaching
                quant.integrate_int(i_slice,:)   = summary_quant.fit(2); % Integrated intensity before and after correction for observational photobleaching
                quant.pix(i_slice,:)             = summary_quant.pix;
                quant.sum(i_slice,:)             = summary_quant.sum;
                quant.mean(i_slice,:)            = summary_quant.mean;
                quant.bgd_sum(i_slice,:)         = summary_quant.bgd_sum;
                quant.bgd_mean(i_slice,:)        = summary_quant.bgd_mean;                
                
            end

            %- Check if there are already fitting results
            if not(isfield(reg_prop(i_reg),'quant_fit_summary_all'))
                reg_prop(i_reg).quant_fit_summary_all_first = quant_fit_summary_all;
                reg_prop(i_reg).quant_fit_summary_all       = quant_fit_summary_all;
            else
                if not(isempty(reg_prop(i_reg).quant_fit_summary_all))
                    reg_prop(i_reg).quant_fit_summary_all       = quant_fit_summary_all;
                else
                    reg_prop(i_reg).quant_fit_summary_all_first = quant_fit_summary_all;
                    reg_prop(i_reg).quant_fit_summary_all       = quant_fit_summary_all;
                end
            end

            %- Save other fitting results
            reg_prop(i_reg).fit_amp  = fit_amp;  
            reg_prop(i_reg).quant    = quant;
        end
    end
end
fprintf('\n');
warning('on')