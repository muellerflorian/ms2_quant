function [file_save path_save] = MQ_settings_save_v2(file_name_full,handles)


%- Ask for file-name if it's not specified
if isempty(file_name_full)    
    
    if not(isempty(handles.file_name_image))
        [dum, name_file] = fileparts(handles.file_name_image); 
        file_name_default_spot = [name_file,'__settings.txt'];
    else
        file_name_default_spot = '_MQ_detection_settings.txt';
    end

    [file_save ,path_save] = uiputfile(file_name_default_spot,'File-name for detection settings');
    file_name_full         = fullfile(path_save,file_save);
    
else   
    %cd(handles.path_name_image)
    
    [dum, file_save,ext] = fileparts(file_name_full); 
    file_save            = [file_save,ext];
end


% Only write if FileName specified
if file_save ~= 0
               
    fid = fopen(fullfile(file_name_full),'w');
    
    %- Header 
    fprintf(fid,'FISH-QUANT\t%s\n', handles.version);
    fprintf(fid,'ANALYSIS SETTINGS %s \n', date);       
    
    %- Experimental parameters    
    fprintf(fid,'\n# EXPERIMENTAL PARAMETERS\n');
    fprintf(fid,'lambda_EM=%g\n',  handles.par_microscope.Em);
    fprintf(fid,'lambda_Ex=%g\n',  handles.par_microscope.Ex);
    fprintf(fid,'NA=%g\n',         handles.par_microscope.NA);
    fprintf(fid,'RI=%g\n',         handles.par_microscope.RI);
    fprintf(fid,'Microscope=%s\n', handles.par_microscope.type);
    fprintf(fid,'Pixel_XY=%g\n',   handles.par_microscope.pixel_size.xy);
    fprintf(fid,'Pixel_Z=%g\n',    handles.par_microscope.pixel_size.z);
    fprintf(fid,'dT=%g\n',         handles.par_microscope.dT);
    
    if isfield(handles,'PSF_theo')
        fprintf(fid,'PSF_THEO_XY=%g\n', handles.PSF_theo.xy_nm);
        fprintf(fid,'PSF_THEO_Z=%g\n',  handles.PSF_theo.z_nm);
    end
     
    %- Settings for subfolders
    fprintf(fid,'\n# SUB-Folders\n');
    fprintf(fid,'FOLDER_SUB_RESULTS=%s\n',      handles.path_sub_results);
    fprintf(fid,'FOLDER_SUB_RESULTS_USE=%g\n',  handles.path_sub_results_status);
    fprintf(fid,'FOLDER_SUB_REGIONS=%s\n',      handles.path_sub_regions);
    fprintf(fid,'FOLDER_SUB_REGIONS_USE=%g\n',  handles.path_sub_regions_status);    
   
    
    %- Settings for filtering
    fprintf(fid,'\n# FILTERING\n');
    if isfield(handles.filter, 'factor_bgd')
        fprintf(fid,'Kernel_bgd_xy=%g\n', handles.filter.factor_bgd);
        fprintf(fid,'Kernel_bgd_z=%g\n', handles.filter.factor_bgd);
        
        fprintf(fid,'Kernel_psf_xy=%g\n', handles.filter.factor_psf);
        fprintf(fid,'Kernel_psf_z=%g\n', handles.filter.factor_psf);
    else
        fprintf(fid,'Kernel_bgd_xy=%g\n', handles.filter.factor_bgd_xy);
        fprintf(fid,'Kernel_bgd_z=%g\n', handles.filter.factor_bgd_z);
        
        fprintf(fid,'Kernel_psf_xy=%g\n', handles.filter.factor_psf_xy);
        fprintf(fid,'Kernel_psf_z=%g\n', handles.filter.factor_psf_z);

    end
    
    %- Settings for OPB correction
    fprintf(fid,'\n# OPB CORRECTION\n');
    fprintf(fid,'OPB_fit_mode=%s\n', handles.opb_fit_mode);
     
    %- Settings for detection
    fprintf(fid,'\n# PRE-DETECTION\n');
    fprintf(fid,'OPT_DETECT_IMG=%s\n', handles.par_detect.flags.image);
    fprintf(fid,'OPT_DETECT_MODE=%s\n', handles.par_detect.flags.detect_mode);
    fprintf(fid,'OPT_DETECT_MIN_INT=%g\n', handles.par_detect.int_min);
    
    
    %- Settings for QUANTIFICATION
    fprintf(fid,'\n# QUANTIFICATION \n');
    fprintf(fid,'OPT_QUANT_REG_PIX_SUM_XY=%g\n', handles.options_quant.N_pix_sum.xy );
    fprintf(fid,'OPT_QUANT_REG_PIX_SUM_Z=%g\n',  handles.options_quant.N_pix_sum.z );
    fprintf(fid,'OPT_QUANT_REG_XY=%g\n',         handles.options_quant.reg_detect.xy );
    fprintf(fid,'OPT_QUANT_REG_Z=%g\n',          handles.options_quant.reg_detect.z );
    fprintf(fid,'OPT_QUANT_FLAG_BGD=%g\n',       handles.options_quant.flag_subtract_bgd);
    fprintf(fid,'OPT_QUANT_FLAG_DIM=%s\n',       handles.options_quant.flag_quant_dim );
    fprintf(fid,'OPT_QUANT_FLAG_POS_REST=%g\n',  handles.options_quant.flag_fit_pos_restrict );
      
    
    %-Limits for fit
    fprintf(fid,'\n# RANGE FOR FITTING \n');
   
    fprintf(fid,'LIMIT_sigma_xy_min=%g\n', handles.fit_limits.sigma_xy_min);
    fprintf(fid,'LIMIT_sigma_xy_max=%g\n', handles.fit_limits.sigma_xy_max);
    fprintf(fid,'LIMIT_sigma_z_min=%g\n',  handles.fit_limits.sigma_z_min);
    fprintf(fid,'LIMIT_sigma_z_max=%g\n',  handles.fit_limits.sigma_z_max);   
    fprintf(fid,'LIMIT_bgd_min=%g\n',      handles.fit_limits.bgd_min);
    fprintf(fid,'LIMIT_bgd_max=%g\n',      handles.fit_limits.bgd_max);     
        
    fprintf(fid,'LIMIT_DEF_sigma_xy_min=%g\n', handles.fit_limits.sigma_xy_min_def);
    fprintf(fid,'LIMIT_DEF_sigma_xy_max=%g\n', handles.fit_limits.sigma_xy_max_def);
    fprintf(fid,'LIMIT_DEF_sigma_z_min=%g\n',  handles.fit_limits.sigma_z_min_def);
    fprintf(fid,'LIMIT_DEF_sigma_z_max=%g\n',  handles.fit_limits.sigma_z_max_def);   
    fprintf(fid,'LIMIT_DEF_bgd_min=%g\n',      handles.fit_limits.bgd_min_def);
    fprintf(fid,'LIMIT_DEF_bgd_max=%g\n',      handles.fit_limits.bgd_max_def);   
    
    fprintf(fid,'LIMIT_PREV_sigma_xy_min=%g\n', handles.fit_limits.sigma_xy_min_prev);
    fprintf(fid,'LIMIT_PREV_sigma_xy_max=%g\n', handles.fit_limits.sigma_xy_max_prev);
    fprintf(fid,'LIMIT_PREV_sigma_z_min=%g\n',  handles.fit_limits.sigma_z_min_prev);
    fprintf(fid,'LIMIT_PREV_sigma_z_max=%g\n',  handles.fit_limits.sigma_z_max_prev);   
    fprintf(fid,'LIMIT_PREV_bgd_min=%g\n',      handles.fit_limits.bgd_min_prev);
    fprintf(fid,'LIMIT_PREV_bgd_max=%g\n',      handles.fit_limits.bgd_max_prev); 
    
    fclose(fid);
end