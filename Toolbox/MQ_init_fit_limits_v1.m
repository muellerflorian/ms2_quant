function handles = MQ_init_fit_limits_v1(handles)

%- Quantification 
handles.fit_limits.sigma_xy_min = 0;
handles.fit_limits.sigma_xy_max = round(5*handles.PSF_theo.xy_nm);

handles.fit_limits.sigma_z_min = 0;
handles.fit_limits.sigma_z_max = round(5*handles.PSF_theo.z_nm);

handles.fit_limits.bgd_min = 0;
handles.fit_limits.bgd_max = inf;

%- Default
handles.fit_limits.sigma_xy_min_def = handles.fit_limits.sigma_xy_min;
handles.fit_limits.sigma_xy_max_def = handles.fit_limits.sigma_xy_max;

handles.fit_limits.sigma_z_min_def = handles.fit_limits.sigma_z_min;
handles.fit_limits.sigma_z_max_def = handles.fit_limits.sigma_z_max;

handles.fit_limits.bgd_min_def = handles.fit_limits.bgd_min;
handles.fit_limits.bgd_max_def = handles.fit_limits.bgd_max;

%- Previous analysis
handles.fit_limits.sigma_xy_min_prev = handles.fit_limits.sigma_xy_min;
handles.fit_limits.sigma_xy_max_prev = handles.fit_limits.sigma_xy_max;

handles.fit_limits.sigma_z_min_prev = handles.fit_limits.sigma_z_min;
handles.fit_limits.sigma_z_max_prev = handles.fit_limits.sigma_z_max;

handles.fit_limits.bgd_min_prev = handles.fit_limits.bgd_min;
handles.fit_limits.bgd_max_prev = handles.fit_limits.bgd_max;