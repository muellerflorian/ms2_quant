function MQ_image_save_v1(name_save,img_type,choice_format,bit_depth)
%
% Function to save PSF as actual image. 
%
% Florian Mueller, muef@gmx.net
%
% === INPUT PARAMETER
% img       ... 3d array with actual image
% name_save ... Name to save file (path+file name). If empty user will be
%               asked to specify name
%
% === FLAGS
%
%
% === OUTPUT PARAMETERS
%
%
% === VERSION HISTORY
%
% v1 Feb 8,2011
% - Initial implementation


global MQ_img_stack


%- Get default parameters
if isempty(name_save)
    file_default                    = ['IMG_',datestr(date, 'yymmdd'),'.tif'];
    [file_name_save,path_name_save] = uiputfile(file_default,'Specify file name to save Image'); 

    name_save = fullfile(path_name_save,file_name_save);
else
    file_name_save = 1;
end

%- Number of input paramters 
if nargin < 4, 
    bit_depth = '16bit'; 
end



%- Generate folder to save individual z-stacks
switch choice_format        
            
    case 'Individual'
        
        %- Generate folders to save all results
        if ~exist(name_save,'dir')
            mkdir(name_save);
        end
end



%% Write image only if file name was specified


disp('Writing image .....')


if file_name_save ~= 0

    %- Number of stacks and Z-slices
    N_stack = length(MQ_img_stack);
    N_z     = size(MQ_img_stack(1).data,3);
    ind_plane = 1;
    
    if ~isfield(MQ_img_stack,'data_opb')
        flag_opb = 0;
    else
        flag_opb = 1;
    end
    
    %- Loop 
    for i_stack = 1:N_stack
        
        name_loop = ['IMG_', sprintf('%05d',i_stack),'.tif'];
        
        
        for i_z = 1:N_z
    
            if flag_opb
            
                %- Get image to write
                switch img_type

                    case 'RAW'
                       img_save  = MQ_img_stack(i_stack).data_opb(:,:,i_z);

                    case 'FILT'  
                        img_save  = MQ_img_stack(i_stack).data_filt(:,:,i_z);

                    case 'BGD_subtract'  
                        img_save  = MQ_img_stack(i_stack).data_opb(:,:,i_z); - MQ_img_stack(i_stack).bgd(:,:,i_z);

                    case 'BGD'
                         img_save  = MQ_img_stack(i_stack).bgd(:,:,i_z);
                end
            else
                img_save  = MQ_img_stack(i_stack).data(:,:,i_z);               
            end
            
            %- Convert image                       
            img_save = uint16(img_save);
            
            
            %- Decide how to write
            switch choice_format
                case 'One stack'          
 
                    %-Write image
                    if ind_plane == 1;
                        imwrite(img_save,name_save,'tif','Compression','none','WriteMode','overwrite')  
                    else
                        imwrite(img_save,name_save,'tif','Compression','none','WriteMode','append')   
                    end
                    ind_plane = ind_plane +1;
            
            
                case 'Individual'
                    
                    name_save_full = fullfile(name_save,name_loop);
                    
                    %-Write image
                    if i_z == 1;
                        imwrite(img_save,name_save_full,'tif','Compression','none','WriteMode','overwrite')  
                    else
                        imwrite(img_save,name_save_full,'tif','Compression','none','WriteMode','append')   
                    end
            end
        end
    end
end

disp('... finished!')
    
    
