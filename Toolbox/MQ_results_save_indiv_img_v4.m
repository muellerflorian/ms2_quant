function [file_save path_save] = MQ_results_save_indiv_img_v3(file_name_full,parameters)


quant_summ         = parameters.quant_summ;
site_label         = parameters.site_label;
par_microscope     = parameters.par_microscope;
path_name_image    = parameters.path_name_image;
file_name_image    = parameters.file_name_image;
file_name_settings = parameters.file_name_settings;
version            = parameters.version;
BGD_img            = parameters.BGD_img;
limits             = parameters.limits;


%- Change to correct folder
current_dir = pwd;
cd(path_name_image)


%- Data structure to write
cell_data  = num2cell(quant_summ);
cell_write = [site_label,cell_data];
cell_write = cell_write';  %- fprintf works on colums - data has to therefore be transformed 

N_col = size(quant_summ,2);
string_print = ['%s\t', repmat('%g\t',1,N_col-1),'%g\n']; 
  
%- Ask for file-name if it's not specified
if isempty(file_name_full)
    cd(path_name_image);

    %- Ask user for file-name for spot results
    [dum, name_file] = fileparts(file_name_image); 
    file_name_default_spot = [name_file,'__QUANT.txt'];

    [file_save,path_save] = uiputfile(file_name_default_spot,'Save results of TXSite quantification');
    file_name_full = fullfile(path_save,file_save);
    
    %- Ask user to specify comment
    prompt = {'Comment (cancel for no comment):'};
    dlg_title = 'User comment for file';
    num_lines = 1;
    def = {''};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
else   
    file_save = 1;
    path_save = fileparts(file_name_full); 
    answer = 'Batch detection';
end


% Only write if FileName specified
if file_save ~= 0
    
    fid = fopen(file_name_full,'w');
    
   %- Header 
    fprintf(fid,'MS2-QUANT\t%s\n', version);
    fprintf(fid,'Region DEFINITION, %s\n', date);
    fprintf(fid,'%s\t%s\n','COMMENT','Outline definition performed in MS2-QUANT (Main program)');   
  
    %- File Name
    fprintf(fid,'%s\t%s\n','FILE',file_name_image);
    fprintf(fid,'%s\t%s\n','PATH',path_name_image);
     
    %- Experimental parameters and analysis settings
    fprintf(fid,'PARAMETERS\n');
    fprintf(fid,'Pix-XY\tPix-Z\tRI\tEx\tEm\tNA\tType\n');
    fprintf(fid,'%g\t%g\t%g\t%g\t%g\t%g\t%s\n', par_microscope.pixel_size.xy, par_microscope.pixel_size.z, par_microscope.RI, par_microscope.Ex, par_microscope.Em,par_microscope.NA, par_microscope.type );
    
    fprintf(fid,'ANALYSIS-SETTINGS \t%s\n', file_name_settings);   
    fprintf(fid,'IMAGING-BACKGROUND\t%g\n', BGD_img);
    
    fprintf(fid,'LIMIT-FIT-SIGMA-XY-MIN\t%g\n', limits.sigma_xy_min);    
    fprintf(fid,'LIMIT-FIT-SIGMA-XY-MAX\t%g\n', limits.sigma_xy_max);
    fprintf(fid,'LIMIT-FIT-SIGMA-Z-MIN\t%g\n',  limits.sigma_z_min);    
    fprintf(fid,'LIMIT-FIT-SIGMA-Z-MAX\t%g\n',  limits.sigma_z_max);    
    fprintf(fid,'LIMIT-FIT-BGD-MIN\t%g\n',      limits.bgd_min);    
    fprintf(fid,'LIMIT-FIT-BGD-MAX\t%g\n',      limits.bgd_max);    
          
    %- Quantification results
    fprintf(fid,'QUANTIFICATION RESULTS\n'); 
    fprintf(fid,'TS_NAME\tQ_Integrate\tQ_AMP\tQ_Sum_pix\tQ_MIP_int\tPix_Y\tPix_X\tPix_Z\tAMP\tBGD\tSigmaXY\tSigmaZ\tMuY\tMuX\tMuZ\tRES\tCent_Y\tCent_X\tCent_Z\tBGD_sum\tBGD_mean\n');
          
    %- Write to file
    fprintf(fid,string_print,cell_write{:}); 
    fclose(fid);
    
end

cd(current_dir)
