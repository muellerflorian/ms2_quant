function [file_save, path_save] = MQ_results_save_converted_v2(file_name_full,parameters)


version         = parameters.version;
data_raw        = parameters.data_raw;
data_converted  = parameters.data_converted;
time            = parameters.time;
header          = parameters.header;
file_name       = parameters.file_name;
path_name       = parameters.path_name;
path_name_settings       = parameters.path_name_settings;
file_name_settings       = parameters.file_name_settings;

file_save = 1;


% Only write if FileName specified
if file_save ~= 0
    
    fid = fopen(file_name_full,'w');
    
   %- Header 
    fprintf(fid,'MS2-QUANT\t%s\n', version);
    fprintf(fid,'Date, %s\n', date);
    fprintf(fid,'%s\n','MS2 quantification converted to nascent mRNA counts');   
  
    %- File Name
    fprintf(fid,'%s\t%s\n','File',file_name);
    fprintf(fid,'%s\t%s\n','Path',path_name);
    
    fprintf(fid,'%s\n', 'SETTINGS'); 
    fprintf(fid,'%s\t%s\n','File',file_name_settings);
    fprintf(fid,'%s\t%s\n','Path',path_name_settings); 
   
           
      %- Make header
      header_row =   'Time[s]';
      N_col_header = length(header);
      
      %- For converted quantifications
      for i =1:N_col_header
        new_text = header{i};
        new_text_add = [new_text,'_conv'];
        header_row = [header_row,'\t',new_text_add];
      end
      
       %- Raw data
       for i =1:N_col_header
           new_text = header{i}; 
           header_row = [header_row,'\t',new_text];
       end
       header_row = [header_row,'\n'];
        
       %- Output parameters
       quant_output = [time data_converted data_raw];
           
       %- String to define output    
       N_col = size(quant_output,2);
       string_print = [repmat('%.2f\t',1,N_col-1),'%.2f\n'];     
                
       %- Write output        
       fprintf(fid,'%s\n', 'QUANTIFICATION'); 
       fprintf(fid,header_row);
       fprintf(fid, string_print,quant_output');       

   fclose(fid);
end

%cd(current_dir)
