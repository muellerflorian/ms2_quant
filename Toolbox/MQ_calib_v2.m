function MQ_calib_v2

% Function to automatically calibrate MS2 quantification files

%% Some parameters

N_frames_LowIllum = 4;  % Number of low illumination stacks

%- Some basic parameters
flag_save_RF   = 1;     % Flag to indicate if special files for Rampfinder should be save (1-YES, 0-NO)
flag_save_plots = 1;    % Flag to indicate if plots should be saved or not (0 means not saving, numbers indicate the column of the calibration file that should be saved).

%- Names for out-put files
name_suffix_MQ          = '_CALIB';
name_suffix_RF          = '_CALIB_RF';
path_save_sub_MQ        = 'MS2_calibrated/calib_MQ';
path_save_sub_plots     = 'MS2_calibrated/calib_plots';
path_save_sub_RF_IntInt = 'MS2_calibrated/calib_RA_IntInt';
path_save_sub_RF_SumPix = 'MS2_calibrated/calib_RA_SumPix';

%- Other parameters: usually don't have to be changed
MS2_analyze_col          = (1:4); % Columns that will be analyzed in data 
header                   = {'Q_IntInt', 'Q_AMP',	'Q_Sum_pix'	, 'Q_MAX_int'}; % Headers for data
version                  = 'v0j'; % Version of MS2-QUANT
par_load.flags.data_type = 'time_course';  % File-type that will be loaded


%% Get file names to calibrate: time-course data
[file_list,path_name_list] = uigetfile({'*.txt'},'Select file(s) with MS2 time-course quantification.','MultiSelect', 'on');

%- If only one file is defined (or 0 for cancel)
if ~iscell(file_list)
    if file_list == 0
        return
    else
        dum =file_list; 
        file_list = {dum};
    end
end
N_file = length(file_list);



%% Get file name with calibration reference and load data
[file_name_ref,path_name_ref] = uigetfile({'*.txt'},'Select file with high-illumination calibration.','MultiSelect', 'off');

if file_name_ref == 0
    return
end

fid = fopen(fullfile(path_name_ref,file_name_ref));
C = textscan(fid, '%s %s %s %f32 %f32 %f32 %f32 %f32 %f32 %f32','delimiter', '\t','HeaderLines',3);
fclose(fid);

file_names_ref = C{1};
nascent_ref    = C{4};
N_file_ref     = length(nascent_ref);


%% Perform calibration

%- Loop over all movies
for i_file = 1:N_file
        
    disp(' ');
    disp(['- Processing file ', num2str(i_file), ' of ', num2str(N_file)]);
        
    %=== Load data (time-course)
    file_name_load = file_list{i_file};
    name_full      = fullfile(path_name_list,file_name_load); 
    
    disp(['File: ', file_name_load])
    disp(['Path: ', path_name_list])

    %== Check if file name is in calibration reference
    [dum, name_load] = fileparts(file_name_load);
    
    %== Find "pt" or "visit" in the file name
    
    %- THIS PART IS SPECIFIC FOR THE NAMING CONVENTION OF MONTPELLIER
    
     %- Check if there is a position indicator 
    [ind_pt_start, ind_pt_end]  = regexp(name_load, '_..._pt_');
    
    % Depending on the version of the software it might be _visit instead of _pt_
    if isempty(ind_pt_start) 
        
        [ind_visit_start, ind_visit_end] = regexp(name_load, '_..._visit_');  
        
        if ~isempty(ind_visit_start) 
            ind_pt_start = ind_visit;
            ind_pt_end   = ind_visit_end;
        end
    end
    
    
    % == Check if  "pt" or "visit" occurs in file-name
    if ~isempty(ind_pt_start)  
        ind_IDENT = ind_pt_start;    
        
    else
        
        %- Check if there are different cells mentioned
        ind_c = regexp(name_load, '_..._c\d');
        
        if ~isempty(ind_c)
            ind_IDENT = ind_c;
        else
        
            ind_txt = regexp(file_name_load, '_\d\d\d.txt');
            
            if ~isempty(ind_txt)
                ind_IDENT = ind_txt;
            else
                ind_IDENT = 0;
            end
        end 
    end
        
    
    %- Get specific part of file-name & generate search string for regexp
    if ind_IDENT
        name_specific = name_load(ind_IDENT:end);
        clear name_search
        name_search = [name_specific(1:3), '\d', name_specific(5:end)];  % \d is a search string for a regular expression that stands for any number [0-9]

        disp(['Specific part of name : ', name_specific])
        disp(['Search string (regexp): ', name_search])
    else
        disp('Could not find unique string to identify calibration reference.');
        disp(['Loaded name : ', name_load])
        disp('Will use entire name to look for calibration reference')
        name_search = name_load;
    end
    
    
    %- Find high-illumination calibration that fits
    index_ref = [];
    for i_ref = 1:N_file_ref
        file_ref_loop = file_names_ref{i_ref};
        dum = regexp(file_ref_loop, name_search);
        
        if not(isempty(dum))
            index_ref = [index_ref,i_ref];
            disp(['High-illumination calibration found: ', file_ref_loop])
        end
    end
 
    
    %- Calibrate if calibration reference can be found
    if not(isempty(index_ref))
        
        if length(index_ref) == 1
        
            %= Load MS2 quantification 
            data_MS2_all = MQ_results_load_v1(name_full,par_load);
            data_MS2_loop = data_MS2_all(1:end-N_frames_LowIllum,:);
            time          = data_MS2_loop(:,1);

            %= Load MS2 quantification of low illumination data
            data_MS2_low_ill_loop = data_MS2_all(end-N_frames_LowIllum + 1:end,:);

            %- Convert data
            data_raw = [];
            N_nasc   = [];

            for i_q = 1:length(MS2_analyze_col)

                %- Get MS2 data for a particular quantification
                i_MS2   = MS2_analyze_col(i_q) + 1; %- First column: time

                INT_MS2         = data_MS2_loop(:,i_MS2);
                INT_MS2_low_ill = data_MS2_low_ill_loop(:,i_MS2);


                %- Get calibration reference
                %  Could be extended to have different reference for different quantifications
                nascent_calib = nascent_ref(index_ref,1);

                %- Transformation parameters
                avg_final  = mean(INT_MS2_low_ill);

                MS2_mapped    =(INT_MS2-0) * (nascent_calib/avg_final);
                N_nasc(:,i_q)   = MS2_mapped; 
                data_raw(:,i_q) = INT_MS2;
            end

            %- Folder for saving
            folder_save =  fullfile(path_name_ref,path_save_sub_MQ);  
            is_dir = exist(folder_save,'dir'); 

            if is_dir == 0
                mkdir(folder_save)
            end

            %- File name for saving
            [aux name]     = fileparts(file_name_load);
            name_save      = [name,name_suffix_MQ,'.txt'];
            name_save_full = fullfile(folder_save,name_save);
            
            
            %- Plot one of the resulting calibration curves
            if flag_save_plots
                
                
                %- Folder for saving
                folder_save_plots =  fullfile(path_name_ref,path_save_sub_plots);  
                is_dir = exist(folder_save_plots,'dir'); 

                if is_dir == 0
                    mkdir(folder_save_plots)
                end
                
                %- Get data 
                i_MS2 = flag_save_plots + 1;
                INT_MS2     = data_raw(:,i_MS2);
                MS2_mapped  = N_nasc(:,i_MS2); 

                
                %- Generate plots
                h1 = figure; set(gcf,'visible','off')
                set(h1,'Color','w')

                subplot(1,2,1)
                plot(time,INT_MS2)
                xlabel('Time [s]')
                ylabel('MS2 intensity')
                title('Raw data')

                subplot(1,2,2)
                plot(time,MS2_mapped)
                xlabel('Time [s]')
                ylabel('Nascent transcripts')
                title('Calibrated data')                
                     
                %- Save plots
                filename_save      = [name,'_calibration'];
                filename_save_full = fullfile(folder_save_plots,filename_save);
                
                saveas(h1,filename_save_full,'png')  

                close(h1)
            end            
            

            %- Parameters for saving of calibration results
            parameters.version         = version;
            parameters.data_raw        = data_raw;
            parameters.data_converted  = N_nasc;
            parameters.time            = time;
            parameters.header          = header;
            parameters.file_name       = file_name_load;
            parameters.path_name       = path_name_ref;
            parameters.path_name_settings   = [];
            parameters.file_name_settings   = [];

            MQ_results_save_converted_v2(name_save_full,parameters);

          
            %- Save file for ramp finder
            if flag_save_RF


                %=== Integrated intensity

                %- Folder for saving
                folder_save =  fullfile(path_name_ref,path_save_sub_RF_IntInt);  
                is_dir = exist(folder_save,'dir'); 

                if is_dir == 0
                    mkdir(folder_save)
                end

                %- File name for saving
                [aux name]     = fileparts(file_name_load);
                name_save      = [name,name_suffix_RF,'.csv'];
                name_save_full = fullfile(folder_save,name_save);
                parameters.col_save = 1;
                parameters.col_text = 'IntInt';

                MQ_results_save_converted_RF_v3(name_save_full,parameters);


                %=== Sum of all pixels

                %- Folder for saving
                folder_save =  fullfile(path_name_ref,path_save_sub_RF_SumPix);  
                is_dir = exist(folder_save,'dir'); 

                if is_dir == 0
                    mkdir(folder_save)
                end

                %- File name for saving
                [aux name]     = fileparts(file_name_load);
                name_save      = [name,name_suffix_RF,'.csv'];
                name_save_full = fullfile(folder_save,name_save);
                parameters.col_save = 3;
                parameters.col_text = 'SumPix';

                MQ_results_save_converted_RF_v3(name_save_full,parameters);

            end

           disp('Calibration FINISHED'); 
     
        else
            disp('!!!!! NO UNIQUE CALIBRATION REFERENCE FOUND!!!!!')
        end
    else
        disp('!!!!! NO CALIBRATION REFERENCE FOUND!!!!!')
    end  
end