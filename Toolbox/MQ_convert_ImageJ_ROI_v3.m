function [reg_prop, path_sub_regions] = MQ_convert_ImageJ_ROI_v3(path_name,file_image,file_ROI,par_convert)
% Uses function ReadImageJROI: http://www.dylan-muir.com/articles/read_ij_roi/

%- If not ROI file is defined use default name
if isempty(file_ROI)
    file_ROI = 'RoiSet.zip';
end


%% Load ROI
cvsROIs = ReadImageJROI(fullfile(path_name,file_ROI));

%% Loop over ROIs
N_ROI = length(cvsROIs);

for i_ROI = 1:N_ROI
    reg_type = cvsROIs{i_ROI}.strType;
    reg_name = cvsROIs{i_ROI}.strName;
    
    
    %- Get geometry of region
    switch reg_type
        
        case 'Freehand'
            coordinates = cvsROIs{i_ROI}.mnCoordinates;
            
            x_pos  = coordinates(:,1)'; % Has to be row vector
            y_pos  = coordinates(:,2)';
            
    
        case 'Rectangle'
            rect_bound = cvsROIs{i_ROI}.vnRectBounds;
            
            x_min = rect_bound(2);
            x_max = rect_bound(4); 
            
            y_min = rect_bound(1);
            y_max = rect_bound(3);   
            
            
            x_pos  = [x_min x_max x_max x_min];
            y_pos  = [y_min y_min y_max y_max];
            
    end
       
    %- Get type of region
    switch reg_name
        case {'T','TxSite'}
            reg_type = 'TS';
            flag_OPB = 0;
            flag_BGD = 0;  
            label = 'TxSite';
            
        case {'C','CTRL'} 
            reg_type = 'TS';   
            flag_OPB = 0;
            flag_BGD = 0; 
            label = 'CTRL';
            
        case {'N','Nucleus'}   
            reg_type = 'OPB';
            flag_OPB = 1;
            flag_BGD = 0;
            label = 'NUCLEUS';

            
        case {'B','BGD'}   
            reg_type = 'BGD';
            flag_OPB = 0;
            flag_BGD = 1;             
            label = 'BGD';  
    end
    
    %- Save region properties
    reg_prop(i_ROI).reg_type = reg_type;
    reg_prop(i_ROI).label    = label;
    reg_prop(i_ROI).flag_OPB = flag_OPB;
    reg_prop(i_ROI).flag_BGD = flag_BGD;
    reg_prop(i_ROI).x = x_pos;
    reg_prop(i_ROI).y = y_pos;
    reg_prop(i_ROI).pos = [];
    
    reg_prop(i_ROI).fit_limits = {};
    reg_prop(i_ROI).quant_fit_summary_all = [];
    reg_prop(i_ROI).quant_fit_summary_all_first = []; 
end


%% Save region definition

%- Define name
[dum name_image] =  fileparts(file_image);
name_save_region = [name_image,par_convert.suffix];

%- Define structure to save
struct_save.version            = par_convert.version;
struct_save.par_microscope     = par_convert.par_microscope; 
struct_save.file_name_image    = file_image;
struct_save.path_name_image    = path_name;
struct_save.file_name_settings = [];
struct_save.BGD_img            = [];
struct_save.reg_prop           = reg_prop;
struct_save.path_sub_regions_status = 1;
struct_save.path_sub_regions   = 'MQ_results';

%- Save
MQ_save_region_v3(struct_save,name_save_region);
path_sub_regions = struct_save.path_sub_regions;
