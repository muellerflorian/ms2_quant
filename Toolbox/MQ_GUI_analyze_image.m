% === Function to analyze image
function handles = MQ_GUI_analyze_image(handles)
global  MQ_img_stack MQ_img_MIP

switch handles.img_type 
    
    case {'3D','3d'}

        %- Dimensions
        handles.N_slice = length(MQ_img_stack);
        [handles.img_dim.Y, handles.img_dim.X, handles.img_dim.Z] = size(MQ_img_stack(1).data);

        %- Analyze raw image
        handles.img_PLOT  =  max(MQ_img_MIP.raw,[],3); 
        handles.img_min   =  min(MQ_img_MIP.raw(:)); 
        handles.img_max   =  max(MQ_img_MIP.raw(:)); 
        handles.img_diff  =  handles.img_max-handles.img_min; 

        %- Analyze raw image - background subtracted
        handles.img_raw_bgd_PLOT  =  max(MQ_img_MIP.raw_bgd_sub,[],3); 
        handles.img_raw_bgd_min   =  min(MQ_img_MIP.raw_bgd_sub(:)); 
        handles.img_raw_bgd_max   =  max(MQ_img_MIP.raw_bgd_sub(:)); 
        handles.img_raw_bgd_diff  =  handles.img_raw_bgd_max-handles.img_raw_bgd_min; 

        %- Analyze pb-corrected image - background subtracted
        handles.img_opd_bgd_PLOT  =  max(MQ_img_MIP.opb_bgd_sub,[],3); 
        handles.img_opd_bgd_min   =  min(MQ_img_MIP.opb_bgd_sub(:)); 
        handles.img_opd_bgd_max   =  max(MQ_img_MIP.opb_bgd_sub(:)); 
        handles.img_opd_bgd_diff  =  handles.img_opd_bgd_max-handles.img_opd_bgd_min; 

        %- Analyze filtered image
        handles.img_filt_PLOT   =  max(MQ_img_MIP.filt,[],3); 
        handles.img_filt_min    =  min(MQ_img_MIP.filt(:)); 
        handles.img_filt_max    =  max(MQ_img_MIP.filt(:)); 
        handles.img_filt_diff   =  handles.img_filt_max-handles.img_filt_min; 

        %- Get first image in stack
        [dim.Y, dim.X, dim.Z] = size(MQ_img_stack(1).data);
        handles.dim           = dim;
      
end