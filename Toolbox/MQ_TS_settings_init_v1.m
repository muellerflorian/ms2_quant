function parameters_quant = MQ_TS_settings_init_v1

parameters_quant.crop_image.xy_nm      = 1500;
parameters_quant.crop_image.z_nm       = 1000;

%parameters_quant.int_th   = 10000;
parameters_quant.conn     = 8;
parameters_quant.min_dist = 10;         % Minimum distance between identified components