function MQ_batch_save_handles_v4(file_name_full,handles)

% Function to write handles structure of GUI to m-file
current_dir = pwd;

%== Ask for file-name if it's not specified
if isempty(file_name_full)
     
    %- Ask user for file-name   
    file_name_default =  ['_MQ_batch_preprocessing_', datestr(date,'yymmdd'), '.mat'];
    [file_save,path_save] = uiputfile(file_name_default,'Save results of analysis [mat file]');
    file_name_full = fullfile(path_save,file_save);
    
else   
    file_save = 1;
end


%==== Save data that will be used in MS2-QUANT 

if file_save ~= 0
    
    %- About image
    global MQ_img_stack MQ_img_MIP
    
    struct_save.par_microscope = handles.par_microscope;
    struct_save.MQ_img_stack   = MQ_img_stack;
    struct_save.MQ_img_MIP     = MQ_img_MIP;
    
    struct_save.img_type = handles.img_type;
    struct_save.img_type_orig = handles.img_type_orig;
    
    struct_save.file_name_image    = handles.file_name_image;
    struct_save.path_name_image    = handles.path_name_image;
    struct_save.folder_sub_results = handles.folder_sub_results; 
    
    %- Results of photobleaching correction
    struct_save.OPB.curves         = handles.OPB.curves;
    
    %- Regions
    struct_save.reg_prop = handles.reg_prop;
    
    %- Flags
    struct_save.flag_BGD = handles.flag_BGD;
    struct_save.flag_OPB = handles.flag_OPB;   
    struct_save.flag_opb_apply = handles.flag_opb_apply;
    
    %- Filtering
    struct_save.filter.factor_bgd_xy = str2double(get(handles.text_kernel_factor_bgd_xy,'String'));
    struct_save.filter.factor_bgd_z = str2double(get(handles.text_kernel_factor_bgd_z,'String'));
  
    struct_save.filter.factor_psf_xy = str2double(get(handles.text_kernel_factor_filter_xy,'String'));
    struct_save.filter.factor_psf_z = str2double(get(handles.text_kernel_factor_filter_z,'String'));
    
    
    struct_save.BGD_img = handles.BGD_img;
    
    %- Save handles
    eval('save(file_name_full,''struct_save'',''-v7.3'')')
end

cd(current_dir)