function MS2_analyze_calib_on_off_v7

%% SCRIPT TO calibrated MS2 quantification results


%% [1] Define files that should be analyzed
[file_name_outline,path_name_files]=uigetfile({'*.txt'},'Select CALIBRATED MS2 file(s).','MultiSelect','on');


%% [2]  Parameters for analysis

%- Determine image file-type for results
img_type = questdlg('In what format would you like to save the plots?', mfilename,'eps', 'png','png');

%- Specify minimum duration a movie must have to be considered; 
%   + In HOURS!!!
%   + Set to 0 to consider all curves
dur_min = 0;

%- Threshold for which cells are considered to be off
th_nascent  = 3;

%- Smoothing span
smooth_span = 8;

%- Minimum # of frames a ON/OFF state has to persist
N_frames_min = 5;

%- Number of bins to fit histograms
n_bins = 20;

%- Should plot with some results be generated
flag_plot = 0;

%- Should all analysis results be saved as a .mat file
flag_save = 1;

%% [3] Call function for analysis

data.type = 'calib';
data.file_name_outline = file_name_outline;
data.path_name_files = path_name_files;

par_analyze.img_type     = img_type;
par_analyze.dur_min      = dur_min;
par_analyze.th_quant     = th_nascent;
par_analyze.smooth_span  = smooth_span;
par_analyze.N_frames_min = N_frames_min;
par_analyze.n_bins       = n_bins;
par_analyze.flag_plot    = flag_plot;
par_analyze.flag_save    = flag_save;

 MQ_analyze_durations_v3(data,par_analyze)


