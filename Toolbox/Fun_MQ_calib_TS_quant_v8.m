function [TS_info, TS_summary_all, mRNA_info, mRNA_summary_all] = Fun_MQ_calib_TS_quant_v8(parameters)


%% Get parameters
file_name       = parameters.file_name;
folder          = parameters.folder;
flag_quant_mRNA = parameters.flag_quant_mRNA;
flag_PSF_indiv  = parameters.flag_PSF_indiv;
img             = parameters.img;

%% Go over file-names of results
results_list = file_name.results;

%= Make cell out of list of filenames if only one is defined
if ~iscell(results_list)
    dum =results_list; 
    results_list = {dum};
end
N_files = length(results_list);


%% Various parameters

%- Match averaging settings to TS-quant settings
img.settings.avg_spots.fact_os.xy = 1;
img.settings.avg_spots.fact_os.z = 1;
img.settings.avg_spots.crop.xy   = img.settings.TS_quant.crop_image.xy_pix;
img.settings.avg_spots.crop.z    = img.settings.TS_quant.crop_image.z_pix;

sett_avg      = img.settings.avg_spots;
sett_TS_quant = img.settings.TS_quant;


%% Loop over all files

ind_cell_all = 1; 
TS_counter   = 1;
mRNA_counter = 1;

TS_name_image  = [];
TS_name_cell   = [];
TS_name_TS     = []; 
TS_summary_all       = [];


folder_results = folder.results;
path_image     = folder.image; 

% == Loop over all files
for i_file =  1:N_files
    
    file_name      = results_list{i_file};
    file_name_full = fullfile(folder_results,file_name);

    
    %-- Create folder to save results if specified
    if parameters.flag_save

       [dum name_load] = fileparts(file_name);
       folder_new = fullfile(folder.results,['TS_QUANT_',datestr(date,'yymmdd')],name_load);               

       is_dir = exist(folder_new,'dir'); 

       if is_dir == 0
           mkdir(folder_new)
       end

       cd(folder_new)
    end
    
    
    % == Load region file and extract outlines of cells, and settings
    disp(' ')

    fprintf('====== Analysing file: %d of %d\n',i_file, N_files);
    disp(['Name : ', file_name])
    
    %- Reinit structure but save various settings
    img.reinit;
    
    %- Open new outline
    status_open = img.load_results(file_name_full,path_image);       

    if ~status_open.outline
        warndlg('OUTLINE could not be opened!','FISH-quant')
        fprintf('=== FILE COULD NOT BE OPENED\n');
        fprintf('File  : %s\n', file_name)
        fprintf('Folder: %s\n', folder_results)
        continue
    end
    
    
    if ~status_open.img
        warndlg('Image could not be opened!','FISH-quant')
        fprintf('=== FILE COULD NOT BE OPENED\n');
        fprintf('File  : %s\n', img.file_names.raw)
        fprintf('Folder: %s\n', path_image)
        continue
    end
    
    
    % == Load settings
    if isempty(img.file_names.settings)
        disp('NO quantification performed because settings file for mature mRNA detection could not be opened')
        continue
    end
    
    status_sett = img.load_settings(fullfile(folder_results,img.file_names.settings));
    
    if status_sett == 0
       disp('NO quantification performed because settings file for mature mRNA detection could not be opened')
       continue
    end

    %- Over-write settings for averaging and TS-quant
    img.settings.avg_spots = sett_avg;
    img.settings.TS_quant  = sett_TS_quant;

    % -- Check how many cells in image
    cell_prop = img.cell_prop;
    N_cell = length(cell_prop);
    
    % ==== Loop over cells
    for ind_cell = 1:N_cell

        disp(' ')
        disp([' ==== CELL ', num2str(ind_cell), ' of ', num2str(N_cell)]);

        % == Analyze detected mRNA in cell

        %- Don't continue if there are no mRNA molecules
        if isempty(cell_prop(ind_cell).spots_fit)
            disp('No spots in this cell.')

            %- Assign empty values such that cell is still listed
            TS_name_image{TS_counter,1}  = file_name;
            TS_name_cell{TS_counter,1}   = cell_prop(ind_cell).label;
            TS_name_TS{TS_counter,1}     = 'NO_spots';
            TS_summary_all(TS_counter,:) = [0 0 0 0 0 0 0];
            TS_counter = TS_counter +1;

            continue
        else

            %- Check if there are any transcription site
            pos_TS_all = cell_prop(ind_cell).pos_TS;
            N_TS       = length(pos_TS_all);

            %- Don't continue if there are no TS
            if N_TS == 0


                disp('No TS in this cell.')

                %- Assign empty values such that cell is still listed
                TS_name_image{TS_counter,1} = file_name;
                TS_name_cell{TS_counter,1}  = cell_prop(ind_cell).label;
                TS_name_TS{TS_counter,1}    = 'NO_TS';
                TS_counter = TS_counter +1;
                TS_summary_all(TS_counter,:) =  [0 0 0 0 0 0 0];

                continue

            else

                %==============================================================
                % -- FOR QUANTIFICATION WITH POOLED AVERAGE
                %==============================================================

                if flag_PSF_indiv

                    %=== Average spots                                     
                    img.avg_spots(ind_cell,[]);

                    %- Save resulting image
                    [dum name_base] = fileparts(img.file_names.raw);
                    name_avg  = [name_base,'__avg_RNA.tif'];
                    file_save = fullfile(folder_new,name_avg);
                    img.save_img(file_save,'avg_ns');
                    
                    
                    %=== Fit averaged spot
                    parameters_fit.flags.ns     = 1; 
                    parameters_fit.flags.output = 0;

                    %= [1] Crop image with size of detection region
                    %      Used to restrict fitting range to avoid outliers
                    parameters_fit.flags.crop  = 1;
                    parameters_fit.par_crop.xy = img.settings.detect.reg_size.xy; 
                    parameters_fit.par_crop.z  = img.settings.detect.reg_size.z; 
                  
                    img.avg_spot_fit(parameters_fit)
                    
                    %- Restrict fitting range
                    sigma_xy_min = 0.5 * img.spot_avg_fit_par.sigma_xy;
                    sigma_xy_max = 1.5 * img.spot_avg_fit_par.sigma_xy;

                    sigma_z_min = 0.5 * img.spot_avg_fit_par.sigma_z;
                    simga_z_max = 1.5 * img.spot_avg_fit_par.sigma_z;

                    parameters_fit.bound.lb = [sigma_xy_min sigma_z_min 0   0   0   0   0]; 
                    parameters_fit.bound.ub = [sigma_xy_max simga_z_max inf inf inf inf inf];
                    
                    
                    %- [2] Don't crop
                    parameters_fit.flags.crop  = 0;                  
                    img.avg_spot_fit(parameters_fit)
                    
                    mRNA_prop.sigma_xy           = img.spot_avg_fit_par.sigma_xy;
                    mRNA_prop.sigma_z            = img.spot_avg_fit_par.sigma_z;
                    mRNA_prop.amp_mean_fit_QUANT = img.spot_avg_fit_par.amp;
                    mRNA_prop.sum_pix            = 0;
                    mRNA_prop.bgd_value          = 0; 
                    
                    img.mRNA_prop = mRNA_prop;

                end


                %=== Loop over transcription sites
                for ind_TS = 1: N_TS

                    %- Get transcription site
                    disp(['+++ TxSite ', num2str(ind_TS), ' of ', num2str(N_TS)]);
                    pos_TS = pos_TS_all(ind_TS); 


                    % -- Fit with individual PSF
                    if flag_PSF_indiv

                        img.settings.TS_quant.file_name_save_STATUS   = '';
                        img.settings.TS_quant.file_name_save_PLOTS_PS = '';

                        if parameters.flag_save
                            img.settings.TS_quant.name_file  = file_name;
                            img.settings.TS_quant.name_cell  = cell_prop(ind_cell).label;
                            img.settings.TS_quant.name_TS    = pos_TS.label;            
                            img.settings.TS_quant.file_name_save_STATUS    = [cell_prop(ind_cell).label,'__',pos_TS.label , '__STATUS.txt'];
                            img.settings.TS_quant.file_name_save_PLOTS_PS  = [cell_prop(ind_cell).label,'__',pos_TS.label , '__PLOTS.PS'];
                            img.settings.TS_quant.file_name_save_PLOTS_PDF = [cell_prop(ind_cell).label,'__',pos_TS.label , '__PLOTS.PDF'];

                            img.settings.TS_quant.flags.output = 1;
                        end

                        if parameters.flag_output
                           img.settings.TS_quant.flags.output = 2; 
                        end

                        %- Call TS quantification
                        img.TS_quant(ind_cell,ind_TS);                        
                        TxSite_quant_indiv        = img.cell_prop(ind_cell).pos_TS(ind_TS).TxSite_quant;
                        TS_analysis_results_indiv = img.cell_prop(ind_cell).pos_TS(ind_TS).TS_analysis_results;
                                               
                        TS_summary_all(TS_counter,:) =  ...
                            [TxSite_quant_indiv.N_mRNA_integrated_int TxSite_quant_indiv.N_mRNA_fitted_amp TxSite_quant_indiv.N_mRNA_trad, ...
                            TS_analysis_results_indiv.TS_Fit_Result.sigma_xy TS_analysis_results_indiv.TS_Fit_Result.sigma_z, ...
                            TS_analysis_results_indiv.TS_Fit_Result.amp TS_analysis_results_indiv.TS_Fit_Result.bgd];

                    end

                    %== Save results
                    TS_name_image{TS_counter,1} = file_name;
                    TS_name_cell{TS_counter,1}  = cell_prop(ind_cell).label;
                    TS_name_TS{TS_counter,1}    = cell_prop(ind_cell).pos_TS(ind_TS).label;

                    TS_counter = TS_counter +1;
                end

                %==== Quantify individual mRNA' as if they were TS
                if flag_quant_mRNA
                    
                  ind_spots_in  = logical(cell_prop(ind_cell).thresh.in == 1);

                  spots_detected_all = cell_prop(ind_cell).spots_detected;
                  spots_detected_in  = spots_detected_all(ind_spots_in,:);
                    
                    
                    N_spots = size(spots_detected_in,1);
            
                    for i_spot = 1:N_spots

                        %- Quant mRNA
                        disp(['+ mRNA ', num2str(i_spot), ' of ', num2str(N_spots)]);

                        x_min = spots_detected_in(i_spot,2);
                        x_max = spots_detected_in(i_spot,2);
                        y_min = spots_detected_in(i_spot,1);
                        y_max = spots_detected_in(i_spot,1);

                        %- Make sure that only spots are considered that are within the image
                        z_pos = spots_detected_in(i_spot,3);                        
                
                        if (z_pos > img.settings.TS_quant.crop_image.z_pix) &&   (z_pos < img.dim.Z - img.settings.TS_quant.crop_image.z_pix + 1)

                            %- Make TS
                            pos_TS_loop.label = ['mRNA_', num2str(i_spot)];
                            pos_TS_loop.x     = [x_min x_min x_max x_max];
                            pos_TS_loop.y     = [y_min y_max y_max y_min];

                            img.cell_prop(ind_cell).pos_TS = pos_TS_loop;

                            % -- Fit with individual PSF
                            if flag_PSF_indiv
                                
                                %- Settings for TS-quant
                                img.settings.TS_quant.file_name_save_STATUS   = '';
                                img.settings.TS_quant.file_name_save_PLOTS_PS = '';
                                img.settings.TS_quant.flags.output            = 0;

                                %- Call TS quantification
                                img.TS_quant(ind_cell,1);                        
                                TxSite_quant_indiv        = img.cell_prop(ind_cell).pos_TS(ind_TS).TxSite_quant;
                                TS_analysis_results_indiv = img.cell_prop(ind_cell).pos_TS(ind_TS).TS_analysis_results;

                               mRNA_summary_all(mRNA_counter,:) =  ...
                                [TxSite_quant_indiv.N_mRNA_integrated_int TxSite_quant_indiv.N_mRNA_fitted_amp TxSite_quant_indiv.N_mRNA_trad, ...
                                TS_analysis_results_indiv.TS_Fit_Result.sigma_xy TS_analysis_results_indiv.TS_Fit_Result.sigma_z, ...
                                TS_analysis_results_indiv.TS_Fit_Result.amp TS_analysis_results_indiv.TS_Fit_Result.bgd];    
                            end

                            %- Save info
                            mRNA_name_image{mRNA_counter,1} = file_name;
                            mRNA_name_cell{mRNA_counter,1}  = cell_prop(ind_cell).label;
                            mRNA_name_TS{mRNA_counter,1}    = pos_TS_loop.label;

                            mRNA_counter = mRNA_counter +1;
                        else
                            disp('Spot too close to upper or lower plane to be quantified')
                        end
                    end
                end    
            end
        end

        %- Update some information
        ind_cell_all = ind_cell_all +1;

    end        

end


%% SAVE RESULTS OF mRNA QUANTIFICATION

if flag_quant_mRNA && mRNA_counter > 1
    
    %== [1]  Save files: POOLED PSF 
    if flag_PSF_indiv
       
        %- Save summary file to same folder: individual
        file_name_save = ['MQ_calib_mRNA_summary_INDIV_' ,  datestr(now, 'yymmdd'),'.txt'];  
        file_save_full = fullfile(parameters.folder.save,file_name_save);
    end
    
    
    %- Summarize all outputs
    cell_data    = num2cell(mRNA_summary_all);   

    cell_write_all = [mRNA_name_image,mRNA_name_cell,mRNA_name_TS,cell_data];
    cell_write_all = cell_write_all';

    fid = fopen(file_save_full,'w');

    %- Header    
    fprintf(fid,'FISH-QUANT\n');
    fprintf(fid,'Analysis of detected transcription sites %s \n', date);
    fprintf(fid,'Name_File\tName_Cell\tName_TxSite\tN_IntInt\tN_AMP\tN_MaxInt\tsimga-xy\tsimga-z\tAMP\tBGD\n');        

    fprintf(fid,'%s\t%s\t%s\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n', cell_write_all{:});

    fclose(fid);
    
    %- Return parameter for file-names and stuff
    mRNA_info = [mRNA_name_image,mRNA_name_cell,mRNA_name_TS];
 
else
    %- Return parameter for file-names and stuff
    mRNA_info        = '';
    mRNA_summary_all = '';
    
end



%% Check if any TS was quantified

if TS_counter == 1
    disp('=== NO TS quantified. Are you sure that there are TS in the data?')
    return
end


%% Save files: Settings

file_name_save = ['MQ_calib_TS_settings_' ,  datestr(now, 'yyyymmdd'),'.txt'];  
file_save_full = fullfile(parameters.folder.save,file_name_save);

img.mRNA_prop.path_name = name_avg;
img.mRNA_prop.file_name = folder_new;

img.save_settings_TS(file_save_full);


%% SAVE RESULTS OF TS QUANTIFICATION

%== [1]  Save files: POOLED PSF 
if flag_PSF_indiv

    %- Save summary file to same folder: individual
    file_name_save = ['MQ_calib_TS_summary_INDIV_' ,  datestr(now, 'yymmdd'),'.txt'];  
    file_save_full = fullfile(parameters.folder.save,file_name_save);

end

%- Summarize all outputs
cell_data    = num2cell(TS_summary_all);   

cell_write_all = [TS_name_image,TS_name_cell,TS_name_TS,cell_data];
cell_write_all = cell_write_all';

fid = fopen(file_save_full,'w');

%- Header    
fprintf(fid,'FISH-QUANT\n');
fprintf(fid,'Analysis of detected transcription sites %s \n', date);
fprintf(fid,'Name_File\tName_Cell\tName_TxSite\tN_IntInt\tN_AMP\tN_MaxInt\tsimga-xy\tsimga-z\tAMP\tBGD\n');        

fprintf(fid,'%s\t%s\t%s\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n', cell_write_all{:});

fclose(fid);

%- Return parameter for file-names and stuff
TS_info = [TS_name_image,TS_name_cell,TS_name_TS];




