function [handles_GUI, path_load] = MQ_batch_load_handles_v5(handles_GUI)
% Function to read analysis results from .mat file

    
%- Ask user for file-name for spot results
[file_load,path_load] =  uigetfile('*.mat','Results of analysis [.mat file]');

if file_load ~= 0

    load(fullfile(path_load,file_load));

    %- Images
    global MQ_img_stack MQ_img_MIP
    MQ_img_stack = struct_save.MQ_img_stack;
    MQ_img_MIP   = struct_save.MQ_img_MIP;

    %- General parameters
    handles_GUI.img_type = struct_save.img_type;
    handles_GUI.img_type_orig = struct_save.img_type_orig;
    
    %- Folder with images and results
    %  Takes the folder of the .mat file since this avoids issues when
    %  files are copied to different folders or even computer. 
    handles_GUI.path_name_image = struct_save.path_name_image;
   
    
    %- Image and subfolders
    handles_GUI.file_name_image    = struct_save.file_name_image;
    handles_GUI.folder_sub_results = struct_save.folder_sub_results;
    
    %- Other parameters
    handles_GUI.reg_prop = struct_save.reg_prop;
    handles_GUI.par_microscope = struct_save.par_microscope;
    
    %- Flags   
    handles_GUI.flag_BGD = struct_save.flag_BGD;
    handles_GUI.flag_OPB = struct_save.flag_OPB;   
    
    if isfield(struct_save,'flag_opb_apply')
        handles_GUI.flag_opb_apply = struct_save.flag_opb_apply;
    else
        handles_GUI.flag_opb_apply = 1;
    end

    %- Results of photobleaching measurement
    if isfield(struct_save,'OPB')
        if isfield(struct_save.OPB,'curves')        
            handles_GUI.OPB.curves = struct_save.OPB.curves;
        end
    end 
    
   if isfield(struct_save,'bgd_imaging')
        handles_GUI.bgd_imaging =  struct_save.bgd_imaging;
   end
    
    
    %- Filtering
    if isfield(struct_save.filter, 'factor_bgd') 
          handles_GUI.filter.factor_bgd_xy =  struct_save.filter.factor_bgd;
          handles_GUI.filter.factor_bgd_z =  struct_save.filter.factor_bgd;
          
          handles_GUI.filter.factor_psf_xy =  struct_save.filter.factor_psf; 
          handles_GUI.filter.factor_psf_z =  struct_save.filter.factor_psf; 
    else
          handles_GUI.filter.factor_bgd_xy =  struct_save.filter.factor_bgd_xy;
          handles_GUI.filter.factor_bgd_z =  struct_save.filter.factor_bgd_z;
          
          handles_GUI.filter.factor_psf_xy =  struct_save.filter.factor_psf_xy; 
          handles_GUI.filter.factor_psf_z =  struct_save.filter.factor_psf_z; 
        
    end
    
    
    handles_GUI.BGD_img = struct_save.BGD_img;
    
else
    handles_GUI = {};
end


