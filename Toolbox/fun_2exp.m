function y = fun_2exp(par,par_fit)

function_flag = par_fit{1};
x             = par_fit{2};

if function_flag == 1
    p1  = par(1);
    l1 = par(2);
    l2 = par(3);
    C  = 0;
    
end

y = 1 - (p1)*exp(-l1.*x) - (1-p1)*exp(-l2.*x);