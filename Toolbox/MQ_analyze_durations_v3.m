function MQ_analyze_durations_v3(data,par_analyze)
%
% == GLOBAL PARAMETERS
%  dT_peak_all   .... contains the time between peaks identified by the
%                     function peakfind

%% Get data
global duration_off_complete dT_peak_all


data_type           = data.type;
file_name_outline   = data.file_name_outline;
path_name_files     = data.path_name_files;

img_type        = par_analyze.img_type;
dur_min         = par_analyze.dur_min;
th_quant        = par_analyze.th_quant;
smooth_span     = par_analyze.smooth_span;
N_frames_min    = par_analyze.N_frames_min;
n_bins          = par_analyze.n_bins;
flag_plot       = par_analyze.flag_plot;
flag_save       = par_analyze.flag_save;

flag_ACF        = 0;
flag_peakfind   = 3;

% Parameters for peakfind
upsam = 3;
gsize = 6;
gstd  = 2;



%% Files that should be analyzed
if ~iscell(file_name_outline)
    dum=file_name_outline;
    file_name_outline={dum};
    
end
N_files = length(file_name_outline);
    

%% Loop over all files & save results
warning('off','all')

%= Folder to save results
path_sub_plots   = ['_MQ_plots_ON-OFF_',datestr(date,'yymmdd'),'_DurMin-',num2str(dur_min) ,'_IntTH-',num2str(th_quant) ,'_Smooth-',num2str(smooth_span),'_MinFrames-',num2str(N_frames_min)'];
path_plots       = fullfile(path_name_files,path_sub_plots);
if ~exist(path_plots,'dir'); mkdir(path_plots); end


if file_name_outline{1} ~=0

    summary_file = [];
    
    matrix_on_off = [];
    
    quant_all     = [];
    quant_all_on  = [];
    
    intint_all     = [];
    intint_all_on  = [];   
    
    ind_col       = 1; 
    total_all     = 0;
    total_on_all  = 0;
    total_off_all = 0;
    
    duration_on_complete  = [];
    duration_off_complete = [];
    
    duration_on_incomplete  = [];
    duration_off_incomplete = [];   
    
    duration_on_always  = [];
    duration_off_always = [];
   
    dT_peak_all = [];
    
    time_total = 0;
       
    ind_file_good = 1;
    file_name_outline_good = {};
    
    %- Loop through file list
    for ind_file = 1:N_files

        
        fprintf('== Processing %d of %d \n',ind_file,N_files)
        
        %- Load file
        switch data_type
            
            case 'quant'
        
                %- Load data
                file_name      = file_name_outline{ind_file};
                file_name_full = fullfile(path_name_files,file_name); 
                [dum, name_base] = fileparts(file_name);
                parameters.flags.data_type = 'time_course';
        
                [data_MS2_loaded, status_file] = MQ_results_load_v1(file_name_full,parameters);
     
                %- Check if file is good
                if ~ (status_file && ( size(data_MS2_loaded,2) == 23))
                    disp('=== FILE can not be opened')
                    disp(file_name)
                   continue 
                end
                
                %- Check if duration is good
                dur_movie = data_MS2_loaded(end,1) / 3600;
                
                if dur_movie < dur_min
                    disp('=== MOVIE is not long enough')
                    disp(file_name)
                    continue
                end
                
                %- Extract data
                time = data_MS2_loaded(:,1);  
                time = time / 60;  % TIME IN MINUTES!!!
                
                data_quant  = data_MS2_loaded(:,5);     
                data_intint = data_MS2_loaded(:,2);
                
            case 'calib'
                
                %- Load file
                file_name      = file_name_outline{ind_file};
                [dum, name_base] = fileparts(file_name);
                file_name_full = fullfile(path_name_files,file_name); 
                [data_MS2_loaded, flag_file] = MQ_results_load_calibrated_v1(file_name_full);

                %- Check if file is good
                if ~(flag_file && ( size(data_MS2_loaded,2) == 9 || size(data_MS2_loaded,2) == 5))
                    disp('=== FILE can not be opened')
                    disp(file_name)
                    continue
                end
                
                %- Check if duration is good
                dur_movie = data_MS2_loaded(end,1) / 3600;
                
                if dur_movie < dur_min
                    disp('=== MOVIE is not long enough')
                    disp(file_name)
                    continue
                end              
                
                %- Extract data
                time = data_MS2_loaded(:,1);  
                time = time / 60;  % TIME IN MINUTES!!!
                
                data_quant  = data_MS2_loaded(:,2);     
                data_intint = data_MS2_loaded(:,6);                           
                
        end
             
        %- Get total duration of movie
        time_movie  = time(end) - time(1);
        dt          = time(2) - time(1);
        time_total  = time_total + time_movie;


        %- Calculate ACF
        %ind_start = 200;

        if flag_ACF
            options.pixelT   = dt;
            options.flag_bin = 0; 
            options.T_max    = 50;

            %- Calc ACF
            if ~isempty(data_quant)
                [ACF{ind_file}.sum_ACF, ACF{ind_file}.sum_time] = autocorr_FFT_v2(data_quant, options); 
            end
        end
        
        
     
        %- Smooth data                  
        data_smooth = runmean(data_quant,floor((smooth_span-1)/2)); 

        %- Find frames where data count is smaller than threholds
        ind_off     = data_smooth < th_quant;
        ind_off_new = ind_off;
        ind_on      = ~ind_off;

        %- Determine time in on and off
        total     = length(ind_off);
        total_on  = sum(ind_on);
        total_off = sum(ind_off);                   

        perc_on  = total_on / total;
        perc_off = total_off / total;

        %- Average data in ON-state
        if sum(ind_on)
            quant_avg_ON  = mean(data_quant(ind_on));
            quant_median_ON = median(data_quant(ind_on));
            quant_std_ON    = std(data_quant(ind_on));
        else
           quant_avg_ON    = 0;
           quant_median_ON = 0;
           quant_std_ON    = 0;
        end

        %- INT all
        quant_all_on                            = [quant_all_on;data_quant(ind_on)]; 
        quant_all(1:length(data_quant),ind_col) = data_quant; 

        intint_all_on                          = [intint_all_on;data_intint(ind_on)]; 
        intint_all(1:length(data_quant),ind_col) = data_intint;
        

        ind_col    = ind_col +1;

        %- ON-OFF time
        total_all     = total_all + total;
        total_on_all  = total_on_all + total_on;
        total_off_all = total_off_all + total_off;


        %=== Find complete events: OFF
        off_events   = {}; off_events.start_abs = [];
        ind_off_abs  = find(ind_off);    

        if not(isempty(ind_off_abs))
            diff_ind_off = diff(ind_off_abs);

            %- Close small gaps
            ind_correct = diff_ind_off > 1 & diff_ind_off <= N_frames_min;
            diff_ind_off(ind_correct) = 1;

            %- Find transitions
            ind_off_event = find(diff_ind_off>1);
            ind_off_event = [0;ind_off_event;length(ind_off_abs)];  % 0 for first because index denotes end of preceding & 1 will be added

            off_events.start_rel = ind_off_event(1:end-1)+1;
            off_events.end_rel   = ind_off_event(2:end);

            %- Erase short events
            d_frames  = off_events.end_rel - off_events.start_rel;
            ind_short = d_frames < N_frames_min -1;

            off_events.start_rel(ind_short) = [];
            off_events.end_rel(ind_short) = [];

            %- Get absolute coordinates and durations
            off_events.start_abs = ind_off_abs(off_events.start_rel);
            off_events.end_abs   = ind_off_abs(off_events.end_rel);
            off_events.duration  = time(off_events.end_abs) - time(off_events.start_abs);

            off_events.duration(not(off_events.start_abs == 1)) = off_events.duration(not(off_events.start_abs == 1)) + dt;

            off_events.complete  = not(off_events.start_abs == 1 | off_events.end_abs == length(time));

            duration_off_complete   = [duration_off_complete,   off_events.duration(off_events.complete)'];
            duration_off_incomplete = [duration_off_incomplete, off_events.duration(not(off_events.complete))' ];

            %- Make new ind_off
            ind_off_new = zeros(size(ind_off));

            for i_event = 1:length(off_events.start_abs)
                i_start     = off_events.start_abs(i_event);
                i_end       = off_events.end_abs(i_event);
                ind_off_new(i_start:i_end) = 1; 
            end


        %- Entire movie is ON    
        else                      
           duration_on_always =  [duration_on_always, time_movie];
       end


        %=== Find complete events: ON
        ind_on_BKP  = ind_on;
        ind_on      = ~(ind_off_new);
        on_events   = {}; on_events.start_abs = [];
        ind_on_abs  = find(ind_on);

        if not(isempty(ind_on_abs))
            diff_ind_on = diff(ind_on_abs);

            %- Close small gaps
            ind_correct = diff_ind_on > 1 & diff_ind_on <= N_frames_min;
            diff_ind_on(ind_correct) = 1;

            %- Find transitions
            ind_on_event = find(diff_ind_on>1);
            ind_on_event = [0;ind_on_event;length(ind_on_abs)];  % 0 for first because index denotes end of preceding & 1 will be added

            on_events.start_rel = ind_on_event(1:end-1)+1;
            on_events.end_rel   = ind_on_event(2:end);

            %- Erase short events
            d_frames  = on_events.end_rel - on_events.start_rel;
            ind_short = d_frames < N_frames_min -1;

            on_events.start_rel(ind_short) = [];
            on_events.end_rel(ind_short) = [];

            %- Get absolute coordinates and durations  
            on_events.start_abs = ind_on_abs(on_events.start_rel);
            on_events.end_abs   = ind_on_abs(on_events.end_rel);
            on_events.duration  = time(on_events.end_abs) - time(on_events.start_abs);

            %- Consider the missing first frame for all events except the first one
            on_events.duration(not(on_events.start_abs == 1)) = on_events.duration(not(on_events.start_abs == 1)) + dt;

            on_events.complete  = not(on_events.start_abs == 1 | on_events.end_abs == length(time));                      
          
            duration_on_complete   = [duration_on_complete,   on_events.duration(on_events.complete)'];                
            duration_on_incomplete = [duration_on_incomplete, on_events.duration(not(on_events.complete))' ];

        %- Entire movies is OFF    
        else
           duration_off_always =  [duration_off_always, time_movie];
        end


        % ==== Find peaks with peakfind
        
          
        %- Create starting plots wiht ON and OFF periods
        if flag_peakfind && flag_plot
            
            
            h_fig_peaks = figure; set(h_fig_peaks,'Color','w'); set(h_fig_peaks, 'visible', 'off');
            hold on
            
                  %- OFF periods
               for i_off =1:length(off_events.start_abs)
                  ind_start = off_events.start_abs(i_off);
                  ind_end   = off_events.end_abs(i_off);

                  time_start = time(ind_start) - dt/2;
                  time_end   = time(ind_end)   + dt/2;

                  area([time_start/60 time_end/60],[th_quant th_quant],'FaceColor',[.95 0 0],'LineStyle','none')                     
               end

               %- ON periods
               for i_on =1:length(on_events.start_abs)
                  ind_start = on_events.start_abs(i_on);
                  ind_end   = on_events.end_abs(i_on);

                  time_start = time(ind_start)- dt/2;
                  time_end   = time(ind_end)  + dt/2;

                  area([time_start/60 time_end/60],[2*th_quant 2*th_quant],'FaceColor',[0 0.95 0],'LineStyle','none','BaseValue',th_quant)                     
               end

               %- Plot thresholds
               plot([time(1) time(end)]/60,[th_quant th_quant],'k','LineWidth',2)

            hold off
            
        end
        
        %- Find peaks by analyzing ON periods separately
        if flag_peakfind == 1
            
             if flag_plot
                %h_fig_peaks = figure; set(h_fig_peaks,'Color','w'); set(h_fig_peaks, 'visible', 'off');
                plot(time/60,data_quant,'k')
             end      
            
            for i=1:length(on_events.start_abs)
               i_start =  on_events.start_abs(i);
               i_end   =  on_events.end_abs(i);
               
               time_loop = time(i_start:i_end);
               data_quant_loop = data_quant(i_start:i_end);
               
               if length(time_loop) > 6
                   try
                       
                      [xout,yout,peakspos]=peakfind(time_loop,data_quant_loop,upsam,gsize,gstd); %,0.1,'rel');

                        time_peaks = xout(peakspos);
                        dT_peak    = diff(time_peaks)';
                        dT_peak_all = [dT_peak_all;dT_peak];

                        if flag_plot
                            hold on
                            plot(xout/60,yout,'k','linewidth',2)
                            plot(xout(peakspos)/60,yout(peakspos),'b.','Markersize',30)
                        end
                   catch
                       
                   end
               end
            end
            
        %- Find peaks in entire movie
        elseif flag_peakfind == 2
            
            [xout,yout,peakspos]=peakfind(time,data_quant,upsam,gsize,gstd,0.1,'rel');
            
            time_peaks = xout(peakspos);
            dT_peak    = diff(time_peaks)';
            dT_peak_all = [dT_peak_all;dT_peak];

            if flag_plot
                hold on
                plot(time/60,data_quant,'k')
                plot(xout/60,yout,'k','linewidth',2)
                plot(xout(peakspos)/60,yout(peakspos),'b.','Markersize',30)
                title(['Found ' num2str(length(peakspos)) ' peaks.'])
            end
            
                
        %- Find peaks in entire movie & then reduce to ON periods
        elseif flag_peakfind == 3
                
             [xout,yout,peakspos]=peakfind(time,data_quant,upsam,gsize,gstd,0.1,'rel');
             %[xout,yout,peakspos]=peakfind(time,data_smooth,upsam,gsize,gstd,0.1,'rel');

             if flag_plot     
                hold on
                    %plot(time/60,data_quant,'k')
                    plot(xout/60,yout,'k')
                hold off
             end      

            % Loop over ON PERIODS
            for i=1:length(on_events.start_abs)
               i_start =  on_events.start_abs(i);
               i_end   =  on_events.end_abs(i);

               
               peakpos_ON = peakspos(peakspos/upsam >= i_start & peakspos/upsam <= i_end);
       
               time_peaks = xout(peakpos_ON);
               dT_peak    = diff(time_peaks)';
               dT_peak_all = [dT_peak_all;dT_peak];

                if flag_plot
                    hold on
                    plot(xout(peakpos_ON)/60,yout(peakpos_ON),'b.','Markersize',30)
                end
            end
        end

       %- Save and close the figure  
       if flag_peakfind && flag_plot  
            
            
             xlabel('Time [h]'); ylabel('y')
             box on

            file_save = [name_base,'_PEAKS'];
            file_save_full = fullfile(path_plots,file_save);
            
            v =axis;axis([time(1)/60 time(end)/60 v(3) v(4)])
            saveas(h_fig_peaks, file_save_full ,img_type)
            close(h_fig_peaks)
        end
        

        %= Store more information
        loop_sum_durations = sum(duration_on_complete) + sum(duration_on_incomplete) + sum(duration_off_complete) + sum(duration_off_incomplete);

        %=== Store results
        summary_file(ind_file_good,:) = [total total_on total_off perc_on perc_off quant_avg_ON quant_median_ON quant_std_ON];
        file_name_outline_good{ind_file_good,1} = file_name;
        
        
        %=== Summarize data
        
        %- OFF periods
       for i_off =1:length(off_events.start_abs)
          ind_start = off_events.start_abs(i_off);
          ind_end   = off_events.end_abs(i_off);
          
          matrix_on_off(ind_file_good,ind_start:ind_end) = -1;
                         
       end

       %- ON periods
       for i_on =1:length(on_events.start_abs)
          ind_start = on_events.start_abs(i_on);
          ind_end   = on_events.end_abs(i_on);

          matrix_on_off(ind_file_good,ind_start:ind_end) = 1;
                        
       end
        
       
        
        %- Update counter
        ind_file_good = ind_file_good +1; 

        
        
        %==== CHECK IF SUM IS CORRECT
        if 0
            dum_loop = 0;
            
            if isfield(on_events,'duration')             
                dum_loop = dum_loop + sum(on_events.duration);
            end
            
            if isfield(off_events,'duration')             
                dum_loop = dum_loop + sum(off_events.duration);
            end           

            dum_loop = dum_loop /dt;
            
            fprintf('%d \t\t %d \n',total , dum_loop) 
        end
        

        %- Plot curve
        if flag_plot

           dt = time(2) - time(1);

           h_fig_periods = figure; 
           set(h_fig_periods,'Color','w'); set(h_fig_periods, 'visible', 'off');
           hold on

               %- OFF periods
               for i_off =1:length(off_events.start_abs)
                  ind_start = off_events.start_abs(i_off);
                  ind_end   = off_events.end_abs(i_off);

                  time_start = time(ind_start) - dt/2;
                  time_end   = time(ind_end)   + dt/2;

                  area([time_start/60 time_end/60],[th_quant th_quant],'FaceColor',[.95 0 0],'LineStyle','none')                     
               end

               %- ON periods
               for i_on =1:length(on_events.start_abs)
                  ind_start = on_events.start_abs(i_on);
                  ind_end   = on_events.end_abs(i_on);

                  time_start = time(ind_start)- dt/2;
                  time_end   = time(ind_end)  + dt/2;

                  area([time_start/60 time_end/60],[2*th_quant 2*th_quant],'FaceColor',[0 0.95 0],'LineStyle','none','BaseValue',th_quant)                     
               end

               %- Plot thresholds
               plot([time(1) time(end)]/60,[th_quant th_quant],'k','LineWidth',2)

               %- Plot curves
               %plot(time/60,data_quant,'k','Color',[0.5 0.5 0.5])
               plot(time/60,data_smooth,'b')

           hold off
           box on
           xlabel('Time [h]')
           ylabel('TS intensity [a.u.]')
           v =axis;axis([time(1)/60 time(end)/60 v(3) v(4)])

           file_save = [name_base,'_ON-OFF'];
           file_save_full = fullfile(path_plots,file_save);
           saveas(h_fig_periods, file_save_full ,img_type)
           close(h_fig_periods)
        end

    end
end



%- Save all events to create text file
duration_on_complete_ALL  = duration_on_complete;
duration_off_complete_ALL = duration_off_complete;

%- Check if there are at least 5 elements
if length(duration_on_complete) <= 5
    disp('No enough complete ON events')
    disp(duration_on_complete)
    
    duration_on_complete = [];
end

if length(duration_off_complete) <= 5
    disp('No enough complete OFF events')
    disp(duration_on_complete)
    duration_off_complete = [];
end

warning('on','all')

%% Distance between peaks analysis


if flag_peakfind
    figure, set(gcf,'Color','w')
    hist(dT_peak_all,50)
    xlabel('Time [min]')
    ylabel('Counts')
    title('dT between peaks')
end

%% Autocorrelation anaylysis
 if flag_ACF

    for i=1:length(ACF)
        %ACF{i}
        ACF_time(i,1:length(ACF{i}.sum_time)) = ACF{i}.sum_time;
        ACF_val(i,1:length(ACF{i}.sum_ACF)) = ACF{i}.sum_ACF;

        %length( ACF{i}.sum_time)
    end


    ACF_mean = mean(ACF_val);
    ACF_std = std(ACF_val);

    figure, set(gcf,'color','w')
    subplot(1,3,1)
    plot(ACF_time(1,:),ACF_mean,'r')
    xlabel('tau [s]')
    ylabel('ACF')
    
    subplot(1,3,2)
    semilogx(ACF_time(1,:),ACF_mean,'r')
    xlabel('tau [s]')
    ylabel('ACF')
    
    subplot(1,3,3)
    loglog(ACF_time(1,:),ACF_mean,'r')
    xlabel('tau [s]')
    ylabel('ACF')
    
 end
    
%% ==========================================================================
%========= DIFFERENT ANALYSIS
%==========================================================================

%===== Analyze intensity
quant_all_mean   = mean(quant_all_on);
quant_all_median = median(quant_all_on);
quant_all_stdev  = std(quant_all_on);

disp('================================')
disp('QUANTIFICATION LEVELS IN ALL IMAGES')
fprintf('NUMBER OF CELLS: %d\n',N_files);
fprintf('MEDIAN: %.1f\n',quant_all_median);
fprintf('MEAN: %.1f\n',quant_all_mean);
fprintf('STDEV: %.1f\n',quant_all_stdev);


%===== Analyze integrated intensity
intint_all_mean   = mean(intint_all_on);
intint_all_median = median(intint_all_on);
intint_all_stdev  = std(intint_all_on);

disp('================================')
fprintf('NUMBER OF CELLS: %d\n',N_files);
fprintf('MEDIAN: %.1f\n',intint_all_median);
fprintf('MEAN: %.1f\n',intint_all_mean);
fprintf('STDEV: %.1f\n',intint_all_stdev);


%===== Analyze total ON/OFF                  
perc_on_all  = total_on_all  / total_all;
perc_off_all = total_off_all / total_all;

disp('  ')
disp('================================')
disp('ON-OFF ')
fprintf('Frames TOTAL: %d\n',total_all);
fprintf('Frames ON: %.0f\n',total_on_all);
fprintf('Frames OFF: %.0f\n',total_off_all);
fprintf('Perc ON: %.3f\n',perc_on_all);
fprintf('Perc OFF: %.3f\n',perc_off_all);


% =========================================================================
% ==== Plot matrix of durations

%- Sort matrix 
dum1 = abs(matrix_on_off);
dum2 = sum(dum1,2);
[dum3, ind_sort] = sort(dum2,'ascend');

matrix_on_off_sort = matrix_on_off(ind_sort,:);

dt = diff(time(1:2)) / 60; % Time in minutes
x = (1:size(matrix_on_off_sort,2)) * dt;
y = (1:size(matrix_on_off_sort,1)); 

h_fig = figure; set(h_fig,'Color','w'); %set(h_fig, 'visible', 'off');
h     = pcolor(x,y,matrix_on_off_sort);
set(gca,'YTick',[1 size(matrix_on_off_sort,1)])
xlabel('Time [h]');
ylabel('Image');

set(h,'EdgeColor','none')
set(gcf,'Position',[200 200 200 300])

cm_custom = [1 0 0; 1 1 1; 0 1 0];
colormap(cm_custom)
box off

file_save = '_PLOTS___SUMMARY_ON_OFF';
file_save_full = fullfile(path_plots,file_save);
saveas(h_fig, file_save_full ,img_type)
%close(h_fig)



%% ===== Histogram of all quantification results
h_fig = figure; 
set(h_fig, 'Color','w');set(h_fig, 'visible', 'off');
hist(quant_all(:),100)
xlabel('TS quantification')
ylabel('Counts')
set(gcf,'Position',[1000        1075         206         129])




%===== PLOT DOTS for intensity per ON
X = 1:size(summary_file,1)';

h_fig = figure; 
set(h_fig, 'Color','w'); set(h_fig, 'visible', 'off');

subplot(2,1,1)
hold on
for i=1:size(summary_file,1)

    %- Plot error bar for each point (with standard error)
    line([X(i) X(i)],[summary_file(i,6)-summary_file(i,8), summary_file(i,6)+summary_file(i,8)],'Color','k','LineWidth',1)
    
    %- Plot mean value
    plot(X(i),summary_file(i,6),'or','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','k');   % 5 For subset; 10 for big one
end
hold off
box on
xlabel('Movie ID'); ylabel('MEAN +/- stdev')

subplot(2,1,2)
hold on
for i=1:size(summary_file,1)

    %- Plot error bar for each point (with standard error)
    line([X(i) X(i)],[summary_file(i,7)-summary_file(i,8), summary_file(i,7)+summary_file(i,8)],'Color','k','LineWidth',1)
    
    %- Plot mean value
    plot(X(i),summary_file(i,7),'or','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','k');   % 5 For subset; 10 for big one
end
hold off
box on
xlabel('Movie ID'); ylabel('MEDIAN +/- stdev')

file_save = '_PLOTS__ON-intensity_per_movie';
file_save_full = fullfile(path_plots,file_save);
saveas(h_fig, file_save_full ,img_type)
close(h_fig)


%===== Pie chart for movie duration of complete and incomplete events

duration_tot_ON_complete   = sum(duration_on_complete);
duration_tot_ON_incomplete = sum(duration_on_incomplete);


duration_tot_OFF_complete   = sum(duration_off_complete);
duration_tot_OFF_incomplete = sum(duration_off_incomplete);


time_total = total_all * (time(2) - time(1));

x       = [ duration_tot_OFF_complete duration_tot_OFF_incomplete ...
            duration_tot_ON_complete duration_tot_ON_incomplete];

h_fig = figure; set(gcf,'color','w'); %set(h_fig, 'visible', 'off');
h_pie = pie(x,{'OFF-complete','OFF-incomplete','ON-complete','ON-incomplete'});


%figure
%h_pie = pie(x,[0 0 0 0]);
hp = findobj(h_pie, 'Type', 'patch');

set(hp(1), 'FaceColor', [0.6 0 0]);
set(hp(2), 'FaceColor', [1.0 0 0]);

set(hp(3), 'FaceColor', [0 0.6 0]);
set(hp(4), 'FaceColor', [0 1.0 0]);

title('Measured ON-OFF durations','FontWeight','bold')
file_save = '_PLOTS__PIE-chart_duration';
file_save_full = fullfile(path_plots,file_save);
saveas(h_fig, file_save_full ,img_type)
close(h_fig)


%==========================================================================
%===== Plot all measured durations
%==========================================================================

% - 1: Once with normal histograms with counts
% - 2: Normalized wiht TOTAL time of all movies

time_total = time_total / (60);

% ==== Plot all measured durations

%== ON duration
h_fig_duration   = figure; 
set(gcf, 'Color','w'); set(gcf, 'visible', 'off');

h_fig_freq   = figure; 
set(gcf, 'Color','w'); set(gcf, 'visible', 'off');


% ON-durations complete 
[cts_data, bin_data] = hist(duration_on_complete,n_bins);
  

figure(h_fig_duration); set(gcf, 'visible', 'off');
subplot(2,2,1)
bar(bin_data,cts_data,'g')
xlabel('Duration [min]'); ylabel('Counts')
title('ON - only complete events')


figure(h_fig_freq); set(gcf, 'visible', 'off');
subplot(2,2,1)
bar(bin_data,cts_data/time_total,'g')
xlabel('Duration [min]'); ylabel('Frequency (total time [h])')
title('ON - only complete events')


% ON-durations ALL: consider incomplete and over entire movie 
[cts_data, bin_data] = hist([duration_on_incomplete,duration_on_complete],n_bins);
[cts_incomplete] = hist(duration_on_incomplete,bin_data); 
[cts_always]     = hist(duration_on_always,bin_data); 


figure(h_fig_duration); set(gcf, 'visible', 'off');
subplot(2,2,2)
bar(bin_data,cts_data,'g')
xlabel('Duration [min]'); ylabel('Counts')
title('ON - all events')


hold on
    bar(bin_data,cts_incomplete,'r')
    bar(bin_data,cts_always,'b')
hold off
legend('All events','Incomplete events','Events over entire movie')  



figure(h_fig_freq); set(gcf, 'visible', 'off');
subplot(2,2,2)
bar(bin_data,cts_data/time_total,'g')
xlabel('Duration [min]'); ylabel('Frequency (total time [h])')
title('ON - all events')


hold on
    bar(bin_data,cts_incomplete/time_total,'r')
    bar(bin_data,cts_always/time_total,'b')
hold off
legend('All events','Incomplete events','Events over entire movie')  



% == OFF duration

bins_off = linspace(0,200,n_bins);


% OFF-durations complete 
[cts_data, bin_data] = histc(duration_off_complete,bins_off);
  

figure(h_fig_duration); set(gcf, 'visible', 'off');
subplot(2,2,3)
bar(bins_off,cts_data,'g')
xlabel('Duration [min]'); ylabel('Counts')
title('OFF - only complete events')
v= axis(gca); axis(gca,[-5 200 v(3) v(4)])

figure(h_fig_freq); set(gcf, 'visible', 'off');
subplot(2,2,3)
bar(bins_off,cts_data/time_total,'g')
xlabel('Duration [min]'); ylabel('Frequency (total time [h])')
title('OFF - only complete events')
v= axis(gca); axis(gca,[-5 200 v(3) v(4)])


% OFF-durations ALL: consider incomplete and over entire movie 
[cts_data, bin_data] = histc([duration_off_incomplete,duration_off_complete],bins_off);
[cts_incomplete]     = histc(duration_off_incomplete,bins_off); 
[cts_always]         = histc(duration_off_always,bins_off); 

figure(h_fig_duration); set(gcf, 'visible', 'off');
subplot(2,2,4)
bar(bins_off,cts_data,'FaceColor','g')
xlabel('Duration [min]'); ylabel('Counts')
title('OFF - all events')

hold on
    bar(bins_off,cts_incomplete,'r')
    bar(bins_off,cts_always,'b')
hold off
legend('All events','Incomplete events','Events over entire movie')  

v= axis(gca); axis(gca,[-5 200 v(3) v(4)])


figure(h_fig_freq); set(gcf, 'visible', 'off');
subplot(2,2,4)
bar(bins_off,cts_data/time_total,'g')
xlabel('Duration [min]'); ylabel('Frequency (total time [h])')
title('OFF - all events')

hold on
    bar(bins_off,cts_incomplete/time_total,'r')
    bar(bins_off,cts_always/time_total,'b')
hold off
legend('All events','Incomplete events','Events over entire movie')  
v= axis(gca); axis(gca,[-5 200 v(3) v(4)])



file_save = '_PLOTS__DURATION_all-counts';
file_save_full = fullfile(path_plots,file_save);
saveas(h_fig_duration, file_save_full ,img_type)
close(h_fig_duration)
 

file_save = '_PLOTS__DURATION_all-freq';
file_save_full = fullfile(path_plots,file_save);
saveas(h_fig_freq, file_save_full ,img_type)
close(h_fig_freq)


%==========================================================================
%===== Duration with small bins - only for long movies
%==========================================================================

switch data_type

    case 'quant'
        
        % OFF-durations complete 
        [cts_data, bin_data] = hist(duration_off_complete,n_bins);
        
        figure; set(gcf,'color','w');set(gcf, 'visible', 'off');
        subplot(1,2,1);set(gcf, 'visible', 'off');
        bar(bin_data,cts_data/time_total,'g')
        xlabel('Duration [min]'); ylabel('Frequency (total time [h])')
        title('OFF duration - normal binning')
        
        bin_unique = unique(duration_off_complete);
        [cts_unique] = hist(duration_off_complete,bin_unique);
        
        subplot(1,2,2);set(gcf, 'visible', 'off');
        bar(bin_unique,cts_unique/time_total,'g')
        xlabel('Duration [min]'); ylabel('Frequency (total time [h])')
        title('OFF duration - fine binning')
        
        file_save = '_PLOTS__DURATION_ON_binning-diff';
        file_save_full = fullfile(path_plots,file_save);
        saveas(gcf, file_save_full ,img_type)
        close(gcf)
        
end


%==========================================================================
%===== FIT DURATIONS WITH exponential distribution
%==========================================================================


disp('  ')
disp('================================')
disp('Fit of complete ON / OFF events with exponential distribution.')


%== Plots
h_fig = figure; 
set(h_fig, 'Color', 'w'); set(h_fig, 'visible', 'off');

%- COMPLETE ON events
if ~isempty(duration_on_complete)
    [mu_ON,muci_ON] = expfit(duration_on_complete);
    ci_ON = (muci_ON(2) - muci_ON(1)) / 2;
    
    fprintf('ON duration (mean +/- 95%% ci): %.1f +/- %.1f \n',mu_ON,ci_ON);
    
    subplot(1,2,1)
    histfit(duration_on_complete,n_bins,'exp')
    xlabel('Duration [min]')
    ylabel('Counts')
    title('ON times (completed)')
    legend('Data', ['Fit:', sprintf('%.1f',mu_ON) , '+/-', sprintf('%.1f',ci_ON)])
    
else
   disp('NO complete ON events') 
    
end

%- COMPLETE OFF events
if ~isempty(duration_off_complete)
    
    
    t_max_off = 200;
    
    
    [mu_OFF,muci_OFF] = expfit(duration_off_complete);
    ci_OFF = (muci_OFF(2) - muci_OFF(1)) / 2;
    
    fprintf('OFF duration (mean +/- 95%% ci): %.1f +/- %.1f \n',mu_OFF,ci_OFF);
    
    subplot(1,2,2)
    histfit(duration_off_complete,n_bins,'exp')
    xlabel('Duration [min]')
    ylabel('Counts')
    title('OFF times (completed)')
    legend('Data', ['Fit:', sprintf('%.1f',mu_OFF) , '+/-', sprintf('%.1f',ci_OFF)])
    
else
    disp('NO complete OFF events') 
    

end


%- Save plots
file_save = '_PLOTS__FIT_duration_exponential';
file_save_full = fullfile(path_plots,file_save);
saveas(h_fig, file_save_full ,img_type)
close(h_fig)



%==========================================================================
%========= FIT BINNED DATA WITH EXPONENTIAL DECAY FUNCTIONS
%==========================================================================

%============ PLOTS
h_fig_L2   = figure; 
set(gcf, 'Color','w');  set(gcf,'Position',[1 1 1300 600]); set(gcf, 'visible', 'off');

h_fig_Chi2 = figure; 
set(gcf, 'Color','w'); set(gcf,'Position',[1 1 1300 600]); set(gcf, 'visible', 'off');

h_fig_cumhist = figure; 
set(gcf, 'Color','w'); set(gcf,'Position',[1 1 1000 300]); set(gcf, 'visible', 'off');



%=========== ON DURATION

if ~isempty(duration_on_complete)

    
    
    %=== Cumulativ histogram

    %- Calc cumulative histogram
    [f_on,x_on,flo_on,fup_on] = ecdf(duration_on_complete, 'bounds','off' );
    x_on(1) = []; f_on(1) = [];

    % ----- Fit with 1-exp
    fun_exp1          = @(x,l) 1 - exp(-l.*x);
    [par_fit_CH_exp1_ON,dum0,residual,dum1,dum2,dum3,J]  = lsqcurvefit(fun_exp1,1/25,x_on,f_on);
    f_fit_exp1_ON    = fun_exp1(x_on,par_fit_CH_exp1_ON);

    [h,p_exp1_ON,ks2stat_exp1_ON] = kstest2(f_on,f_fit_exp1_ON);

    %- Calc error with error propagation
    ci             = nlparci(par_fit_CH_exp1_ON,residual,'jacobian',J);
    ci_exp1        = (ci(:,2) - ci(:,1)) / 2;
    err_CH_ON_exp1 = ci_exp1 / par_fit_CH_exp1_ON^2;
    
    
    % ----- Fit with 2-exp
    lb = [0 0 0];
    ub = [1 1e5 1e5];
    par_fit{1} = 1;
    par_fit{2} = x_on;
    par_start  = [0.8,1.25*par_fit_CH_exp1_ON,0.1*par_fit_CH_exp1_ON];
    [par_fit_CH_exp2_ON,dum0,residual,dum1,dum2,dum3,J] = lsqcurvefit(@fun_2exp,par_start,par_fit,f_on,lb,ub);
    f_fit_exp2_ON   = fun_2exp(par_fit_CH_exp2_ON,par_fit);

    [h,p_exp2_ON,ks2stat_exp2_ON] = kstest2(f_on,f_fit_exp2_ON);

    %- Calc error with error propagation
    ci = nlparci(par_fit_CH_exp2_ON,residual,'jacobian',J);
    ci_exp2 = (ci(:,2) - ci(:,1)) / 2;

    err_CH_ON_exp2(1,1)   = ci_exp2(1);
    err_CH_ON_exp2(2:3,1) = ci_exp2(2:3)' ./ par_fit_CH_exp2_ON(2:3).^2;


    
    %==== PLOT
    figure(h_fig_cumhist); set(gcf, 'visible', 'off');

    subplot(1,2,1)
    title('ON-duration')
    hold on
    plot(x_on,f_on,'k','LineWidth',2)
    plot(x_on,f_fit_exp1_ON,'b','LineWidth',2)
    plot(x_on,f_fit_exp2_ON,'--r','LineWidth',2)
    hold off
    box on
    xlabel('Time [min]')
    ylabel('Cummulative frequency')

    legend('Data', ['Fit 1-exp: t1=',  sprintf('%.1f',1/par_fit_CH_exp1_ON),', p=',  sprintf('%.3f',p_exp1_ON),char(10)], ...
                   ['Fit 2-exp: t1=',  sprintf('%.1f',1/par_fit_CH_exp2_ON(2)), ', t2=',  sprintf('%.1f',1/par_fit_CH_exp2_ON(3)),', F1=',  sprintf('%.3f',par_fit_CH_exp2_ON(1)),',',char(10) ...  
                                      'p=',  sprintf('%.3f',p_exp2_ON)],4)

 
    %============ Chi-squared
    
    %= 1 exp
    [parFit_ON_exp1_chi2, fit_ON_exp1_chi2, err_ON_exp1_chi2,cts_data_chi2,bin_data_chi2] = fit_dist_FM_v1(duration_on_complete,n_bins,'exp1','chi2');
    
    %= 2 exp
    [parFit_ON_exp2_chi2, fit_ON_exp2_chi2, err_ON_exp2_chi2,dum1,dum2,fit_all_exp2_chi2] = fit_dist_FM_v1(duration_on_complete,n_bins,'exp2','chi2');
    
    %- AIC
    N = sum(cts_data_chi2>0);
    K = 2+1;
    
    AICc_ON_exp1_chi2 = N * log( err_ON_exp1_chi2 / N) + 2*K + 2*K*(K+1) / (N-K-1);
    
    K = 4+1;    
    AICc_ON_exp2_chi2 = N * log( err_ON_exp2_chi2 / N) + 2*K + 2*K*(K+1) / (N-K-1);
    
    dAICc = AICc_ON_exp1_chi2 - AICc_ON_exp2_chi2;    
    ev_ratio_ON_chi2 = 1/exp(-0.5*dAICc);
    
    
    %=== PLOTS
    figure(h_fig_Chi2); set(gcf, 'visible', 'off');
    subplot(2,3,1)
    hold on
    bar(bin_data_chi2,cts_data_chi2,'g')
    plot(bin_data_chi2,fit_ON_exp1_chi2,'r','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('chi2: ON duration - 1 exp')
    legend('Data', ['Fit: F1=', sprintf('%.1f',parFit_ON_exp1_chi2(1)),', k1=', sprintf('%.1f',parFit_ON_exp1_chi2(2)), char(10), 'chi2=', sprintf('%.1f',err_ON_exp1_chi2)  ])
    
    
    subplot(2,3,2)
    hold on
    bar(bin_data_chi2,cts_data_chi2,'g')
    plot(bin_data_chi2,fit_ON_exp1_chi2,'--b','LineWidth',2)
    plot(bin_data_chi2,fit_ON_exp2_chi2,'r','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('chi2: ON duration - 2 exp')
    legend('Data','Fit 1exp', ['F1=', sprintf('%.1f',parFit_ON_exp2_chi2(1)),', k1=', sprintf('%.1f',parFit_ON_exp2_chi2(2)), char(10), 'F2=', num2str(round(parFit_ON_exp2_chi2(3))),', k2=', sprintf('%.1f',parFit_ON_exp2_chi2(4)),char(10), 'chi2=', sprintf('%.1f',err_ON_exp2_chi2)  ])

    subplot(2,3,3)
    hold on
    plot(bin_data_chi2,fit_all_exp2_chi2(:,1),'-r','LineWidth',2)
    plot(bin_data_chi2,fit_all_exp2_chi2(:,2),'-b','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('chi2: ON duration - 2 exp')
    legend('Exp2-1','Exp2-2')

    
    
    
    %============ L2-norm
    
     %= 1 exp
    [parFit_ON_exp1_L2, fit_ON_exp1_L2, err_ON_exp1_L2,cts_data_L2,bin_data_L2] = fit_dist_FM_v1(duration_on_complete,n_bins,'exp1','L2');
    
     %= 2 exp
    [parFit_ON_exp2_L2, fit_ON_exp2_L2, err_ON_exp2_L2,dum1,dum2,fit_all] = fit_dist_FM_v1(duration_on_complete,n_bins,'exp2','L2');
    
   
    %- AIC
    N = sum(bin_data_L2>0);
    K = 2+1;
    
    AICc_ON_exp1_L2 = N * log( err_ON_exp1_L2 / N) + 2*K + 2*K*(K+1) / (N-K-1);
    
    K = 4+1;    
    AICc_ON_exp2_L2 = N * log( err_ON_exp2_L2 / N) + 2*K + 2*K*(K+1) / (N-K-1);
    
    dAICc = AICc_ON_exp1_L2 - AICc_ON_exp2_L2;    
    ev_ratio_ON_L2 = 1/exp(-0.5*dAICc);
    
    
    %=== PLOTS
    figure(h_fig_L2); set(gcf, 'visible', 'off');
    subplot(2,3,1)
    hold on
    bar(bin_data_L2,cts_data_L2,'g')
    plot(bin_data_L2,fit_ON_exp1_L2,'r','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('L2: ON duration - 1 exp')
    legend('Data', ['Fit: F1=', sprintf('%.1f',parFit_ON_exp1_L2(1)),', k1=', sprintf('%.1f',parFit_ON_exp1_L2(2)), char(10), 'ssr=', sprintf('%.1f',err_ON_exp1_L2)  ])

    subplot(2,3,2)
    hold on
    bar(bin_data_L2,cts_data_L2,'g')
    plot(bin_data_L2,fit_ON_exp1_L2,'--b','LineWidth',2)
    plot(bin_data_L2,fit_ON_exp2_L2,'r','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('L2: ON duration - 2 exp')
    legend('Data','Fit 1exp', ['F1=', sprintf('%.1f',parFit_ON_exp2_L2(1)),', k1=', sprintf('%.1f',parFit_ON_exp2_L2(2)), char(10), 'F2=', sprintf('%.1f',parFit_ON_exp2_L2(3)),', k2=', sprintf('%.1f',parFit_ON_exp2_L2(4)) ,char(10), 'ssr=', sprintf('%.1f',err_ON_exp2_L2)  ])

    subplot(2,3,3)
    hold on
    plot(bin_data_L2,fit_all(:,1),'-r','LineWidth',2)
    plot(bin_data_L2,fit_all(:,2),'-b','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('L2: ON duration - 2 exp')
    legend('Exp2-1','Exp2-2')
    
else
    parFit_ON_exp1_chi2 = [0 0];
    err_ON_exp1_chi2    = 0; 
    parFit_ON_exp2_chi2 = [0 0 0 0];
    err_ON_exp2_chi2    = 0;
    
    parFit_ON_exp1_L2 = [0 0];
    err_ON_exp1_L2   = 0; 
    parFit_ON_exp2_L2 = [0 0 0 0];
    err_ON_exp2_L2    = 0;
    
end


%=========== OFF DURATION
if ~isempty(duration_off_complete)
    
   
    %=== Cumulative histograms

    %- Calc cumulative histogram
    [f_off,x_off,flo_off,fup_off] = ecdf(duration_off_complete);
    x_off(1) = []; f_off(1) = [];
    

    % ----- Fit with 1-exp
    fun_exp1             = @(x,l) 1 - exp(-l.*x);
    [par_fit_CH_exp1_OFF,dum0,residual,dum1,dum2,dum3,J]  = lsqcurvefit(fun_exp1,1/25,x_off,f_off);
    f_fit_exp1_OFF    = fun_exp1(x_off,par_fit_CH_exp1_OFF);

    [h,p_exp1_OFF,ks2stat_exp1_OFF] = kstest2(f_off,f_fit_exp1_OFF);

    
    %- Calc error with error propagation
    ci             = nlparci(par_fit_CH_exp1_OFF,residual,'jacobian',J);
    ci_exp1        = (ci(:,2) - ci(:,1)) / 2;
    err_CH_OFF_exp1 = ci_exp1 / par_fit_CH_exp1_OFF^2;
      
    
    % ----- Fit with 2-exp
    lb = [0 0 0];
    ub = [1 1e5 1e5];
    par_fit{1} = 1;
    par_fit{2} = x_off;
    par_start  = [0.8,1.25*par_fit_CH_exp1_OFF,0.1*par_fit_CH_exp1_OFF];
    
    [par_fit_CH_exp2_OFF,dum0,residual,dum1,dum2,dum3,J]   = lsqcurvefit(@fun_2exp,par_start,par_fit,f_off,lb,ub);
    f_fit_exp2_OFF   = fun_2exp(par_fit_CH_exp2_OFF,par_fit);

    [h,p_exp2_OFF,ks2stat_exp2_OFF] = kstest2(f_off,f_fit_exp2_OFF);

    
    %- Calc error with error propagation
    ci = nlparci(par_fit_CH_exp2_OFF,residual,'jacobian',J);
    ci_exp2 = (ci(:,2) - ci(:,1)) / 2;
    ci_exp2_inv = (1./ci(:,2) - 1./ci(:,1)) / 2;
    
    err_CH_OFF_exp2(1,1)   = ci_exp2(1);
    err_CH_OFF_exp2(2:3,1) = ci_exp2(2:3)' ./ par_fit_CH_exp2_OFF(2:3).^2;
    
    
    % - Plot
    figure(h_fig_cumhist); set(gcf, 'visible', 'on');
    subplot(1,2,2)
    title('OFF-duration')
    hold on
    plot(x_off,f_off,'k','LineWidth',2)
    plot(x_off,f_fit_exp1_OFF,'b','LineWidth',2)
    plot(x_off,f_fit_exp2_OFF,'--r','LineWidth',2)
    hold off
    box on
    xlabel('Time [min]')
    ylabel('Cummulative frequency')

    v= axis;
    axis([0 200 v(3) v(4)])
    
    legend('Data', ['Fit 1-exp: t1=', sprintf('%.1f',1/par_fit_CH_exp1_OFF),', p=', sprintf('%.3f',p_exp1_OFF),char(10)], ...
                   ['Fit 2-exp: t1=', sprintf('%.1f',1/par_fit_CH_exp2_OFF(2)), ', t2=', sprintf('%.1f',1/par_fit_CH_exp2_OFF(3)),', F1=', sprintf('%.3f',par_fit_CH_exp2_OFF(1)),',',char(10), ... 
                                      'p=', sprintf('%.3f',p_exp2_OFF)],4)

    
    %============ Chi-squared
    
    %= 1 exp
    [parFit_OFF_exp1_chi2, fit_OFF_exp1_chi2, err_OFF_exp1_chi2,cts_data_chi2,bin_data_chi2] = fit_dist_FM_v1(duration_off_complete,n_bins,'exp1','chi2');
    
    %= 2 exp
    [parFit_OFF_exp2_chi2, fit_OFF_exp2_chi2, err_OFF_exp2_chi2,dum1,dum2,fit_all] = fit_dist_FM_v1(duration_off_complete,n_bins,'exp2','chi2');
    
    
    %- AIC
    N = sum(cts_data_chi2>0);
    K = 2+1;
    
    AICc_OFF_exp1_chi2 = N * log( err_OFF_exp1_chi2 / N) + 2*K + 2*K*(K+1) / (N-K-1);
    
    K = 4+1;    
    AICc_OFF_exp2_chi2 = N * log( err_OFF_exp2_chi2 / N) + 2*K + 2*K*(K+1) / (N-K-1);
    
    dAICc = AICc_OFF_exp1_chi2 - AICc_OFF_exp2_chi2;    
    ev_ratio_OFF_chi2 = 1/exp(-0.5*dAICc);
    
    
    
    %=== PLOTS
    figure(h_fig_Chi2); set(gcf, 'visible', 'off');
    subplot(2,3,4)
    hold on
    bar(bin_data_chi2,cts_data_chi2,'g')
    plot(bin_data_chi2,fit_OFF_exp1_chi2,'r','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('chi2: OFF duration - 1 exp')
    legend('Data', ['Fit: F1=', sprintf('%.1f',parFit_OFF_exp1_chi2(1)),', k1=', sprintf('%.1f',parFit_OFF_exp1_chi2(2)), char(10), 'chi2=', sprintf('%.1f',err_OFF_exp1_chi2)  ])
    
    
    subplot(2,3,5)
    hold on
    bar(bin_data_chi2,cts_data_chi2,'g')
    plot(bin_data_chi2,fit_OFF_exp1_chi2,'--b','LineWidth',2)
    plot(bin_data_chi2,fit_OFF_exp2_chi2,'r','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('chi2: OFF duration - 2 exp')
    legend('Data','Fit 1exp', ['F1=', sprintf('%.1f',parFit_OFF_exp2_chi2(1)),', k1=', sprintf('%.1f',parFit_OFF_exp2_chi2(2)), char(10), 'F2=', sprintf('%.1f',parFit_OFF_exp2_chi2(3)),', k2=', sprintf('%.1f',parFit_OFF_exp2_chi2(4)),char(10), 'chi2=', sprintf('%.1f',err_OFF_exp2_chi2)  ])

    subplot(2,3,6)
    hold on
    plot(bin_data_chi2,fit_all(:,1),'-r','LineWidth',2)
    plot(bin_data_chi2,fit_all(:,2),'-b','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('chi2: OFF duration - 2 exp')
    legend('Exp2-1','Exp2-2')
    
    
    %============ L2-norm
    
     %= 1 exp
    [parFit_OFF_exp1_L2, fit_OFF_exp1_L2, err_OFF_exp1_L2,cts_data_L2,bin_data_L2] = fit_dist_FM_v1(duration_off_complete,n_bins,'exp1','L2');
    
     %= 2 exp
    [parFit_OFF_exp2_L2, fit_OFF_exp2_L2, err_OFF_exp2_L2,dum1,dum2,fit_all] = fit_dist_FM_v1(duration_off_complete,n_bins,'exp2','L2');
    
    
    %- AIC
    N = sum(bin_data_L2>0);
    K = 2+1;
    
    AICc_OFF_exp1_L2 = N * log( err_OFF_exp1_L2 / N) + 2*K + 2*K*(K+1) / (N-K-1);
    
    K = 4+1;    
    AICc_OFF_exp2_L2 = N * log( err_OFF_exp2_L2 / N) + 2*K + 2*K*(K+1) / (N-K-1);
    
    dAICc = AICc_OFF_exp1_L2 - AICc_OFF_exp2_L2;    
    ev_ratio_OFF_L2 = 1/exp(-0.5*dAICc);
    
   
    %=== PLOTS
    figure(h_fig_L2); set(gcf, 'visible', 'off');
    subplot(2,3,4)
    hold on
    bar(bin_data_L2,cts_data_L2,'g')
    plot(bin_data_L2,fit_OFF_exp1_L2,'r','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('L2: OFF duration - 1 exp')
    legend('Data', ['Fit: F1=', sprintf('%.1f',parFit_OFF_exp1_L2(1)),', k1=', sprintf('%.1f',parFit_OFF_exp1_L2(2)), char(10), 'ssr=', sprintf('%.1f',err_OFF_exp1_L2)  ])

    subplot(2,3,5)
    hold on
    bar(bin_data_L2,cts_data_L2,'g')
    plot(bin_data_L2,fit_OFF_exp1_L2,'--b','LineWidth',2)
    plot(bin_data_L2,fit_OFF_exp2_L2,'r','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('L2: OFF duration - 2 exp')
    legend('Data','Fit 1exp', ['F1=', sprintf('%.1f',parFit_OFF_exp2_L2(1)),', k1=', sprintf('%.1f',parFit_OFF_exp2_L2(2)), char(10), 'F2=', sprintf('%.1f',parFit_OFF_exp2_L2(3)),', k2=', sprintf('%.1f',parFit_OFF_exp2_L2(4)),char(10), 'ssr=', sprintf('%.1f',err_OFF_exp2_L2)  ])
   
    subplot(2,3,6)
    hold on
    plot(bin_data_L2,fit_all(:,1),'-r','LineWidth',2)
    plot(bin_data_L2,fit_all(:,2),'-b','LineWidth',2)
    hold off
    box on
    xlabel('Duration [min]'); ylabel('Counts')
    title('L2: OFF duration - 2 exp')
    legend('Exp2-1','Exp2-2')
    
else
    parFit_OFF_exp1_chi2 = [0 0];
    err_OFF_exp1_chi2    = 0; 
    parFit_ON_exp2_chi2 = [0 0 0 0];
    err_ON_exp2_chi2    = 0;
    
    parFit_OFF_exp1_L2 = [0 0];
    err_OFF_exp1_L2   = 0; 
    parFit_OFF_exp2_L2 = [0 0 0 0];
    err_OFF_exp2_L2    = 0;
end   

if ishandle(h_fig_L2)
    file_save = '_PLOTS__FIT_duration_exponential_1-2_L2';
    file_save_full = fullfile(path_plots,file_save);
    
    saveas(h_fig_L2, file_save_full ,img_type)
    hgexport(h_fig_L2, file_save_full, hgexport('factorystyle'), 'Format', 'png');   % Conserves image as shown on the screen
    close(h_fig_L2)
end

if ishandle(h_fig_Chi2)
    file_save = '_PLOTS__FIT_duration_exponential_1-2_chi2';
    file_save_full = fullfile(path_plots,file_save);
    
    saveas(h_fig_Chi2, file_save_full ,img_type)
    hgexport(h_fig_Chi2, file_save_full, hgexport('factorystyle'), 'Format', 'png'); 
    close(h_fig_Chi2)
end    


if ishandle(h_fig_cumhist)
    file_save = '_PLOTS__FIT_duration_CUMULATIVE';
    file_save_full = fullfile(path_plots,file_save);
    
    saveas(h_fig_cumhist, file_save_full ,img_type)
    hgexport(h_fig_cumhist, file_save_full, hgexport('factorystyle'), 'Format', 'png'); 
    
    %close(h_fig_cumhist)
end 


%% ==========================================================================
% ==== Write to file
%==========================================================================

cell_data    = num2cell(summary_file);  
cell_write   = [file_name_outline_good,cell_data]'; 
  
name_save      = ['_MS2_QUANT_analyze_ON_OFF_',datestr(date,'yymmdd'),'_DurMin-',num2str(dur_min) ,'_IntTH-',num2str(th_quant) ,'_Smooth-',num2str(smooth_span),'_MinFrames-',num2str(N_frames_min),datestr(date,'_yymmdd'),'.txt'];
name_save_full = fullfile(path_name_files,name_save);

% Only write if FileName specified
if name_save ~= 0

    disp(' ')
    disp('================================')
    disp('Results saved in file listed below:')
    disp(name_save_full)

    %- String to define output  
    N_output = size(summary_file,2);
    string_print = ['%s\t', repmat('%.2f\t',1,N_output-1),'%.2f\n'];     

    %- Write output 
    fid = fopen(name_save_full,'w');
  
    fprintf(fid,'NUMBER OF CELLS: %d\n',N_files);
    
    fprintf(fid,'\n===== INTENSITY DISTRIBUTION\n');
    fprintf(fid,'MEDIAN (Max int, int int)\t%.1f\t%.1f\n',quant_all_median,intint_all_median);
    fprintf(fid,'MEAN (Max int, int int)\t%.1f\t%.1f\n',  quant_all_mean,intint_all_mean);
    fprintf(fid,'STDEV (Max int, int int)\t%.1f\t%.1f\n', quant_all_stdev,intint_all_stdev);
    
    fprintf(fid,'\n===== TOTAL ON-OFF\n');
    fprintf(fid,'Frames TOTAL\t%d\n',total_all);
    fprintf(fid,'Frames ON\t%.0f\n',total_on_all);
    fprintf(fid,'Frames OFF\t%.0f\n',total_off_all);
    fprintf(fid,'Perc ON\t%.3f\n',perc_on_all);
    fprintf(fid,'Perc OFF\t%.3f\n',perc_off_all);
    
    fprintf(fid,'\n===== ON/OFF events: \thow many \tsum [s] \tmedian [s] \tmean[s] \tstdev [s]&\n');
    fprintf(fid,'ON-complete\t%g\t%g\t%g\t%g\t%g\n',length(duration_on_complete_ALL),sum(duration_on_complete_ALL),median(duration_on_complete_ALL),mean(duration_on_complete_ALL),std(duration_on_complete_ALL));
    fprintf(fid,'ON-incomplete\t%g\t%g\t%g\t%g\t%g\n',length(duration_on_incomplete),sum(duration_on_incomplete),median(duration_on_incomplete),mean(duration_on_incomplete),std(duration_on_incomplete));
    fprintf(fid,'OFF-complete\t%g\t%g\t%g\t%g\t%g\n',length(duration_off_complete_ALL),sum(duration_off_complete_ALL),median(duration_off_complete_ALL),mean(duration_off_complete_ALL),std(duration_off_complete_ALL));
    fprintf(fid,'OFF-incomplete\t%g\t%g\t%g\t%g\t%g\n',length(duration_off_incomplete),sum(duration_off_incomplete),median(duration_off_incomplete),mean(duration_off_incomplete),std(duration_off_incomplete));
    
    fprintf(fid,'\n===== FIT OF COMPLETE DURATIONS - CUMMULATIVE HISTOGRAM\n');
    
    
    if ~isempty(duration_on_complete)    
    
        fprintf(fid,' = ON duration\n');
        fprintf(fid,'ON-1exp (t1,p-value,ks-state): \t %.1f \t %f \t %.4f \n',1/par_fit_CH_exp1_ON,p_exp1_ON,ks2stat_exp1_ON);
        fprintf(fid,'ON-1exp (ci: t1) \t %.1f \n\n',err_CH_ON_exp1);

        fprintf(fid,'ON-2exp (F1,t1,t2,p-value,ks-state): \t %.2f \t %.1f \t %.1f \t %f \t %.4f\n',par_fit_CH_exp2_ON(1),1/par_fit_CH_exp2_ON(2),1/par_fit_CH_exp2_ON(3),p_exp2_ON,ks2stat_exp2_ON);       
        fprintf(fid,'ON-2exp (ci: F1,t1,t2) \t %.2f \t %.1f \t %.1f \n\n',err_CH_ON_exp2(1),err_CH_ON_exp2(2),err_CH_ON_exp2(3));

    end
    
    if ~isempty(duration_off_complete)    
        fprintf(fid,'\n  = OFF duration\n');
        fprintf(fid,'OFF-1exp (t1,p-value,ks-state): \t %.1f \t %f \t %.4f \n',1/par_fit_CH_exp1_OFF,p_exp1_OFF,ks2stat_exp1_OFF);
        fprintf(fid,'OFF-1exp (ci: t1) \t %.1f \n\n',err_CH_OFF_exp1);

        fprintf(fid,'OFF-2exp (F1,t1,t2,p-value,ks-state): \t %.2f \t %.1f \t %.1f \t %f \t %.4f\n',par_fit_CH_exp2_OFF(1),1/par_fit_CH_exp2_OFF(2),1/par_fit_CH_exp2_OFF(3),p_exp2_OFF,ks2stat_exp2_OFF);       
        fprintf(fid,'OFF-2exp (ci: F1,t1,t2) \t %.2f \t %.1f \t %.1f \n\n',err_CH_OFF_exp2(1),err_CH_OFF_exp2(2),err_CH_OFF_exp2(3));
    end
    
    fprintf(fid,'\n===== FIT OF COMPLETE DURATIONS - Matlab exponential distribution\n');
    if ~isempty(duration_on_complete)   
        fprintf(fid,'ON (mean, 95%% ci): \t %.0f \t %.0f \n',round(mu_ON),round(ci_ON));
    end
    
     if ~isempty(duration_off_complete)   
        fprintf(fid,'OFF (mean, 95%% ci): \t %.0f \t %.0f \n',round(mu_OFF),round(ci_OFF));
     end

    fprintf(fid,'\n===== FIT OF COMPLETE DURATIONS - FLORIANs exponential distribution (L2)\n');
    
    if  ~isempty(duration_on_complete)   
        fprintf(fid,' = ON duration\n');
        fprintf(fid,'ON-1exp (F1,k1,ssr): \t %.0f \t %.0f \t %.0f \n',round(parFit_ON_exp1_L2(1)),round(parFit_ON_exp1_L2(2)),err_ON_exp1_L2);
        fprintf(fid,'ON-2exp (F1,k1,F2,k2,ssr): \t %.0f \t %.0f \t %.0f \t %.0f \t %.0f\n',round(parFit_ON_exp2_L2(1)),round(parFit_ON_exp2_L2(2)),round(parFit_ON_exp2_L2(3)),round(parFit_ON_exp2_L2(4)),err_ON_exp2_L2);       
        fprintf(fid,'AICc (1exp, 2exp, ev ratio): \t %.0f \t %.0f \t %.0f \n',AICc_ON_exp1_L2,AICc_ON_exp2_L2,ev_ratio_ON_L2);
    end
    
     if ~isempty(duration_off_complete)   
        fprintf(fid,'\n =  OFF duration\n');
        fprintf(fid,'OFF-1exp (F1,k1,ssr): \t %.0f \t %.0f \t %.0f \n',round(parFit_OFF_exp1_L2(1)),round(parFit_OFF_exp1_L2(2)),err_OFF_exp1_L2);
        fprintf(fid,'OFF-2exp (F1,k1,F2,k2,ssr):\t %.0f \t %.0f \t %.0f \t %.0f \t %.0f \n',round(parFit_OFF_exp2_L2(1)),round(parFit_OFF_exp2_L2(2)),round(parFit_OFF_exp2_L2(3)),round(parFit_OFF_exp2_L2(4)),err_OFF_exp2_L2);
        fprintf(fid,'AICc (1exp, 2exp, ev ratio): \t %.0f \t %.0f \t %.0f \n',AICc_OFF_exp1_L2,AICc_OFF_exp2_L2,ev_ratio_OFF_L2);
     end

        
    fprintf(fid,'\n===== FIT OF COMPLETE DURATIONS - FLORIANs exponential distribution (chi-squared)\n');
    
    if ~isempty(duration_on_complete)   
        fprintf(fid,' = ON duration\n');
        fprintf(fid,'ON-1exp (F1,k1,ssr): \t %.0f \t %.0f \t %.0f \n',round(parFit_ON_exp1_chi2(1)),round(parFit_ON_exp1_chi2(2)),err_ON_exp1_chi2);
        fprintf(fid,'ON-2exp (F1,k1,F2,k2,ssr): \t %.0f \t %.0f \t %.0f \t %.0f \t %.0f\n',round(parFit_ON_exp2_chi2(1)),round(parFit_ON_exp2_chi2(2)),round(parFit_ON_exp2_chi2(3)),round(parFit_ON_exp2_chi2(4)),err_ON_exp2_chi2);       
        fprintf(fid,'AICc (1exp, 2exp, ev ratio): \t %.0f \t %.0f \t %.0f \n',AICc_ON_exp1_chi2,AICc_ON_exp2_chi2,ev_ratio_ON_chi2);
    end
    
     if ~isempty(duration_off_complete)   
        fprintf(fid,'\n =  OFF duration\n');
        fprintf(fid,'OFF-1exp (F1,k1,ssr): \t %.0f \t %.0f \t %.0f \n',round(parFit_OFF_exp1_chi2(1)),round(parFit_OFF_exp1_chi2(2)),err_OFF_exp1_chi2);
        fprintf(fid,'OFF-2exp (F1,k1,F2,k2,ssr):\t %.0f \t %.0f \t %.0f \t %.0f \t %.0f \n',round(parFit_OFF_exp2_chi2(1)),round(parFit_OFF_exp2_chi2(2)),round(parFit_OFF_exp2_chi2(3)),round(parFit_OFF_exp2_chi2(4)),err_OFF_exp2_chi2);
        fprintf(fid,'AICc (1exp, 2exp, ev ratio): \t %.0f \t %.0f \t %.0f \n',AICc_OFF_exp1_chi2,AICc_OFF_exp2_chi2,ev_ratio_OFF_chi2);
     end
    
    
    fprintf(fid,'\n===== Measured ON-OFF durations\n');
    fprintf(fid,'ON-duration-complete [min]\t');
        fprintf(fid,'%g\t',duration_on_complete_ALL);
        fprintf(fid,'\n');
        
    fprintf(fid,'ON-duration-incomplete [min]\t');
        fprintf(fid,'%g\t',duration_on_incomplete);
        fprintf(fid,'\n');
        
   fprintf(fid,'ON-duration-always [min]\t');
        fprintf(fid,'%g\t',duration_on_always);
        fprintf(fid,'\n');     
        
 
    fprintf(fid,'\nOFF-duration-complete [min]\t');
        fprintf(fid,'%g\t',duration_off_complete_ALL);
        fprintf(fid,'\n');
        
    fprintf(fid,'OFF-duration-incomplete [min]\t');
        fprintf(fid,'%g\t',duration_off_incomplete);
        fprintf(fid,'\n');
   
    fprintf(fid,'OFF-duration-always [min]\t');
        fprintf(fid,'%g\t',duration_off_always);
        fprintf(fid,'\n');   
        
        
    fprintf(fid,'\nFILE\tTOTAL\tTOTAL_ON\tTOTAL_OFF\tPERC_ON\tPERC_OFF\tINT_AVG\tINT_MEDIAN\tINT_STDV\n'); % Header
    fprintf(fid, string_print,cell_write{:});       

    fclose(fid);
end


%% Save as .mat file
if flag_save
    name_save      = ['_MS2_QUANT_analyze_ON_OFF_SUMMARY_',datestr(date,'yymmdd'),'_DurMin-',num2str(dur_min) ,'_IntTH-',num2str(th_quant) ,'_Smooth-',num2str(smooth_span),'_MinFrames-',num2str(N_frames_min),datestr(date,'_yymmdd'),'.mat'];
    name_save_full = fullfile(path_name_files,name_save);
    save(name_save_full)
end
