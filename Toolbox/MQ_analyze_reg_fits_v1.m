function [reg_stats, reg_prop, status_reg_analyze] = MQ_analyze_reg_fits_v1(reg_prop,fit_limits,int_min)

N_reg              = length(reg_prop);
status_reg_analyze = 0;

for i_reg = 1:N_reg
    
        if reg_prop(i_reg).flag_BGD == 0 && reg_prop(i_reg).flag_OPB == 0
            
            reg_stats(i_reg).flag_TS = 1;
            summary_fit       = reg_prop(i_reg).quant_fit_summary_all;
            summary_fit_first = reg_prop(i_reg).quant_fit_summary_all_first;   
            
            
            if not(isempty(summary_fit))

                %- Consider only frames with the filtered intensity above the threshold
               % detect_int = reg_prop(i_reg).detect_int;
               % ind_avg    = (detect_int>int_min);

                if isfield(reg_prop(i_reg),'detect_int')
                    detect_int = reg_prop(i_reg).detect_int;
                else
                    detect_int = inf*ones(size(reg_prop(i_reg).time));
                end    
                ind_avg    = (detect_int>int_min);
                
                
                %- Calculate mean, median, stdev
                status_reg_analyze = 1;
                reg_stats(i_reg).status_analyze = 1;
                reg_stats(i_reg).ind_avg = ind_avg;
                
                reg_stats(i_reg).sigmaxy_mean =   round(mean(summary_fit(ind_avg,3)));
                reg_stats(i_reg).sigmaz_mean  =   round(mean(summary_fit(ind_avg,4)));
                reg_stats(i_reg).bgd_mean     =   round(mean(summary_fit(ind_avg,2)));

                reg_stats(i_reg).sigmaxy_median = round(median(summary_fit(ind_avg,3)));
                reg_stats(i_reg).sigmaz_median  = round(median(summary_fit(ind_avg,4)));
                reg_stats(i_reg).bgd_median     = round(median(summary_fit(ind_avg,2)));

                reg_stats(i_reg).sigmaxy_stdev =  round(std(summary_fit(ind_avg,3)));
                reg_stats(i_reg).sigmaz_stdev  =  round(std(summary_fit(ind_avg,4)));
                reg_stats(i_reg).bgd_stdev     =  round(std(summary_fit(ind_avg,2)));


                %- PREVIOUS: Calculate mean, median, stdev
                reg_stats(i_reg).sigmaxy_first_mean =  round(mean(summary_fit_first(ind_avg,3)));
                reg_stats(i_reg).sigmaz_first_mean  =  round(mean(summary_fit_first(ind_avg,4)));
                reg_stats(i_reg).bgd_first_mean     =  round(mean(summary_fit_first(ind_avg,2)));

                reg_stats(i_reg).sigmaxy_first_median =  round(median(summary_fit_first(ind_avg,3)));
                reg_stats(i_reg).sigmaz_first_median  =  round(median(summary_fit_first(ind_avg,4)));
                reg_stats(i_reg).bgd_first_median     =  round(median(summary_fit_first(ind_avg,2)));

                reg_stats(i_reg).sigmaxy_first_stdev =  round(std(summary_fit_first(ind_avg,3)));
                reg_stats(i_reg).sigmaz_first_stdev  =  round(std(summary_fit_first(ind_avg,4)));
                reg_stats(i_reg).bgd_first_stdev     =  round(std(summary_fit_first(ind_avg,2)));            

                %- Set default range 
                if isempty(reg_prop(i_reg).fit_limits)
                   reg_prop(i_reg).fit_limits.sigma_xy_min = fit_limits.sigma_xy_min_def;
                   reg_prop(i_reg).fit_limits.sigma_xy_max = fit_limits.sigma_xy_max_def;
                   reg_prop(i_reg).fit_limits.sigma_z_min  = fit_limits.sigma_z_min_def;
                   reg_prop(i_reg).fit_limits.sigma_z_max  = fit_limits.sigma_z_max_def;
                   reg_prop(i_reg).fit_limits.bgd_min      = fit_limits.bgd_min_def;
                   reg_prop(i_reg).fit_limits.bgd_max      = fit_limits.bgd_max_def;      
                end
            else
                reg_stats(i_reg).status_analyze = 0;
            end
  
        else          
           reg_stats(i_reg).flag_TS = 0;
        end
end
