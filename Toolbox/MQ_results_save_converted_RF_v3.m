function [file_save] = MQ_results_save_converted_RF_v3(file_name_full,parameters)


data_converted  = parameters.data_converted;
time            = parameters.time;
col_save        = parameters.col_save;
col_text        = parameters.col_text;


file_save = 1;



%=== Save Sum

% Only write if FileName specified
if file_save ~= 0
       
    %- Output values
    quant_output = [time data_converted(:,col_save)];

    %- String to define output    
    string_print = '%.2f;%.2f\n';     

    %- Write output 
    fid = fopen(file_name_full,'w');
  
    fprintf(fid,'Time(s);%s\n',col_text); % Header
    fprintf(fid, string_print,quant_output');       

    fclose(fid);
end

%cd(current_dir)
