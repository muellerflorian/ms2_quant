function MS2_correct_calibration_opb_v4

%% Flags to save images: regions and opb measurement
flags.save              = 1;  % 0: NO, 1:png, 2:EPS; will be save in a subfolder: Plots_PB_correction
flags.confirm_name_save = 0;  % 0: don't ask for confirmation of file-name to save, 1: ask for confirmation


%% Load Image
[file_name_all,path_name] = uigetfile({'.tif';'.dv'},'Select image for OPB correction.','MultiSelect','on');

if not(iscell(file_name_all))
   dum =  file_name_all;
   
   file_name_all = [];
   file_name_all{1} = dum;  
end

N_files = length(file_name_all);


%% Loop over all files
flag_regions = 0;

%- Load new image
img = FQ_img;
 
for i_file = 1:N_files

    file_name_loop = file_name_all{i_file};
    [dum, name_img,ext_img]= fileparts(file_name_loop); 
    file_name_full = fullfile(path_name,file_name_loop );

    img = img.reinit;
    status_file = img.load_img(file_name_full,'raw');
    
    if ~status_file
        disp('File could not be opened')
        disp(file_name_full)
        continue
    end
    
    %- Assign to other structure
    img_struct.data = img.raw;
    img_MIP = max(img.raw,[],3);

    h_draw = 100;
    
    if not(flag_regions)
    
        %= Define regions for measurement

        %== [1] Define background region

        h_draw = figure(100);  set(h_draw,'Color','w')
        imshow(img_MIP,[]);
        colormap('hot')
        title('Pick the background for substraction')
        mask_bgd = roipoly; % Binary mask
        poly_bgd = mask2poly(mask_bgd,'MINDIST'); 
        x_bgd = poly_bgd(2:end,1);
        y_bgd = poly_bgd(2:end,2);
        hold on
            plot(x_bgd,y_bgd,'b','Linewidth', 2) 
        hold off
        
        %== [2] Define region for observational photobleaching correction 
        figure(100); 
        title('Pick the observational photobleaching correction')
        mask_opb = roipoly; % Binary mask
        poly_opb = mask2poly(mask_opb,'MINDIST'); 
        x_opb = poly_opb(2:end,1);
        y_opb = poly_opb(2:end,2);
        hold on
            plot(x_opb,y_opb,'g','Linewidth', 2) 
        hold off
        flag_regions = 1;
    end


    % == Show regions used
    if ~ishandle(h_draw)
        h_draw = figure; 
        imshow(img_MIP,[]);
        colormap('hot')
        hold on
        plot(x_bgd,y_bgd,'b','Linewidth', 2) 
        plot(x_opb,y_opb,'g','Linewidth', 2) 
        hold off
    end   

    if flags.save     
        
       folder_save =  fullfile(path_name,'Plots_PB_correction');  
       is_dir = exist(folder_save,'dir'); 

       if is_dir == 0
           mkdir(folder_save)
       end
        
        filename_save      = fullfile(['REGIONS_',name_img]);
        filename_save_full = fullfile(folder_save,filename_save);

         if flags.save == 1
            saveas(h_draw,filename_save_full,'png')  
         elseif flags.save == 2
            saveas(h_draw,filename_save_full,'eps') 
         end

         close(h_draw)
    end


    %=== Background subtraction and measurement in control region
    N_Z = size(img_struct.data,3);

    opb = [];
    bgd = [];

    %- Loop over stack
    for i_Z = 1:N_Z

        %- Get data for frame, determine background, and subtract it
        data     = double(img_struct.data(:,:,i_Z));
        bgd      = mean(data(mask_bgd));
        data_bgd = data - bgd;


        %- Get values in control region
        opb_corr = mean(data_bgd(mask_opb));

        if i_Z == 1
            opb_corr_norm = 1; 
        else
            opb_corr_norm = opb_corr / img_struct.opb_corr(1,1);
        end


        data_opb_corr = data_bgd / opb_corr_norm;

        %- Save images and other stuff
        img_struct.data_bgd_sub(:,:,i_Z) = data_bgd;
        img_struct.data_opb_corr(:,:,i_Z) = data_opb_corr;    

        img_struct.bgd(i_Z,1)           = bgd;
        img_struct.opb_corr(i_Z,1)      = opb_corr;
        img_struct.opb_corr_norm(i_Z,1) = opb_corr_norm;

    end        

    %=== Plots

    h1 = figure; set(h1,'Color','w')
    subplot(1,2,1)
    hold on
    plot(img_struct.bgd,'b')
    plot(img_struct.opb_corr,'r')
    hold off
    box on
    legend('BGD','OPB correction')
    title('Area measurements')

    subplot(1,2,2)
    hold on
    plot(img_struct.opb_corr_norm,'r')
    hold off
    box on
    title('OPB correction')

    

    if flags.save     
        filename_save  = fullfile(['QUANT_',name_img]);
        filename_save_full = fullfile(folder_save,filename_save);

         if flags.save == 1
            saveas(h1,filename_save_full,'png')  
         elseif flags.save == 2
            saveas(h1,filename_save_full,'eps') 
         end

         close(h1)
    end

    %- Save intensity data
    int_all = [];
    int_all(:,1) = img_struct.opb_corr;
    int_all(:,2) = img_struct.bgd;
    int_all(:,3) = img_struct.opb_corr_norm;

    data_all(i_file).int = int_all;

    %=== Save image

    %- Save image
    file_name_save      = [name_img,'_opb.tif'];
    path_name_save      = path_name;
    file_name_save_full = fullfile(path_name,file_name_save);
    
    if flags.confirm_name_save
        [file_name_save,path_name_save] = uiputfile(file_name_save,'Specify file name to save filtered image');   
    end

    if file_name_save ~= 0
        name_save = fullfile(path_name_save,file_name_save);
        
        %- Choose in which format the file should be saved
        int_max = max(max(img_struct.data_opb_corr));
        
        if int_max < 65536
            image_save_v2(img_struct.data_opb_corr,name_save,16);
            disp('Image saved as 16-bit')
        else
            image_save_v2(img_struct.data_opb_corr,name_save,32);
            disp('Image saved as 32-bit')
        end
        
        disp(['File-name: ', name_save])
    end
end