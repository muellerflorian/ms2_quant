function [parFit_OPB, tList_all, opb_fit_summary] = MQ_OPB_quant_v3(int,bgd,parameters)


warning('off')

%% Get parameters
dT    = parameters.dT;
flags = parameters.flags;

%% int has to be a row vector
if size(int,2) ~= 1
    int = int';
end

if size(bgd,2) ~= 1
    bgd = bgd';
end

%% Subtract background
int_bgd = int - bgd;

%% Get curve and time-points
tList     = (0:length(int_bgd)-1)'*dT;
tList_all = tList;

ind_nan = find(isnan(int_bgd));
int_bgd(ind_nan) = [];
tList(ind_nan)   = [];

%% Fit to triple exponential

switch flags.fit_mode

    case {'exp','EXP'}
        max_int  = mean(int_bgd(1:10));

        %- Boundaries of fitting parameters
        lb = [0       0     0       0      0       0 ];
        ub = [max_int 100000 max_int 100000  max_int 100000];

        %- Fit
        par_mod     = {2,tList,0};
        par_start   = [max_int/3 100 max_int/3 1000 max_int/3 2500];
        opb_initial = fun_trip_exp_dec_v1(par_start,par_mod);

        opts = optimset('Display','off');
        
        %- Best fit
        parFit_OPB = lsqcurvefit(@fun_trip_exp_dec_v1, par_start, par_mod, int_bgd ,lb,ub,opts);
        opb_fit    = fun_trip_exp_dec_v1(parFit_OPB,par_mod);

    case {'lin','LIN'}
        parFit_OPB = polyfit(tList,int_bgd,1);
        opb_fit    = polyval(parFit_OPB,tList);
end

%- Assign parameters  


opb_fit_norm         = opb_fit/opb_fit(1);
opb_fit_summary(:,1) = tList;
opb_fit_summary(:,2) = int_bgd;  % Aug 8 2013: used to be int
opb_fit_summary(:,3) = opb_fit;
opb_fit_summary(:,4) = opb_fit_norm;


global opb_fit_global
opb_fit_global = opb_fit_summary;


%% Plot output
if flags.output   
    
    h_fig = figure; 
    
    %- When saving don't show figure
    if flags.save 
         set(h_fig, 'visible', 'off')
    end
       
    subplot(1,3,1)
    hold on
    plot(tList, int,'k')
    plot(tList, int_bgd,'b')
    if length(bgd) == 1
        plot(tList, bgd*ones(length(tList)),'r')
    else
        plot(tList, bgd,'r')
    end
    hold off
    box on
    legend('Measured','Minus BGD','BGD')
    xlabel('Time [s]')
    ylabel('Intensity [AU]')    
    v= axis;
    axis([tList(1) tList(end) 0 v(4)])
    
    subplot(1,3,2)
    hold on
    plot(tList, int_bgd,'k')
    plot(tList, opb_fit,'r')
    hold off
    box on
    legend('Measured','Best fit')
    xlabel('Time [s]')
    ylabel('Intensity [AU]') 
    v= axis;
    axis([tList(1) tList(end) 0 v(4)])
    
    subplot(1,3,3)
    plot(tList, opb_fit_norm,'r')
    hold off
    box on
    legend('OPB correction curve')
    xlabel('Time [s]')
    v= axis;
    axis([tList(1) tList(end) 0 1.05])
    
    set(h_fig,'Color','w')

    if flags.save 
        saveas(h_fig,parameters.file_save,'png')
        close(h_fig);      
    end
end

warning('on')

