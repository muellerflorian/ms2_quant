# MS2-quant #

Matlab toolbox to analyze single molecule MS2 data. 

### Requirements ###
Program was developed and tested in Matlab R2014a on Mac OS 10.9.5. Some functions might NOT work in earlier versions. Please contact us if you encounter any problems.

The following toolboxes are required

* Optimization toolbox
* Statistics toolbox
* Image processing toolbox
* (Optional) Parallel processing toolbox 

Program is provided as an ZIP archive. On rare occasions problems might occur when unzipping under Windows 7 with the built-in unzipper. Here the folders are encrypted and are shown in green. This can be avoided by using the free program 7-Zip (http://www.7-zip.org/). 

### Installation instructions ###

1. Download code from the Downloads section (Select Download repository) https://bitbucket.org/muellerflorian/ms2_quant/overview
1. The code is provided in an zip archive with a name like muellerflorian-ms2_quant-b4177b99dc53.zip. The last part of the name will change over time reflecting new version.  To avoid updating the path-definition each time you download a new version of MS2-quant, we recommend copying the content of this archive to a folder called MS2_quant in the user folder of Matlab. 
1. Under windows this folder is usually C:\Users\usr_name\Documents\MATLAB, where usr_name is the user name. This path can be found with the Matlab command userpath. 
1. Create a folder MS2-quant, and copy the content of the downloaded archive in this folder. In the example above, you will have a folder C:\Users\muellerf\Documents\MATLAB\MS2_QUANT with all the source code.
1. Update Matlab path definition. This can be done with a few simple steps in Matlab.
    1. In the Matlab menu select File > Set Path
    1. This will open a dialog box. In this box select Add with subfolders …
    1. This will open another dialog. Here select the folder of MS2_QUANT from step 1, e.g. C:\Users\muellerf\Documents\MATLAB\MS2_quant. Click OK.
    1. To save this settings press Save. Depending on the settings of the installation of Matlab this might results in a warning saying that the changes to path cannot be saved. Matlab proposes to save the path-definition file pathdef.m to another location. Click Yes. Select a directory of choice, e.g. the Matlab work directory of the user.

For more details, we refer to the FQ manual.

### Getting started ###
We provide several files to help new user getting used to MS2-quant. We recommend working with the example data to familiarize yourself with the basic functionality. 

* Example data. This zip archive contains already processed MS2 data. For these data the entire analysis has been performed and the different results files are provided.

* User manual. A PDF containing with a detailed user manual describing the entire functionality of FISH-quant. 

### Development team ###
Developed by Florian MUELLER (Institut Pasteur,ENS Paris).