function varargout = MS2_QUANT_batch_quant(varargin)
% MS2_QUANT_BATCH_QUANT MATLAB code for MS2_QUANT_batch_quant.fig

% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MS2_QUANT_batch_quant

% Last Modified by GUIDE v2.5 18-Dec-2015 14:58:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MS2_QUANT_batch_quant_OpeningFcn, ...
                   'gui_OutputFcn',  @MS2_QUANT_batch_quant_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MS2_QUANT_batch_quant is made visible.
function MS2_QUANT_batch_quant_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

function varargout = MS2_QUANT_batch_quant_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%==========================================================================
% FOLDER LIST
%==========================================================================

%=== Add folders
function button_add_folders_Callback(hObject, eventdata, handles)

choice  = 'Yes';
ind_dir = 1;

status_advanced = get(handles.checkbox_advanced_folder_selection,'Value');

while strcmp(choice,'Yes')
    
    %- Ask for folder and add it if specified        
    if status_advanced
        folder_name = uipickfiles('REFilter','^')';
    else
        folder_name = uigetdir(pwd,'Specify folder with pre-processed .mat file') ; 
    end
    if ~iscell(folder_name)
        dum=folder_name;
        folder_name={dum};
    end
    
    
    if folder_name{1} ~= 0

        str_list_old = get(handles.listbox_files,'String');

        if isempty(str_list_old)
            str_list_new = folder_name;
        else
            str_list_new = [str_list_old;folder_name];
        end

        %- Sometimes there are problems with the list-box value
        if isempty(get(handles.listbox_files,'Value'))
            set(handles.listbox_files,'Value',1);
        end

        set(handles.listbox_files,'String',str_list_new);

        %- Save results
        guidata(hObject, handles); 

    end

    
    %- Ask if more folders should be added
    choice = questdlg('Add another folder for quantification?','MQ-batch', 'Yes','No','Yes'); 
end


%=== Delete selected file
function button_folder_delete_Callback(hObject, eventdata, handles)

str_list = get(handles.listbox_files,'String');

if not(isempty(str_list))

    %- Ask user to confirm choice
    choice = questdlg('Do you really want to remove this folder?', 'MS2-QUANT', 'Yes','No','No');

    if strcmp(choice,'Yes')

        %- Extract index of highlighted cell
        ind_sel  = get(handles.listbox_files,'Value');

        %- Delete highlighted cell
        str_list(ind_sel) = [];
        set(handles.listbox_files,'String',str_list)
        handles.file_list(ind_sel) = [];
        
        %- Save results
        guidata(hObject, handles);    
        
        %- Update status
        set(handles.listbox_files,'Value',1)
    end
end


%== Delete all files
function button_delete_all_Callback(hObject, eventdata, handles)

%- Ask user to confirm choice
choice = questdlg('Do you really want to remove all folders?', 'MS2-QUANT', 'Yes','No','No');

if strcmp(choice,'Yes')
    
    set(handles.listbox_files,'String',{})
    set(handles.listbox_files,'Value',1)
    handles.file_list = {};
    
    %- Save results
    guidata(hObject, handles);
end


%==========================================================================
% PROCESS
%==========================================================================


%=== Process all files
function button_process_Callback(hObject, eventdata, handles)

set(handles.h_MQ_batch_quant,'Pointer','watch');

%- Get files
folder_list = get(handles.listbox_files,'String');
N_folder  = length(folder_list);

%- Suffix for results
suffix_results  = ['_QUANT_', datestr(date,'yymmdd'), '.txt'];
    
status_update(hObject, eventdata, handles,{' ';'=== PROCESSING: details in main Matlab window'}); 
    

%- Loop over all folders   
for i_folder = 1:N_folder
    
    folder_loop = folder_list{i_folder};
    dir_struct  = dir(folder_loop);
    
    fprintf('\n ===== PROCESSING FOLDER\n%s\n',folder_loop)

    %- Get index of all directories
    ind_dir = find(cell2mat({dir_struct.isdir}'));
    
    fprintf('\n == LOOPING OF ALL subfolders\n')
    
    for iDum = 1:length(ind_dir)
        
        %- Get file-name
        iProc      = ind_dir(iDum);
        name_full  = dir_struct(iProc).name;
        [dum ,name_base] = fileparts(name_full);
        
        %=== Check if folder with same name exists
        if any(strcmp('MQ_results',name_base)) 
            
            fprintf('\n = Found MQ_results folder - will attempt processing\n')
            
            %=== Get folder content
            folder_MQ       = fullfile(folder_loop,'MQ_results');
            dir_struct_proc = dir(folder_MQ);
            names_dir_proc  = {dir_struct_proc.name};
            
            %==== Find .mat file
            cell_ind_mat_file = strfind(names_dir_proc,'_MQ_batch_preprocessing');
            ind_mat_file      = find(~cellfun(@isempty, cell_ind_mat_file));
    
            if isempty(ind_mat_file)
                fprintf('\n No pre-processed file found in folder. Will continue with next file.\n')
                fprintf('%s',folder_MQ); 
                continue
                
            elseif length(ind_mat_file)    > 1
                fprintf('\n More than one pre-processed file found in folder. Make sure that only one is present. Will continue with next file.\n')
                fprintf('%s',folder_MQ)
                continue
            else
                name_mat = names_dir_proc{ind_mat_file};
                disp(['Mat file      : ', name_mat])        
            end
            
            
            %==== Region file
            cell_ind_reg_file = strfind(names_dir_proc,'__regions.txt');
            ind_reg_file = find(~cellfun(@isempty, cell_ind_reg_file));

            if isempty(ind_reg_file)
                fprintf('\n No region file found in folder. Will continue with next file.\n')
                fprintf('%s',folder_MQ); 
                continue
                
            elseif length(ind_mat_file)    > 1
                fprintf('\n More than one region file found in folder. Make sure that only one is present. Will continue with next file.\n')
                fprintf('%s',folder_MQ)
                continue
            else
                name_reg = names_dir_proc{ind_reg_file};
                disp(['Region file   : ', name_reg])        
            end 
            
             
            %==== Settings file
            cell_ind_sett_file = strfind(names_dir_proc,'_settings.txt');
            ind_sett_file = find(~cellfun(@isempty, cell_ind_sett_file));
            name_sett = [];
            if isempty(ind_sett_file)
                fprintf('\n No settings file found in folder.\n')
                fprintf('%s',folder_MQ); 
                status_sett = 0;
                
                
            elseif length(ind_mat_file)    > 1
                fprintf('\n More than one settings file found in folder. Make sure that only one is present. \n')
                fprintf('%s',folder_MQ)
                status_sett = 0;
                continue
            else
                name_sett = names_dir_proc{ind_sett_file};
                status_sett = 1;
                disp(['Settings file : ', name_sett])        
            end 
            
            
            %===== Perform processing
            fprintf('\n\n = Processing\n')
            clear handles_proc
            handles_proc.options_quant = MQ_settings_quant_init_v2;

            %- [1.a] Load region
            [handles_proc.reg_prop, file_names, par_microscope, BGD_img] = MQ_load_region_v3(fullfile(folder_MQ,name_reg));   

            %- Check if positions are saved
            N_reg = length(handles_proc.reg_prop);
            status_reg_pos = 0;
            for i_reg =1:N_reg
                if ~isempty(handles_proc.reg_prop(i_reg).pos)
                    status_reg_pos = 1;
                end
            end

            if ~status_reg_pos
                fprintf('\n\n = POSITION OF TS IS NOT DEFINED. FILE WILL NOT BE PROCESSED.\n')
                continue
            end


            %- [1.b] Load .mat file
            disp('Loading data ...')
            load(fullfile(folder_MQ,name_mat));

            %- Images & regions
            global MQ_img_stack
            MQ_img_stack = struct_save.MQ_img_stack;
            struct_save.MQ_img_stack = [];

            img_stack_first     = MQ_img_stack(1).data;  % Necessary to define binary mask for OPB measurement 
            [dim.Y dim.X dim.Z] = size(img_stack_first);
            handles_proc.dim         = dim;

            handles_proc.par_microscope = par_microscope;
            handles_proc                = MQ_GUI_analyze_reg_prop(handles_proc);
           
            %- Assign results of photobleaching correction if present
            handles_proc.OPB.curves = 0;
            if isfield(struct_save,'OPB')
                if isfield(struct_save.OPB,'curves')
                    handles_proc.OPB.curves  = struct_save.OPB.curves;
                end
            else
                disp(' No results for OPB correction found in MAT file!');
            end

            % Update a few status reports 
            handles_proc.status_image       = 1;
            handles_proc.status_filtered    = 1;    % Image filtered
            handles_proc.status_outline     = 1;    % Outlines defined
            handles_proc.status_plot_first  = 1;    % Indicate if plot command was never used
            handles_proc.status_time_series = 1;
            handles_proc.status_OPB_quant   = 1;

            flag_opb_apply = 1;
            opb_norm    = [];
            bgd_imaging = [];

            %- Calculate theoretical PSF
            [PSF_theo.xy_nm, PSF_theo.z_nm] = sigma_PSF_BoZhang_v1(par_microscope);
            PSF_theo.xy_pix = PSF_theo.xy_nm / par_microscope.pixel_size.xy ;
            PSF_theo.z_pix  = PSF_theo.z_nm  / par_microscope.pixel_size.z ;

            handles_proc.PSF_theo = PSF_theo;
            handles_proc          = MQ_init_fit_limits_v1(handles_proc);

            %- [1.c] Load settings
            int_min = -inf;
            if ~isempty(name_sett)
                handles_proc = MQ_settings_load_v2(fullfile(folder_MQ,name_sett),handles_proc);

                if isfield(handles_proc,'par_detect')
                    if isfield(handles_proc.par_detect,'int_min')
                        int_min = handles_proc.par_detect.int_min;
                    end
                end
            end
            
            %====== Fitting
            dum_str  = get(handles.popup_select_fit,'String');
            dum_val  = get(handles.popup_select_fit,'Value');
            options_fit.flag_struct.fit_data  = dum_str{dum_val};
      
            %= Parameters for fitting  
            options_fit.pixel_size =  par_microscope.pixel_size;
            options_fit.par_start  =  [];
            options_fit.PSF_theo   =  handles_proc.PSF_theo;
            options_fit.fit_limits =  handles_proc.fit_limits;

            par_fit.options_fit        = options_fit;
            par_fit.par_microscope     = par_microscope;
            par_fit.options_quant      = handles_proc.options_quant;
            par_fit.flag_struct.output = 0;
            par_fit.flag_struct.fit_TS = 1;
            par_fit.flag_struct.fit_TS_Gauss = 1;
            
            par_fit.opb_norm           = opb_norm;
            par_fit.bgd_imaging        = bgd_imaging;
            par_fit.flag_opb_apply     = flag_opb_apply;
            par_fit.status_filtered    = handles_proc.status_filtered;

            %== First round of fitting
            par_fit.options_fit.fit_mode   =  'sigma_free_xz'; 
            handles_proc.reg_prop          = MQ_quant_3D_v11(handles_proc.reg_prop,par_fit);

            %== Restrict paramters
            fprintf('Restrict parameters\n')
            [reg_stats, reg_prop, status_reg_analyze] = MQ_analyze_reg_fits_v1(handles_proc.reg_prop,handles_proc.fit_limits,int_min);

            for i_reg = 1:N_reg   
               handles_proc.reg_prop(i_reg).fit_limits.sigma_xy_min = reg_stats(i_reg).sigmaxy_median - reg_stats(i_reg).sigmaxy_stdev;
               handles_proc.reg_prop(i_reg).fit_limits.sigma_xy_max = reg_stats(i_reg).sigmaxy_median + reg_stats(i_reg).sigmaxy_stdev;
               handles_proc.reg_prop(i_reg).fit_limits.sigma_z_min  = reg_stats(i_reg).sigmaz_median  - reg_stats(i_reg).sigmaz_stdev;
               handles_proc.reg_prop(i_reg).fit_limits.sigma_z_max  = reg_stats(i_reg).sigmaz_median  + reg_stats(i_reg).sigmaz_stdev;
               handles_proc.reg_prop(i_reg).fit_limits.bgd_min      = reg_stats(i_reg).bgd_median;
               handles_proc.reg_prop(i_reg).fit_limits.bgd_max      = reg_stats(i_reg).bgd_median     + 1;                   
            end       

            %== Second round of fitting
            par_fit.options_fit.fit_mode   =  'sigma_free_xz'; 
            handles_proc.reg_prop = MQ_quant_3D_v11(handles_proc.reg_prop,par_fit);
            
      
            %== Delete image
            MQ_img_stack = [];
            
            %==  SAVE RESULTS     
            for i_reg = 1:N_reg
                reg_prop_loop = handles_proc.reg_prop(i_reg);

                if reg_prop_loop.flag_OPB == 0 && reg_prop_loop.flag_BGD == 0 

                    %- Get file name
                    name_file = reg_prop_loop.label;
                    file_name_save   = [name_file,suffix_results];      

                    %- Full name to save file
                    file_name_full   = fullfile(folder_MQ,file_name_save); 

                    parameters.reg_prop           = reg_prop_loop;
                    parameters.par_microscope     = handles_proc.par_microscope;
                    parameters.path_name_image    = struct_save.path_name_image;
                    parameters.file_name_image    = struct_save.file_name_image;
                    parameters.file_name_settings = name_sett;
                    parameters.version            = 'vi0';
                    parameters.BGD_img            = BGD_img;
                    parameters.path_save          = folder_MQ;
                    parameters.label              = reg_prop_loop.label;
                    parameters.detect_int         = int_min;
                    %- Verify if the detection intensity was saved
                    if isfield(reg_prop,'detect_int')
                        parameters.detect_int         = reg_prop_loop.reg_prop.detect_int;
                    end
                    
                    %- Photobleaching correction
                    if isfield(handles_proc,'OPB')
                        parameters.OPB_curves = handles_proc.OPB.curves;
                    else
                        parameters.OPB_curves = [];
                    end


                    %- Limits of fit
                    if isempty(reg_prop_loop.fit_limits)
                        parameters.limits = handles_proc.fit_limits;
                    else
                        parameters.limits = reg_prop_loop.fit_limits;
                    end   

                    MQ_results_save_v6(file_name_full,parameters);
                end
            end
                   
            if ~status_sett 
                
                disp('No settings file defined ...')

            end
            
        end
    end
end
    
set(handles.h_MQ_batch_quant,'Pointer','arrow');


%==========================================================================
% MISC functions
%==========================================================================

%== Update status
% status_update(hObject, eventdata, handles,{'  ';'## Settings loaded'});     
function status_update(hObject, eventdata, handles,status_text)
status_old = get(handles.listbox_status,'String');
status_new = [status_old;status_text];
set(handles.listbox_status,'String',status_new)
set(handles.listbox_status,'ListboxTop',round(size(status_new,1)))
drawnow
guidata(hObject, handles); 



%==========================================================================
% NOT USED
%==========================================================================

function listbox_files_Callback(hObject, eventdata, handles)

function listbox_files_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_subfolder_reg_Callback(hObject, eventdata, handles)

function text_subfolder_reg_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function checkbox1_Callback(hObject, eventdata, handles)

function listbox_status_Callback(hObject, eventdata, handles)

function listbox_status_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function checkbox_advanced_folder_selection_Callback(hObject, eventdata, handles)

function popup_select_fit_Callback(hObject, eventdata, handles)

function popup_select_fit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
