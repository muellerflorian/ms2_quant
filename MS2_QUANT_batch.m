function varargout = MS2_QUANT_batch(varargin)
% MS2_QUANT_BATCH MATLAB code for MS2_QUANT_batch.fig
%      MS2_QUANT_BATCH, by itself, creates a new MS2_QUANT_BATCH or raises the existing
%      singleton*.
%
%      H = MS2_QUANT_BATCH returns the handle to a new MS2_QUANT_BATCH or the handle to
%      the existing singleton*.
%
%      MS2_QUANT_BATCH('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MS2_QUANT_BATCH.M with the given input arguments.
%
%      MS2_QUANT_BATCH('Property','Value',...) creates a new MS2_QUANT_BATCH or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MS2_QUANT_batch_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MS2_QUANT_batch_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MS2_QUANT_batch

% Last Modified by GUIDE v2.5 17-Apr-2014 10:27:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MS2_QUANT_batch_OpeningFcn, ...
                   'gui_OutputFcn',  @MS2_QUANT_batch_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MS2_QUANT_batch is made visible.
function MS2_QUANT_batch_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


%- Default parameters
handles = MQ_init_v5(handles);
handles = MQ_init_fit_limits_v1(handles);
handles = MQ_populate_v2(handles);
handles.options_quant = MQ_settings_quant_init_v2;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MS2_QUANT_batch wait for user response (see UIRESUME)
% uiwait(handles.h_MQ_batch);


% --- Outputs from this function are returned to the command line.
function varargout = MS2_QUANT_batch_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%==========================================================================
% Experimental parameters
%==========================================================================

%== Modify the experimental settings
function button_define_exp_Callback(hObject, eventdata, handles)
par_microscope = handles.par_microscope;

dlgTitle = 'Experimental parameters';

prompt(1) = {'Pixel-size xy [nm]'};
prompt(2) = {'Pixel-size z [nm]'};
prompt(3) = {'Refractive index'};
prompt(4) = {'Numeric aperture NA'};
prompt(5) = {'Emission wavelength'};
prompt(6) = {'Excitation wavelength'};
prompt(7) = {'Microscope'};
prompt(8) = {'Time between frames [s]'};

defaultValue{1} = num2str(par_microscope.pixel_size.xy);
defaultValue{2} = num2str(par_microscope.pixel_size.z);
defaultValue{3} = num2str(par_microscope.RI);
defaultValue{4} = num2str(par_microscope.NA);
defaultValue{5} = num2str(par_microscope.Em);
defaultValue{6} = num2str(par_microscope.Ex);
defaultValue{7} = num2str(par_microscope.type);
defaultValue{8} = num2str(par_microscope.dT);

userValue = inputdlg(prompt,dlgTitle,1,defaultValue);

if( ~ isempty(userValue))
    par_microscope.pixel_size.xy = str2double(userValue{1});
    par_microscope.pixel_size.z  = str2double(userValue{2});   
    par_microscope.RI            = str2double(userValue{3});   
    par_microscope.NA            = str2double(userValue{4});
    par_microscope.Em            = str2double(userValue{5});   
    par_microscope.Ex            = str2double(userValue{6});
    par_microscope.type          = userValue{7}; 
    par_microscope.dT            = str2double(userValue{8});  
end

handles.par_microscope = par_microscope;

%- Update theoretical PSF
handles = update_PSF_theo(hObject, eventdata, handles);
guidata(hObject, handles);


%==== Function to update theoretical PSF
function handles = update_PSF_theo(hObject, eventdata, handles)

%- Calculate theoretical PSF and show it 
[PSF_theo.xy_nm, PSF_theo.z_nm] = sigma_PSF_BoZhang_v1(handles.par_microscope);
PSF_theo.xy_pix = PSF_theo.xy_nm / handles.par_microscope.pixel_size.xy ;
PSF_theo.z_pix  = PSF_theo.z_nm  / handles.par_microscope.pixel_size.z ;

set(handles.text_psf_theo_xy,'String',num2str(round(PSF_theo.xy_nm)));
set(handles.text_psf_theo_z, 'String',num2str(round(PSF_theo.z_nm)));

%- Update handles structure
handles.PSF_theo       = PSF_theo;
guidata(hObject, handles);


%==========================================================================
% Add files
%==========================================================================

%=== Add files
function button_file_add_Callback(hObject, eventdata, handles)

current_dir = pwd;

%- Get file name
[file_name,path_name] = uigetfile({'*.tif';'*.stk'},'Select file with outline definition or image files','MultiSelect', 'off');

if ~iscell(file_name)
    dum =file_name; 
    file_name = {dum};
end

if file_name{1} ~= 0 
    
    str_list_old = get(handles.listbox_files,'String');
    
    if isempty(str_list_old)
        str_list_new = file_name;
    else
        str_list_new = [str_list_old;file_name];
    end
    
    %- Sometimes there are problems with the list-box value
    if isempty(get(handles.listbox_files,'Value'))
        set(handles.listbox_files,'Value',1);
    end
    
    set(handles.listbox_files,'String',str_list_new);
    
    
    %- Save values
    N_files = size(str_list_new,1);
    handles.file_list{N_files}.name        = file_name{1};
    handles.file_list{N_files}.path        = path_name;
    handles.file_list{N_files}.dT          = handles.par_microscope.dT; 
    handles.file_list{N_files}.flag_dT_sep = 0;  
     
    %- Save results
    guidata(hObject, handles); 

    %- Display item
    listbox_files_Callback(hObject, eventdata, handles)
end

%- Go back to original image
cd(current_dir);


%=== Add folders
function button_add_folders_Callback(hObject, eventdata, handles)


folder_name = uipickfiles('REFilter','^')';

if ~iscell(folder_name)
    dum=folder_name;
    folder_name={dum};
end
    

%- Continue if folders are specified
if folder_name{1} ~= 0
    
    for i_f = 1:length(folder_name)
        
        dir_loop = folder_name{i_f};

        dir_struct       = dir(dir_loop);
        [sorted_names]   = sortrows({dir_struct.name}');
        cell_ind_dir_tif = strfind(sorted_names,'.tif');
        ind_dir_tif      = find(~cellfun(@isempty, cell_ind_dir_tif));

        %- No tif file in folder
        if isempty(ind_dir_tif)
            disp(' ')
            disp('NOT .tif file in folder')
            disp(dir_loop)
            continue
        end
        
        %- No tif file in folder
        if length(ind_dir_tif) > 1
            disp(' ')
            disp('Found multiple .tif files in folder - ONLY one is allowed')
            disp(dir_loop)
            continue
        end
        
        %- Get file-name
        file_name = sorted_names{ind_dir_tif};
        disp(' ')
        disp('Image found')
        disp(file_name)
              
        
        %- Update list with file names        
        str_list_old = get(handles.listbox_files,'String');

        if isempty(str_list_old)
            str_list_new = file_name;
        else
            str_list_new = [str_list_old;file_name];
        end

        %- Sometimes there are problems with the list-box value
        if isempty(get(handles.listbox_files,'Value'))
            set(handles.listbox_files,'Value',1);
        end

        set(handles.listbox_files,'String',str_list_new);
        
        %- Save values
        N_files = size(str_list_new,1);
        handles.file_list{N_files}.name        = file_name;
        handles.file_list{N_files}.path        = dir_loop;
        handles.file_list{N_files}.dT          = handles.par_microscope.dT; 
        handles.file_list{N_files}.flag_dT_sep = 0;   
    end

%- Save results
guidata(hObject, handles); 

%- Display item
listbox_files_Callback(hObject, eventdata, handles)

end


%=== Delete selected file
function button_file_delete_Callback(hObject, eventdata, handles)

str_list = get(handles.listbox_files,'String');

if not(isempty(str_list))

    %- Ask user to confirm choice
    choice = questdlg('Do you really want to remove this file?', 'MS2-QUANT', 'Yes','No','No');

    if strcmp(choice,'Yes')

        %- Extract index of highlighted cell
        ind_sel  = get(handles.listbox_files,'Value');

        %- Delete highlighted cell
        str_list(ind_sel) = [];
        set(handles.listbox_files,'String',str_list)
        handles.file_list(ind_sel) = [];
        
        %- Save results
        guidata(hObject, handles);    
        
        %- Update status
        set(handles.listbox_files,'Value',1)
        listbox_files_Callback(hObject, eventdata, handles)
     
%        controls_enable(hObject, eventdata, handles)        
    end
end


%== Delete all files
function button_file_delete_all_Callback(hObject, eventdata, handles)
%- Ask user to confirm choice
choice = questdlg('Do you really want to remove all files?', 'MS2-QUANT', 'Yes','No','No');

if strcmp(choice,'Yes')
    
    set(handles.listbox_files,'String',{})
    set(handles.listbox_files,'Value',1)
    handles.file_list = {};
    
    %- Save results
    guidata(hObject, handles);
end


%=== Display currently selected file
function listbox_files_Callback(hObject, eventdata, handles)

ind_file = get(handles.listbox_files,'Value');

if not(isempty(ind_file)) && not(isempty(handles.file_list))
    path_name     = handles.file_list{ind_file}.path;
    dT_file       = handles.file_list{ind_file}.dT; 
    flag_dT_sep   = handles.file_list{ind_file}.flag_dT_sep; 

    set(handles.text_path_image,'String', path_name);
    set(handles.text_dT_list,'String', num2str(dT_file));
    set(handles.checkbox_dT_sep,'Value', flag_dT_sep);

    %- Enable text box
    if flag_dT_sep
       set(handles.text_dT_list,'Enable', 'on');
    else
       set(handles.text_dT_list,'Enable', 'off');
    end
end


%==========================================================================
% Specify different dTs for different images
%==========================================================================

%=== Display currently selected file
function checkbox_dT_sep_Callback(hObject, eventdata, handles)

status_enable = get(handles.checkbox_dT_sep,'Value');

ind_file = get(handles.listbox_files,'Value');
handles.file_list{ind_file}.flag_dT_sep = status_enable;

guidata(hObject, handles);
listbox_files_Callback(hObject, eventdata, handles)


%=== Display currently selected file
function text_dT_list_Callback(hObject, eventdata, handles)

ind_file    = get(handles.listbox_files,'Value');
flag_dT_sep = handles.file_list{ind_file}.flag_dT_sep; 

if flag_dT_sep
    handles.file_list{ind_file}.dT = str2double(get(handles.text_dT_list,'String'));
    guidata(hObject, handles); 
end


%==========================================================================
% MISC functions
%==========================================================================

%= Process files
function button_process_Callback(hObject, eventdata, handles)

set(handles.h_MQ_batch,'Pointer','watch');

par_microscope_def = handles.par_microscope;

%- Get files
file_list = handles.file_list;
N_files = length(file_list);

%- What kind of image data will be read in?
sel_val = get(handles.popup_type_image,'Value');
sel_str = get(handles.popup_type_image,'String');    
img_type = sel_str{sel_val};

%- Number of z-slices
N_Z = str2double(get(handles.text_N_z_slices,'String'));

if isnan(N_Z)
    warndlg('Number of z-slices have to be defined.','MS2-QUANT')
    return
end
    
%- BGD value
bgd_value = str2double(get(handles.text_bdg,'String'));
handles.BGD_img = bgd_value;

if isnan(bgd_value)
    warndlg('Background value has to be defined.','MS2-QUANT')
    return
end
    
%- Get factor for filtering
kernel_bgd_xy = str2double(get(handles.text_kernel_factor_bgd_xy,'String'));
kernel_bgd_z  = str2double(get(handles.text_kernel_factor_bgd_z,'String'));

kernel_SNR_xy = str2double(get(handles.text_kernel_factor_filter_xy,'String'));
kernel_SNR_z  = str2double(get(handles.text_kernel_factor_filter_z,'String'));

filter.pad    = 3*kernel_bgd_xy;

%- Type of file for region definition
file_reg_str = get(handles.popup_type_region_file,'String');
tile_reg_ind = get(handles.popup_type_region_file,'Value');
file_reg_sel = file_reg_str{tile_reg_ind};


%- Save parameters for file-loading
image_specs.type = img_type;
image_specs.N_Z  = N_Z;

status_update(hObject, eventdata, handles,{' '; '### Processing files: see command window for a detailed progress report'})

disp(' ')
disp('=== MS2-QUANT: batch processing')   

%- Loop over files
for i_file = 1: N_files
           
    status_update(hObject, eventdata, handles,{['- File ', num2str(i_file) , ' of ', num2str(N_files)]}); 
    disp(' ')
    disp(['+++ File ', num2str(i_file) , ' of ', num2str(N_files), ' +++']) 
    
    
    %== Reset some parameters
    handles.flag_BGD = 0;
    handles.flag_OPB = 0;
    par_microscope   = par_microscope_def;
    
    handles.reg_prop = [];    
    clear global MQ_img_stack
    clear global MQ_img_MIP
    global MQ_img_stack MQ_img_MIP
    
    clear image_struct
    clear img_MIP_xy

    %=== [1] Extract parameters for file  
    file_name = file_list{i_file}.name;
    file_path = file_list{i_file}.path;
    flag_dT_sep = file_list{i_file}.flag_dT_sep;
    
    if flag_dT_sep
        dT = file_list{i_file}.dT;
        par_microscope.dT = dT;
    end
        
    %=== [2] Load image
    disp(['- Loading image: ', file_name])
    disp(['  Folder:  ', file_path])
    
    folder_MQ = fullfile(file_path,'MQ_results');
    if ~exist(folder_MQ); mkdir(folder_MQ); end;
    
    
    %- Load image
    image_specs.name = file_name;
    image_specs.path = file_path;
    
    [handles, status_image, MQ_img_stack, img_MIP_xy] = MQ_load_images_v4(handles,image_specs);

    %- If image is loaded
    if status_image
                   
        %- Assign various MIP's
        MQ_img_MIP.raw          = img_MIP_xy; 
        MQ_img_MIP.filt         = img_MIP_xy; 
        MQ_img_MIP.raw_bgd_sub  = img_MIP_xy; 
        MQ_img_MIP.opb_bgd_sub  = img_MIP_xy; 
        
        %- Analyze image   
        handles = MQ_GUI_analyze_image(handles);
    
        %=== Load ImageJ or MQ regions and convert to outlines; save outline
        
        switch  file_reg_sel
            
            case 'ImageJ regions'
        
                disp('- Generate region file from ImageJ')

                %- Check if file is present
                if ~exist(fullfile(file_path,'RoiSet.zip'))
                    errordlg('No file with FIJI regions found. will exit.',mfilename)
                    disp(fullfile(folder_MQ,'RoiSet.zip'))
                    continue
                end
                
                par_convert.par_microscope = par_microscope;
                par_convert.version        = 'v1';
                par_convert.suffix         = '__regions.txt';

                reg_prop = MQ_convert_ImageJ_ROI_v3(file_path,file_name,[],par_convert);
                
            case 'MQ regions'

                disp('--- Generate region file from MS2-quant')
                                
                %- Find all files containing _regions in their name
                dir_struct_reg     = dir(file_path);
                [sorted_names_reg] = sortrows({dir_struct_reg.name}');

                cell_ind_reg_file = strfind(sorted_names_reg,'_regions.txt');
                ind_reg_file = find(~cellfun(@isempty, cell_ind_reg_file));

                %- Check that there is only one file
                if isempty(ind_reg_file)
                    errordlg('No MQ regions file found. will exit.',mfilename)
                    disp(folder_MQ)
                    continue
                elseif length(ind_reg_file) > 1;
                    errordlg('More than one region files found. will exit.',mfilename)
                    disp(folder_MQ)
                    continue
                end
                    
                %- Open region file               
                [reg_prop] = MQ_load_region_v3(fullfile(folder_MQ,sorted_names_reg{ind_reg_file}));         
      
            otherwise
                disp('No allowed selection for region definition')
                flag_continue = 0;
        end
                
      %- Loop over region to make sure labels are correct
        for i_reg = 1:length(reg_prop)

            %- Rename regions based on the already assigned label
            reg_name = reg_prop(i_reg).label;

            switch reg_name
                case {'T','TxSite'} 
                   reg_prop(i_reg).label = 'TxSite';

                case {'C','CTRL'}  
                    reg_prop(i_reg).label = 'CTRL';

                case {'N','Nucleus'}  
                    reg_prop(i_reg).label = 'NUCLEUS';

                case {'B','BGD'}              
                    reg_prop(i_reg).label = 'BGD';  
            end  

            %- Flag for OPB = nucleus
            if reg_prop(i_reg).flag_OPB                      
                reg_prop(i_reg).label = 'NUCLEUS';
            end

            %- Flag for BGD = background
            if reg_prop(i_reg).flag_BGD                      
                reg_prop(i_reg).label = 'BGD';
            end
        end
            
        handles.reg_prop = reg_prop;
  
                        
        %== Analyze region properties (generating of binary masks)
        fprintf('\n>>> Analyze regions');
        handles = MQ_GUI_analyze_reg_prop(handles);

        %== Photobleaching Correction
        fprintf('\n>>> Photobleaching correction ');
        handles = MQ_GUI_OPB_quantify(handles);
            
        name_save = fullfile(folder_MQ,'Plots_OPB_correction');
        save2pdf(name_save,gcf,300); close(gcf)
        
        
        %== FILTERING
        fprintf('\n>>> Filtering ');
        handles =  MQ_GUI_filter(handles);  
        
        %== Saving results
        fprintf('\n>>> Saving file .... ');
        file_name      = ['_MQ_batch_preprocessing_', datestr(date,'yymmdd'), '.mat'];
        file_name_full = fullfile(folder_MQ,file_name);  

        folder_sub_save = 'MQ_results';
        handles.folder_sub_results = folder_sub_save;

        MQ_batch_save_handles_v4(file_name_full,handles);

        fprintf('\n++++ Finished with pre-processing of file \n');

        clear MQ_img_MIP MQ_img_stack
    end
end

clear global MQ_img_stack
clear global MQ_img_MIP


%==========================================================================
% Analyze image and regions
%==========================================================================
% 
% %== Function to analyze detected regions
% function handles = analyze_reg_prop(hObject, eventdata, handles)
% 
% reg_prop = handles.reg_prop;
% 
% %- Populate pop-up menu with labels of cells
% N_reg = size(reg_prop,2);
% 
% if N_reg > 0
% 
%     %- Call pop-up function to show results and bring values into GUI
%     for ind_reg = 1:N_reg
%         
%         %== Check if one region is defined as background
%         flag_OPB = reg_prop(ind_reg).flag_OPB;
%         if flag_OPB
%             handles.flag_OPB = ind_reg;
%         end
%         
%         %== Check if one region is defined as background
%         flag_BGD = reg_prop(ind_reg).flag_BGD;
%         if flag_BGD
%             handles.flag_BGD = ind_reg;
%         end
%                         
%         %=== Name of region
%         str_menu{ind_reg,1} = reg_prop(ind_reg).label;
%         
%         %== Get information about size of region
%         dim.x_pix = max(reg_prop(ind_reg).x) - min(reg_prop(ind_reg).x);
%         dim.y_pix = max(reg_prop(ind_reg).y) - min(reg_prop(ind_reg).y);
%         
%         dim.x_nm = dim.x_pix * handles.par_microscope.pixel_size.xy;
%         dim.y_nm = dim.x_pix * handles.par_microscope.pixel_size.xy;
%         
%         dim.x_min = min(reg_prop(ind_reg).x);
%         dim.x_max = max(reg_prop(ind_reg).x);
%         
%         dim.x_min_nm = dim.x_min * handles.par_microscope.pixel_size.xy;
%         dim.x_max_nm = dim.x_max * handles.par_microscope.pixel_size.xy;
%         
%         dim.y_min = min(reg_prop(ind_reg).y);
%         dim.y_max = max(reg_prop(ind_reg).y);
%         
%         dim.y_min_nm = dim.y_min * handles.par_microscope.pixel_size.xy;
%         dim.y_max_nm = dim.y_max * handles.par_microscope.pixel_size.xy;
%          
%         %- Save information
%         reg_prop(ind_reg).dim = dim;
%         
%         %== Get binary mask
%   
%         x = handles.reg_prop(ind_reg).x;
%         y = handles.reg_prop(ind_reg).y;
%         
%         m = handles.dim.Y; 
%         n = handles.dim.X;         
%         
%         BW_2D = poly2mask(x, y, m, n);
%         BW_3D = repmat(BW_2D,[1,1,handles.dim.Z]);
%         
%         %- Assing parameters
%         reg_prop(ind_reg).BW_2D = BW_2D;
%         reg_prop(ind_reg).BW_3D = BW_3D;
%         
%     end  
% else
%     str_menu = {' '};
% end
% 
% handles.reg_prop = reg_prop;
% 
% 
% % === Function to analyze image
% function handles = analyze_image(hObject, eventdata, handles)
% global  MQ_img_stack MQ_img_MIP
% 
% switch handles.img_type 
%     
%     case {'3D','3d'}
% 
%         %- Dimensions
%         handles.N_slice = length(MQ_img_stack);
%         [handles.img_dim.Y handles.img_dim.X handles.img_dim.Z] = size(MQ_img_stack(1).data);
% 
%         %- Analyze image
%         handles.img_PLOT  =  max(MQ_img_MIP.img_MIP,[],3); 
%         handles.img_min   =  min(MQ_img_MIP.img_MIP(:)); 
%         handles.img_max   =  max(MQ_img_MIP.img_MIP(:)); 
%         handles.img_diff  =  handles.img_max-handles.img_min; 
% 
%         %- Analyze filtered image
%         handles.img_filt_PLOT   =  max(MQ_img_MIP.img_filt_MIP,[],3); 
%         handles.img_filt_min    =  min(MQ_img_MIP.img_filt_MIP(:)); 
%         handles.img_filt_max    =  max(MQ_img_MIP.img_filt_MIP(:)); 
%         handles.img_filt_diff   =  handles.img_filt_max-handles.img_filt_min; 
% 
%         %- Analyze background subtracted image
%         handles.img_M_bgd_PLOT   =  max(MQ_img_MIP.img_M_bgd_MIP,[],3); 
%         handles.img_M_bgd_min    =  min(MQ_img_MIP.img_M_bgd_MIP(:)); 
%         handles.img_M_bgd_max    =  max(MQ_img_MIP.img_M_bgd_MIP(:)); 
%         handles.img_M_bgd_diff   =  handles.img_M_bgd_max-handles.img_M_bgd_min; 
% 
%         %- Analyze background image
%         handles.bgd_PLOT      =  max(MQ_img_MIP.bgd_MIP,[],3); 
%         handles.bgd_min       =  min(MQ_img_MIP.bgd_MIP(:)); 
%         handles.bgd_max       =  max(MQ_img_MIP.bgd_MIP(:)); 
%         handles.bgd_diff      =  handles.bgd_max-handles.bgd_min; 
% 
%         %- Get first image in stack
%         img_stack_first     = MQ_img_stack(1).data;  % Necessary to define binary mask for OPB measurement 
%         [dim.Y dim.X dim.Z] = size(img_stack_first);
%         handles.dim         = dim;
%         
%         
% 	case {'2D','2d'}
% 
%         %- Dimensions
%         handles.N_slice = size(MQ_img_stack.data,3);
%         [handles.img_dim.Y handles.img_dim.X handles.img_dim.Z] = size(MQ_img_stack.data);
% 
%         %- Analyze image
%         handles.img_PLOT  =  max(MQ_img_MIP.img_MIP,[],3); 
%         handles.img_min   =  min(MQ_img_MIP.img_MIP(:)); 
%         handles.img_max   =  max(MQ_img_MIP.img_MIP(:)); 
%         handles.img_diff  =  handles.img_max-handles.img_min; 
% 
%         %- Analyze filtered image
%         handles.img_filt_PLOT   =  max(MQ_img_MIP.img_filt_MIP,[],3); 
%         handles.img_filt_min    =  min(MQ_img_MIP.img_filt_MIP(:)); 
%         handles.img_filt_max    =  max(MQ_img_MIP.img_filt_MIP(:)); 
%         handles.img_filt_diff   =  handles.img_filt_max-handles.img_filt_min; 
% 
%         %- Analyze background subtracted image
%         handles.img_M_bgd_PLOT   =  max(MQ_img_MIP.img_M_bgd_MIP,[],3); 
%         handles.img_M_bgd_min    =  min(MQ_img_MIP.img_M_bgd_MIP(:)); 
%         handles.img_M_bgd_max    =  max(MQ_img_MIP.img_M_bgd_MIP(:)); 
%         handles.img_M_bgd_diff   =  handles.img_M_bgd_max-handles.img_M_bgd_min; 
% 
%         %- Analyze background image
%         handles.bgd_PLOT      =  max(MQ_img_MIP.bgd_MIP,[],3); 
%         handles.bgd_min       =  min(MQ_img_MIP.bgd_MIP(:)); 
%         handles.bgd_max       =  max(MQ_img_MIP.bgd_MIP(:)); 
%         handles.bgd_diff      =  handles.bgd_max-handles.bgd_min; 
% 
%         %- Get first image in stack
%         img_stack_first     = MQ_img_stack(1).data;  % Necessary to define binary mask for OPB measurement 
%         [dim.Y dim.X dim.Z] = size(img_stack_first);
%         handles.dim         = dim;
% end
% 
% %- Save everything
% guidata(hObject, handles); 
% set(handles.h_MQ_batch,'Pointer','arrow');


%==========================================================================
% MISC functions
%==========================================================================

%== Change fitting mode
function pushbutton_change_OPB_fit_Callback(hObject, eventdata, handles)

%- User-dialog
dlgTitle = 'Options for OPB correction quantification';
prompt_avg(1) = {'Fit mode: lin (linear), exp (3 exponentials)'};
defaultValue_avg{1} = num2str(handles.opb_fit_mode);

options.Resize='on';
userValue = inputdlg(prompt_avg,dlgTitle,1,defaultValue_avg,options);

%- Return results if specified
if( ~ isempty(userValue))
    handles.opb_fit_mode     = userValue{1};
    guidata(hObject, handles); 
end



%== Update status
% status_update(hObject, eventdata, handles,{'  ';'## Settings loaded'});     
function status_update(hObject, eventdata, handles,status_text)
status_old = get(handles.listbox_status,'String');
status_new = [status_old;status_text];
set(handles.listbox_status,'String',status_new)
set(handles.listbox_status,'ListboxTop',round(size(status_new,1)))
drawnow
guidata(hObject, handles); 


%==========================================================================
% UNUSED FUNCTIONS
%==========================================================================

function listbox_files_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_kernel_factor_bgd_Callback(hObject, eventdata, handles)

function text_kernel_factor_bgd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_kernel_factor_filter_Callback(hObject, eventdata, handles)

function text_kernel_factor_filter_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function listbox_status_Callback(hObject, eventdata, handles)

function listbox_status_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popupmenu2_Callback(hObject, eventdata, handles)

function popupmenu2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_N_z_slices_Callback(hObject, eventdata, handles)

function text_N_z_slices_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_bdg_Callback(hObject, eventdata, handles)

function text_bdg_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function checkbox_opb_apply_Callback(hObject, eventdata, handles)

function text_kernel_factor_bgd_xy_Callback(hObject, eventdata, handles)

function text_kernel_factor_bgd_xy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_kernel_factor_filter_xy_Callback(hObject, eventdata, handles)

function text_kernel_factor_filter_xy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_kernel_factor_bgd_z_Callback(hObject, eventdata, handles)

function text_kernel_factor_bgd_z_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_kernel_factor_filter_z_Callback(hObject, eventdata, handles)

function text_kernel_factor_filter_z_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popup_type_image_Callback(hObject, eventdata, handles)

function popup_type_image_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit9_Callback(hObject, eventdata, handles)

function edit9_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit10_Callback(hObject, eventdata, handles)


if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit11_Callback(hObject, eventdata, handles)

function edit11_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit12_Callback(hObject, eventdata, handles)

function edit12_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popup_type_region_file_Callback(hObject, eventdata, handles)

function popup_type_region_file_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
